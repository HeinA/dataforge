alter table Dataforge.[LookupType]
add [Checksum] as checksum([Text])

alter table Dataforge.[Lookup]
add [Checksum] as checksum(LookupTypeId, [Text], SystemId, StringReference, IntegerReference)

drop index idx_Lookup_IntegerReference on [Dataforge].[Lookup]
drop index idx_Lookup_StringReference on [Dataforge].[Lookup]
drop index idx_Lookup_Text on [Dataforge].[Lookup]
drop index idx_Lookup_SystemId on [Dataforge].[Lookup]

CREATE UNIQUE NONCLUSTERED INDEX idx_Lookup_IntegerReference
ON [Dataforge].[Lookup](LookupTypeId, IntegerReference)
WHERE IntegerReference IS NOT NULL;

CREATE UNIQUE NONCLUSTERED INDEX idx_Lookup_StringReference
ON [Dataforge].[Lookup](LookupTypeId, StringReference)
WHERE StringReference IS NOT NULL;

CREATE UNIQUE NONCLUSTERED INDEX idx_Lookup_SystemId
ON [Dataforge].[Lookup](SystemId)
WHERE SystemId IS NOT NULL;

CREATE UNIQUE NONCLUSTERED INDEX idx_Lookup_Text
ON [Dataforge].[Lookup](LookupTypeId, [Text])
