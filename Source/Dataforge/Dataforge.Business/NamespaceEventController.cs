﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business
{
  public abstract class NamespaceEventController
  {
    public abstract void AppendKnownNamespaces(Collection<string> namespaces);
  }
}
