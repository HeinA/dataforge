﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business
{
  [DataContract(IsReference = true, Namespace = Namespaces.Dataforge)]
  public abstract class BusinessObject : INotifyPropertyChanged, IDataErrorInfo
  {
    #region Constructors

    protected BusinessObject()
    {
      CatalogIsOwned();
    }

    #endregion

    #region Public Properties

    #region DataMember int Id

    static int _currentId = 0;
    public const string PN_ID = "Id";
    [DataMember(Name = PN_ID, EmitDefaultValue = false)]
    int _id = --_currentId;
    public int Id
    {
      get { return _id; }
      set
      {
        if (SetValue<int>(ref _id, value, PN_ID))
        {
          foreach (var child in Children)
          {
            child.SetReferenceValue(value, PN_OWNER);
          }
          foreach (var child in ExtensionObjectCollection)
          {
            child.SetReferenceValue(value, PN_OWNER);
          }
          foreach (var child in AssociativeObjectCollection)
          {
            BusinessObject abo = child as BusinessObject;
            abo.SetReferenceValue(value, PN_OWNER);
          }
        }
      }
    }

    #endregion

    #region DataMember bool IsNew

    const string PN_ISNEW = "IsNew";
    [DataMember(Name = PN_ISNEW, EmitDefaultValue = false)]
    bool _isNew = true;
    public bool IsNew
    {
      get { return _isNew; }
      set { _isNew = value; }
    }

    #endregion

    #region DataMember bool IsDirty

    const string PN_ISDIRTY = "IsDirty";
    [DataMember(Name = PN_ISDIRTY, EmitDefaultValue = false)]
    bool _isDirty = false;
    public bool IsDirty
    {
      get { return _isDirty; }
      set { _isDirty = value; }
    }

    #endregion

    #region DataMember bool IsDeleted

    public const string PN_ISDELETED = "IsDeleted";
    [DataMember(Name = PN_ISDELETED, EmitDefaultValue = false)]
    bool _isDeleted = false;
    public bool IsDeleted
    {
      get { return _isDeleted; }
      set
      {
        if (SetValue<bool>(ref _isDeleted, value, PN_ISDELETED))
        {
          OnDeleteStateChanged();
          foreach (var child in Children)
          {
            child.IsDeleted = value;
          }
          foreach (var child in ExtensionObjectCollection)
          {
            child.IsDeleted = value;
          }
          foreach (var child in AssociativeObjectCollection)
          {
            BusinessObject abo = child as BusinessObject;
            abo.IsDeleted = value;
          }
        }
      }
    }

    protected virtual void OnDeleteStateChanged()
    {
    }

    #endregion

    #endregion

    #region Protected Properties

    static object _ownedLock;
    static object OwnedLock
    {
      get { return _ownedLock ?? (_ownedLock = new object()); }
    }

    static Dictionary<Type, Type> _isOwnedDictionary;
    static Dictionary<Type, Type> IsOwnedDictionary
    {
      get { return _isOwnedDictionary ?? (_isOwnedDictionary = new Dictionary<Type, Type>()); }
    }

    private void CatalogIsOwned()
    {
      lock (OwnedLock)
      {
        Type t = this.GetType();
        if (!IsOwnedDictionary.ContainsKey(t))
        {
          var ta = (TableAttribute)t.GetCustomAttribute(typeof(TableAttribute), true);
          if (!string.IsNullOrWhiteSpace(ta.OwnerColumn))
          {
            if (ta.OwnerType == null) throw new InvalidOperationException("OwnerType not set");
            IsOwnedDictionary.Add(t, ta.OwnerType);
          }
        }
      }
    }

    protected internal bool IsOwnedBy(Type ownerType)
    {
      lock (OwnedLock)
      {
        if (!IsOwnedDictionary.ContainsKey(this.GetType())) return false;
        return IsOwnedDictionary[this.GetType()] == ownerType;
      }
    }

    protected internal bool IsOwned
    {
      get
      {
        lock (OwnedLock)
        {
          return IsOwnedDictionary.ContainsKey(this.GetType());
        }
      }
    }

    #endregion

    #region References

    [DataMember(Name = "References", EmitDefaultValue = false)]
    BasicCollection<PropertyReference> _references;
    BasicCollection<PropertyReference> References
    {
      get
      {
        if (_references == null) _references = new BasicCollection<PropertyReference>();
        return _references;
      }
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "0#")]
    public bool SetReferenceValue(int value, string propertyName, params string[] additionalProperyNotifications)
    {
      if (string.IsNullOrWhiteSpace(propertyName)) throw new ArgumentException(Dataforge.Business.Properties.Resources.InvalidParameter, "propertiesChanged");

      PropertyReference pv = References.FirstOrDefault(p => p.PropertyName == propertyName);
      int oldValue = 0;
      if (pv != null)
      {
        oldValue = (int)pv.Reference;
      }

      if (oldValue != value)
      {
        if (!SuppressEventState) SetOriginalValue(propertyName, oldValue);
        if (pv != null)
        {
          pv.Reference = value;
        }
        else
        {
          References.Add(new PropertyReference(propertyName, value));
        }
        if (!SuppressEventState)
        {
          IsDirty = true;
          OnPropertyChanged(propertyName);
          foreach (string pn in additionalProperyNotifications)
          {
            OnPropertyChanged(pn);
          }
        }
        OnCompositionChanged(this, CompositionChangedType.PropertyChanged, this);
        return true;
      }

      return false;
    }

    public int GetReferenceValue(string propertyName)
    {
      PropertyReference pv = References.FirstOrDefault(p => p.PropertyName == propertyName);
      if (pv != null) return (int)pv.Reference;
      return 0;
    }

    protected T GetCachedObject<T>(string propertyName)
      where T : BusinessObject
    {
      int reference = GetReferenceValue(propertyName);
      if (reference == 0)
      {
        ConstructorInfo ci = typeof(T).GetConstructor(new Type[] { typeof(bool) });
        if (ci == null) throw new InvalidOperationException("BusinessObject does not implement the nullabe constructor.");
        return (T)ci.Invoke(new object[] { true });
      }
      return Cache.Instance.GetObject<T>(reference);
    }

    protected void SetCachedObject<T>(T value, string propertyName)
      where T : BusinessObject
    {
      if (value == null) SetReferenceValue(0, propertyName);
      else SetReferenceValue(value.Id, propertyName);
    }

    #endregion

    #region Associative Objects

    BasicCollection<IAssociativeObject> _associativeObjectCollection;
    [DataMember(Name = "AssociativeObjects", EmitDefaultValue = false)]
    internal BasicCollection<IAssociativeObject> AssociativeObjectCollection
    {
      get
      {
        if (_associativeObjectCollection == null && !IsSerializing) _associativeObjectCollection = new BasicCollection<IAssociativeObject>();
        if (_associativeObjectCollection != null && IsSerializing && _associativeObjectCollection.Count == 0) return null;
        return _associativeObjectCollection;
      }
      set { _associativeObjectCollection = value; }
    }

    public T GetAssociativeObject<T>(int id)
      where T : IAssociativeObject
    {
      return (T)AssociativeObjectCollection.FirstOrDefault(o => o.GetType() == typeof(T) && o.ReferenceId == id);
    }

    public T GetAssociativeObject<T>(BusinessObject obj)
      where T : IAssociativeObject
    {
      return (T)AssociativeObjectCollection.FirstOrDefault(o => o.GetType() == typeof(T) && o.ReferenceId == obj.Id);
    }

    internal IAssociativeObject GetAssociativeObject(IAssociativeObject obj)
    {
      return AssociativeObjectCollection.FirstOrDefault(o => o.GetType() == obj.GetType() && o.ReferenceId == obj.ReferenceId);
    }

    internal IAssociativeObject GetAssociativeObject(Type type, int id)
    {
      return AssociativeObjectCollection.FirstOrDefault(o => o.GetType() == type && o.ReferenceId == id);
    }

    internal IEnumerable<IAssociativeObject> GetAssociativeObjects(Type type)
    {
      return AssociativeObjectCollection.Where(o => o.GetType() == type).ToList<IAssociativeObject>();
    }

    public BasicCollection<T> GetAssociativeObjects<T>()
    {
      return new BasicCollection<T>(GetAssociativeObjects(typeof(T)).Cast<T>().ToList());
    }

    internal BusinessObject SetAssociativeObject<T>(T obj)
      where T : IAssociativeObject
    {
      T ass = GetAssociativeObject<T>(obj.ReferenceId);
      BusinessObject bo = obj as BusinessObject;
      if (ass != null)
      {
        bo.Owner = this;
        if (ass.BusinessObjectReference == null)
        {
          using (new EventStateSuppressor(bo))
          {
            ass.BusinessObjectReference = obj.BusinessObjectReference;
          }
        }
        return ass as BusinessObject;
      }
      AssociativeObjectCollection.Add(obj);
      bo.SuppressEventState = SuppressEventState;
      OnCompositionChanged(this, CompositionChangedType.ChildAdded, bo);
      return obj as BusinessObject;
    }

    internal BusinessObject SetAssociativeObject(Type type, IAssociativeObject obj)
    {
      IAssociativeObject ass = GetAssociativeObject(obj);
      BusinessObject bo = obj as BusinessObject;
      if (ass != null)
      {
        if (ass.BusinessObjectReference == null)
          ass.BusinessObjectReference = obj.BusinessObjectReference;
        return ass as BusinessObject;
      }
      AssociativeObjectCollection.Add(obj);
      bo.SuppressEventState = SuppressEventState;
      bo.Owner = this;
      OnCompositionChanged(this, CompositionChangedType.ChildAdded, bo);
      return obj as BusinessObject;
    }

    #endregion

    #region Extension Objects

    BasicCollection<ExtensionObject> _extensionObjectCollection;
    [DataMember(Name = "ExtensionObjects", EmitDefaultValue = false)]
    BasicCollection<ExtensionObject> ExtensionObjectCollection
    {
      get
      {
        if (_extensionObjectCollection == null && !IsSerializing) _extensionObjectCollection = new BasicCollection<ExtensionObject>();
        if (_extensionObjectCollection != null && IsSerializing && _extensionObjectCollection.Count == 0) return null;
        return _extensionObjectCollection;
      }
      set { _extensionObjectCollection = value; }
    }

    public void AddExtensionObject<T>(T obj)
      where T : ExtensionObject
    {
      if (obj == null) throw new ArgumentNullException("obj");
      if (ExtensionObjectCollection.FirstOrDefault(o => o.GetType() == typeof(T) && o.Id == obj.Id) != null) throw new Exception("Extension object already added.");
      obj.SuppressEventState = SuppressEventState;
      ExtensionObjectCollection.Add(obj);
    }

    public Collection<T> GetExtensionObjects<T>()
      where T : ExtensionObject
    {
      Collection<T> col = new Collection<T>();
      foreach (T obj in ExtensionObjectCollection.Where(o => o.GetType() == typeof(T)))
      {
        col.Add(obj);
      }
      return col;
    }

    internal Collection<ExtensionObject> GetExtensionObjects()
    {
      Collection<ExtensionObject> col = new Collection<ExtensionObject>();
      foreach (ExtensionObject obj in ExtensionObjectCollection)
      {
        col.Add(obj);
      }
      return col;
    }

    public T GetExtensionObject<T>(int id)
      where T : ExtensionObject
    {
      return (T)ExtensionObjectCollection.FirstOrDefault(o => o.GetType() == typeof(T) && o.Id == id);
    }

    public T GetExtensionObject<T>()
      where T : ExtensionObject
    {
      return (T)ExtensionObjectCollection.FirstOrDefault(o => o.GetType() == typeof(T));
    }

    #endregion

    #region Utilities

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "0#")]
    protected bool SetValue<T>(ref T field, T value, string propertyName, params string[] additionalProperyNotifications)
    {
      if (string.IsNullOrWhiteSpace(propertyName)) throw new ArgumentException(Dataforge.Business.Properties.Resources.InvalidParameter, "propertiesChanged");

      if ((field == null && value != null) || (field != null && !field.Equals(value)))
      {
        if (!SuppressEventState) SetOriginalValue(propertyName, field);
        field = value;
        UseLastErrorMessage = false;
        CalculateError();
        if (!SuppressEventState)
        {
          IsDirty = true;
          OnPropertyChanged(propertyName);
          foreach (string pn in additionalProperyNotifications)
          {
            OnPropertyChanged(pn);
          }
        }
        OnCompositionChanged(this, CompositionChangedType.PropertyChanged, this);
        return true;
      }

      return false;
    }

    #endregion

    #region Owner

    public const string PN_OWNER = "Owner";
    BusinessObject _owner;
    public BusinessObject Owner
    {
      get { return _owner; }
      private set
      {
        if (!IsOwned) return;
        if (_owner == value) return;

        if (_owner == null && value != null)
        {
          _owner = value;
          SuppressEventState = value.SuppressEventState;
          SetReferenceValue(value.Id, PN_OWNER);
          OnOwnerChanged();
          return;
        }
        if (_owner != null && value == null)
        {
          _owner = null;
          SetReferenceValue(0, PN_OWNER);
          OnOwnerChanged();
          return;
        }
        if (_owner != null && value != null)
        {
          throw new InvalidOperationException("This object is already ownened by another object.  Please remove it from it's current owner first.");
        }
      }
    }

    protected virtual void OnOwnerChanged()
    {
    }

    #endregion

    #region Children

    List<BusinessObject> _children;
    List<BusinessObject> Children
    {
      get
      {
        if (_children == null) _children = new List<BusinessObject>();
        return _children;
      }
    }

    protected internal void AddChild(BusinessObject child)
    {
      if (child == null) throw new ArgumentNullException("child");
      if (!child.IsOwnedBy(this.GetType())) return;

      child.SuppressEventState = SuppressEventState;
      if (child.Owner == null || !child.Owner.Equals(this))
      {
        child.Owner = this;
        Children.Add(child);
        OnCompositionChanged(this, CompositionChangedType.ChildAdded, child);
      }
    }

    protected internal void RemoveChild(BusinessObject child)
    {
      if (child == null) throw new ArgumentNullException("child");

      if ((child.Owner != null) && child.Owner.Equals(this))
      {
        child.Owner = null;
        Children.Remove(child);
        OnCompositionChanged(this, CompositionChangedType.ChildRemoved, child);
      }
    }

    #endregion

    #region CompositionChanged

    public event EventHandler<CompositionChangedEventArgs> CompositionChanged;

    protected virtual void OnCompositionChanged(BusinessObject source, CompositionChangedType compositionChangedType, BusinessObject item)
    {
      UseLastErrorMessage = false;
      if (Owner != null) Owner.OnCompositionChanged(source, compositionChangedType, item);
      if (SuppressEventState || source.SuppressEventState) return;
      CalculateError();
      if (CompositionChanged != null)
      {
        CompositionChanged(source, new CompositionChangedEventArgs(compositionChangedType, item));
      }
    }

    #endregion

    #region Optimistic Concurrency

    [DataMember(Name = "OriginalValues", EmitDefaultValue = false)]
    BasicCollection<PropertyReference> _originalValues;
    internal BasicCollection<PropertyReference> OriginalValues
    {
      get { return _originalValues ?? (_originalValues = new BasicCollection<PropertyReference>()); }
    }

    void SetOriginalValue(string propertyName, object value)
    {
      try
      {
        if (IsNew || SuppressEventState || IsSerializing || propertyName == PN_ISDIRTY || propertyName == PN_ISDELETED) return;
        PropertyInfo pi = this.GetType().GetProperty(propertyName, BindingFlags.Instance | BindingFlags.Public);
        if (pi.PropertyType.IsSubclassOf(typeof(BusinessObject))) return;
        ColumnAttribute ca = pi.GetCustomAttribute<ColumnAttribute>();
        if (ca != null && ca.IgnoreConcurrency) return;
        if (OriginalValues.FirstOrDefault(o => o.PropertyName == propertyName) != null) return;
        OriginalValues.Add(new PropertyReference(propertyName, value));
      }
      catch
      {
        throw;
      }
    }

    public object GetOriginalValue(string propertyName)
    {
      PropertyReference pv = OriginalValues.FirstOrDefault(p => p.PropertyName == propertyName);
      if (pv != null) return pv.Reference;
      PropertyInfo pi = this.GetType().GetProperty(propertyName, BindingFlags.Instance | BindingFlags.Public);
      if (pi == null) return null;
      return pi.GetValue(this);
    }

    public object GetOriginalValue(PropertyInfo pi)
    {
      PropertyReference pv = OriginalValues.FirstOrDefault(p => p.PropertyName == pi.Name);
      if (pv != null) return pv.Reference;
      return pi.GetValue(this);
    }

    #endregion

    #region PropertyChanged

    public event PropertyChangedEventHandler PropertyChanged;

    bool _suppressEventState;
    internal bool SuppressEventState
    {
      get { return _suppressEventState; }
      set
      {
        _suppressEventState = value;
        foreach (var child in Children)
        {
          child.SuppressEventState = value;
        }
        foreach (var child in ExtensionObjectCollection)
        {
          child.SuppressEventState = value;
        }
        foreach (var child in AssociativeObjectCollection)
        {
          BusinessObject abo = child as BusinessObject;
          abo.SuppressEventState = value;
        }
      }
    }

    protected void OnPropertyChanged(string propertyName)
    {
      if (!SuppressEventState && !IsSerializing)
      {
        if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
      }
    }

    #endregion

    #region Error Checking

    bool _useLastErrorMessage;
    bool UseLastErrorMessage
    {
      get { return _useLastErrorMessage; }
      set
      {
        _useLastErrorMessage = value;
        if (Owner != null && !value) Owner.UseLastErrorMessage = value;
      }
    }

    public const string PN_HASERROR = "HasError";
    public bool HasError
    {
      get { return !IsDeleted && !string.IsNullOrWhiteSpace(Error); }
    }

    public const string PN_ERROR = "Error";
    string _error;
    public string Error
    {
      get
      {
        if (IsDeleted) return string.Empty;
        CalculateError();
        return _error;
      }
      private set
      {
        if (_error != value)
        {
          _error = value;
          OnPropertyChanged(PN_ERROR);
          OnPropertyChanged(PN_HASERROR);
        }
      }
    }

    void CalculateError()
    {
      if (UseLastErrorMessage) return;

      Collection<string> errors = new Collection<string>();
      GetErrors(errors, null);

      foreach (var bo in Children)
      {
        if (!string.IsNullOrWhiteSpace(bo.Error))
        {
          errors.Add(Dataforge.Business.Properties.Resources.BusinessObjectChildErrors);
          break;
        }
      }

      string error = string.Join("\n", errors);
      if (string.IsNullOrWhiteSpace(error)) error = null;

      UseLastErrorMessage = true;
      Error = error;
    }

    string IDataErrorInfo.this[string columnName]
    {
      get
      {
        if (IsDeleted) return null;
        Collection<string> errors = new Collection<string>();
        GetErrors(errors, columnName);
        return string.Join("\n", errors);
      }
    }

      protected virtual void GetErrors(Collection<string> errors, string propertyName)
      {
      }

    #endregion

    #region Serialization

    bool _isSerializing;
    protected bool IsSerializing
    {
      get { return _isSerializing; }
      private set { _isSerializing = value; }
    }

    [OnSerializing]
    void Serializing(StreamingContext context)
    {
      IsSerializing = true;
    }

    [OnSerialized]
    void Serialized(StreamingContext context)
    {
      IsSerializing = false;
    }

    [OnDeserialized]
    void OnDeserialized(StreamingContext ctx)
    {
      using (new EventStateSuppressor(this))
      {
        CatalogIsOwned();

        foreach (PropertyInfo pi in this.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance)
          .Where(pi => pi.PropertyType.GetInterfaces().Any(x => x == typeof(IInternalOwnedCollection)) 
            && pi.PropertyType.IsGenericType 
            && pi.PropertyType.GetGenericTypeDefinition() != typeof(AssociativeObjectCollection<,>)))
        {
          IInternalOwnedCollection o = (IInternalOwnedCollection)pi.GetValue(this);
          o.SetOwner(this);
        }
      }
    }

    #endregion

    #region Equality

    public override bool Equals(object obj)
    {
      if (obj != null && obj.GetType() == this.GetType())
      {
        BusinessObject bo = obj as BusinessObject;
        return bo.Id == this.Id;
      }
      return base.Equals(obj);
    }

    public override int GetHashCode()
    {
      return Id.GetHashCode();
    }

    #endregion
  }
}
