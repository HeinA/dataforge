﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business
{
  public interface IIdentifiableObject
  {
    Guid GlobalIdentifier { get; }
    string StringIdentifier { get; set; }
  }
}
