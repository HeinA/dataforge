﻿using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using Dataforge.Business.Data;
using Dataforge.Business.Service;
using Dataforge.Business.Service.Wcf;
using Dataforge.Data;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business
{
  public class Cache
  {
    #region static Cache Instance

    static Cache _instance;
    public static Cache Instance
    {
      get
      {
        if (_instance == null) _instance = new Cache();
        return _instance;
      }
    }

    #endregion

    #region BasicCollection<BusinessObject> LoadCache()

    public static BasicCollection<BusinessObject> LoadCache()
    {
      using (new ConnectionScope())
      {
        BasicCollection<BusinessObject> col = new BasicCollection<BusinessObject>();
        DataView dv = new DataView(DataFilters.CacheLoad);
        foreach (Type mapperType in CacheMappers.MapperTypes)
        {
          IORMapper mapper = CacheMappers.GetMapper(mapperType);
          foreach (BusinessObject bo in mapper.LoadCache(dv))
          {
            col.Add(bo);
          }
        }
        return col;
      }
    }

    #endregion

    #region void LoadCache(BasicCollection<BusinessObject> col)

    public void LoadCache(BasicCollection<BusinessObject> col)
    {
      lock (_lock)
      {
        _objectDictionary = null;
        BasicCollection<BusinessObject> loaded = new BasicCollection<BusinessObject>();
        foreach (BusinessObject bo in col)
        {
          LoadObject(bo, loaded);
        }
      }
      RefreshCacheCollections();
    }

    void LoadObject(BusinessObject bo, BasicCollection<BusinessObject> loaded)
    {
      if (loaded.Contains(bo)) return;
      loaded.Add(bo);

      Type t = bo.GetType();
      while (t != typeof(BusinessObject))
      {
        if (!ObjectDictionary.ContainsKey(t)) ObjectDictionary.Add(t, new CollectionDictionary());
        CollectionDictionary colDict = ObjectDictionary[t];

        if (colDict.Collection.Contains(bo)) colDict.Collection.Remove(bo);
        colDict.Collection.Add(bo);

        if (colDict.Dictionary.ContainsKey(bo.Id)) colDict.Dictionary.Remove(bo.Id);
        colDict.Dictionary.Add(bo.Id, bo);

        t = t.BaseType;
      }

      foreach (PropertyInfo pi in bo.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public))
      {
        if (pi.PropertyType == typeof(BusinessObject) || pi.PropertyType.IsSubclassOf(typeof(BusinessObject)))
        {
          BusinessObject bo1 = pi.GetValue(bo) as BusinessObject;
          if (bo1 != null) LoadObject(bo1, loaded);
        }

        if (typeof(IList).IsAssignableFrom(pi.PropertyType))
        {
          IList list = pi.GetValue(bo) as IList;
          if (list != null)
          {
            foreach (object oo in list)
            {
              BusinessObject bo2 = oo as BusinessObject;
              if (bo2 != null) LoadObject(bo2, loaded);
            }
          }
        }
      }
    }

    #endregion

    #region Clone

    BasicCollection<T> CloneCollection<T>(BasicCollection<T> source)
    {
      using (MemoryStream stream = new MemoryStream())
      {
        DataContractSerializer dcs = new DataContractSerializer(source.GetType(), KnownTypesProvider.KnownTypes);
        dcs.WriteObject(stream, source);
        stream.Position = 0;
        return (BasicCollection<T>)dcs.ReadObject(stream);
      }
    }

    T Clone<T>(T source)
    {
      using (MemoryStream stream = new MemoryStream())
      {
        DataContractSerializer dcs = new DataContractSerializer(source.GetType(), KnownTypesProvider.KnownTypes);
        dcs.WriteObject(stream, source);
        stream.Position = 0;
        return (T)dcs.ReadObject(stream);
      }
    }

    #endregion

    #region T GetObject<T>(int id)

    public T GetObject<T>(int id)
      where T : BusinessObject
    {
      lock (_lock)
      {
        if (!ObjectDictionary.ContainsKey(typeof(T))) return null;
        CollectionDictionary colDict = ObjectDictionary[typeof(T)];
        if (!colDict.Dictionary.ContainsKey(id)) return null;
        return Clone<T>((T)colDict.Dictionary[id]);
      }
    }

    #endregion

    #region BasicCollection<T> GetObjects<T>()

    public BasicCollection<T> GetObjects<T>()
      where T : BusinessObject
    {
      CacheCollection<T> cc = new CacheCollection<T>();
      RegisterCacheCollection(cc);
      ((ICacheCollection)cc).Refresh();
      return cc;
    }

    internal BasicCollection<T> GetObjectsInternal<T>()
      where T : BusinessObject
    {
      lock (_lock)
      {
        if (ObjectDictionary.ContainsKey(typeof(T)))
        {
          return CloneCollection<T>(new BasicCollection<T>(ObjectDictionary[typeof(T)].Collection.Cast<T>().ToList<T>()));
        }
        return new BasicCollection<T>();
      }
    }

    #endregion

    #region BasicCollection<T> GetObjects<T>(Func<T, bool> predicate)

    public BasicCollection<T> GetObjects<T>(Func<T, bool> predicate)
      where T : BusinessObject
    {
      CacheCollection<T> cc = new CacheCollection<T>(predicate);
      RegisterCacheCollection(cc);
      ((ICacheCollection)cc).Refresh();
      return cc;
    }

    internal BasicCollection<T> GetObjectsInternal<T>(Func<T, bool> predicate)
      where T : BusinessObject
    {
      lock (_lock)
      {
        if (ObjectDictionary.ContainsKey(typeof(T)))
        {
          return CloneCollection<T>(new BasicCollection<T>(ObjectDictionary[typeof(T)].Collection.Cast<T>().Where<T>(predicate).ToList<T>()));
        }
        return new BasicCollection<T>();
      }
    }

    #endregion

    #region BasicCollection<T> GetObjects<T>(Func<T, object> sort, bool ascending)

    public BasicCollection<T> GetObjects<T>(Func<T, object> sort, bool ascending)
      where T : BusinessObject
    {
      CacheCollection<T> cc = new CacheCollection<T>(sort, ascending);
      RegisterCacheCollection(cc);
      ((ICacheCollection)cc).Refresh();
      return cc;
    }

    internal BasicCollection<T> GetObjectsInternal<T>(Func<T, object> sort, bool ascending)
      where T : BusinessObject
    {
      lock (_lock)
      {
        if (ObjectDictionary.ContainsKey(typeof(T)))
        {
          if (ascending) return CloneCollection<T>(new BasicCollection<T>(ObjectDictionary[typeof(T)].Collection.Cast<T>().OrderBy(sort).ToList<T>()));
          else return CloneCollection<T>(new BasicCollection<T>(ObjectDictionary[typeof(T)].Collection.Cast<T>().OrderByDescending(sort).ToList<T>()));
        }
        return new BasicCollection<T>();
      }
    }

    #endregion

    #region BasicCollection<T> GetObjects<T>(Func<T, bool> predicate, Func<T, object> sort, bool ascending)

    public BasicCollection<T> GetObjects<T>(Func<T, bool> predicate, Func<T, object> sort, bool ascending)
      where T : BusinessObject
    {
      CacheCollection<T> cc = new CacheCollection<T>(predicate, sort, ascending);
      RegisterCacheCollection(cc);
      ((ICacheCollection)cc).Refresh();
      return cc;
    }

    internal BasicCollection<T> GetObjectsInternal<T>(Func<T, bool> predicate, Func<T, object> sort, bool ascending)
      where T : BusinessObject
    {
      lock (_lock)
      {
        if (ObjectDictionary.ContainsKey(typeof(T)))
        {
          if (ascending) return CloneCollection<T>(new BasicCollection<T>(ObjectDictionary[typeof(T)].Collection.Cast<T>().Where<T>(predicate).OrderBy(sort).ToList<T>()));
          else return CloneCollection<T>(new BasicCollection<T>(ObjectDictionary[typeof(T)].Collection.Cast<T>().Where<T>(predicate).OrderByDescending(sort).ToList<T>()));
        }
        return new BasicCollection<T>();
      }
    }

    #endregion



    #region Private

    static readonly object _lock = new object();

    #region class CollectionDictionary

    class CollectionDictionary
    {
      BasicCollection<BusinessObject> _collection;
      public BasicCollection<BusinessObject> Collection
      {
        get
        {
          if (_collection == null) _collection = new BasicCollection<BusinessObject>();
          return _collection;
        }
      }

      Dictionary<int, BusinessObject> _dictionary;
      public Dictionary<int, BusinessObject> Dictionary
      {
        get
        {
          if (_dictionary == null) _dictionary = new Dictionary<int, BusinessObject>();
          return _dictionary;
        }
      }
    }

    #endregion

    #region Collection<WeakReference> CacheCollections

    Collection<WeakReference> _cacheCollections;
    Collection<WeakReference> CacheCollections
    {
      get { return _cacheCollections ?? (_cacheCollections = new Collection<WeakReference>()); }
    }

    #endregion

    #region RegisterCacheCollection(ICacheCollection cacheCollection)

    void RegisterCacheCollection(ICacheCollection cacheCollection)
    {
      CacheCollections.Add(new WeakReference(cacheCollection));
    }

    #endregion

    #region void RefreshCacheCollections()

    void RefreshCacheCollections()
    {
      Stack<WeakReference> deadCollections = new Stack<WeakReference>();
      foreach (WeakReference wr in CacheCollections)
      {
        if (!wr.IsAlive) deadCollections.Push(wr);
        else
          ((ICacheCollection)wr.Target).Refresh();
      }

      while (deadCollections.Count > 0)
      {
        WeakReference wr = deadCollections.Pop();
        CacheCollections.Remove(wr);
      }
    }

    #endregion

    #region Dictionary<Type, CollectionDictionary> ObjectDictionary

    Dictionary<Type, CollectionDictionary> _objectDictionary;
    Dictionary<Type, CollectionDictionary> ObjectDictionary
    {
      get
      {
        if (_objectDictionary == null) _objectDictionary = new Dictionary<Type, CollectionDictionary>();
        return _objectDictionary;
      }
    }

    #endregion

    #endregion
  }
}
