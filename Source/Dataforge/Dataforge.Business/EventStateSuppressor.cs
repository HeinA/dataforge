﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business
{
  /// <summary>
  /// Will suppress PropertyChanged & CompositionChanged events, aswell as the Dirty flag being set.
  /// </summary>
  public class EventStateSuppressor : IDisposable
  {
    readonly BusinessObject _bo;
    readonly IList _list;

    //[ThreadStatic]
    static Dictionary<BusinessObject, EventStateSuppressor> _contextDictionary = new Dictionary<BusinessObject, EventStateSuppressor>();
    public static Dictionary<BusinessObject, EventStateSuppressor> ContextDictionary
    {
      get { return EventStateSuppressor._contextDictionary; }
    }

    /// <summary>
    /// Will suppress PropertyChanged &amp; CompositionChanged events of the specified BusinessObject and all it's children, aswell as their Dirty flag being set.
    /// </summary>
    /// <param name="bo">The targe BusinessObject</param>
    public EventStateSuppressor(BusinessObject bo)
    {
      if (bo == null) throw new ArgumentNullException("bo");

      _bo = bo;
      if (!_bo.SuppressEventState)
      {
        ContextDictionary.Add(_bo, this);
        _bo.SuppressEventState = true;
      }
    }

    /// <summary>
    /// Will suppress PropertyChanged &amp; CompositionChanged events of the specified BusinessObject list and all it's children, aswell as their Dirty flag being set.
    /// </summary>
    /// <param name="bo">The targe BusinessObject</param>
    public EventStateSuppressor(IList list)
    {
      if (list == null) throw new ArgumentNullException("list");

      _list = list;
      foreach (object o in _list)
      {
        BusinessObject bo = o as BusinessObject;
        if (bo != null)
        {
          if (!bo.SuppressEventState)
          {
            ContextDictionary.Add(bo, this);
            bo.SuppressEventState = true;
          }
        }
      }      
    }

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    bool _disposed;
    protected virtual void Dispose(bool disposing)
    {
      if (_disposed) return;
      if (disposing)
      {
        if (_bo != null)
        {
          if (ContextDictionary.ContainsKey(_bo) && ContextDictionary[_bo].Equals(this))
          {
            //if (_bo.Owner != null)
            //  _bo.SuppressEventState = _bo.Owner.SuppressEventState;
            //else
              _bo.SuppressEventState = false;
            ContextDictionary.Remove(_bo);
          }
        }
        if (_list != null)
        {
          foreach (object o in _list)
          {
            BusinessObject bo = o as BusinessObject;
            if (bo != null)
            {
              if (ContextDictionary.ContainsKey(bo) && ContextDictionary[bo].Equals(this))
              {
                //if (bo.Owner != null)
                //  bo.SuppressEventState = bo.Owner.SuppressEventState;
                //else
                  bo.SuppressEventState = false;
                ContextDictionary.Remove(bo);
              }
            }
          }
        }
      }
      _disposed = true;
    }
  }
}
