﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business
{
  public interface IAssociativeObject
  {
    int ReferenceId
    {
      get;
      set;
    }

    BusinessObject BusinessObjectReference
    {
      get;
      set;
    }
  }
}
