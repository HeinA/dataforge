﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Data
{
  public class CachedObjectRequiresChecksumColumnException : Exception
  {
    protected CachedObjectRequiresChecksumColumnException(SerializationInfo info, StreamingContext context)
      :base(info, context)
    {
    }

    public CachedObjectRequiresChecksumColumnException(string message)
      : base(message)
    {
    }

    public CachedObjectRequiresChecksumColumnException(string message, Exception innerException)
      : base(message, innerException)
    {
    }
  }
}
