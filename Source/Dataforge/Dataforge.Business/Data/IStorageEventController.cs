﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Data
{
  interface IStorageEventController
  {
    void OnAfterLoad(BusinessObject obj, string filterKey);
    bool OnBeforeSave(BusinessObject obj);
    void OnAfterSave(BusinessObject obj);
    bool OnBeforeDelete(BusinessObject obj);
  }
}
