﻿using System;
using System.Collections.Generic;
using System.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Data
{
  public abstract class StorageEventController<T> : IStorageEventController
    where T : BusinessObject
  {
    public abstract void OnAfterLoad(T obj, string filterKey);
    public abstract bool OnBeforeSave(T obj);
    public abstract void OnAfterSave(T obj);
    public abstract bool OnBeforeDelete(T obj);

    void IStorageEventController.OnAfterLoad(BusinessObject obj, string filterKey)
    {
      OnAfterLoad((T)obj, filterKey);
    }

    bool IStorageEventController.OnBeforeSave(BusinessObject obj)
    {
      return OnBeforeSave((T)obj);
    }

    void IStorageEventController.OnAfterSave(BusinessObject obj)
    {
      OnAfterSave((T)obj);
    }

    bool IStorageEventController.OnBeforeDelete(BusinessObject obj)
    {
      return OnBeforeDelete((T)obj);
    }
  }
}
