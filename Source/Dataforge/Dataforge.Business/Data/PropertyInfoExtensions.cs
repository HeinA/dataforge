﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Data
{
  public static class PropertyInfoExtensions
  {
    static Dictionary<PropertyInfo, Delegate> _propertySetterDictionary;
    static Dictionary<PropertyInfo, Delegate> PropertySetterDictionary
    {
      get
      {
        if (_propertySetterDictionary == null) _propertySetterDictionary = new Dictionary<PropertyInfo, Delegate>();
        return _propertySetterDictionary;
      }
    }

    static Dictionary<PropertyInfo, Delegate> _propertyGetterDictionary;
    static Dictionary<PropertyInfo, Delegate> PropertyGetterDictionary
    {
      get
      {
        if (_propertyGetterDictionary == null) _propertyGetterDictionary = new Dictionary<PropertyInfo, Delegate>();
        return _propertyGetterDictionary;
      }
    }

    static Func<T, object> GetValueGetter<T>(this PropertyInfo propertyInfo)
    {
      if (typeof(T) != propertyInfo.DeclaringType)
      {
        throw new ArgumentException();
      }
      if (PropertyGetterDictionary.ContainsKey(propertyInfo)) return (Func<T, object>)PropertyGetterDictionary[propertyInfo];

      var instance = Expression.Parameter(propertyInfo.DeclaringType, "i");
      var property = Expression.Property(instance, propertyInfo);
      var convert = Expression.TypeAs(property, typeof(object));
      Delegate d = Expression.Lambda(convert, instance).Compile();
      PropertyGetterDictionary.Add(propertyInfo, d);
      return (Func<T, object>)d;

    }

    static Action<T, object> GetValueSetter<T>(this PropertyInfo propertyInfo)
    {
      if (typeof(T) != propertyInfo.DeclaringType)
      {
        throw new ArgumentException();
      }
      if (PropertySetterDictionary.ContainsKey(propertyInfo)) return (Action<T, object>)PropertySetterDictionary[propertyInfo];

      var instance = Expression.Parameter(propertyInfo.DeclaringType, "i");
      var argument = Expression.Parameter(typeof(object), "a");
      var setterCall = Expression.Call(
          instance,
          propertyInfo.GetSetMethod(),
          Expression.Convert(argument, propertyInfo.PropertyType));
      Delegate d = Expression.Lambda(setterCall, instance, argument).Compile();
      PropertySetterDictionary.Add(propertyInfo, d);
      return (Action<T, object>)d;
    }

    static object _lock = new object();

    public static object GetValue<T>(PropertyInfo pi, T obj)
    {
      lock (_lock)
      {
        var getter = pi.GetValueGetter<T>();
        return getter(obj);
      }
    }

    public static void SetValue<T>(PropertyInfo pi, T obj, object value)
    {
      lock (_lock)
      {
        var setter = pi.GetValueSetter<T>();
        setter(obj, value);
      }
    }
  }
}
