﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Data
{
  public class DataRow
  {
    public DataRow(DataView view, Type type, int id)
    {
      _view = view;
      _objectType = type;
      _id = id;
    }

    DataView _view;
    public DataView View
    {
      get { return _view; }
    }

    Type _objectType;
    public Type ObjectType
    {
      get { return _objectType; }
    }

    int _id;
    public int Id
    {
      get { return _id; }
    }

    Dictionary<string, object> _columns;
    public Dictionary<string, object> Columns
    {
      get
      {
        if (_columns == null) _columns = new Dictionary<string, object>();
        return _columns;
      }
    }

    BusinessObject _object = null;
    public BusinessObject GetObject(IORMapper mapper)
    {
      if (_object != null) return _object;
      _object = (BusinessObject)Activator.CreateInstance(ObjectType);
      using (new EventStateSuppressor(_object))
      {
        mapper.LoadObject(_object, View, this);
      }
      return _object;
    }
  }
}
