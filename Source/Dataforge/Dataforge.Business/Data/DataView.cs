﻿using Dataforge.Business.Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Data
{
  public class DataView
  {
    public DataView()
    {      
    }

    public DataView(string filterKey)
      : this()
    {
      _filterKey = filterKey;
    }

    string _filterKey;
    public string FilterKey
    {
      get { return _filterKey; }
    }

    public int ExecuteCommand(Type type, string idColumn, IDbCommand cmd)
    {
      return ExecuteCommand(type, idColumn, cmd, false);
    }

    List<Type> _fullyLodedTypes;
    List<Type> FullyLodedTypes
    {
      get
      {
        if (_fullyLodedTypes == null) _fullyLodedTypes = new List<Type>();
        return _fullyLodedTypes;
      }
    }

    public int ExecuteCommand(Type type, string idColumn, IDbCommand cmd, bool allObjectsLoaded)
    {
      if (type == null) throw new ArgumentNullException("type");
      if (string.IsNullOrWhiteSpace(idColumn)) throw new ArgumentOutOfRangeException("idColumn");
      if (cmd == null) throw new ArgumentNullException("cmd");

      if (FullyLodedTypes.Contains(type)) return 0;
      if (allObjectsLoaded) 
        FullyLodedTypes.Add(type);

#if SQL
      Debug.WriteLine(cmd.CommandText);
      Debug.IndentLevel++;
      foreach (IDataParameter p in cmd.Parameters)
      {
        Debug.WriteLine("{0} -> {1}", p.ParameterName, p.Value);
      }
      Debug.IndentLevel--;
#endif

      using (DataSet ds = new DataSet())
      {
        ds.Locale = CultureInfo.InvariantCulture;
        using (IDataReader r = cmd.ExecuteReader())
        {
          ds.Load(r, LoadOption.OverwriteChanges, string.Empty);
        }

        int i = 0;
        foreach (System.Data.DataRow dr in ds.Tables[0].Rows)
        {
          DataRow dr1 = new DataRow(this, type, (int)dr[idColumn]);
          foreach (DataColumn col in dr.Table.Columns)
          {
            dr1.Columns.Add(col.ColumnName, dr[col.ColumnName] == DBNull.Value ? null : dr[col.ColumnName]);
          }
          Rows.Add(dr1);
          i++;
        }
        return i;
      }
    }

    Collection<DataRow> _rows;
    public Collection<DataRow> Rows
    {
      get
      {
        if (_rows == null) _rows = new Collection<DataRow>();
        return _rows;
      }
    }

    public Collection<int> GetIdCollection(Type objectType)
    {
      Collection<int> ids = new Collection<int>();
      foreach (DataRow dr in Rows.Where(dr => dr.ObjectType == objectType || dr.ObjectType.IsSubclassOf(objectType)))
      {
        ids.Add(dr.Id);
      }
      return ids;
    }

    public Collection<int> GetReferenceCollection(Type objectType, string referenceColumn)
    {
      Collection<int> ids = new Collection<int>();
      foreach (DataRow dr in Rows.Where(dr => dr.ObjectType == objectType || dr.ObjectType.IsSubclassOf(objectType)))
      {
        ids.Add((int)dr.Columns[referenceColumn]);
      }
      return ids;
    }

    public Collection<DataRow> GetRowsByType(Type objectType)
    {
      Collection<DataRow> rows = new Collection<DataRow>();
      foreach (DataRow dr in Rows.Where(dr => dr.ObjectType == objectType || dr.ObjectType.IsSubclassOf(objectType)))
      {
        rows.Add(dr);
      }
      return rows;
    }

    public Collection<DataRow> GetRowsByType(Type objectType, string ownerColumn, int ownerId)
    {
      Collection<DataRow> rows = new Collection<DataRow>();
      foreach (DataRow dr in Rows.Where(dr => (dr.ObjectType == objectType || dr.ObjectType.IsSubclassOf(objectType)) && ((int)(dr.Columns[ownerColumn] ?? 0)) == ownerId))
      {
        rows.Add(dr);
      }
      return rows;
    }

    public DataRow GetRow(Type objectType, int id)
    {
      return Rows.FirstOrDefault(dr => (dr.ObjectType == objectType || dr.ObjectType.IsSubclassOf(objectType)) && (dr.Id == id));
    }
  }
}
