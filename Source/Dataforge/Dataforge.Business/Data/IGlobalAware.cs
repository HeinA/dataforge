﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Data
{
  public interface IGlobalAware
  {
    bool IsGlobal { get; set; }
  }
}
