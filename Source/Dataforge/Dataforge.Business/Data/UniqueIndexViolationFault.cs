﻿using Dataforge.Business.Constants;
using Dataforge.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Data
{
  [DataContract(Namespace = Namespaces.Dataforge)]
  public class UniqueIndexViolationFault : DataforgeFault
  {
  }
}
