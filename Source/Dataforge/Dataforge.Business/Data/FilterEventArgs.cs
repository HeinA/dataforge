﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Data
{
  public class FilterQueryEventArgs : EventArgs
  {
    public FilterQueryEventArgs(Type objectType, string filterKey)
    {
      _objectType = objectType;
      _filterKey = filterKey;
    }

    Type _objectType;
    public Type ObjectType
    {
      get { return _objectType; }
    }

    string _filterKey;
    public string FilterKey
    {
      get { return _filterKey; }
    }

    Collection<DataFilterCondition> _filters;
    public Collection<DataFilterCondition> Filters
    {
      get
      {
        if (_filters == null) _filters = new Collection<DataFilterCondition>();
        return _filters;
      }
    }
  }
}
