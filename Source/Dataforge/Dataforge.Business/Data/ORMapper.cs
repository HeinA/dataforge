﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using Dataforge.Business.Security;
using Dataforge.Business.Service;
using Dataforge.Business.Service.Wcf;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Composition;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Data
{
  public abstract class ORMapper<T> : IORMapper, IObjectStore<T>
    where T : BusinessObject
  {
    #region Contructors

    protected ORMapper()
    {
      //Debug.WriteLine("***********  {0}", this.GetType());
    }

    #endregion

    #region OnImportsSatisfied

    [OnImportsSatisfied]
    public virtual void OnImportsSatisfied()
    {
      var tableAttributes = (TableAttribute[])ObjectType.GetCustomAttributes(typeof(TableAttribute), false);
      if (tableAttributes.Length == 0) 
        throw new TableAttributeException("Table Attribute is not defined");
      TableAttribute = tableAttributes[0];

      PopulatePropertyInfoCollection(ObjectType);

      if (typeof(T).IsSubclassOf(typeof(IdentifiableObject))) IsGlobalIdentifiable = true;

      foreach (var prop in PropertyInfoCollection)
      {
        var attributes = (ColumnAttribute[])prop.GetCustomAttributes(typeof(ColumnAttribute), false);
        ColumnAttributeDictionary.Add(prop, attributes[0]);
        ColumnAttributeDictionaryByName.Add(prop.Name, attributes[0]);
      }

      foreach (var prop in OwnedCollection)
      {
        Type[] types = prop.PropertyType.GetGenericArguments();
        IORMapper mapper = (IORMapper)DataManager.Instance.GetObjectMapper(types[0]);
        if (mapper.OwnerColumn != null)
        {
          OwnedMapperDictionary.Add(prop, mapper);
        }
      }

      GetSubClassMappers();
      GetExtensionObjectMappers();
    }

    #endregion

    #region Public Properties

    public Type ObjectType
    {
      get
      {
        return typeof(T);
      }
    }

    #region Table Attributes

    TableAttribute _tableAttribute;
    TableAttribute TableAttribute
    {
      get { return _tableAttribute; }
      set { _tableAttribute = value; }
    }

    public string TableName
    {
      get
      {
        return EncapsulateName(TableAttribute.Name ?? ObjectType.Name);
      }
    }

    public string TableSchema
    {
      get
      {
        return EncapsulateName(TableAttribute.Schema);
      }
    }

    public string FullTableName
    {
      get
      {
        return string.Format(CultureInfo.InvariantCulture, TableAttribute.Schema == null ? "{2}" : "{0}{1}{2}", TableSchema, SchemaSeparator, TableName);
      }
    }

    public string IdColumn
    {
      get
      {
        return TableAttribute.IdColumn ?? "Id";
      }
    }

    public string GlobalIdentifierColumn
    {
      get
      {
        return TableAttribute.GlobalIdentifierColumn ?? "GlobalIdentifier";
      }
    }

    bool _isGlobalIdentifiable = false;
    public bool IsGlobalIdentifiable
    {
      get { return _isGlobalIdentifiable; }
      private set { _isGlobalIdentifiable = value; }
    }

    public string OwnerColumn
    {
      get
      {
        return TableAttribute.OwnerColumn;
      }
    }

    //public bool IsCircularReference
    //{
    //  get { return TableAttribute.IsCircularReference; }
    //}

    public string RowColumn
    {
      get
      {
        return TableAttribute.RowColumn;
      }
    }

    public string ColumnColumn
    {
      get
      {
        return TableAttribute.ColumnColumn;
      }
    }

    public bool FlattenBase
    {
      get { return TableAttribute.FlattenBase; }
    }

    public bool Global
    {
      get { return TableAttribute.Global; }
    }

    public string ApplicationDomainColumn
    {
      get
      {
        return TableAttribute.ApplicationDomainColumn ?? "ApplicationDomain";
      }
    }

    public bool FlattenToSubChild
    {
      get { return TableAttribute.FlattenToSubClass; }
    }

    public string ReferenceColumn
    {
      get { return TableAttribute.ReferenceColumn; }
    }

    public Type OwnerType
    {
      get { return TableAttribute.OwnerType; }
    }

    public bool IsCached(PropertyInfo pi)
    {
      return CacheOwned.ContainsKey(pi);
    }

    #endregion

    #endregion

    #region Property Info & Mappers

    #region Mappers

    IORMapper _baseMapper;
    protected IORMapper BaseMapper
    {
      get { return _baseMapper; }
      private set { _baseMapper = value; }
    }

    IORMapper _rowMapper;
    public IORMapper RowMapper
    {
      get { return _rowMapper; }
      set { _rowMapper = value; }
    }

    IORMapper _columnMapper;
    public IORMapper ColumnMapper
    {
      get { return _columnMapper; }
      set { _columnMapper = value; }
    }

    PropertyInfo _cellCollection;
    public PropertyInfo CellCollection
    {
      get { return _cellCollection; }
      set { _cellCollection = value; }
    }

    void GetSubClassMappers()
    {
      foreach (Assembly ass in AppDomain.CurrentDomain.GetAssemblies())
      {
        foreach (Type t in ass.GetTypes().Where(o => o.IsDefined(typeof(TableAttribute)) && !o.IsAbstract && o.IsSubclassOf(typeof(T))))
        {
          SubClassMappers.Add((IORMapper)DataManager.Instance.GetObjectMapper(t));
        }
      }
    }

    void GetExtensionObjectMappers()
    {
      foreach (Assembly ass in AppDomain.CurrentDomain.GetAssemblies())
      {
        foreach (Type t in ass.GetTypes().Where(o => o.BaseType != null && o.IsSubclassOf(typeof(ExtensionObject)) && o.IsDefined(typeof(TableAttribute)) && ((TableAttribute)o.GetCustomAttribute(typeof(TableAttribute))).OwnerType == typeof(T)))
        {
          ExtensionObjectMapperDictionary.Add(t, (IORMapper)DataManager.Instance.GetObjectMapper(t));
        }
      }
    }

    Dictionary<Type, IORMapper> _extensionObjectMapperDictionary;
    Dictionary<Type, IORMapper> ExtensionObjectMapperDictionary
    {
      get
      {
        if (_extensionObjectMapperDictionary == null) _extensionObjectMapperDictionary = new Dictionary<Type,IORMapper>();
        return _extensionObjectMapperDictionary;
      }
    }

    Dictionary<PropertyInfo, IORMapper> _associativeExtensionObjectMapperDictionary;
    Dictionary<PropertyInfo, IORMapper> AssociativeExtensionObjectMapperDictionary
    {
      get
      {
        if (_associativeExtensionObjectMapperDictionary == null) _associativeExtensionObjectMapperDictionary = new Dictionary<PropertyInfo, IORMapper>();
        return _associativeExtensionObjectMapperDictionary;
      }
    }

    Dictionary<PropertyInfo, IORMapper> _associativeExtensionObjectReferenceMapperDictionary;
    Dictionary<PropertyInfo, IORMapper> AssociativeExtensionObjectReferenceMapperDictionary
    {
      get
      {
        if (_associativeExtensionObjectReferenceMapperDictionary == null) _associativeExtensionObjectReferenceMapperDictionary = new Dictionary<PropertyInfo, IORMapper>();
        return _associativeExtensionObjectReferenceMapperDictionary;
      }
    }

    Collection<IORMapper> _subClassMappers;
    protected Collection<IORMapper> SubClassMappers
    {
      get
      {
        if (_subClassMappers == null) _subClassMappers = new Collection<IORMapper>();
        return _subClassMappers;
      }
    }

    Collection<IORMapper> _searchJoins;
    public Collection<IORMapper> SearchJoins
    {
      get
      {
        if (_searchJoins == null) _searchJoins = new Collection<IORMapper>();
        return _searchJoins;
      }
    }

    Collection<PropertyInfo> _searchProperties;
    public Collection<PropertyInfo> SearchProperties
    {
      get
      {
        if (_searchProperties == null) _searchProperties = new Collection<PropertyInfo>();
        return _searchProperties;
      }
    }

    Dictionary<PropertyInfo, SearchAttribute> _searchPropertyAttributes;
    public Dictionary<PropertyInfo, SearchAttribute> SearchPropertyAttributes
    {
      get
      {
        if (_searchPropertyAttributes == null) _searchPropertyAttributes = new Dictionary<PropertyInfo, SearchAttribute>();
        return _searchPropertyAttributes;
      }
    }

    string GetSearchAlias(PropertyInfo pi)
    {
      SearchAttribute sa = null;
      if (SearchPropertyAttributes.ContainsKey(pi)) sa = SearchPropertyAttributes[pi];
      if (sa == null) return null;
      return sa.Alias ?? GetColumnName(pi);
    }

    string GetSearchFormat(PropertyInfo pi)
    {
      SearchAttribute sa = null;
      if (SearchPropertyAttributes.ContainsKey(pi)) sa = SearchPropertyAttributes[pi];
      if (sa == null) return null;
      return sa.Format;
    }

    #endregion

    void PopulatePropertyInfoCollection(Type t)
    {
      Type baseClass = t.BaseType;
      if (baseClass != typeof(BusinessObject))
      {
        var tableAttributes = (TableAttribute[])baseClass.GetCustomAttributes(typeof(TableAttribute), false);
        if (tableAttributes.Length == 0) throw new TableAttributeException("Table Attribute is undefined");
        TableAttribute baseAttribute = tableAttributes[0];

        while (baseAttribute.FlattenToSubClass)
        {
          LoadObjectCollections(baseClass);
          baseClass = baseClass.BaseType;
          if (baseClass == typeof(BusinessObject)) break;
          var tableAttributes1 = (TableAttribute[])baseClass.GetCustomAttributes(typeof(TableAttribute), false);
          if (tableAttributes1.Length == 0) throw new TableAttributeException("Table Attribute is undefined");
          baseAttribute = tableAttributes1[0];
        }
        if (baseClass != typeof(BusinessObject))
        {
          if (FlattenBase)
          {
            PopulatePropertyInfoCollection(baseClass);
          }
          else
          {
            BaseMapper = (IORMapper)DataManager.Instance.GetObjectMapper(baseClass);
          }
        }
      }

      LoadObjectCollections(t);
    }

    void LoadObjectCollections(Type t)
    {
      foreach (var pi in t.GetProperties(BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly).Where(prop => prop.IsDefined(typeof(ColumnAttribute), false)))
      {
        SearchAttribute sa = (SearchAttribute)pi.GetCustomAttributes(typeof(SearchAttribute), false).FirstOrDefault();
        if (sa != null)
        {
          SearchProperties.Add(pi);
          SearchPropertyAttributes.Add(pi, sa);
        }
        PropertyInfoCollection.Add(pi);
        if (pi.PropertyType.IsSubclassOf(typeof(BusinessObject)))
        {
          ReferencePropertyInfoCollection.Add(pi);
          if ( !IsReferenceColumn(pi)) ReferenceMapperDictionary.Add(pi, DataManager.Instance.GetObjectMapper(pi.PropertyType));
        }
      }

      foreach (PropertyInfo pi in t.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly).Where(p => p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(BusinessObjectCollection<>)))
      {
        CacheAttribute ca = (CacheAttribute)pi.GetCustomAttributes(typeof(CacheAttribute), false).FirstOrDefault();
        if (ca != null) CacheOwned.Add(pi, true);
        SearchAttribute sa = (SearchAttribute)pi.GetCustomAttributes(typeof(SearchAttribute), false).FirstOrDefault();
        if (sa != null && typeof(IList).IsAssignableFrom(pi.PropertyType)) SearchJoins.Add(DataManager.Instance.GetObjectMapper(pi.PropertyType.GetGenericArguments()[0]));
        OwnedCollection.Add(pi);
      }

      if (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(TableObject<,,>))
      {
        PropertyInfo pi = t.GetProperty("Cells", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.DeclaredOnly);
        CacheOwned.Add(pi, true);
        OwnedCollection.Add(pi);
        Type[] types = t.GetGenericArguments();
        ColumnMapper = DataManager.Instance.GetObjectMapper(types[0]);
        RowMapper = DataManager.Instance.GetObjectMapper(types[1]);
        CellCollection = pi;
      }

      foreach (PropertyInfo pi in t.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly).Where(p => p.PropertyType.IsGenericType && (p.PropertyType.GetGenericTypeDefinition() == typeof(AssociativeObjectCollection<,>)))) //p.PropertyType.GetGenericTypeDefinition() == typeof(AssociativeReferenceCollection<>) || 
      {
        CacheAttribute ca = (CacheAttribute)pi.GetCustomAttributes(typeof(CacheAttribute), false).FirstOrDefault();
        if (ca != null) CacheOwned.Add(pi, true);
        AssociativeCollection.Add(pi);
        Type[] attributes = pi.PropertyType.GetGenericArguments();
        AssociativeExtensionObjectMapperDictionary.Add(pi, (IORMapper)DataManager.Instance.GetObjectMapper(attributes[0]));
        if (attributes.Length > 1) AssociativeExtensionObjectReferenceMapperDictionary.Add(pi, (IORMapper)DataManager.Instance.GetObjectMapper(attributes[1]));
      }
    }

    Dictionary<PropertyInfo, bool> _cacheOwned;
    Dictionary<PropertyInfo, bool> CacheOwned
    {
      get
      {
        if (_cacheOwned == null) _cacheOwned = new Dictionary<PropertyInfo, bool>();
        return _cacheOwned;
      }
    } 

    Collection<PropertyInfo> _ownedCollection;
    Collection<PropertyInfo> OwnedCollection
    {
      get
      {
        if (_ownedCollection == null) _ownedCollection = new Collection<PropertyInfo>();
        return _ownedCollection;
      }
    }

    Collection<PropertyInfo> _associativeCollection;
    Collection<PropertyInfo> AssociativeCollection
    {
      get
      {
        if (_associativeCollection == null) _associativeCollection = new Collection<PropertyInfo>();
        return _associativeCollection;
      }
    }

    Dictionary<PropertyInfo, IORMapper> _referenceMapperDictionary;
    Dictionary<PropertyInfo, IORMapper> ReferenceMapperDictionary
    {
      get
      {
        if (_referenceMapperDictionary == null) _referenceMapperDictionary = new Dictionary<PropertyInfo, IORMapper>();
        return _referenceMapperDictionary;
      }
    }

    Collection<PropertyInfo> _referencePropertyInfoCollection;
    protected Collection<PropertyInfo> ReferencePropertyInfoCollection
    {
      get
      {
        if (_referencePropertyInfoCollection == null) _referencePropertyInfoCollection = new Collection<PropertyInfo>();
        return _referencePropertyInfoCollection;
      }
      private set { _referencePropertyInfoCollection = value; }
    }

    Dictionary<PropertyInfo, IORMapper> _ownedMapperDictionary;
    Dictionary<PropertyInfo, IORMapper> OwnedMapperDictionary
    {
      get
      {
        if (_ownedMapperDictionary == null) _ownedMapperDictionary = new Dictionary<PropertyInfo, IORMapper>();
        return _ownedMapperDictionary;
      }
    }

    Collection<PropertyInfo> _propertyInfoCollection;
    protected Collection<PropertyInfo> PropertyInfoCollection
    {
      get
      {
        if (_propertyInfoCollection == null) _propertyInfoCollection = new Collection<PropertyInfo>();
        return _propertyInfoCollection;
      }
      private set { _propertyInfoCollection = value; }
    }

    Dictionary<PropertyInfo, ColumnAttribute> _columnAttributeDictionary;
    Dictionary<PropertyInfo, ColumnAttribute> ColumnAttributeDictionary
    {
      get
      {
        if (_columnAttributeDictionary == null) _columnAttributeDictionary = new Dictionary<PropertyInfo, ColumnAttribute>();
        return _columnAttributeDictionary;
      }
      set { _columnAttributeDictionary = value; }
    }

    Dictionary<string, ColumnAttribute> _columnAttributeDictionaryByName;
    Dictionary<string, ColumnAttribute> ColumnAttributeDictionaryByName
    {
      get
      {
        if (_columnAttributeDictionaryByName == null) _columnAttributeDictionaryByName = new Dictionary<string, ColumnAttribute>();
        return _columnAttributeDictionaryByName;
      }
      set { _columnAttributeDictionaryByName = value; }
    }

    #endregion

    #region Sql Formatting

    protected virtual string EncapsulateName(string name)
    {
      if (string.IsNullOrWhiteSpace(name)) return null;
      return string.Format(CultureInfo.InvariantCulture, "[{0}]", name);
    }

    protected string GetAlias(ref int iJoinCount)
    {
      return string.Format("A{0}", iJoinCount++);
    }

    protected virtual string GetFilterType(FilterType filter)
    {
      switch (filter)
      {
        case FilterType.Equals:
          return "=";

        case FilterType.NotEquals:
          return "<>";

        case FilterType.GreaterThan:
          return ">";

        case FilterType.GreaterThanEqual:
          return ">=";

        case FilterType.LessThan:
          return "<";

        case FilterType.LessThanEqual:
          return "=";

        case FilterType.Like:
          return "LIKE";

        case FilterType.IsNull:
          return "IS NULL";

        case FilterType.IsNotNull:
          return "IS NOT NULL";
      }

      return null;
    }
   
    protected virtual string OrderBy
    {
      get { return "ORDER BY"; }
    }

    protected virtual string OrderBySeparator
    {
      get { return ","; }
    }

    public virtual string GetOrderByColumns()
    {
      string orderBy = null;
      if (BaseMapper != null) orderBy = BaseMapper.GetOrderByColumns();
      foreach (var pi in PropertyInfoCollection.Where(prop => prop.IsDefined(typeof(ColumnAttribute), false) && GetColumnSortOrder(prop) >= 0).OrderBy(prop => GetColumnSortOrder(prop)))
      {
        orderBy += string.Format(CultureInfo.InvariantCulture, orderBy == null ? "{0}" : "{1} {0}",  FormatOrderBy(GetColumnName(pi), GetColumnSortDirection(pi)), OrderBySeparator);
      }
      return orderBy;
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
    protected virtual string GetOrderByClause()
    {
      string orderBy = GetOrderByColumns();
      if (orderBy == null) return null;
      return string.Format(CultureInfo.InvariantCulture, "{0} {1}", OrderBy, orderBy);
    }

    public virtual string GetReadColumns()
    {
      string columns = null;
      if (BaseMapper != null) columns = BaseMapper.GetReadColumns();

      if (typeof(IGlobalAware).IsAssignableFrom(this.ObjectType) && !typeof(IAssociativeObject).IsAssignableFrom(ObjectType))
      {
        columns += string.Format(CultureInfo.InvariantCulture, columns == null ? "{1}.{0}" : ", {1}.{0}", EncapsulateName(ApplicationDomainColumn), FullTableName);
      }

      if (IsGlobalIdentifiable)
      {
        columns += string.Format(CultureInfo.InvariantCulture, columns == null ? "{1}.{0}" : ", {1}.{0}", EncapsulateName(GlobalIdentifierColumn), FullTableName);
      }

      foreach (PropertyInfo pi in PropertyInfoCollection.Where(prop => IsReadColumn(prop)))
      {
        columns += string.Format(CultureInfo.InvariantCulture, columns == null ? "{1}.{0}" : ", {1}.{0}", EncapsulateName(GetColumnName(pi)), FullTableName);
      }

      if (OwnerColumn != null)
      {
        columns += string.Format(CultureInfo.InvariantCulture, columns == null ? "{1}.{0}" : ", {1}.{0}", EncapsulateName(OwnerColumn), FullTableName);
      }

      if (RowColumn != null)
      {
        columns += string.Format(CultureInfo.InvariantCulture, columns == null ? "{1}.{0}" : ", {1}.{0}", EncapsulateName(RowColumn), FullTableName);
      }

      if (ColumnColumn != null)
      {
        columns += string.Format(CultureInfo.InvariantCulture, columns == null ? "{1}.{0}" : ", {1}.{0}", EncapsulateName(ColumnColumn), FullTableName);
      }

      if (ReferenceColumn != null)
      {
        columns += string.Format(CultureInfo.InvariantCulture, columns == null ? "{1}.{0}" : ", {1}.{0}", EncapsulateName(ReferenceColumn), FullTableName);
      }

      if (BaseMapper == null) columns = string.Format(CultureInfo.InvariantCulture, columns == null ? "{2}.{0}" : "{2}.{0}, {1}", EncapsulateName(IdColumn), columns, FullTableName);

      return columns;
    }

    public virtual string GetInsertColumns(BusinessObject bo, bool insertId)
    {
      string columns = null;

      if (IsGlobalIdentifiable)
      {
        columns += string.Format(CultureInfo.InvariantCulture, columns == null ? "{1}.{0}" : ", {1}.{0}", EncapsulateName(GlobalIdentifierColumn), FullTableName);
      }

      bool appDomainSpecified = false;
      foreach (PropertyInfo pi in PropertyInfoCollection.Where(prop => IsWriteColumn(prop)))
      {
        if (GetColumnName(pi) == ApplicationDomainColumn) appDomainSpecified = true;
        columns += string.Format(CultureInfo.InvariantCulture, columns == null ? "{1}.{0}" : ", {1}.{0}", EncapsulateName(GetColumnName(pi)), FullTableName);
      }

      if (OwnerColumn != null)
      {
        columns += string.Format(CultureInfo.InvariantCulture, columns == null ? "{1}.{0}" : ", {1}.{0}", EncapsulateName(OwnerColumn), FullTableName);
      }

      if (!Global && !appDomainSpecified && SecurityContext.User != null && !typeof(IAssociativeObject).IsAssignableFrom(ObjectType))
      {
        IGlobalAware ga = bo as IGlobalAware;
        if (ga == null || !ga.IsGlobal)
        {
          columns += string.Format(CultureInfo.InvariantCulture, columns == null ? "{1}.{0}" : ", {1}.{0}", EncapsulateName(ApplicationDomainColumn), FullTableName);
        }
      }

      if (RowColumn != null)
      {
        columns += string.Format(CultureInfo.InvariantCulture, columns == null ? "{1}.{0}" : ", {1}.{0}", EncapsulateName(RowColumn), FullTableName);
      }

      if (ColumnColumn != null)
      {
        columns += string.Format(CultureInfo.InvariantCulture, columns == null ? "{1}.{0}" : ", {1}.{0}", EncapsulateName(ColumnColumn), FullTableName);
      }

      if (ReferenceColumn != null)
      {
        columns += string.Format(CultureInfo.InvariantCulture, columns == null ? "{1}.{0}" : ", {1}.{0}", EncapsulateName(ReferenceColumn), FullTableName);
      }

      if (BaseMapper != null || insertId)
      {
        columns += string.Format(CultureInfo.InvariantCulture, columns == null ? "{1}.{0}" : ", {1}.{0}", EncapsulateName(IdColumn), FullTableName);
      }
      
      return columns;
    }

    public virtual string GetInsertValues(IDbCommand cmd, BusinessObject bo, bool insertId, ref int iCmd)
    {
      throw new NotImplementedException();
    }

    public virtual string GetUpdateColumns(IDbCommand cmd, BusinessObject bo, ref int iCmd)
    {
      throw new NotImplementedException();
    }

    public virtual string GetUpdateConditions(IDbCommand cmd, BusinessObject bo, ref int iCmd)
    {
      throw new NotImplementedException();
    }

    public virtual string GetJoin()
    {
      string join = null;
      if (BaseMapper != null)
      {
        join += string.Format(CultureInfo.InvariantCulture, "INNER JOIN {0} ON {1}.{2}={0}.{3} ", BaseMapper.FullTableName, FullTableName, EncapsulateName(IdColumn), EncapsulateName(BaseMapper.IdColumn));
        join += BaseMapper.GetJoin();
      }
      return join;
    }

    public virtual string GetJoin(string alias)
    {
      string join = null;
      if (BaseMapper != null)
      {
        join += string.Format(CultureInfo.InvariantCulture, "INNER JOIN {0} ON {1}.{2}={0}.{3} ", BaseMapper.FullTableName, alias, EncapsulateName(IdColumn), EncapsulateName(BaseMapper.IdColumn));
        join += BaseMapper.GetJoin();
      }
      return join;
    }

    protected virtual string FormatOrderBy(string column, SortDirection direction)
    {
      return string.Format(CultureInfo.InvariantCulture, "{2}.{0} {1}", EncapsulateName(column), direction == SortDirection.Ascending ? "ASC" : "DESC", FullTableName);
    }

    protected virtual string SchemaSeparator
    {
      get
      {
        return ".";
      }
    }

    #endregion

    #region Property Attributes

    protected string GetColumnName(PropertyInfo pi)
    {
      if (pi == null) throw new ArgumentNullException("pi");
      return ColumnAttributeDictionary[pi].Name ?? pi.Name;
    }

    protected string GetColumnName(string pName)
    {
      if (pName == null) throw new ArgumentNullException("pName");
      if (!ColumnAttributeDictionaryByName.ContainsKey(pName)) return pName;
      return ColumnAttributeDictionaryByName[pName].Name ?? pName;
    }

    protected bool IsReadColumn(PropertyInfo pi)
    {
      if (pi == null) throw new ArgumentNullException("pi");
      return ColumnAttributeDictionary[pi].Read && pi.CanWrite;
    }

    protected bool IgnoreConcurrencyColumn(PropertyInfo pi)
    {
      if (pi == null) throw new ArgumentNullException("pi");
      return ColumnAttributeDictionary[pi].IgnoreConcurrency;
    }

    protected bool IsWriteColumn(PropertyInfo pi)
    {
      if (pi == null) throw new ArgumentNullException("pi");
      return ColumnAttributeDictionary[pi].Write && pi.CanRead;
    }

    protected bool IgnoreNullColumn(PropertyInfo pi)
    {
      if (pi == null) throw new ArgumentNullException("pi");
      return ColumnAttributeDictionary[pi].IgnoreNull;
    }

    protected bool IsReferenceColumn(PropertyInfo pi)
    {
      if (pi == null) throw new ArgumentNullException("pi");
      if (!ColumnAttributeDictionary.ContainsKey(pi))
      {
        ColumnAttribute ca = pi.GetCustomAttribute<ColumnAttribute>();
        return ca.IsReference;
      }
      return ColumnAttributeDictionary[pi].IsReference;
    }

    protected int GetColumnSortOrder(PropertyInfo pi)
    {
      if (pi == null) throw new ArgumentNullException("pi");
      return ColumnAttributeDictionary[pi].SortOrder;
    }

    protected SortDirection GetColumnSortDirection(PropertyInfo pi)
    {
      if (pi == null) throw new ArgumentNullException("pi");
      return ColumnAttributeDictionary[pi].SortDirection;
    }

    #endregion

    #region DataView Fillers

    public virtual void FillDataView(DataView dv, Collection<DataFilterCondition> conditions)
    {
      FillChildren(dv, conditions);
    }

    public virtual void FillDataViewById(DataView dv, int id, Collection<DataFilterCondition> conditions)
    {
      FillChildren(dv, conditions);
    }

    public virtual void FillDataViewByReference(DataView dv, string referenceColumn, int? reference, Collection<DataFilterCondition> conditions)
    {
      FillChildren(dv, conditions);
    }

    public virtual void FillDataViewByReferences(DataView dv, string referenceColumn, Collection<int> references, Collection<DataFilterCondition> conditions)
    {
      FillChildren(dv, conditions);
    }

    void FillChildren(DataView dv, Collection<DataFilterCondition> conditions)
    {
      if (conditions != null)
      {
        for (int i = conditions.Count - 1; i >= 0; i--)
        {
          DataFilterCondition c = conditions[i];
          if (c.RemoveAfterFirstExecution)
            conditions.RemoveAt(i);
        }
      }

      foreach (PropertyInfo pi in ReferencePropertyInfoCollection)
      {
        if (ReferenceMapperDictionary.ContainsKey(pi))
        {
          IORMapper mapper = ReferenceMapperDictionary[pi];
          mapper.FillDataViewByReferences(dv, mapper.IdColumn, dv.GetReferenceCollection(ObjectType, GetColumnName(pi)), conditions);
        }
      }

      foreach (PropertyInfo pi in OwnedCollection)
      {
        if (dv.FilterKey != DataFilters.CacheLoad || IsCached(pi))
        {
          if (OwnedMapperDictionary.ContainsKey(pi))
          {
            IORMapper mapper = (IORMapper)OwnedMapperDictionary[pi];
            mapper.FillDataViewByReferences(dv, mapper.OwnerColumn, dv.GetIdCollection(ObjectType), conditions);
          }
        }
      }

      if (ColumnMapper != null)
      {
        ColumnMapper.FillDataViewByReferences(dv, ColumnMapper.OwnerColumn, dv.GetIdCollection(ObjectType), conditions);
      }

      if (RowMapper != null)
      {
        RowMapper.FillDataViewByReferences(dv, RowMapper.OwnerColumn, dv.GetIdCollection(ObjectType), conditions);
      }

      foreach (IORMapper mapper in ExtensionObjectMapperDictionary.Values)
      {
        mapper.FillDataViewByReferences(dv, mapper.OwnerColumn, dv.GetIdCollection(ObjectType), conditions);
      }

      foreach (PropertyInfo pi in AssociativeCollection)
      {
        if (dv.FilterKey != DataFilters.CacheLoad || IsCached(pi))
        {
          IORMapper mapper = (IORMapper)AssociativeExtensionObjectMapperDictionary[pi];
          mapper.FillDataViewByReferences(dv, mapper.OwnerColumn, dv.GetIdCollection(ObjectType), conditions);
          mapper = null;
          if (AssociativeExtensionObjectReferenceMapperDictionary.ContainsKey(pi)) mapper = (IORMapper)AssociativeExtensionObjectReferenceMapperDictionary[pi];
          if (mapper != null && (!CacheMappers.MapperTypes.Contains(mapper.ObjectType) || CacheMappers.MapperTypes.Contains(ObjectType)))
          {
            if (mapper.OwnerColumn == null)
            {
              mapper.FillDataView(dv, conditions);
            }
            else
            {
              Collection<int> ids = dv.GetIdCollection(mapper.OwnerType);
              if (ids.Count == 0)
              {
                mapper.FillDataView(dv, conditions);
              }
              else
              {
                mapper.FillDataViewByReferences(dv, mapper.OwnerColumn, dv.GetIdCollection(mapper.OwnerType), conditions);
              }
            }
          }
        }
      }
    }

    #endregion

    #region Object Loaders

    public ICollection LoadCache(DataView dv)
    {
      return Load(dv, null);
    }

    public void LoadObject(BusinessObject obj, DataView view, DataRow row)
    {
      LoadObject(obj, view, row, true);
    }

    public void LoadObject(BusinessObject obj, DataView view, DataRow row, bool loadSubClasses)
    {
      if (obj == null) throw new ArgumentNullException("obj");
      if (view == null) throw new ArgumentNullException("view");
      if (row == null) throw new ArgumentNullException("row");

      obj.Id = (int)row.Columns[IdColumn];
      if (!string.IsNullOrWhiteSpace(OwnerColumn))
      {
        obj.SetReferenceValue((int)(row.Columns[OwnerColumn] ?? 0), BusinessObject.PN_OWNER);
      }

      if (BaseMapper != null)
      {
        BaseMapper.LoadObject(obj, view, row, false);
      }

      if (obj is IAssociativeObject)
      {
        ((IAssociativeObject)obj).ReferenceId = (int)row.Columns[ReferenceColumn];
      }

      if (loadSubClasses)
      {
        foreach (IORMapper mapper in SubClassMappers)
        {
          if (mapper.ObjectType == obj.GetType() || obj.GetType().IsSubclassOf(mapper.ObjectType))
            mapper.LoadObject(obj, view, row, loadSubClasses);
        }
      }

      IGlobalAware ga = obj as IGlobalAware;
      if (ga != null)
      {
        object o = row.Columns[GetColumnName(ApplicationDomainColumn)];
        if (o == DBNull.Value || o == null) ga.IsGlobal = true;
        else ga.IsGlobal = false;
      }

      foreach (PropertyInfo pi in PropertyInfoCollection.Where(prop => IsReadColumn(prop)))
      {
        object o = row.Columns[GetColumnName(pi)];
        if (o == DBNull.Value) o = null;
        if (pi.PropertyType == typeof(BusinessObject) || pi.PropertyType.IsSubclassOf(typeof(BusinessObject)))
        {
          if (ReferenceMapperDictionary.ContainsKey(pi))
          {
            IORMapper mapper = ReferenceMapperDictionary[pi];
            DataRow dr2 = view.GetRow(ColumnMapper.ObjectType, (int)o);
            BusinessObject ro = (BusinessObject)dr2.GetObject(mapper);
            pi.SetValue(obj, ro);
          }
          else
          {
            obj.SetReferenceValue((int)o, pi.Name);
          }
        }
        else pi.SetValue(obj, o);
      }

      if (IsGlobalIdentifiable)
      {
        IdentifiableObject io = obj as IdentifiableObject;
        io.GlobalIdentifier = (Guid)row.Columns[GlobalIdentifierColumn];
      }

      foreach (PropertyInfo pi in OwnedCollection)
      {
        if (view.FilterKey != DataFilters.CacheLoad || IsCached(pi))
        {
          if (OwnedMapperDictionary.ContainsKey(pi))
          {
            IList collection = (IList)pi.GetValue(obj);
            IORMapper childMapper = OwnedMapperDictionary[pi];

            foreach (DataRow dr1 in view.GetRowsByType(childMapper.ObjectType, childMapper.OwnerColumn, obj.Id))
            {
              BusinessObject o = (BusinessObject)dr1.GetObject(childMapper);
              if (pi == CellCollection)
              {
                ITableCell tc = (ITableCell)o;
                BusinessObject bo = tc as BusinessObject;
                using (new EventStateSuppressor(bo))
                {
                  int columnId = (int)dr1.Columns[childMapper.ColumnColumn];
                  if (ColumnMapper != null)
                  {
                    DataRow dr2 = view.GetRow(ColumnMapper.ObjectType, columnId);
                    BusinessObject o1 = (BusinessObject)dr2.GetObject(ColumnMapper);
                    tc.BusinessObjectColumn = o1;
                  }
                  else
                  {
                    tc.SetColumnId(columnId);
                  }

                  int rowId = (int)dr1.Columns[childMapper.RowColumn];
                  if (RowMapper != null)
                  {
                    DataRow dr2 = view.GetRow(RowMapper.ObjectType, rowId);
                    BusinessObject o2 = (BusinessObject)dr2.GetObject(RowMapper);
                    tc.BusinessObjectRow = o2;
                  }
                  else
                  {
                    tc.SetRowId(rowId);
                  }

                  ((ITableObject)obj).AddCell(tc);
                }
              }
              else
              {
                collection.Add(o);
              }
            }
          }
        }
      }

      foreach (PropertyInfo pi in AssociativeCollection)
      {
        if (view.FilterKey != DataFilters.CacheLoad || IsCached(pi))
        {
          IORMapper childMapper = AssociativeExtensionObjectMapperDictionary[pi];
          IList list = (IList)pi.GetValue(obj);
          foreach (DataRow dr1 in view.GetRowsByType(childMapper.ObjectType, childMapper.OwnerColumn, obj.Id))
          {
            IAssociativeObject o = (IAssociativeObject)dr1.GetObject(childMapper);
            BusinessObject bo1 = o as BusinessObject;
            using (new EventStateSuppressor(obj))
            using (new EventStateSuppressor(bo1))
            {
              obj.SetAssociativeObject(childMapper.ObjectType, o);
              IORMapper referenceMapper = null;
              if (AssociativeExtensionObjectReferenceMapperDictionary.ContainsKey(pi)) referenceMapper = AssociativeExtensionObjectReferenceMapperDictionary[pi];
              if (referenceMapper != null && (!CacheMappers.MapperTypes.Contains(referenceMapper.ObjectType) || CacheMappers.MapperTypes.Contains(ObjectType)))
              {
                BusinessObject bo = (BusinessObject)view.GetRow(referenceMapper.ObjectType, o.ReferenceId).GetObject(referenceMapper);
                list.Add(bo);
              }
              //else
              //{
              //  list.Add(o.ReferenceId);
              //}
            }
          }
        }
      }

      foreach (IORMapper mapper in ExtensionObjectMapperDictionary.Values)
      {
        foreach (DataRow dr1 in view.GetRowsByType(mapper.ObjectType, mapper.OwnerColumn, obj.Id))
        {
          ExtensionObject o = (ExtensionObject)dr1.GetObject(mapper);
          obj.AddExtensionObject(o);
        }
      }

      obj.IsNew = false;

      foreach (var controller in DataManager.Instance.GetDataEventControllers<T>())
      {
        controller.OnAfterLoad((T)obj, view.FilterKey);
      }
    }

    #region Load

    public virtual BasicCollection<T> Load()
    {
      return Load((string)null, null);
    }

    public virtual BasicCollection<T> Load(string filterKey)
    {
      return Load(filterKey, null);
    }

    public virtual BasicCollection<T> Load(Collection<DataFilterCondition> conditions)
    {
      return Load((string)null, conditions);
    }

    public virtual BasicCollection<T> Load(string filterKey, Collection<DataFilterCondition> conditions)
    {
      DataView dv = new DataView(filterKey);
      return Load(dv, conditions);
    }

    public virtual BasicCollection<T> Load(DataView dv, Collection<DataFilterCondition> conditions)
    {
      BasicCollection<T> collection = new BasicCollection<T>();

      bool loadRootsOnly = false;
      if (!string.IsNullOrWhiteSpace(OwnerColumn) && conditions == null)
      {
        conditions = new Collection<DataFilterCondition>();
        conditions.Add(new DataFilterCondition(typeof(T), OwnerColumn, FilterType.IsNull, null, true));
        loadRootsOnly = true;
      }

      FillDataView(dv, conditions);

      if (loadRootsOnly)
      {
        foreach (DataRow dr in dv.GetRowsByType(typeof(T)).Where(r => r.Columns[OwnerColumn] == null))
        {
          T o = (T)dr.GetObject(this);
          collection.Add(o);
        }
      }
      else
      {
        foreach (DataRow dr in dv.GetRowsByType(typeof(T)))
        {
          T o = (T)dr.GetObject(this);
          collection.Add(o);
        }
      }

      return collection;
    }

    #endregion

    #region LoadById

    public virtual T LoadById(int id)
    {
      return LoadById(id, null, null);
    }

    public virtual T LoadById(int id, Collection<DataFilterCondition> conditions)
    {
      return LoadById(id, null, conditions);
    }

    public virtual T LoadById(int id, string filterKey)
    {
      return LoadById(id, filterKey, null);
    }

    public virtual T LoadById(int id, string filterKey, Collection<DataFilterCondition> conditions)
    {
      DataView dv = new DataView(filterKey);
      FillDataViewById(dv, id, conditions);

      Collection<DataRow> rows = dv.GetRowsByType(typeof(T));
      if (rows.Count == 0) return null;
      T o = (T)rows[0].GetObject(this);
      return o;
    }

    #endregion

    #region LoadByReference

    public virtual BasicCollection<T> LoadByReference(string referenceColumn, int? reference)
    {
      return LoadByReference(referenceColumn, reference, null, null);
    }

    public virtual BasicCollection<T> LoadByReference(string referenceColumn, int? reference, string filterKey)
    {
      return LoadByReference(referenceColumn, reference, filterKey, null);
    }

    public virtual BasicCollection<T> LoadByReference(string referenceColumn, int? reference, Collection<DataFilterCondition> conditions)
    {
      return LoadByReference(referenceColumn, reference, null, conditions);
    }

    public virtual BasicCollection<T> LoadByReference(string referenceColumn, int? reference, string filterKey, Collection<DataFilterCondition> conditions)
    {
      BasicCollection<T> collection = new BasicCollection<T>();

      DataView dv = new DataView(filterKey);
      FillDataViewByReference(dv, referenceColumn, reference, conditions);

      foreach (DataRow dr in dv.GetRowsByType(typeof(T)))
      {
        T o = (T)dr.GetObject(this);
        collection.Add(o);
      }

      return collection;
    }

    #endregion

    #region LoadByReferences

    public virtual BasicCollection<T> LoadByReferences(string referenceColumn, Collection<int> references)
    {
      return LoadByReferences(referenceColumn, references, null, null);
    }

    public virtual BasicCollection<T> LoadByReferences(string referenceColumn, Collection<int> references, string filterKey)
    {
      return LoadByReferences(referenceColumn, references, filterKey, null);
    }

    public virtual BasicCollection<T> LoadByReferences(string referenceColumn, Collection<int> references, Collection<DataFilterCondition> conditions)
    {
      return LoadByReferences(referenceColumn, references, null, conditions);
    }

    public virtual BasicCollection<T> LoadByReferences(string referenceColumn, Collection<int> references, string filterKey, Collection<DataFilterCondition> conditions)
    {
      BasicCollection<T> collection = new BasicCollection<T>();

      DataView dv = new DataView(filterKey);
      FillDataViewByReferences(dv, referenceColumn, references, conditions);

      foreach (DataRow dr in dv.GetRowsByType(typeof(T)))
      {
        T o = (T)dr.GetObject(this);
        collection.Add(o);
      }

      return collection;
    }

    #endregion

    #endregion

    #region Persistance

    public void Save(BusinessObject obj)
    {
      Stack<BusinessObject> savedStack = new Stack<BusinessObject>();
      Save(obj, savedStack, true);
      ProcessAfterSavedEvents(savedStack);
    }

    public virtual void Save(ICollection col)
    {
      if (col == null) throw new ArgumentNullException("col");
      Stack<BusinessObject> savedStack = new Stack<BusinessObject>();
      foreach (BusinessObject obj in col)
      {
        Save(obj, savedStack, true);
      }
      ProcessAfterSavedEvents(savedStack);
    }

    void ProcessAfterSavedEvents(Stack<BusinessObject> savedStack)
    {
      BusinessObject bo = null;
      while (savedStack.Count > 0)
      {
        bo = savedStack.Pop();
        foreach (var controller in DataManager.Instance.GetDataEventControllers(bo.GetType()).Reverse())
        {
          controller.OnAfterSave((T)bo);
        }
      }
    }

    public virtual void Save(BusinessObject obj, Stack<BusinessObject> savedStack, bool searchUp)
    {
      if (obj == null) throw new ArgumentNullException("obj");
      if (obj.Id == 0) return;

      #region Before Save/Delete Events

      if (obj.IsDeleted)
      {
        foreach (var controller in DataManager.Instance.GetDataEventControllers<T>())
        {
          if (!controller.OnBeforeDelete((T)obj))
          {
            obj.IsDeleted = false;
            break;
          }
        }
      }
      
      if (!obj.IsDeleted)
      {
        if (obj.IsNew)
        {
          foreach (var controller in DataManager.Instance.GetDataEventControllers<T>())
          {
            if (!controller.OnBeforeSave((T)obj)) return;
          }
        }
        else
        {
          if (obj.IsDirty)
          {
            foreach (var controller in DataManager.Instance.GetDataEventControllers<T>())
            {
              if (!controller.OnBeforeSave((T)obj)) return;
            }
          }
        }
      }

      #endregion

      if (searchUp && BaseMapper != null) BaseMapper.Save(obj);
      if (obj.Id == 0) return;

      foreach (PropertyInfo pi in ReferencePropertyInfoCollection.Where(prop => IsWriteColumn(prop) && !IsReferenceColumn(prop)))
      {
        BusinessObject bo = pi.GetValue(obj) as BusinessObject;
        if (bo.IsDirty)
        {
          IORMapper referenceMapper = DataManager.Instance.GetObjectMapper(pi.PropertyType);
          referenceMapper.Save(bo, savedStack, true);
          if (IsReferenceColumn(pi)) obj.SetReferenceValue(bo.Id, pi.Name);
        }
      }

      if (obj.IsDeleted)
      {
        if (!obj.IsNew)
        {
          Delete(obj);
          FlagIsCached(this);
        }
        obj.Id = 0;
        return;
      }

      if (obj.IsNew)
      {
        Insert(obj);
        savedStack.Push(obj);
        FlagIsCached(this);
      }
      else
      {
        if (obj.IsDirty)
        {
          Update(obj);
          savedStack.Push(obj);
          FlagIsCached(this);
        }
      }

      foreach (IORMapper mapper in SubClassMappers)
      {
        mapper.Save(obj, savedStack, false);
      }

      #region Children etc.

      if (RowMapper != null && ColumnMapper != null && CellCollection != null)
      {
        ICollection collection = CellCollection.GetValue(obj) as ICollection;
        foreach (ITableCell bo in collection)
        {
          RowMapper.Save(bo.BusinessObjectRow, savedStack, true);
          ColumnMapper.Save(bo.BusinessObjectColumn, savedStack, true);
        }
      }

      foreach (PropertyInfo pi in OwnedCollection)
      {
        if (OwnedMapperDictionary.ContainsKey(pi))
        {
          ICollection collection1 = pi.GetValue(obj) as ICollection;
          IORMapper childMapper = OwnedMapperDictionary[pi];
          foreach (BusinessObject bo in collection1)
          {
            childMapper.Save(bo, savedStack, true);
          }
        }
      }

      foreach(PropertyInfo pi in AssociativeCollection)
      {
        IORMapper childMapper = AssociativeExtensionObjectMapperDictionary[pi];
        ICollection list = (ICollection)pi.GetValue(obj);
        IORMapper referenceMapper = null;
        if (AssociativeExtensionObjectReferenceMapperDictionary.ContainsKey(pi)) referenceMapper = AssociativeExtensionObjectReferenceMapperDictionary[pi];
        foreach (object o in list)
        {
          int id = 0;
          BusinessObject bo = o as BusinessObject;
          if (bo == null)
          {
            id = (int)o;
          }
          else
          {
            id = bo.Id;
          }
          IAssociativeObject ao = obj.GetAssociativeObject(childMapper.ObjectType, id);
          BusinessObject bo1 = ao as BusinessObject;
          if (referenceMapper != null && ao.BusinessObjectReference.IsOwned)
          {
            referenceMapper.Save(ao.BusinessObjectReference, savedStack, true);
          }
          childMapper.Save(bo1, savedStack, true);
        }
      }

      foreach (BusinessObject ao in obj.AssociativeObjectCollection)
      {
        if (ao.IsDirty && ao.IsDeleted)
        {
          DataManager.Instance.GetObjectMapper(ao.GetType()).Save(ao);
        }
      }

      foreach (ExtensionObject eo in obj.GetExtensionObjects())
      {
        if (ExtensionObjectMapperDictionary.ContainsKey(eo.GetType()))
        {
          IORMapper mapper = ExtensionObjectMapperDictionary[eo.GetType()];
          mapper.Save(eo, savedStack, true);
        }
      }

      #endregion

      obj.IsNew = false;
      obj.IsDirty = false;
    }

    void FlagIsCached(IORMapper mapper)
    {
      if (CacheMappers.IsCached(mapper))
      {
        DataforgeCacheExtension.RefreshCache = true;
        return;
      }

      if (mapper.OwnerType != null)
      {
        FlagIsCached(DataManager.Instance.GetObjectMapper(mapper.OwnerType));
      }
    }

    public virtual void Delete(BusinessObject obj)
    {
      throw new NotImplementedException();
    }

    public virtual void Insert(BusinessObject obj)
    {
      throw new NotImplementedException();
    }

    public virtual void Update(BusinessObject obj)
    {
      throw new NotImplementedException();
    }

    #endregion

    #region Searcing

    public virtual string GetSearchReferenceJoin(IORMapper callingMapper)
    {
      string innerJoins = null;
      if (BaseMapper != null && callingMapper != this) innerJoins += BaseMapper.GetSearchParentJoin(callingMapper);
      foreach (IORMapper mapper in SearchJoins)
      {
        innerJoins += mapper.GetSearchReferenceJoin(callingMapper);
      }
      if (callingMapper == this) return innerJoins;
      return string.Format("INNER JOIN {0} ON {0}.{1}={2}.{3} {4} ", FullTableName, EncapsulateName(OwnerColumn), callingMapper.FullTableName, EncapsulateName(callingMapper.IdColumn), innerJoins);
    }

    public virtual string GetSearchParentJoin(IORMapper callingMapper)
    {
      string innerJoins = null;
      if (BaseMapper != null) innerJoins += BaseMapper.GetSearchParentJoin(callingMapper);
      if (callingMapper == this) return innerJoins;
      return string.Format("INNER JOIN {0} ON {0}.{1}={2}.{3} {4} ", FullTableName, EncapsulateName(IdColumn), callingMapper.FullTableName, EncapsulateName(callingMapper.IdColumn), innerJoins);
    }

    public virtual string GetSearchColumns()
    {
      string columns = null;
      if (BaseMapper != null) columns += BaseMapper.GetSearchColumns();
      foreach (PropertyInfo pi in SearchProperties)
      {
        string columnAlias = GetSearchAlias(pi);
        columns += string.Format(string.IsNullOrWhiteSpace(columns) ? "{0}.{1} AS {2}" : ", {0}.{1} AS {2}", FullTableName, EncapsulateName(GetColumnName(pi)), EncapsulateName(columnAlias));
      }
      foreach (IORMapper mapper in SearchJoins)
      {
        string joinedColumns = mapper.GetSearchColumns();
        if (joinedColumns != null) columns += string.Format(string.IsNullOrWhiteSpace(columns) ? "{0}" : ", {0}", joinedColumns);
      }
      return columns;
    }

    public virtual string GetSearchOrderBy()
    {
      string orderBy = null;
      if (BaseMapper != null) orderBy = BaseMapper.GetSearchOrderBy();
      foreach (var pi in PropertyInfoCollection.Where(prop => prop.IsDefined(typeof(ColumnAttribute), false) && GetColumnSortOrder(prop) >= 0).OrderBy(prop => GetColumnSortOrder(prop)))
      {
        orderBy += string.Format(CultureInfo.InvariantCulture, string.IsNullOrWhiteSpace(orderBy) ? "{0}" : "{1} {0}", FormatOrderBy(GetColumnName(pi), GetColumnSortDirection(pi)), OrderBySeparator);
      }
      foreach (IORMapper mapper in SearchJoins)
      {
        string joinedOrderBy = mapper.GetSearchOrderBy();
        if (joinedOrderBy != null) orderBy += string.Format(string.IsNullOrWhiteSpace(orderBy) ? "{0}" : "{1} {0}", joinedOrderBy, OrderBySeparator);
      }
      return orderBy;
    }

    public virtual void GetSearchColumnFormats(Collection<string> formatStrings)
    {
      if (BaseMapper != null) BaseMapper.GetSearchColumnFormats(formatStrings);
      foreach (PropertyInfo pi in SearchProperties)
      {
        formatStrings.Add(GetSearchFormat(pi));
      }
      foreach (IORMapper mapper in SearchJoins)
      {
        mapper.GetSearchColumnFormats(formatStrings);
      }
    }

    public virtual void GetSearchHeaderNames(Collection<string> headers)
    {
      if (BaseMapper != null) BaseMapper.GetSearchHeaderNames(headers);
      foreach (PropertyInfo pi in SearchProperties)
      {
        headers.Add(GetSearchAlias(pi));
      }
      foreach (IORMapper mapper in SearchJoins)
      {
        mapper.GetSearchHeaderNames(headers);
      }
    }

    public abstract string GetSearchConditions(Collection<DataFilterCondition> filter, ref int iParam, IDbCommand cmd);

    public abstract System.Data.DataSet GetSearchDataSet(Collection<DataFilterCondition> filter);

    public virtual SearchResults Search(Collection<DataFilterCondition> filter)
    {
      System.Data.DataSet ds = GetSearchDataSet(filter);

      Collection<string> formatStrings = new Collection<string>();
      GetSearchColumnFormats(formatStrings);
      BasicCollection<string> headers = new BasicCollection<string>();
      GetSearchHeaderNames(headers);

      SearchResults results = new SearchResults(headers);
      foreach (System.Data.DataRow dr in ds.Tables[0].Rows)
      {
        results.AddResult(new SearchResult(dr, formatStrings));
      }

      return results;
    }

    #endregion
  }
}
