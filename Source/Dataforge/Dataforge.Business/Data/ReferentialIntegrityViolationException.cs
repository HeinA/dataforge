﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Data
{
  [Serializable]
  public class ReferentialIntegrityViolationException : Exception
  {
    protected ReferentialIntegrityViolationException(SerializationInfo info, StreamingContext context)
      :base(info, context)
    {
    }

    public ReferentialIntegrityViolationException(string message)
      : base(message)
    {
    }

    public ReferentialIntegrityViolationException(string message, Exception innerException)
      : base(message, innerException)
    {
    }
  }
}
