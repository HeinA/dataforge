﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Collections;
using Dataforge.Business.Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Data
{
  internal static class CacheMappers
  {
    static Dictionary<Type, IORMapper> _mappers;
    static Dictionary<Type, IORMapper> Mappers
    {
      get
      {
        if (_mappers == null)
        {
          _mappers = new Dictionary<Type, IORMapper>();
          Initialize();
        }
        return _mappers;
      }
    }

    static object _lock;
    static object Lock
    {
      get
      {
        if (_lock == null) _lock = new object();
        return _lock;
      }
    }

    static void Initialize()
    {
      foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
      {
        foreach (Type t in a.GetTypes().Where(t => t.IsDefined(typeof(CacheAttribute))))
        {
          Mappers.Add(t, DataManager.Instance.GetObjectMapper(t));
        }
      }
    }

    public static bool IsCached(IORMapper mapper)
    {
      return Mappers.ContainsValue(mapper);
    }

    //public static void AddMapper(Type t, IORMapper mapper)
    //{
    //  lock (Lock)
    //  {
    //    Mappers.Add(t, mapper);
    //  }
    //}

    public static IORMapper GetMapper(Type t)
    {
      lock (Lock)
      {
        return Mappers[t];
      }
    }

    public static IEnumerable<Type> MapperTypes
    {
      get
      {
        lock (Lock)
        {
          Collection<Type> col = new Collection<Type>();
          foreach (Type cacheType in Mappers.Keys)
          {
            CacheAttribute ca = cacheType.GetCustomAttribute<CacheAttribute>();
            if (ca.Priority >= 0)
            {
              col.Insert(ca.Priority > col.Count ? col.Count : ca.Priority, cacheType);
            }
            else
            {
              col.Add(cacheType);
            }
          }
          return col;
        }
      }
    }


    public static object Collection { get; set; }
  }
}
