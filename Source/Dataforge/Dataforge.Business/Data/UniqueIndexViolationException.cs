﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Data
{
  [Serializable]
  public class UniqueIndexViolationException : Exception
  {
    protected UniqueIndexViolationException(SerializationInfo info, StreamingContext context)
      :base(info, context)
    {
    }

    public UniqueIndexViolationException(string message)
      : base(message)
    {
    }

    public UniqueIndexViolationException(string message, Exception innerException)
      : base(message, innerException)
    {
    }
  }
}
