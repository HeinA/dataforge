﻿using Dataforge.Business.Collections;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Data
{
  public interface IObjectStore<T>
    where T : BusinessObject
  {
    #region Object Loaders

    BasicCollection<T> Load();
    BasicCollection<T> Load(string filterKey);
    BasicCollection<T> Load(Collection<DataFilterCondition> conditions);
    BasicCollection<T> Load(string filterKey, Collection<DataFilterCondition> conditions);

    T LoadById(int id);
    T LoadById(int id, string filterKey);
    T LoadById(int id, Collection<DataFilterCondition> conditions);
    T LoadById(int id, string filterKey, Collection<DataFilterCondition> conditions);

    BasicCollection<T> LoadByReference(string referenceColumn, int? reference);
    BasicCollection<T> LoadByReference(string referenceColumn, int? reference, string filterKey);
    BasicCollection<T> LoadByReference(string referenceColumn, int? reference, Collection<DataFilterCondition> conditions);
    BasicCollection<T> LoadByReference(string referenceColumn, int? reference, string filterKey, Collection<DataFilterCondition> conditions);

    BasicCollection<T> LoadByReferences(string referenceColumn, Collection<int> references);
    BasicCollection<T> LoadByReferences(string referenceColumn, Collection<int> references, string filterKey);
    BasicCollection<T> LoadByReferences(string referenceColumn, Collection<int> references, Collection<DataFilterCondition> conditions);
    BasicCollection<T> LoadByReferences(string referenceColumn, Collection<int> references, string filterKey, Collection<DataFilterCondition> conditions);

    #endregion

    #region Persistance

    void Save(BusinessObject obj);
    void Save(ICollection col);

    #endregion

    #region Searching

    SearchResults Search(Collection<DataFilterCondition> filter);

    #endregion
  }
}
