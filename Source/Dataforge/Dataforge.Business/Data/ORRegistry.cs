﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Data
{
  public static class ORRegistry
  {
    static Dictionary<Type, object> _ORMappers;
    static Dictionary<Type, object> ORMappers
    {
      get
      {
        if (_ORMappers == null) _ORMappers = new Dictionary<Type, object>();
        return _ORMappers;
      }
    }

    public static void RegisterORMapper<T>(IORMapper<T> mapper)
      where T : BusinessObject
    {
      ORMappers.Add(typeof(T), mapper);
    }

    public static IORMapper<T> GetORMapper<T>()
      where T : BusinessObject
    {
      return (IORMapper<T>)ORMappers[typeof(T)];
    }

    public static event EventHandler<FilterEventArgs> Filter;
    internal static void OnFilter(object source, FilterEventArgs e)
    {
      if (Filter != null)
      {
        Filter(source, e);
      }
    }
  }
}
