﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Data
{
  public class InsertFailedException : Exception
  {
    protected InsertFailedException(SerializationInfo info, StreamingContext context)
      :base(info, context)
    {
    }

    public InsertFailedException(string message)
      : base(message)
    {
    }

    public InsertFailedException(string message, Exception innerException)
      : base(message, innerException)
    {
    }
  }
}
