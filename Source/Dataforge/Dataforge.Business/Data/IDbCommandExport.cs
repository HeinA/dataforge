﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Data
{
  public interface IDbCommandExport
  {
    IDbCommand DbCommand { get; }
  }
}
