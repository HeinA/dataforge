﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Data
{
  public abstract class DataFilter<T>
    where T : BusinessObject
  {
    public abstract void GetConditions(Collection<DataFilterCondition> conditions);
  }
}
