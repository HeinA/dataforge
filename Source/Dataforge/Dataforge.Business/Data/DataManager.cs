﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Data;
using Dataforge.Business.Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Composition.Hosting;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Dataforge.Business.Data
{
  public class DataManager
  {
    public DataManager()
    {
    }

    #region DataManager Instance

    static DataManager _instance;
    public static DataManager Instance
    {
      get
      {
        if (_instance == null) { _instance = new DataManager(); } 
        return _instance;
      }
    }

    #endregion

    #region IObjectStore<T> GetObjectStore<T>()

    public IObjectStore<T> GetObjectStore<T>()
      where T : BusinessObject
    {
      return (IObjectStore<T>)MefContainer.Instance.CompositionHost.GetExport<ORMapper<T>>();
    }

    #endregion

    #region GetDataFilterConditions

    public Collection<DataFilterCondition> GetDataFilterConditions<T>(string key)
      where T : BusinessObject
    {
      return GetDataFilterConditions<T>(key, new Collection<DataFilterCondition>());
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter")]
    public Collection<DataFilterCondition> GetDataFilterConditions<T>(string key, Collection<DataFilterCondition> conditions)
      where T : BusinessObject
    {
      if (conditions == null) conditions = new Collection<DataFilterCondition>();
      foreach (var filter in MefContainer.Instance.CompositionHost.GetExports<DataFilter<T>>(key))
      {
        filter.GetConditions(conditions);
      }
      return conditions;
    }

    #endregion

    #region IDbCommand GetCommand()

    public IDbCommand GetCommand()
    {
      return MefContainer.Instance.CompositionHost.GetExport<IDbCommand>();
    }

    #endregion


    #region Internal

    #region IORMapper GetObjectMapper(Type type)

    MethodInfo _miGetORMapper = null;
    internal IORMapper GetObjectMapper(Type type)
    {
      if (_miGetORMapper == null) _miGetORMapper = DataManager.Instance.GetType().GetMethod("GetObjectStore", new Type[] { });
      MethodInfo mi = _miGetORMapper.MakeGenericMethod(type);
      return (IORMapper)mi.Invoke(DataManager.Instance, new object[] { });
    }

    #endregion

    #region IEnumerable<IStorageEventController<T>> GetDataEventControllers<T>()

    internal IEnumerable<StorageEventController<T>> GetDataEventControllers<T>()
      where T : BusinessObject
    {
      return MefContainer.Instance.CompositionHost.GetExports<StorageEventController<T>>();
    }

    #endregion

    #region IEnumerable<IStorageEventController> GetDataEventControllers(Type type)

    MethodInfo _miGetDataEventControllers = null;
    internal IEnumerable<IStorageEventController> GetDataEventControllers(Type type)
    {
      if (_miGetDataEventControllers == null) _miGetDataEventControllers = DataManager.Instance.GetType().GetMethod("GetDataEventControllers", BindingFlags.NonPublic | BindingFlags.Instance, null, new Type[] { }, null);
      MethodInfo mi = _miGetDataEventControllers.MakeGenericMethod(type);
      return (IEnumerable<IStorageEventController>)mi.Invoke(DataManager.Instance, new object[] { });
    }

    #endregion

    #endregion
  }
}
