﻿using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Data
{
  [DataContract(IsReference = true, Namespace = Namespaces.Dataforge)]
  public class DataFilterCondition
  {
    public DataFilterCondition(Type applicableType, string column, FilterType filterType, object filter)
    {
      _applicableType = applicableType;
      _typeName = string.Format("{0}, {1}", _applicableType.FullName, _applicableType.Assembly.GetName().Name);
      _column = column;
      _filterType = filterType;
      _filter = filter;
    }

    public DataFilterCondition(Type applicableType, string column, FilterType filterType, object filter, bool removeAfterFirstExecution)
    {
      _applicableType = applicableType;
      _typeName = string.Format("{0}, {1}", _applicableType.FullName, _applicableType.Assembly.GetName().Name);
      _column = column;
      _filterType = filterType;
      _filter = filter;
      _removeAfterFirstExecution = removeAfterFirstExecution;
    }

    #region DataMember bool RemoveAfterFirstExecution

    public const string PN_REMOVEAFTERFIRSTEXECUTION = "RemoveAfterFirstExecution";
    [DataMember(Name = PN_REMOVEAFTERFIRSTEXECUTION, EmitDefaultValue = false)]
    bool _removeAfterFirstExecution;
    public bool RemoveAfterFirstExecution
    {
      get { return _removeAfterFirstExecution; }
    }

    #endregion

    [DataMember(Name = "ApplicableType", EmitDefaultValue = false)]
    string _typeName;

    Type _applicableType;
    public Type ApplicableType
    {
      get
      {
        if (_applicableType == null)
        {
          _applicableType = Type.GetType(_typeName);
        }
        return _applicableType;
      }
    }

    [DataMember(Name = "Column", EmitDefaultValue = false)]
    string _column;
    public string Column
    {
      get { return _column; }
    }

    [DataMember(Name = "Filter", EmitDefaultValue = false)]
    object _filter;
    public object Filter
    {
      get { return _filter; }
    }

    [DataMember(Name = "FilterType", EmitDefaultValue = false)]
    FilterType _filterType;
    public FilterType FilterType
    {
      get { return _filterType; }
    }
  }
}
