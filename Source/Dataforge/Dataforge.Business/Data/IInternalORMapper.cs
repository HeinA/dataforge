﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Data
{
  interface IInternalORMapper
  {
    string OwnerColumn { get; }

    void FillDataViewByReferencesInternal(DataView dv, string referenceColumn, Collection<int> references, Collection<DataFilterCondition> conditions);
  }
}
