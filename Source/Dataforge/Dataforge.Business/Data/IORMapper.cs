﻿using Dataforge.Business.Collections;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Data
{
  public interface IORMapper
  {
    #region Sql Formatting

    string GetReadColumns();

    string GetJoin();

    string GetOrderByColumns();

    string GetSearchColumns();
    string GetSearchOrderBy();
    void GetSearchHeaderNames(Collection<string> headers);
    void GetSearchColumnFormats(Collection<string> formatStrings);
    string GetSearchParentJoin(IORMapper callingMapper);
    string GetSearchReferenceJoin(IORMapper callingMapper);
    string GetSearchConditions(Collection<DataFilterCondition> filter, ref int iParam, System.Data.IDbCommand cmd);

    SearchResults Search(Collection<DataFilterCondition> filter);

    #endregion

    #region DataView Fillers

    void FillDataView(DataView dv, Collection<DataFilterCondition> conditions);
    void FillDataViewById(DataView dv, int id, Collection<DataFilterCondition> conditions);
    void FillDataViewByReference(DataView dv, string referenceColumn, int? reference, Collection<DataFilterCondition> conditions);
    void FillDataViewByReferences(DataView dv, string referenceColumn, Collection<int> references, Collection<DataFilterCondition> conditions);

    #endregion

    #region Properties

    Type ObjectType { get; }
    string TableName { get; }
    string TableSchema { get; }
    string FullTableName { get; }
    string IdColumn { get; }
    string OwnerColumn { get; }
    string ReferenceColumn { get; }
    bool FlattenBase { get; }
    bool FlattenToSubChild { get; }
    Type OwnerType { get; }

    string RowColumn { get; }
    string ColumnColumn { get; }
    bool IsGlobalIdentifiable { get; }
    string GlobalIdentifierColumn { get; }

    #endregion

    #region Load Object

    ICollection LoadCache(DataView dv);
    void LoadObject(BusinessObject obj, DataView view, DataRow row);
    void LoadObject(BusinessObject obj, DataView view, DataRow row, bool loadInherited);

    #endregion

    #region Persistance

    void Save(BusinessObject obj);
    void Save(BusinessObject obj, Stack<BusinessObject> savedQueue, bool searchUp);
    void Save(ICollection col);

    void Insert(BusinessObject obj);
    void Update(BusinessObject obj);
    void Delete(BusinessObject obj);

    #endregion
  }
}
