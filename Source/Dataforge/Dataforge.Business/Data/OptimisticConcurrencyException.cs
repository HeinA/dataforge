﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Data
{
  [Serializable]
  public class OptimisticConcurrencyException : Exception
  {
    protected OptimisticConcurrencyException(SerializationInfo info, StreamingContext context)
      :base(info, context)
    {
    }

    public OptimisticConcurrencyException(string message)
      : base(message)
    {
    }

    public OptimisticConcurrencyException(string message, Exception innerException)
      : base(message, innerException)
    {
    }
  }
}
