﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Data
{
  public class DataContainer
  {
    static UnityContainer _instance;

    public static UnityContainer Instance
    {
      get
      {
        if (_instance == null) _instance = new UnityContainer();
        return _instance;
      }
    }
  }
}
