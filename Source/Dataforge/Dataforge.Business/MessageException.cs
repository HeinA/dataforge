﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business
{
  [Serializable]
  public class MessageException : Exception
  {
    protected MessageException(SerializationInfo info, StreamingContext context)
      :base(info, context)
    {
    }

    public MessageException(string message)
      : base(message)
    {
    }

    public MessageException(string message, Exception innerException)
      : base(message, innerException)
    {
    }  
  }
}
