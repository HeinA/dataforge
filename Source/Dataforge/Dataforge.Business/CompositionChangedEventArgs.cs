﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business
{
  public enum CompositionChangedType
  {
    PropertyChanged,
    ChildAdded,
    ChildRemoved
  }

  public class CompositionChangedEventArgs : EventArgs
  {
    public CompositionChangedEventArgs(CompositionChangedType compositionChangedType, BusinessObject item)
      : base()
    {
      _compositionChangedType = compositionChangedType;
      _item = item;
    }

    CompositionChangedType _compositionChangedType;
    public CompositionChangedType CompositionChangedType
    {
      get { return _compositionChangedType; }
    }

    BusinessObject _item;
    public BusinessObject Item
    {
      get { return _item; }
    }
  }
}
