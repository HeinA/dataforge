﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business
{
  [Table(FlattenToSubClass = true)]
  [DataContract(IsReference = true, Namespace = Namespaces.Dataforge)]
  public abstract class AssociativeObject<T> : BusinessObject, IAssociativeObject
    where T : BusinessObject
  {
    #region DataMember T Reference

    public const string PN_REFERENCE = "Reference";
    protected T m_reference;
    [DataMember(Name = PN_REFERENCE, EmitDefaultValue = false)]
    public virtual T Reference
    {
      get
      {
        if (IsSerializing) return null;
        return m_reference ?? (m_reference = GetCachedObject<T>(PN_REFERENCE));
      }
      internal set
      {
        if (SetValue<T>(ref m_reference, value, PN_REFERENCE))
        {
          SetCachedObject<T>(m_reference, PN_REFERENCE);
        }
      }
    }

    #endregion

    #region IAssociativeObject

    BusinessObject IAssociativeObject.BusinessObjectReference
    {
      get { return (BusinessObject)Reference; }
      set { Reference = (T)value; }
    }

    int IAssociativeObject.ReferenceId
    {
      get { return GetReferenceValue(PN_REFERENCE); }
      set { SetReferenceValue(value, PN_REFERENCE); }
    }

    #endregion
  }
}
