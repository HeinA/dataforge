﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Security
{
  [DataContract(IsReference = true, Namespace = Namespaces.Dataforge)]
  [Table(Schema = "Dataforge", OwnerColumn = "User", OwnerType = typeof(User), ReferenceColumn = "OrganizationalUnit")]
  public class UserOrganizationalUnit : AssociativeObject<OrganizationalUnit>
  {
  }
}
