﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dataforge.Business.Security
{
  [DataContract(IsReference = true, Namespace = Namespaces.Dataforge)]
  [Table(Schema = "Dataforge")]
  public class User : IdentifiableObject
  {
    #region DataMember int ApplicationDomain

    public const string PN_APPLICATIONDOMAIN = "ApplicationDomain";
    [DataMember(Name = PN_APPLICATIONDOMAIN, EmitDefaultValue = false)]
    int _applicationDomain;
    [Column]
    public int ApplicationDomain
    {
      get { return _applicationDomain == 0 ? SecurityContext.User.ApplicationDomain : _applicationDomain; }
      private set { SetValue<int>(ref _applicationDomain, value, PN_APPLICATIONDOMAIN); }
    }

    #endregion

    #region DataMember string Username

    public const string PN_USERNAME = "Username";
    [DataMember(Name = PN_USERNAME, EmitDefaultValue = false)]
    string _username;
    [Search]
    [Column]
    public string Username
    {
      get { return _username; }
      set { SetValue<string>(ref _username, value, PN_USERNAME); }
    }

    #endregion

    #region DataMember string PasswordHash

    public const string PN_PASSWORDHASH = "PasswordHash";
    [DataMember(Name = PN_PASSWORDHASH, EmitDefaultValue = false)]
    string _passwordHash;
    public string PasswordHash
    {
      get { return _passwordHash; }
      set { SetValue<string>(ref _passwordHash, value, PN_PASSWORDHASH); }
    }

    #endregion

    #region DataMember string SaltedPasswordHash

    public const string PN_SALTEDPASSWORDHASH = "SaltedPasswordHash";
    string _saltedPasswordHash;
    [Column(IgnoreNull = true)]
    public string SaltedPasswordHash
    {
      get { return _saltedPasswordHash; }
      set { SetValue<string>(ref _saltedPasswordHash, value, PN_SALTEDPASSWORDHASH); }
    }

    #endregion

    #region DataMember string Firstnames

    public const string PN_FIRSTNAMES = "Firstnames";
    [DataMember(Name = PN_FIRSTNAMES, EmitDefaultValue = false)]
    string _firstnames;
    [Column]
    public string Firstnames
    {
      get { return _firstnames; }
      set { SetValue<string>(ref _firstnames, value, PN_FIRSTNAMES); }
    }

    #endregion

    #region DataMember string Lastname

    public const string PN_LASTNAME = "Lastname";
    [DataMember(Name = PN_LASTNAME, EmitDefaultValue = false)]
    string _lastname;
    [Column]
    public string Lastname
    {
      get { return _lastname; }
      set { SetValue<string>(ref _lastname, value, PN_LASTNAME); }
    }

    #endregion

    #region DataMember AssociativeObjectCollection<UserRole, Role> Roles

    AssociativeObjectCollection<UserRole, Role> _roles;
    public AssociativeObjectCollection<UserRole, Role> Roles
    {
      get
      {
        if (_roles == null) using (new EventStateSuppressor(this)) { _roles = new AssociativeObjectCollection<UserRole, Role>(this); }
        return _roles;
      }
    }

    #endregion

    #region DataMember AssociativeObjectCollection<UserOrganizationalUnit, OrganizationalUnit> OrganizationalUnits

    AssociativeObjectCollection<UserOrganizationalUnit, OrganizationalUnit> _organizationalUnits;
    public AssociativeObjectCollection<UserOrganizationalUnit, OrganizationalUnit> OrganizationalUnits
    {
      get 
      {
        if (_organizationalUnits == null) using (new EventStateSuppressor(this)) { _organizationalUnits = new AssociativeObjectCollection<UserOrganizationalUnit, OrganizationalUnit>(this); }
        return _organizationalUnits;
      }
    }

    #endregion

    protected override void GetErrors(System.Collections.ObjectModel.Collection<string> errors, string propertyName)
    {
      if (string.IsNullOrWhiteSpace(propertyName) || propertyName == "Username")
      {
        if (string.IsNullOrWhiteSpace(Username)) errors.Add("User name is a mandatory field.");
      }

      if (string.IsNullOrWhiteSpace(propertyName) || propertyName == "PasswordHash")
      {
        if (PasswordHash == null && IsNew) errors.Add("Password is not set.");
      }

      base.GetErrors(errors, propertyName);
    }
  }
}
