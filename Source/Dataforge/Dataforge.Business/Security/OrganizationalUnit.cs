﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Dataforge.Business.Security
{
  [DataContract(IsReference = true, Namespace = Namespaces.Dataforge)]
  [Table(Schema = "Dataforge", OwnerColumn = "Parent", OwnerType = typeof(OrganizationalUnit))] //, IsCircularReference = true)]
  [Cache]
  public class OrganizationalUnit : IdentifiableObject
  {
    public OrganizationalUnit Parent
    {
      get { return (OrganizationalUnit)Owner; }
    }

    #region DataMember string Name

    public const string PN_NAME = "Name";
    [DataMember(Name = PN_NAME, EmitDefaultValue = false)]
    string _name;
    [Column]
    public string Name
    {
      get { return _name; }
      set { SetValue<string>(ref _name, value, PN_NAME); }
    }

    #endregion

    #region DataMember bool UserAssignable

    public const string PN_USERASSIGNABLE = "UserAssignable";
    [DataMember(Name = PN_USERASSIGNABLE, EmitDefaultValue = false)]
    bool _userAssignable;
    [Column]
    public bool UserAssignable
    {
      get { return _userAssignable; }
      set { SetValue<bool>(ref _userAssignable, value, PN_USERASSIGNABLE); }
    }

    #endregion

    #region Reference Member OrganizationalStructureType StructureType

    public const string PN_STRUCTURETYPE = "StructureType";
    [Column(IsReference = true)]
    public OrganizationalStructureType StructureType
    {
      get { return Cache.Instance.GetObject<OrganizationalStructureType>(GetReferenceValue(PN_STRUCTURETYPE)); }
      set { SetReferenceValue(value.Id, PN_STRUCTURETYPE); }
    }

    #endregion

    #region DataMember BusinessObjectCollection<OrganizationalUnit> SubUnits

    [DataMember(Name = "SubUnits")]
    BusinessObjectCollection<OrganizationalUnit> _subUnits;
    [Cache]
    public BusinessObjectCollection<OrganizationalUnit> SubUnits
    {
      get
      {
        if (_subUnits == null) _subUnits = new BusinessObjectCollection<OrganizationalUnit>(this);
        return _subUnits;
      }
    }

    #endregion

    #region void GetErrors(Collection<string> errors, string propertyName)

    protected override void GetErrors(Collection<string> errors, string propertyName)
    {
      if (propertyName == null || propertyName == PN_NAME)
      {
        if (string.IsNullOrWhiteSpace(Name)) errors.Add("Name is a mandatory field.");
      }

      base.GetErrors(errors, propertyName);
    }

    #endregion
  }
}