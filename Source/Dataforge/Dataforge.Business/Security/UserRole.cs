﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dataforge.Business.Security
{
  [DataContract(IsReference = true, Namespace = Namespaces.Dataforge)]
  [Table(Schema = "Dataforge", OwnerColumn = "User", OwnerType = typeof(User), ReferenceColumn = "Role")]
  public class UserRole : AssociativeObject<Role>
  {
  }
}