﻿using Dataforge.Business.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Security
{
  [Table(Schema = "Dataforge")]
  class UserSalt : BusinessObject
  {
    #region string Salt

    public const string PN_SALT = "Salt";
    string _salt;
    [Column]
    public string Salt
    {
      get { return _salt; }
      set { SetValue<string>(ref _salt, value, PN_SALT); }
    }

    #endregion
  }
}
