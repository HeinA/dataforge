﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Security
{
  [Serializable]
  public class AuthorizationException : Exception
  {
    protected AuthorizationException(SerializationInfo info, StreamingContext context)
      :base(info, context)
    {
    }

    public AuthorizationException()
      : base()
    {
    }

    public AuthorizationException(string message)
      : base(message)
    {
    }

    public AuthorizationException(string message, Exception innerException)
      : base(message, innerException)
    {
    }  
  }
}
