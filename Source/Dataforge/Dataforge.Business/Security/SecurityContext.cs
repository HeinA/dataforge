﻿using Dataforge.Business.Constants;
using Dataforge.Business.Service.Wcf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Security
{
  public class SecurityContext
  {
    static object _lock;
    static object Lock
    {
      get
      {
        if (_lock == null) _lock = new object();
        return _lock;
      }
    }

    public static void AddAuthentication(AuthenticationResult authentication)
    {
      if (authentication.SessionToken == Guid.Empty) throw new InvalidOperationException("Session Token is not set.");
      if (authentication.User == null) throw new InvalidOperationException("User is not set.");

      lock (Lock)
      {
        if (OperationContext.Current == null) _sessionToken = authentication.SessionToken;
        if (UserDictionary.ContainsKey(authentication.SessionToken)) UserDictionary.Remove(authentication.SessionToken);
        UserDictionary.Add(authentication.SessionToken, authentication.User);
      }
    }

    public static void SetImpersonationToken(AuthenticationResult authentication)
    {
      if (authentication.SessionToken == Guid.Empty) throw new InvalidOperationException("Session Token is not set.");
      if (authentication.User == null) throw new InvalidOperationException("User is not set.");

      lock (Lock)
      {
        if (OperationContext.Current == null) _impersonationToken = authentication.SessionToken;
        if (UserDictionary.ContainsKey(authentication.SessionToken)) UserDictionary.Remove(authentication.SessionToken);
        UserDictionary.Add(authentication.SessionToken, authentication.User);
      }
    }

    public static void RemoveAuthentication(Guid sessionToken)
    {
      lock (Lock)
      {
        if (OperationContext.Current == null) _sessionToken = Guid.Empty;
        if (UserDictionary.ContainsKey(sessionToken)) UserDictionary.Remove(sessionToken);
      }
    }

    public static void RemoveImpersonation(Guid sessionToken)
    {
      lock (Lock)
      {
        if (OperationContext.Current == null) _impersonationToken = Guid.Empty;
        if (UserDictionary.ContainsKey(sessionToken)) UserDictionary.Remove(sessionToken);
      }
    }

    public static User User
    {
      get
      {
        lock (Lock)
        {
          if (!UserDictionary.ContainsKey(SessionToken)) return null;
          return UserDictionary[SessionToken];
        }
      }
    }

    static Guid _sessionToken = Guid.Empty;
    static Guid _impersonationToken = Guid.Empty;
    public static Guid SessionToken
    {
      get
      {
        if (OperationContext.Current != null)
        {
          return OperationContext.Current.IncomingMessageHeaders.GetHeader<Guid>(DataforgeClientMessageInspector.SessionToken, Namespaces.Dataforge);
        }
        if (ImpersonationToken != Guid.Empty) return ImpersonationToken;
        return _sessionToken;
      }
    }

    public static Guid ImpersonationToken
    {
      get
      {
        return _impersonationToken;
      }
    }

    static Dictionary<Guid, User> _userDictionary;
    static Dictionary<Guid, User> UserDictionary
    {
      get
      {
        if (_userDictionary == null) _userDictionary = new Dictionary<Guid, User>();
        return _userDictionary;
      }
    }
  }
}
