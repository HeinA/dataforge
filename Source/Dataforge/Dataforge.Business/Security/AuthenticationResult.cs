﻿using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Security
{
  [DataContract(Namespace = Namespaces.Dataforge, IsReference = true)]
  public class AuthenticationResult
  {
    public AuthenticationResult(Guid sessionToken, User user)
    {
      _sessionToken = sessionToken;
      _user = user;
    }

    public AuthenticationResult(string error)
    {
      _error = error;
    }

    public const string PN_SESSIONTOKEN = "SessionToken";
    [DataMember(Name = PN_SESSIONTOKEN, EmitDefaultValue = false)]
    Guid _sessionToken = Guid.Empty;
    public Guid SessionToken
    {
      get { return _sessionToken; }
    }

    public const string PN_USER = "User";
    [DataMember(Name = PN_USER, EmitDefaultValue = false)]
    User _user;
    public User User
    {
      get { return _user; }
    }

    public const string PN_ERROR = "Error";
    [DataMember(Name = PN_ERROR, EmitDefaultValue = false)]
    string _error;
    public string Error
    {
      get { return _error; }
    }
  }
}
