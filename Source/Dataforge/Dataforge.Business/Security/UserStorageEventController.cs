﻿using Dataforge.Business.Data;
using System;
using System.Collections.Generic;
using System.Composition;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Security
{
  [Export(typeof(StorageEventController<User>))]
  class UserStorageEventController : StorageEventController<User>
  {
    string CreateUserSalt()
    {
      byte[] saltbytes = new byte[32];
      RNGCryptoServiceProvider p = new RNGCryptoServiceProvider();
      p.GetNonZeroBytes(saltbytes);
      return ASCIIEncoding.ASCII.GetString(saltbytes);
    }

    public override void OnAfterLoad(User obj, string filterKey)
    {
    }

    public override bool OnBeforeSave(User obj)
    {
      return true;
    }

    public override void OnAfterSave(User obj)
    {
      IObjectStore<UserSalt> saltStore = DataManager.Instance.GetObjectStore<UserSalt>();
      UserSalt userSalt = saltStore.LoadById(obj.Id);
      if (userSalt == null && obj.PasswordHash != null)
      {
        userSalt = new UserSalt();
        userSalt.Id = obj.Id;
        userSalt.Salt = CreateUserSalt();
        saltStore.Save(userSalt);

        //IObjectStore<User> userStore = DataManager.Instance.GetObjectStore<User>();
        //obj.SaltedPasswordHash = ASCIIEncoding.ASCII.GetString(algoritm.ComputeHash(ASCIIEncoding.ASCII.GetBytes(string.Format("{0}{1}", userSalt.Salt, obj.PasswordHash))));
        //userStore.Save(obj);
      }
      if (userSalt != null && obj.PasswordHash != null)
      {
        HashAlgorithm algoritm = new SHA256Managed();
        IObjectStore<User> userStore = DataManager.Instance.GetObjectStore<User>();
        obj.SaltedPasswordHash = ASCIIEncoding.ASCII.GetString(algoritm.ComputeHash(ASCIIEncoding.ASCII.GetBytes(string.Format("{0}{1}", userSalt.Salt, obj.PasswordHash))));
        userStore.Save(obj);
      }
      if (userSalt == null && obj.PasswordHash == null) throw new Exception("New user requires a password.");
    }

    public override bool OnBeforeDelete(User obj)
    {
      return true;
    }
  }
}
