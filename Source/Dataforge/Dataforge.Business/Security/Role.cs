﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Constants;
using Dataforge.Business.Data;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Dataforge.Business.Security
{
  [DataContract(IsReference = true, Namespace = Namespaces.Dataforge)]
  [Table(Schema = "Dataforge")]
  [Cache(Priority = 0)]
  public class Role : IdentifiableObject, IGlobalAware
  {
    #region DataMember string Name

    public const string PN_NAME = "Name";
    [DataMember(Name = PN_NAME, EmitDefaultValue = false)]
    string _name;
    [Column]
    public string Name
    {
      get { return _name; }
      set { SetValue<string>(ref _name, value, PN_NAME); }
    }

    #endregion

    #region DataMember bool IsGlobal

    const string PN_ISGLOBAL = "IsGlobal";
    [DataMember(Name = PN_ISGLOBAL, EmitDefaultValue = false)]
    bool _isGlobal;
    public bool IsGlobal
    {
      get { return _isGlobal; }
      set { SetValue<bool>(ref _isGlobal, value, PN_ISGLOBAL); }
    }

    #endregion
  }
}