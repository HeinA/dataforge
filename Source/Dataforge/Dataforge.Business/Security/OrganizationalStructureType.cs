﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace Dataforge.Business.Security
{
  [DataContract(IsReference = true, Namespace = Namespaces.Dataforge)]
  [Table(Schema = "Dataforge", OwnerColumn = "Parent", OwnerType = typeof(OrganizationalStructureType))] //, IsCircularReference = true)]
  [Cache]
  public class OrganizationalStructureType : IdentifiableObject
  {
    #region OrganizationalStructureElement ParentElement

    OrganizationalStructureType Parent
    {
      get { return (OrganizationalStructureType)Owner; }
    }

    #endregion

    #region DataMember string Name

    public const string PN_NAME = "Name";
    [DataMember(Name = PN_NAME, EmitDefaultValue = false)]
    string _name;
    [Column]
    public string Name
    {
      get { return _name; }
      set { SetValue<string>(ref _name, value, PN_NAME); }
    }

    #endregion

    #region DataMember string SystemIdentifier

    public const string PN_SYSTEMIDENTIFIER = "SystemIdentifier";
    [DataMember(Name = PN_SYSTEMIDENTIFIER, EmitDefaultValue = false)]
    string _systemIdentifier;
    [Column]
    public string SystemIdentifier
    {
      get { return _systemIdentifier; }
      set { SetValue<string>(ref _systemIdentifier, value, PN_SYSTEMIDENTIFIER); }
    }

    #endregion

    #region DataMember BusinessObjectCollection<OrganizationalStructureType> SubTypes

    [DataMember(Name = "SubTypes")]
    BusinessObjectCollection<OrganizationalStructureType> _subTypes;
    [Cache]
    public BusinessObjectCollection<OrganizationalStructureType> SubTypes
    {
      get
      {
        if (_subTypes == null) _subTypes = new BusinessObjectCollection<OrganizationalStructureType>(this);
        return _subTypes;
      }
    }

    #endregion

    #region void GetErrors(Collection<string> errors, string propertyName)

    protected override void GetErrors(Collection<string> errors, string propertyName)
    {
      if (propertyName == null || propertyName == PN_NAME)
      {
        if (string.IsNullOrWhiteSpace(Name)) errors.Add("Name is a mandatory field.");
      }

      base.GetErrors(errors, propertyName);
    }

    #endregion
  }
}