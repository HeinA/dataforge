﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Composition.Hosting;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business
{
  class MefContainer
  {
    #region Private

    #region MefContainer Instance

    static MefContainer _instance;
    public static MefContainer Instance
    {
      get
      {
        lock (Lock)
        {
          if (_instance == null)
          {
            _instance = new MefContainer();
            _instance.Initialize();
          }
        }
        return _instance;
      }
    }

    #endregion

    #region void Initialize()

    void Initialize()
    {
      Utilities.PreLoadDeployedAssemblies();

      Collection<Assembly> assemblies = new Collection<Assembly>();

      foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
      {
        assemblies.Add(a);
      }

      ContainerConfiguration config = new ContainerConfiguration()
        .WithAssemblies(assemblies);     
      CompositionHost = config.CreateContainer();
    }

    #endregion

    #region object Lock

    static object _lock;
    static object Lock
    {
      get
      {
        if (_lock == null) _lock = new object();
        return _lock;
      }
    }

    #endregion

    #endregion

    #region Internal

    #region CompositionHost CompositionHost

    CompositionHost _container;
    internal CompositionHost CompositionHost
    {
      get { return _container; }
      private set { _container = value; }
    }

    #endregion

    #region Collection<string> GetKnownNamespaces()

    Collection<string> _namespaces;

    internal string[] GetKnownNamespaces()
    {
      if (_namespaces == null)
      {
        _namespaces = new Collection<string>();
        foreach (NamespaceEventController controller in CompositionHost.GetExports<NamespaceEventController>())
        {
          controller.AppendKnownNamespaces(_namespaces);
        }
      }
      return _namespaces.ToArray();
    }

    #endregion

    #endregion
  }
}
