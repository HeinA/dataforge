﻿using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business
{
  [DataContract(IsReference = true, Namespace = Namespaces.Dataforge)]
  public class SearchResult
  {
    public SearchResult(DataRow dr, Collection<string> formats)
    {
      _values = new BasicCollection<string>();
      for(int i=0; i<dr.Table.Columns.Count-1; i++)
      {
        string format = "{0}";
        if (formats[i] != null) format = string.Format("{{0:{0}}}", formats[i]);
        _values.Add(string.Format(format, dr[i+1]));
      }
      _id = (int)dr[0];
    }

    [DataMember(Name = "Id")]
    int _id;
    public int Id
    {
      get { return _id; }
    }

    [DataMember(Name = "Values")]
    BasicCollection<string> _values;
    public string[] Values
    {
      get { return _values.ToArray(); }
    }

    string this[int i]
    {
      get { return _values[i]; }
    }

    public override string ToString()
    {
      string s = string.Format("[{0}] ", Id);
      foreach (string v in Values)
      {
        s += string.Format(", \"{0}\"", v);
      }
      return s;
    }
  }
}
