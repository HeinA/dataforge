﻿using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business
{
  [ConfigurationElementType(typeof(CustomHandlerData))]
  public class TestHandler : IExceptionHandler
  {
    public Exception HandleException(Exception exception, Guid handlingInstanceId)
    {
      throw new NotImplementedException();
    }
  }
}
