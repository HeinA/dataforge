﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business
{
  [Table(FlattenToSubClass = true)]
  [DataContract(IsReference = true, Namespace = Namespaces.Dataforge)]
  public abstract class ExtensionObject : BusinessObject
  {
  }
}
