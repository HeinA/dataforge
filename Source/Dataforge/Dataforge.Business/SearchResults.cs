﻿using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business
{
  [DataContract(IsReference = true, Namespace = Namespaces.Dataforge)]
  public class SearchResults
  {
    public SearchResults(BasicCollection<string> headers)
    {
      _headers = headers;
    }

    [DataMember(Name = "Headers")]
    BasicCollection<string> _headers;
    public string[] Headers
    {
      get { return _headers.ToArray(); }
    }

    [DataMember(Name = "Results")]
    BasicCollection<SearchResult> _results;
    public SearchResult[] Results
    {
      get
      {
        if (_results == null) _results = new BasicCollection<SearchResult>();
        return _results.ToArray();
      }
    }

    internal void AddResult(SearchResult result)
    {
      if (_results == null) _results = new BasicCollection<SearchResult>();
      _results.Add(result);
    }

    public override string ToString()
    {
      string s = null;
      foreach (string v in Headers)
      {
        s += string.Format(s == null ? "\" {0}\"" : ", \" {0}\"", v);
      }
      return s;
    }
  }
}
