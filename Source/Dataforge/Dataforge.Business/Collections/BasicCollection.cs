﻿using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Collections
{
  [CollectionDataContract(Namespace = Namespaces.Dataforge)]
  public class BasicCollection<T> : ObservableCollection<T>
  {
    public BasicCollection()
    {
    }

    public BasicCollection(IList<T> list)
      : base(list)
    {
    }
    
    public BasicCollection(IEnumerable<T> collection)
      : base(collection)
    {
    }

    public void FireReset()
    {
      OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
    }
  }
}
