﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Collections
{
  [Serializable]
  public class ReferenceObjectNullException : Exception
  {
    protected ReferenceObjectNullException(SerializationInfo info, StreamingContext context)
      :base(info, context)
    {
    }

    public ReferenceObjectNullException(string message)
      : base(message)
    {
    }

    public ReferenceObjectNullException(string message, Exception innerException)
      : base(message, innerException)
    {
    }
  }
}
