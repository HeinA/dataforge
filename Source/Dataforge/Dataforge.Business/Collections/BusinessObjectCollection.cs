﻿using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Dataforge.Business.Collections
{
  [CollectionDataContract(Namespace = Namespaces.Dataforge)]
  public class BusinessObjectCollection<T> : BasicCollection<T>, IInternalOwnedCollection
    where T : BusinessObject
  {
    public BusinessObjectCollection()
      : base()
    {
    }

    public BusinessObjectCollection(BusinessObject owner)
      : base()
    {
      Owner = owner;
    }

    BusinessObject _owner;
    internal BusinessObject Owner
    {
      get { return _owner; }
      set { _owner = value; }
    }

    internal bool SuppressEvents
    {
      get
      {
        if (Owner == null) return true;
        return Owner.SuppressEventState;
      }
    }

    protected override void InsertItem(int index, T item)
    {
      OnBeforeItemInserted(item);
      base.InsertItem(index, item);
    }

    protected virtual void OnBeforeItemInserted(T item)
    {
      if (Owner != null) Owner.AddChild(item);
      item.CompositionChanged += Item_CompositionChanged;
    }

    protected virtual void OnBeforeItemRemoved(T item)
    {
      if (Owner != null) Owner.RemoveChild(item);
      item.CompositionChanged -= Item_CompositionChanged;
    }

    protected override void ClearItems()
    {
      foreach (var bo in this.Items)
      {
        OnBeforeItemRemoved(bo);
      }
      base.ClearItems();
    }

    protected override void RemoveItem(int index)
    {
      OnBeforeItemRemoved(Items[index]);
      base.RemoveItem(index);
    }

    protected override void SetItem(int index, T item)
    {
      OnBeforeItemRemoved(Items[index]);
      OnBeforeItemInserted(item);
      base.SetItem(index, item);
    }

    /// <summary>
    /// Called by BusinessObject apon deserialization
    /// </summary>
    /// <param name="owner"></param>
    void IInternalOwnedCollection.SetOwner(BusinessObject owner)
    {
      this.Owner = owner;
      foreach (var bo in this.Items)
      {
        Owner.AddChild(bo);
      }
    }

    protected void Item_CompositionChanged(object sender, CompositionChangedEventArgs e)
    {
      if (!SuppressEvents)
        OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, sender, sender));
    }

    protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
    {
      if (!SuppressEvents) 
        base.OnCollectionChanged(e);
    }
  }
}
