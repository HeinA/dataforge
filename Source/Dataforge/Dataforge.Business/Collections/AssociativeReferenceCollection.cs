﻿using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Collections
{
  [CollectionDataContract(Namespace = Namespaces.Dataforge)]
  public class AssociativeReferenceCollection<TAssociative> : ObservableCollection<int>, IInternalOwnedCollection
    where TAssociative : AssociativeObject, new()
  {
    private AssociativeReferenceCollection()
      : base()
    {
    }

    public AssociativeReferenceCollection(BusinessObject owner)
      : base()
    {
      Owner = owner;
    }

    BusinessObject _owner;
    BusinessObject Owner
    {
      get { return _owner; }
      set { _owner = value; }
    }

    internal bool RaiseEvents
    {
      get
      {
        if (Owner == null) return true;
        return !Owner.SuppressEvents;
      }
    }

    protected override void InsertItem(int index, int item)
    {
      if (Owner != null) 
      {
        TAssociative a = new TAssociative();
        a.ReferenceId = item;
        Owner.SetAssociativeObject<TAssociative>(a);
      }
      base.InsertItem(index, item);
    }

    protected override void ClearItems()
    {
      foreach (var i in this.Items)
      {
        if (Owner != null) Owner.RemoveAssociativeObject<TAssociative>(i);
      }
      base.ClearItems();
    }

    protected override void RemoveItem(int index)
    {
      if (Owner != null) Owner.RemoveAssociativeObject<TAssociative>(Items[index]);
      base.RemoveItem(index);
    }

    protected override void SetItem(int index, int item)
    {
      if (Owner != null)
      {
        Owner.RemoveAssociativeObject<TAssociative>(Items[index]);
        TAssociative a = new TAssociative();
        a.ReferenceId = item;
        Owner.SetAssociativeObject<TAssociative>(a);
      }
      base.SetItem(index, item);
    }

    protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
    {
      if (RaiseEvents) base.OnCollectionChanged(e);
    }

    public void SetOwner(BusinessObject owner)
    {
      Owner = owner;
    }
  }
}
