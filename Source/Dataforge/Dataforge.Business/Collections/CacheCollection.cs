﻿using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Collections
{
  [CollectionDataContract(Namespace = Namespaces.Dataforge)]
  public sealed class CacheCollection<T> : BasicCollection<T>, ICacheCollection
    where T : BusinessObject
  {
    #region Constructors

    internal CacheCollection()
    {
    }

    internal CacheCollection(Func<T, bool> predicate)
    {
      Predicate = predicate;
    }

    internal CacheCollection(Func<T, object> sort, bool ascending)
    {
      Sort = sort;
      Ascending = ascending;
    }

    internal CacheCollection(Func<T, bool> predicate, Func<T, object> sort, bool ascending)
    {
      Predicate = predicate;
      Sort = sort;
      Ascending = ascending;
    }

    #endregion

    #region Properties

    Func<T, bool> _predicate;
    Func<T, bool> Predicate
    {
      get { return _predicate; }
      set { _predicate = value; }
    }

    Func<T, object> _sort;
    Func<T, object> Sort
    {
      get { return _sort; }
      set { _sort = value; }
    }

    bool _ascending;
    bool Ascending
    {
      get { return _ascending; }
      set { _ascending = value; }
    }

    #endregion

    #region ICacheCollection.Refresh()

    void ICacheCollection.Refresh()
    {
      if (Predicate == null && Sort == null)
      {
        this.Clear();
        foreach (T o in Cache.Instance.GetObjectsInternal<T>()) this.Add(o);
        return;
      }

      if (Predicate != null && Sort == null)
      {
        this.Clear();
        foreach (T o in Cache.Instance.GetObjectsInternal<T>(Predicate)) this.Add(o);
        return;
      }

      if (Predicate == null && Sort != null)
      {
        this.Clear();
        foreach (T o in Cache.Instance.GetObjectsInternal<T>(Sort, Ascending)) this.Add(o);
        return;
      }

      if (Predicate != null && Sort != null)
      {
        this.Clear();
        foreach (T o in Cache.Instance.GetObjectsInternal<T>(Predicate, Sort, Ascending)) this.Add(o);
        return;
      }
    }

    #endregion
  }
}
