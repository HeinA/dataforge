﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Collections
{
  /// <summary>
  /// Do not add a [DataMember] attribute to Associative Object Collections!
  /// </summary>
  public class AssociativeObjectCollection<TAssociative, TReference> : BusinessObjectCollection<TReference>
    where TAssociative : IAssociativeObject, new()
    where TReference : BusinessObject
  {
    public AssociativeObjectCollection()
      : base()
    {
    }

    public AssociativeObjectCollection(BusinessObject owner)
      : base(owner)
    {
      using (new EventStateSuppressor(this))
      {
        foreach (TAssociative a in Owner.GetAssociativeObjects(typeof(TAssociative)))
        {
          BusinessObject bo = Owner.SetAssociativeObject<TAssociative>(a);
          if (!bo.IsDeleted)
          {
            bo.CompositionChanged += Item_CompositionChanged;
            TReference item = (TReference)a.BusinessObjectReference ?? Cache.Instance.GetObject<TReference>(a.ReferenceId);
            base.InsertItem(this.Count, item);
          }
        }
      }
    }

    protected override void OnBeforeItemInserted(TReference item)
    {
      if (item == null) throw new ReferenceObjectNullException(typeof(TReference).Name);

      if (Owner != null)
      {
        item.SuppressEventState = Owner.SuppressEventState;

        TAssociative a = new TAssociative();
        BusinessObject abo  = a as BusinessObject;
        abo.SuppressEventState = Owner.SuppressEventState;

        a.BusinessObjectReference = item;
        BusinessObject bo = a as BusinessObject;
        bo.CompositionChanged -= Item_CompositionChanged;
        bo = Owner.SetAssociativeObject<TAssociative>(a);
        bo.IsDeleted = false;
        bo.CompositionChanged += Item_CompositionChanged;
      }

      base.OnBeforeItemInserted(item);
    }

    protected override void OnBeforeItemRemoved(TReference item)
    {
      if (item == null) throw new ReferenceObjectNullException(typeof(TReference).Name);

      if (Owner != null)
      {
        TAssociative a = (TAssociative)Owner.GetAssociativeObject<TAssociative>(item.Id);
        BusinessObject bo = a as BusinessObject;
        bo.IsDeleted = true;
        bo.CompositionChanged -= Item_CompositionChanged;
      }

      base.OnBeforeItemRemoved(item);
    }
  }
}
