﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Collections
{
  public class OwnedBusinessObjectCollection<T> : BusinessObjectCollection<T>, IInternalOwnedBusinessObjectCollection
    where T : BusinessObject
  {
    internal OwnedBusinessObjectCollection()
      : base()
    {
    }

    public OwnedBusinessObjectCollection(BusinessObject owner)
      : base(owner)
    {
    }

    protected override void InsertItem(int index, T item)
    {
      base.InsertItem(index, item);
    }

    protected override void ClearItems()
    {
      foreach (var bo in this.Items)
      {
      }
      base.ClearItems();
    }

    protected override void RemoveItem(int index)
    {
      base.RemoveItem(index);
    }

    protected override void SetItem(int index, T item)
    {
      base.SetItem(index, item);
    }

    /// <summary>
    /// Called apon Deserialization
    /// </summary>
    /// <param name="owner"></param>
    void IInternalOwnedBusinessObjectCollection.SetOwner(BusinessObject owner)
    {
      this.Owner = owner;
      foreach (var bo in this.Items)
      {
        Owner.AddChild(bo);
      }
    }

    protected override void OnCollectionChanged(NotifyCollectionChangedEventArgs e)
    {
      if (RaiseEvents) base.OnCollectionChanged(e);
    }
  }
}
