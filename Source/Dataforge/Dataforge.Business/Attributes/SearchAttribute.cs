﻿using Dataforge.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Attributes
{
  /// <summary>
  /// The property or owned collection is searchable
  /// </summary>
  [AttributeUsage(AttributeTargets.Property)]
  public class SearchAttribute : Attribute
  {
    string _alias;
    /// <summary>
    /// The alias of the searchable property, for return to the UI
    /// </summary>
    public string Alias
    {
      get { return _alias; }
      set { _alias = value; }
    }

    string _format;
    /// <summary>
    /// The format that should be applied to the resulting values.
    /// </summary>
    public string Format
    {
      get { return _format; }
      set { _format = value; }
    }
  }
}
