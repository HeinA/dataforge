﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Attributes
{
  [AttributeUsage(AttributeTargets.Class)]
  public sealed class ExtensionAttribute : Attribute
  {
  }
}
