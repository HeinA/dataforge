﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Attributes
{
  /// <summary>
  /// The object is stored in a database table.
  /// </summary>
  [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
  public sealed class TableAttribute : Attribute
  {
    string _schema;
    /// <summary>
    /// The database schema of the table.
    /// </summary>
    public string Schema
    {
      get { return _schema; }
      set { _schema = value; }
    }

    string _Name;
    /// <summary>
    /// The database name of the table.
    /// Default is the name of class name.
    /// </summary>
    public string Name
    {
      get { return _Name; }
      set { _Name = value; }
    }

    string _idColumn;
    /// <summary>
    /// The id column of the table
    /// Default is "Id"
    /// </summary>
    public string IdColumn
    {
      get { return _idColumn; }
      set { _idColumn = value; }
    }

    string _globalIdentifierColumn;
    /// <summary>
    /// The global identifier column of the table, if applicable.
    /// Default is "GlobalIdentifier".
    /// </summary>
    public string GlobalIdentifierColumn
    {
      get { return _globalIdentifierColumn; }
      set { _globalIdentifierColumn = value; }
    }

    string _ownerColumn;
    /// <summary>
    /// The owner reference column of the table, if applicable.
    /// </summary>
    public string OwnerColumn
    {
      get { return _ownerColumn; }
      set { _ownerColumn = value; }
    }

    //bool _isCircularReference = false;
    ///// <summary>
    ///// The owner reference, references the same table.
    ///// Default is false.
    ///// </summary>
    //public bool IsCircularReference
    //{
    //  get { return _isCircularReference; }
    //  set { _isCircularReference = value; }
    //}

    Type _ownerType;
    /// <summary>
    /// The owner type.
    /// </summary>
    public Type OwnerType
    {
      get { return _ownerType; }
      set { _ownerType = value; }
    }

    string _rowColumn;
    /// <summary>
    /// The  row reference column of the TableObject
    /// </summary>
    public string RowColumn
    {
      get { return _rowColumn; }
      set { _rowColumn = value; }
    }

    string _columnColumn;
    /// <summary>
    /// The column reference column of the TableObject
    /// </summary>
    public string ColumnColumn
    {
      get { return _columnColumn; }
      set { _columnColumn = value; }
    }

    bool _flattenBase = false;
    /// <summary>
    /// The base object will be stored in the same table.
    /// Default is false
    /// </summary>
    public bool FlattenBase
    {
      get { return _flattenBase; }
      set { _flattenBase = value; }
    }

    bool _flattenToSubClass = false;
    /// <summary>
    /// The object will be stored in a descendant's table
    /// Default is false
    /// </summary>
    public bool FlattenToSubClass
    {
      get { return _flattenToSubClass; }
      set { _flattenToSubClass = value; }
    }

    string _referenceColumn;
    /// <summary>
    /// The reference column name of an AssociativeObject
    /// </summary>
    public string ReferenceColumn
    {
      get { return _referenceColumn; }
      set { _referenceColumn = value; }
    }

    bool _global = false;
    /// <summary>
    /// The object is available to all applications.  No application domain reference is stored with the object.
    /// Default is false.
    /// </summary>
    public bool Global
    {
      get { return _global; }
      set { _global = value; }
    }

    string _applicationDomainColumn;
    /// <summary>
    /// The column name of the application domain reference.  
    /// Default is "ApplicationDomain"
    /// </summary>
    public string ApplicationDomainColumn
    {
      get { return _applicationDomainColumn; }
      set { _applicationDomainColumn = value; }
    }
  }
}
