﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Attributes
{
  /// <summary>
  /// The property is involved in database operations.
  /// </summary>
  [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
  public sealed class ColumnAttribute : Attribute
  {
    string _name;
    /// <summary>
    /// The database name of the column.
    /// Default is the property name.
    /// </summary>
    public string Name
    {
      get { return _name; }
      set { _name = value; }
    }

    bool _read = true;
    /// <summary>
    /// Coulumn must be selected during read operations.
    /// Default is true.
    /// </summary>
    public bool Read
    {
      get { return _read; }
      set { _read = value; }
    }

    bool write = true;
    /// <summary>
    /// Column must be written during write operations.
    /// Default is true.
    /// </summary>
    public bool Write
    {
      get { return write; }
      set { write = value; }
    }

    int _sortOrder = -1;
    /// <summary>
    /// The sorting precedence of the column. (-1 to not sort by this column)
    /// Default is -1.
    /// </summary>
    public int SortOrder
    {
      get { return _sortOrder; }
      set { _sortOrder = value; }
    }

    SortDirection _sortDirection = SortDirection.Ascending;
    /// <summary>
    /// The direction of the sort.
    /// Default is Ascending.
    /// </summary>
    public SortDirection SortDirection
    {
      get { return _sortDirection; }
      set { _sortDirection = value; }
    }

    bool _ignoreNull = false;
    /// <summary>
    /// Null values will be ignored during writing operations.
    /// Default is false.
    /// </summary>
    public bool IgnoreNull
    {
      get { return _ignoreNull; }
      set { _ignoreNull = value; }
    }

    bool _isReference = false;
    /// <summary>
    /// The referenced object may be null.  The referenced object's id will be read from / written to the object's reference collection.
    /// Applies only to BusinessObject properties.
    /// Default is false.
    /// </summary>
    public bool IsReference
    {
      get { return _isReference; }
      set { _isReference = value; }
    }

    bool _ignoreConcurrency = false;
    public bool IgnoreConcurrency
    {
      get { return _ignoreConcurrency; }
      set { _ignoreConcurrency = value; }
    }
  }
}
