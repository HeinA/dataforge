﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Attributes
{
  /// <summary>
  /// The object or owned collection should be cached.
  /// </summary>
  [AttributeUsage(AttributeTargets.Class | AttributeTargets.Property)]
  public class CacheAttribute : Attribute
  {
    int _priority = -1;
    public int Priority
    {
      get { return _priority; }
      set { _priority = value; }
    }
  }
}
