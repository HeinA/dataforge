﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Attributes
{
  [Serializable]
  public class TableAttributeException : Exception
  {
    protected TableAttributeException(SerializationInfo info, StreamingContext context)
      :base(info, context)
    {
    }

    public TableAttributeException(string message)
      : base(message)
    {
    }

    public TableAttributeException(string message, Exception innerException)
      : base(message, innerException)
    {
    }
  }
}
