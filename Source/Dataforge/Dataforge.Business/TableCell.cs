﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business
{
  [DataContract(IsReference = true, Namespace = Namespaces.Dataforge)]
  [Table(FlattenToSubClass = true)]
  public abstract class TableCell<TColumn, TRow> : BusinessObject, ITableCell
    where TColumn : BusinessObject
    where TRow : BusinessObject
  {
    //#region DataMember int RowId

    //public const string PN_ROWID = "RowId";
    //[DataMember(Name = PN_ROWID, EmitDefaultValue = false)]
    //int _rowId = 0;
    //public int RowId
    //{
    //  get
    //  {
    //    if (Row != null) return Row.Id;
    //    return _rowId;
    //  }
    //  internal set { SetValue<int>(ref _rowId, value, PN_ROWID); }
    //}

    //#endregion

    #region DataMember TRow Row

    public const string PN_ROW = "Row";
    TRow _row;
    [DataMember(Name = PN_ROW, EmitDefaultValue = false)]
    public virtual TRow Row
    {
      get
      {
        if (_row == null) _row = GetCachedObject<TRow>(PN_ROW);
        return _row;
      }
      internal set
      {
        if (SetValue<TRow>(ref _row, value, PN_ROW))
        {
          SetCachedObject<TRow>(_row, PN_ROW);
        }
      }
    }

    #endregion

    //#region DataMember int ColumnId

    //public const string PN_COLUMNID = "ColumnId";
    //[DataMember(Name = PN_COLUMNID, EmitDefaultValue = false)]
    //int _columnId = 0;
    //public int ColumnId
    //{
    //  get
    //  {
    //    if (Column != null) return Column.Id;
    //    return _columnId;
    //  }
    //  set { SetValue<int>(ref _columnId, value, PN_COLUMNID); }
    //}

    //#endregion

    #region DataMember TColumn Column

    public const string PN_COLUMN = "Column";
    TColumn _column;
    [DataMember(Name = PN_COLUMN, EmitDefaultValue = false)]
    public virtual TColumn Column
    {
      get
      {
        if (_column == null) _column = GetCachedObject<TColumn>(PN_COLUMN);
        return _column;
      }
      internal set
      {
        if (SetValue<TColumn>(ref _column, value, PN_COLUMN))
        {
          SetCachedObject<TColumn>(_column, PN_COLUMN);
        }
      }
    }

    #endregion

    #region ITableCell

    BusinessObject ITableCell.BusinessObjectRow
    {
      get { return Row; }
      set { Row = (TRow)value; }
    }

    int ITableCell.RowId
    {
      get { return GetReferenceValue(PN_ROW); }
    }

    int ITableCell.ColumnId
    {
      get { return GetReferenceValue(PN_COLUMN); }
    }

    void ITableCell.SetRowId(int id)
    {
      SetReferenceValue(id, PN_ROW);
    }

    void ITableCell.SetColumnId(int id)
    {
      SetReferenceValue(id, PN_COLUMN);
    }

    BusinessObject ITableCell.BusinessObjectColumn
    {
      get { return Column; }
      set { Column = (TColumn)value; }
    }

    #endregion
  }
}
