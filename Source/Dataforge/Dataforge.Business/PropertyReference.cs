﻿using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business
{
  [DataContract(IsReference = true, Namespace = Namespaces.Dataforge, Name="PropertyReference")]
  class PropertyReference
  {
    public PropertyReference(string propertyName, object value)
    {
      _propertyName = propertyName;
      _reference = value;
    }

    #region string PropertyName

    [DataMember(Name = "Property", EmitDefaultValue = false)]
    string _propertyName;
    public string PropertyName
    {
      get { return _propertyName; }
    }

    #endregion

    #region object Reference

    [DataMember(Name = "Reference", EmitDefaultValue = false)]
    object _reference;
    public object Reference
    {
      get { return _reference; }
      set { _reference = value; }
    }

    #endregion
  }
}
