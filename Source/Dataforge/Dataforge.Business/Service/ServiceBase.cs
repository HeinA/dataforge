﻿using Dataforge.Business.Collections;
using Dataforge.Business.Data;
using Dataforge.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Dataforge.Business.Service
{
  public abstract class ServiceBase
  {
    protected BasicCollection<T> Load<T>()
      where T : BusinessObject
    {
      using (new ConnectionScope())
      {
        IObjectStore<T> store = DataManager.Instance.GetObjectStore<T>();
        return store.Load();
      }
    }

    protected BasicCollection<T> Save<T>(BasicCollection<T> col)
    where T : BusinessObject
    {
      using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
      using (new ConnectionScope())
      {
        IObjectStore<T> store = DataManager.Instance.GetObjectStore<T>();
        store.Save(col);
        BasicCollection<T> ret = store.Load();
        scope.Complete();
        return ret;
      }
    }
  }
}
