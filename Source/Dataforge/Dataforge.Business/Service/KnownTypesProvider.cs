﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Collections;
using Dataforge.Business.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Service
{
  static class KnownTypesProvider
  {
    static object _lock;
    static object Lock
    {
      get
      {
        if (_lock == null) _lock = new object();
        return _lock;
      }
    }

    static Collection<Type> _faults;
    static Dictionary<Type, string> _faultNamespaces;
    static Collection<Type> _knownTypes;

    static KnownTypesProvider()
    {
      try
      {
        lock (Lock)
        {
          Utilities.PreLoadDeployedAssemblies();

          _faults = new Collection<Type>();
          _faultNamespaces = new Dictionary<Type, string>();
          _knownTypes = new Collection<Type>();
          Collection<Type> genericTypes = new Collection<Type>();

          foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
          {
            try
            {
              foreach (Type t in a.GetTypes().Where(t => t.IsSubclassOf(typeof(DataforgeFault))))
              {
                _faults.Add(t);
                DataContractAttribute dca = t.GetCustomAttributes<DataContractAttribute>().FirstOrDefault();
                if (dca == null) throw new InvalidOperationException("Fault must have a DataContract attribute.");
                _faultNamespaces.Add(t, dca.Namespace);
              }

              foreach (Type t in a.GetTypes().Where(t => !t.IsAbstract && t.IsDefined(typeof(TableAttribute)) && t.IsDefined(typeof(DataContractAttribute))))
              {
                _knownTypes.Add(t);

                Type ct = typeof(BasicCollection<>);
                Type ct1 = ct.MakeGenericType(new Type[] { t });
                _knownTypes.Add(ct1);
                //foreach (PropertyInfo pi in t.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly).Where(p => p.PropertyType.IsGenericType && p.PropertyType.GetGenericTypeDefinition() == typeof(BusinessObjectCollection<>))) // || p.PropertyType.GetGenericTypeDefinition() == typeof(AssociativeReferenceCollection<>) || p.PropertyType.GetGenericTypeDefinition() == typeof(AssociativeObjectCollection<,>))
                //{
                //  Type[] types = pi.PropertyType.GetGenericArguments();
                //  if (!_knownTypes.Contains(types[0])) _knownTypes.Add(types[0]);
                //  if (types.Length > 1 && !_knownTypes.Contains(types[1])) _knownTypes.Add(types[1]);
                //  _knownTypes.Add(pi.PropertyType);
                //}
              }
            }
            catch
            {
              throw;
            }
          }
          _knownTypes.Add(typeof(DataFilterCondition));
          _knownTypes.Add(typeof(SearchResult));
          _knownTypes.Add(typeof(SearchResults));
        }
      }
      catch (Exception ex)
      {
        if (ex is System.Reflection.ReflectionTypeLoadException)
        {
          var typeLoadException = ex as ReflectionTypeLoadException;
          var loaderExceptions = typeLoadException.LoaderExceptions;
          foreach (var loaderException in loaderExceptions)
          {
            throw loaderException;
          }
        }
      }
    }

    public static Collection<Type> KnownTypes
    {
      get { return _knownTypes; }
    }

    public static Collection<Type> Faults
    {
      get { return _faults; }
    }

    public static string GetFaultNamespace(Type faultType)
    {
      return _faultNamespaces[faultType];
    }
  }
}
