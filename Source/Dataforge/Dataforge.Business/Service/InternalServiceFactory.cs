﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Globalization;
using System.Net.NetworkInformation;
using System.Net;
using System.Threading;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Dataforge.Business.Security;
using Dataforge.Business.Service;
using Dataforge.Business;

namespace Dataforge.Business.Service
{
  internal interface IDisposableWrapper<T> : IDisposable
  {
    T Instance { get; }
  }

  internal class DisposableWrapper<T> : IDisposableWrapper<T> where T : class
  {
    public T Instance { get; private set; }
    ChannelFactory<T> _factory;
    public DisposableWrapper(ChannelFactory<T> factory)
    {
      _factory = factory;
      if (_factory != null) Instance = _factory.CreateChannel();
    }

    protected virtual void Dispose(bool disposing)
    {
      if (disposing)
      {
        (Instance as IDisposable).Dispose();
      }
    }

    public void Dispose()
    {
      try
      {
        Dispose(true);
        GC.SuppressFinalize(this);
      }
      catch
      {
      }
      Instance = default(T);
    }
  }

  internal class ClientWrapper<TProxy> : DisposableWrapper<TProxy>
      where TProxy : class
  {
    public ClientWrapper(ChannelFactory<TProxy> proxy) : base(proxy) { }
    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        if (this.Instance != null)
        {
          if ((this.Instance as IClientChannel).State == CommunicationState.Faulted)
          {
            (this.Instance as IClientChannel).Abort();
          }
          else
          {
            (this.Instance as IClientChannel).Close();
          }
        }
      }

      base.Dispose(disposing);
    }
  }

  internal static class WCFExtensions
  {
    public static IDisposableWrapper<TProxy> Wrap<TProxy>(
        this ChannelFactory<TProxy> proxy)
        where TProxy : class
    {

      return new ClientWrapper<TProxy>(proxy);
    }
  }

  internal static class InternalServiceFactory
  {
    static object _lock = new object();

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
    public static IDisposableWrapper<T> GetService<T>()
      where T : class
    {
      lock (_lock)
      {
        ChannelFactory<T> factory = null;
        if (!FactoryDictionary.ContainsKey(typeof(T).FullName))
        {
          factory = new ChannelFactory<T>(typeof(T).FullName);
          factory.Open();
          FactoryDictionary.Add(typeof(T).FullName, factory);
        }
        else
        {
          factory = (ChannelFactory<T>)FactoryDictionary[typeof(T).FullName];
        }
        IDisposableWrapper<T> wrapper = WCFExtensions.Wrap<T>(factory);
        return wrapper;
      }
    }

    static Dictionary<string, object> _factoryDictionary = new Dictionary<string, object>();
    static Dictionary<string, object> FactoryDictionary
    {
      get { return InternalServiceFactory._factoryDictionary; }
    }
  }
}
