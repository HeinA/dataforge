﻿using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Service
{
  [DataContract(Namespace = Namespaces.Dataforge)]
  public class SessionExpiredFault : DataforgeFault
  {
  }
}
