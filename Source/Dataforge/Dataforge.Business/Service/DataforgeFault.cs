﻿using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Service
{
  [DataContract(Namespace = Namespaces.Dataforge)]
  public abstract class DataforgeFault
  {
    [DataMember]
    string _message = string.Empty;
    public string Message
    {
      get { return _message; }
      set { _message = value; }
    }
  }
}
