﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Service
{
  [Serializable]
  public class CouldNotConnectToServiceException : Exception
  {
    protected CouldNotConnectToServiceException(SerializationInfo info, StreamingContext context)
      :base(info, context)
    {
    }

    public CouldNotConnectToServiceException(string message)
      : base(message)
    {
    }

    public CouldNotConnectToServiceException(string message, Exception innerException)
      : base(message, innerException)
    {
    }  
  }
}
