﻿using Dataforge.Business.Activities;
using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using Dataforge.Business.Data;
using Dataforge.Business.Documents;
using Dataforge.Business.Security;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Service
{
  [ServiceContract(Namespace = Namespaces.Dataforge)]
  public interface IDataforgeService
  {
    #region Authentication

    [OperationContract]
    AuthenticationResult Login(int applicationDomainId, string username, string passwordHash);

    #endregion

    #region Users

    [OperationContract]
    BasicCollection<User> LoadUsers();

    [OperationContract]
    BasicCollection<User> SaveUsers(BasicCollection<User> col);

    #endregion

    #region Documents

    [OperationContract]
    BasicCollection<Document> LoadDocuments(int documentType);

    [OperationContract]
    Document SaveDocument(Document doc);

    #endregion

    #region Cache

    [OperationContract]
    BasicCollection<BusinessObject> LoadCache();

    #endregion

    #region OrganizationalStructure

    [OperationContract]
    BasicCollection<OrganizationalStructureType> LoadOrganizationalStructure();

    [OperationContract]
    BasicCollection<OrganizationalStructureType> SaveOrganizationalStructure(BasicCollection<OrganizationalStructureType> col);

    #endregion

    #region OrganizationalUnits

    [OperationContract]
    BasicCollection<OrganizationalUnit> LoadOrganizationalUnits();

    [OperationContract]
    BasicCollection<OrganizationalUnit> SaveOrganizationalUnits(BasicCollection<OrganizationalUnit> col);


    #endregion

    #region Activities

    [OperationContract]
    BasicCollection<OperationGroup> LoadOperationGroups();

    [OperationContract]
    BasicCollection<OperationGroup> SaveOperationGroups(BasicCollection<OperationGroup> col);

    [OperationContract]
    BasicCollection<Activity> LoadActivities();

    [OperationContract]
    BasicCollection<Activity> SaveActivities(BasicCollection<Activity> col);

    #endregion
  }
}
