﻿using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Service
{
  [ServiceContract(Namespace = Namespaces.Dataforge)]
  public interface IServiceHost
  {
    [OperationContract(IsOneWay = true)]
    void Terminate();
  }
}
