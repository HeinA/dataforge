﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Service
{
  [Serializable]
  public class SessionExpiredException : Exception
  {
    protected SessionExpiredException(SerializationInfo info, StreamingContext context)
      :base(info, context)
    {
    }

    public SessionExpiredException()
      : base()
    {
    }

    public SessionExpiredException(string message)
      : base(message)
    {
    }

    public SessionExpiredException(string message, Exception innerException)
      : base(message, innerException)
    {
    }  
  }
}
