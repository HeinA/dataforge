﻿using Dataforge.Business.Activities;
using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using Dataforge.Business.Data;
using Dataforge.Business.Documents;
using Dataforge.Business.Security;
using Dataforge.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Dataforge.Business.Service
{
  [ServiceBehavior(Namespace = Namespaces.Dataforge)]
  [ExceptionShielding]
  public class DataforgeService : ServiceBase, IDataforgeService
  {
    public DataforgeService()
    {
      DataManager.Instance.GetObjectStore<User>();
      DataManager.Instance.GetObjectStore<UserSalt>();
    }

    #region Authentication

    public AuthenticationResult Login(int applicationDomainId, string username, string passwordHash)
    {
      using (new ConnectionScope())
      {
        Collection<DataFilterCondition> filter = new Collection<DataFilterCondition>();
        filter.Add(new DataFilterCondition(typeof(User), User.PN_USERNAME, FilterType.Equals, username));
        filter.Add(new DataFilterCondition(typeof(User), User.PN_APPLICATIONDOMAIN, FilterType.Equals, applicationDomainId));
        IObjectStore<User> userStore = DataManager.Instance.GetObjectStore<User>();
        SearchResults r = userStore.Search(filter);
        if (r.Results.Length == 1)
        {
          IObjectStore<UserSalt> saltStore = DataManager.Instance.GetObjectStore<UserSalt>();
          UserSalt userSalt = saltStore.LoadById(r.Results[0].Id);
          User user = userStore.LoadById(r.Results[0].Id);

          HashAlgorithm algoritm = new SHA256Managed();
          string saltedPasswordHash = ASCIIEncoding.ASCII.GetString(algoritm.ComputeHash(ASCIIEncoding.ASCII.GetBytes(string.Format("{0}{1}", userSalt.Salt, passwordHash))));

          if (saltedPasswordHash == user.SaltedPasswordHash)
          {
            Guid session = Guid.NewGuid();
            AuthenticationResult result = new AuthenticationResult(session, user);
            SecurityContext.AddAuthentication(result);
            return result;
          }
        }
      }

      return new AuthenticationResult("Invalid username or password.");
    }

    #endregion

    #region Users

    public BasicCollection<User> LoadUsers()
    {
      return Load<User>();
    }

    public BasicCollection<User> SaveUsers(BasicCollection<User> col)
    {
      return Save<User>(col);
    }

    #endregion

    #region Documents

    public BasicCollection<Document> LoadDocuments(int documentType)
    {
      using (new ConnectionScope())
      {
        Collection<DataFilterCondition> c = new Collection<DataFilterCondition>();
        c.Add(new DataFilterCondition(typeof(Document), "DocumentTypeId", FilterType.Equals, documentType));

        IObjectStore<Document> store = DataManager.Instance.GetObjectStore<Document>();
        BasicCollection<Document> col = store.Load(c);
        return col;
      }
    }

    public Document SaveDocument(Document doc)
    {
      using (var scope = new TransactionScope(TransactionScopeOption.Required))
      using (new ConnectionScope())
      {
        IObjectStore<Document> store = DataManager.Instance.GetObjectStore<Document>();
        store.Save(doc);
        if (doc.Id == 0) return null;
        Document d = store.LoadById(doc.Id);
        scope.Complete();
        return d;
      }
    }

    #endregion

    #region Cache

    public BasicCollection<BusinessObject> LoadCache()
    {
      return Cache.LoadCache();
    }

    #endregion

    #region OrganizationalStructure

    public BasicCollection<OrganizationalStructureType> LoadOrganizationalStructure()
    {
      return Load<OrganizationalStructureType>();
    }

    public BasicCollection<OrganizationalStructureType> SaveOrganizationalStructure(BasicCollection<OrganizationalStructureType> col)
    {
      return Save<OrganizationalStructureType>(col);
    }

    #endregion

    #region OrganizationalUnits

    public BasicCollection<OrganizationalUnit> LoadOrganizationalUnits()
    {
      return Load<OrganizationalUnit>();
    }

    public BasicCollection<OrganizationalUnit> SaveOrganizationalUnits(BasicCollection<OrganizationalUnit> col)
    {
      return Save<OrganizationalUnit>(col);
    }


    #endregion

    #region Activities

    public BasicCollection<OperationGroup> LoadOperationGroups()
    {
      return Load<OperationGroup>();
    }

    public BasicCollection<OperationGroup> SaveOperationGroups(BasicCollection<OperationGroup> col)
    {
      return Save<OperationGroup>(col);
    }

    public BasicCollection<Activity> LoadActivities()
    {
      return Load<Activity>();
    }

    public BasicCollection<Activity> SaveActivities(BasicCollection<Activity> col)
    {
      return Save<Activity>(col);
    }

    #endregion
  }
}