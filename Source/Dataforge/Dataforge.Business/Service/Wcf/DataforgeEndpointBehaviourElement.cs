﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Configuration;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Service.Wcf
{
  public class DataforgeEndpointBehaviourElement : BehaviorExtensionElement
  {
    protected override object CreateBehavior()
    {
      return new DataforgeEndpointBehaviour();
    }

    public override Type BehaviorType
    {
      get
      {
        return typeof(DataforgeEndpointBehaviour);
      }
    }
  }
}
