﻿using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Service.Wcf
{
  public class DataforgeObjectSerializer : XmlObjectSerializer
  {
    DataContractSerializer _dcs;
    DataContractSerializer Dcs
    {
      get { return _dcs; }
    }

    internal DataforgeObjectSerializer(DataContractSerializer dcs)
    {
      _dcs = dcs;
    }

    public override bool IsStartObject(System.Xml.XmlDictionaryReader reader)
    {
      return _dcs.IsStartObject(reader);
    }

    public override object ReadObject(System.Xml.XmlDictionaryReader reader, bool verifyObjectName)
    {
      return _dcs.ReadObject(reader, verifyObjectName);
    }

    public override void WriteEndObject(System.Xml.XmlDictionaryWriter writer)
    {
      _dcs.WriteEndObject(writer);
    }

    public override void WriteObjectContent(System.Xml.XmlDictionaryWriter writer, object graph)
    {
      _dcs.WriteObjectContent(writer, graph);
    }

    public override void WriteStartObject(System.Xml.XmlDictionaryWriter writer, object graph)
    {
      _dcs.WriteStartObject(writer, graph);
      writer.WriteAttributeString("xmlns", "a1", null, Namespaces.Dataforge);
      writer.WriteAttributeString("xmlns", "a2", null, "http://www.w3.org/2001/XMLSchema");
      int i = 1;
      foreach (string s in MefContainer.Instance.GetKnownNamespaces())
      {
        writer.WriteAttributeString("xmlns", string.Format("b{0}", i++), null, s);
      }
    }
  }
}
