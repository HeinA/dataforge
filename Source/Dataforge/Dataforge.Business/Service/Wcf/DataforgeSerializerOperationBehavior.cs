﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Dataforge.Business.Service.Wcf
{
  public class DataforgeSerializerOperationBehavior : DataContractSerializerOperationBehavior
  {
    public DataforgeSerializerOperationBehavior(OperationDescription operationDescription) :
      base(operationDescription)
    {
    }

    public override XmlObjectSerializer CreateSerializer(Type type, string name, string ns, IList<Type> knownTypes)
    {
      return new DataforgeObjectSerializer((DataContractSerializer)base.CreateSerializer(type, name, ns, knownTypes));
    }

    public override XmlObjectSerializer CreateSerializer(Type type, XmlDictionaryString name, XmlDictionaryString ns, IList<Type> knownTypes)
    {
      return new DataforgeObjectSerializer((DataContractSerializer)base.CreateSerializer(type, name, ns, knownTypes));
    }
  }
}
