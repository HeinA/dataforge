﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Service.Wcf
{
  public class DataforgeCacheExtension : IExtension<OperationContext>
  {
    OperationContext _operationContext;
    OperationContext OperationContext
    {
      get { return _operationContext; }
      set { _operationContext = value; }
    }

    bool _refreshCache = false;
    public static bool RefreshCache
    {
      get
      {
        if (Current == null) return false;
        return Current._refreshCache;
      }
      set
      {
        if (Current != null) Current._refreshCache = value;
      }
    }

    static DataforgeCacheExtension Current
    {
      get
      {
        if (OperationContext.Current == null) return null;
        return OperationContext.Current.Extensions.Find<DataforgeCacheExtension>();
      }
    }

    public void Attach(OperationContext owner)
    {
      OperationContext = owner;
    }

    public void Detach(OperationContext owner)
    {
      if (OperationContext == owner)
      {
        OperationContext = null;
      }
      else throw new InvalidOperationException("Invalid OperationContext.");
    }
  }
}
