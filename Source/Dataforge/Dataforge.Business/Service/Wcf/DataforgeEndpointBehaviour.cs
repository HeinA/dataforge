﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Service.Wcf
{
  public class DataforgeEndpointBehaviour : IEndpointBehavior
  {
    public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
    {
    }

    public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
    {
      clientRuntime.MessageInspectors.Add(new DataforgeClientMessageInspector());
    }

    public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
    {
      endpointDispatcher.DispatchRuntime.MessageInspectors.Add(new DataforgeDispatchMessageInspector());
    }

    public void Validate(ServiceEndpoint endpoint)
    {
      #region Apply Known Types & Fault Contracts

      foreach (OperationDescription opDesc in endpoint.Contract.Operations)
      {
        foreach (Type t in KnownTypesProvider.KnownTypes)
        {
          opDesc.KnownTypes.Add(t);
        }

        foreach (Type t in KnownTypesProvider.Faults)
        {
          FaultDescription faultDescription = new FaultDescription(string.Format("{0}/{1}/{2}_{3}", opDesc.DeclaringContract.Namespace, opDesc.DeclaringContract.Name, opDesc.Name, t.Name));
          faultDescription.Namespace = KnownTypesProvider.GetFaultNamespace(t);
          faultDescription.DetailType = t;
          faultDescription.Name = t.Name;
          opDesc.Faults.Add(faultDescription);
        }
      }

      #endregion

      foreach (OperationDescription desc in endpoint.Contract.Operations)
      {
        DataContractSerializerOperationBehavior dcsOperationBehavior = desc.Behaviors.Find<DataContractSerializerOperationBehavior>();
        if (dcsOperationBehavior != null)
        {
          int idx = desc.Behaviors.IndexOf(dcsOperationBehavior);
          desc.Behaviors.Remove(dcsOperationBehavior);
          desc.Behaviors.Insert(idx, new DataforgeSerializerOperationBehavior(desc));
        }
      }

      endpoint.Binding.Namespace = Namespaces.Dataforge;
    }
  }
}
