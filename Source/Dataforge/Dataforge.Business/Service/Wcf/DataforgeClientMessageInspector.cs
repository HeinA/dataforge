﻿using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using Dataforge.Business.Security;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Service.Wcf
{
  public class DataforgeClientMessageInspector : IClientMessageInspector
  {
    public const string SessionToken = "SessionToken";

    public void AfterReceiveReply(ref Message reply, object correlationState)
    {
      bool refreshCache = reply.Headers.GetHeader<bool>(DataforgeDispatchMessageInspector.RefreshCache, Namespaces.Dataforge);
      if (refreshCache)
      {
        BasicCollection<BusinessObject> cache = null;
        BackgroundWorker bw = new BackgroundWorker();
        bw.DoWork += (sender, e) =>
        {
          try
          {
            using (var svc = InternalServiceFactory.GetService<IDataforgeService>())
            {
              cache = svc.Instance.LoadCache();
            }
          }
          catch (Exception ex)
          {
            if (ExceptionPolicy.HandleException(ex, "Default Policy"))
              throw;
          }
        };
        bw.RunWorkerCompleted += (sender, e) =>
        {
          Cache.Instance.LoadCache(cache);
          bw.Dispose();
        };
        bw.RunWorkerAsync();
      }
    }

    public object BeforeSendRequest(ref Message request, IClientChannel channel)
    {
      MessageHeader<Guid> customHeaderUserID = new MessageHeader<Guid>(SecurityContext.ImpersonationToken != Guid.Empty ? SecurityContext.ImpersonationToken : SecurityContext.SessionToken);
      MessageHeader h = customHeaderUserID.GetUntypedHeader(SessionToken, Namespaces.Dataforge);
      request.Headers.Add(h);

      return Guid.NewGuid();
    }
  }
}
