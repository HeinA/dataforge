﻿using Dataforge.Business.Constants;
using Dataforge.Business.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Service.Wcf
{
  public class DataforgeDispatchMessageInspector : IDispatchMessageInspector
  {
    public const string RefreshCache = "RefreshCache";
    const string LoginAction = "http://dataforgeconsulting.com/Business/IDataforgeService/Login";
    const string TerminateAction = "http://dataforgeconsulting.com/Business/IServiceHost/Terminate";

    public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
    {
      if (request.Headers.Action != LoginAction && request.Headers.Action != TerminateAction && SecurityContext.User == null) 
        throw new SessionExpiredException();

      OperationContext.Current.Extensions.Add(new DataforgeCacheExtension());

      return null;
    }

    public void BeforeSendReply(ref Message reply, object correlationState)
    {
      if (DataforgeCacheExtension.RefreshCache)
      {
        MessageHeader<bool> customHeaderUserID = new MessageHeader<bool>(true);
        MessageHeader h = customHeaderUserID.GetUntypedHeader(RefreshCache, Namespaces.Dataforge);
        reply.Headers.Add(h);

        BackgroundWorker bw = new BackgroundWorker();
        bw.DoWork += (sender, e) =>
          {
            Cache.Instance.LoadCache(Cache.LoadCache());
          };
        bw.RunWorkerCompleted += (sender, e) =>
          {
            bw.Dispose();
          };
        bw.RunWorkerAsync();
      }
      else
      {
        MessageHeader<bool> customHeaderUserID = new MessageHeader<bool>(false);
        MessageHeader h = customHeaderUserID.GetUntypedHeader(RefreshCache, Namespaces.Dataforge);
        if (reply != null) reply.Headers.Add(h);
      }
    }
  }
}
