﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Constants
{
  public class Activities
  {
    public const int OrganizationalStructureManagement = 10001;
    public const int OrganizationalUnitManagement = 10002;
    public const int UserManagement = 10003;
    public const int OperationManagement = 10051;
    public const int ActivityManagement = 10052;
  }
}
