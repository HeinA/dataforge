﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Constants
{
  public class Operations
  {
    public const int Save = 1000001;
    public const int ToggleDelete = 1000002;
    public const int Refresh = 1000003;

    public const int OrganizationalStructure_AddRootType = 1000101;
    public const int OrganizationalStructure_AddSubType = 1000102;

    public const int OrganizationalUnit_AddRoot = 1000201;
    public const int OrganizationalUnit_AddSubUnit = 1000202;

    public const int User_AddUser = 1000301;
    public const int User_SetPassword = 1000302;

    public const int Operations_AddOperation = 1005101;
    public const int Operations_AddGroup = 1005102;
    public const int Operations_SetIcon = 1005103;
  }
}
