﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using Dataforge.Business.Data;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.Serialization;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Dataforge.Business.Activities
{
  [DataContract(IsReference = true, Namespace = Namespaces.Dataforge)]
  [Table(Schema = "Dataforge", OwnerColumn = "Group", OwnerType = typeof(OperationGroup))]
  public class Operation : OperationBase, IGlobalAware
  {
    #region Constructors

    public Operation()
    {
      using (new EventStateSuppressor(this))
      {
        Id = 0;
      }
    }

    #endregion

    #region OperationGroup Group

    public OperationGroup Group
    {
      get { return (OperationGroup)Owner ?? Cache.Instance.GetObject<OperationGroup>(GetReferenceValue(PN_OWNER)); }
    }

    #endregion

    #region DataMember string Text

    public const string PN_TEXT = "Text";
    [DataMember(Name = PN_TEXT, EmitDefaultValue = false)]
    string _text;
    [Column(SortOrder = 0)]
    public string Text
    {
      get { return _text; }
      set { SetValue<string>(ref _text, value, PN_TEXT); }
    }

    #endregion

    #region DataMember string Description

    public const string PN_DESCRIPTION = "Description";
    [DataMember(Name = PN_DESCRIPTION, EmitDefaultValue = false)]
    string _description;
    [Column]
    public string Description
    {
      get { return _description; }
      set { SetValue<string>(ref _description, value, PN_DESCRIPTION); }
    }

    #endregion

    #region DataMember bool Enabled

    public const string PN_ENABLED = "Enabled";
    [DataMember(Name = PN_ENABLED, EmitDefaultValue = false)]
    bool _enabled = true;
    public bool Enabled
    {
      get { return _enabled; }
      set
      {
        if (SetValue<bool>(ref _enabled, value, PN_ENABLED))
        {
          Visible = Enabled;
        }
      }
    }

    #endregion

    #region object Tag

    object _tag;
    public object Tag
    {
      get { return _tag; }
      set { _tag = value; }
    }

    #endregion

    #region string Base64Image

    #region DataMember string Base64Image

    const string PN_BASE64IMAGE = "Base64Image";
    [DataMember(Name = PN_BASE64IMAGE, EmitDefaultValue = false)]
    string _base64Image;
    [Column(IgnoreConcurrency = true)]
    public string Base64Image
    {
      get { return _base64Image; }
      set { SetValue<string>(ref _base64Image, value, PN_BASE64IMAGE); }
    }

    #endregion

    #region void SetImageFromFile(string filename)

    public void SetImageFromFile(string filename)
    {
      using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
      {
        byte[] imgBytes = new byte[fs.Length];
        fs.Read(imgBytes, 0, Convert.ToInt32(fs.Length));
        Base64Image = Convert.ToBase64String(imgBytes, Base64FormattingOptions.InsertLineBreaks);

      }
    }

    #endregion

    #endregion

    #region DataMember bool IsGlobal

    const string PN_ISGLOBAL = "IsGlobal";
    [DataMember(Name = PN_ISGLOBAL, EmitDefaultValue = false)]
    bool _isGlobal;
    public bool IsGlobal
    {
      get { return _isGlobal; }
      set { SetValue<bool>(ref _isGlobal, value, PN_ISGLOBAL); }
    }

    #endregion

    #region Validation

    protected override void GetErrors(System.Collections.ObjectModel.Collection<string> errors, string propertyName)
    {
      if (string.IsNullOrWhiteSpace(propertyName) || propertyName == "Id")
      {
        if (Id <= 0) errors.Add("Id is a mandatory field");
      }

      if (string.IsNullOrWhiteSpace(propertyName) || propertyName == "Text")
      {
        if (string.IsNullOrWhiteSpace(Text)) errors.Add("Text is a mandatory field");
      }

      if (string.IsNullOrWhiteSpace(propertyName) || propertyName == "Description")
      {
        if (string.IsNullOrWhiteSpace(Description)) errors.Add("Description is a mandatory field");
      }

      base.GetErrors(errors, propertyName);
    }

    #endregion
  }
}