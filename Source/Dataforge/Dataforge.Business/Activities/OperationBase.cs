﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Activities
{
  [DataContract(IsReference = true, Namespace = Namespaces.Dataforge)]
  [Table(Schema = "Dataforge", FlattenToSubClass = true)]
  public abstract class OperationBase : IdentifiableObject
  {
    #region DataMember bool Visible

    public const string PN_VISIBLE = "Visible";
    [DataMember(Name = PN_VISIBLE, EmitDefaultValue = false)]
    bool _visible = true;
    public bool Visible
    {
      get { return _visible; }
      set { SetValue<bool>(ref _visible, value, PN_VISIBLE); }
    }

    #endregion

    #region DataMember bool IsContextOperation

    public const string PN_ISCONTEXTOPERATION = "IsContextOperation";
    [DataMember(Name = PN_ISCONTEXTOPERATION, EmitDefaultValue = false)]
    bool _isContextOperation;
    [Column]
    public bool IsContextOperation
    {
      get { return _isContextOperation; }
      set { SetValue<bool>(ref _isContextOperation, value, PN_ISCONTEXTOPERATION); }
    }

    #endregion

    #region Type ContextType

    #region DataMember string ContextTypeName

    public const string PN_CONTEXTTYPENAME = "ContextTypeName";
    [DataMember(Name = PN_CONTEXTTYPENAME, EmitDefaultValue = false)]
    string _contextTypeName;
    [Column]
    public string ContextTypeName
    {
      get { return _contextTypeName; }
      set { SetValue<string>(ref _contextTypeName, value, PN_CONTEXTTYPENAME); }
    }

    #endregion

    Type _contextType;
    public Type ContextType
    {
      get
      {
        if (_contextType == null && !string.IsNullOrWhiteSpace(ContextTypeName)) _contextType = Type.GetType(ContextTypeName);
        return _contextType;
      }
      set
      {
        if (ContextType != value)
        {
          _contextType = value;
          if (value == null) ContextTypeName = null;
          else ContextTypeName = string.Format("{0}, {1}", value.FullName, value.Assembly.GetName().Name);
          OnPropertyChanged("ContextType");
        }
      }
    }

    #endregion
  }
}
