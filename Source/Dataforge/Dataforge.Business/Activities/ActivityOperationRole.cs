﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Constants;
using Dataforge.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Activities
{
  [DataContract(IsReference = true, Namespace = Namespaces.Dataforge)]
  [Table(Schema = "Dataforge", OwnerColumn = "ActivityOperation", OwnerType = typeof(ActivityOperation), ReferenceColumn = "Role")]
  public class ActivityOperationRole : AssociativeObject<Role>
  {
  }
}
