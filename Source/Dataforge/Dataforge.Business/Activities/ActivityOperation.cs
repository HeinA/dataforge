﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using Dataforge.Business.Data;
using Dataforge.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Activities
{
  [DataContract(IsReference = true, Namespace = Namespaces.Dataforge)]
  [Table(Schema = "Dataforge", OwnerColumn = "Activity", OwnerType = typeof(Activity), ReferenceColumn = "Operation")]
  public class ActivityOperation : AssociativeObject<Operation>, IIdentifiableObject
  {
    #region Constructors

    public Activity Activity
    {
      get { return (Activity)Owner; }
    }

    #endregion

    #region DataMember int SortOrder

    public const string PN_SORTORDER = "SortOrder";
    [DataMember(Name = PN_SORTORDER, EmitDefaultValue = false)]
    int _sortOrder;
    [Column(SortOrder = 0)]
    public int SortOrder
    {
      get { return _sortOrder; }
      set { SetValue<int>(ref _sortOrder, value, PN_SORTORDER); }
    }

    #endregion

    #region DataMember AssociativeObjectCollection<ActivityOperationRole, Role> Roles

    AssociativeObjectCollection<ActivityOperationRole, Role> _roles;
    [Cache]
    public AssociativeObjectCollection<ActivityOperationRole, Role> Roles
    {
      get
      {
        if (_roles == null) using (new EventStateSuppressor(this)) { _roles = new AssociativeObjectCollection<ActivityOperationRole, Role>(this); }
        return _roles;
      }
    }

    #endregion

    protected override void OnDeleteStateChanged()
    {
      base.OnDeleteStateChanged();
    }

    #region IIdentifiableObject

    #region DataMember Guid GlobalIdentifier

    public const string PN_GLOBALIDENTIFIER = "GlobalIdentifier";
    [DataMember(Name = PN_GLOBALIDENTIFIER, EmitDefaultValue = false)]
    Guid _globalIdentifier = Guid.NewGuid();
    public Guid GlobalIdentifier
    {
      get { return _globalIdentifier; }
      internal set { SetValue<Guid>(ref _globalIdentifier, value, PN_GLOBALIDENTIFIER); }
    }

    #endregion

    #region DataMember string StringIdentifier

    public const string PN_STRINGIDENTIFIER = "StringIdentifier";
    [DataMember(Name = PN_STRINGIDENTIFIER, EmitDefaultValue = false)]
    string _stringIdentifier;
    public string StringIdentifier
    {
      get { return !string.IsNullOrWhiteSpace(_stringIdentifier) ? _stringIdentifier : GlobalIdentifier.ToString("B").ToUpperInvariant(); }
      set { SetValue<string>(ref _stringIdentifier, value, PN_STRINGIDENTIFIER); }
    }

    #endregion

    #region Equality

    public override bool Equals(object obj)
    {
      if (obj != null && obj.GetType() == this.GetType())
      {
        IIdentifiableObject io = obj as IIdentifiableObject;
        return io.StringIdentifier == this.StringIdentifier;
      }
      return base.Equals(obj);
    }

    public override int GetHashCode()
    {
      return StringIdentifier.GetHashCode();
    }

    #endregion

    #endregion
  }
}
