﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using Dataforge.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Activities
{
  [DataContract(IsReference = true, Namespace = Namespaces.Dataforge)]
  [Table(Schema = "Dataforge")]
  [Cache(Priority = 0)]
  public class OperationGroup : IdentifiableObject, IGlobalAware
  {
    #region DataMember string GroupName

    public const string PN_GROUPNAME = "GroupName";
    [DataMember(Name = PN_GROUPNAME, EmitDefaultValue = false)]
    string _groupName;
    [Column(SortOrder = 0)]
    public string GroupName
    {
      get { return _groupName; }
      set { SetValue<string>(ref _groupName, value, PN_GROUPNAME); }
    }

    #endregion

    #region DataMember bool IsGlobal

    const string PN_ISGLOBAL = "IsGlobal";
    [DataMember(Name = PN_ISGLOBAL, EmitDefaultValue = false)]
    bool _isGlobal;
    public bool IsGlobal
    {
      get { return _isGlobal; }
      set { SetValue<bool>(ref _isGlobal, value, PN_ISGLOBAL); }
    }

    #endregion

    #region DataMember BusinessObjectCollection<Operation> Operations

    [DataMember(Name = "Operations")]
    BusinessObjectCollection<Operation> _operations;
    [Cache]
    public BusinessObjectCollection<Operation> Operations
    {
      get
      {
        if (_operations == null) _operations = new BusinessObjectCollection<Operation>(this);
        return _operations;
      }
    }

    #endregion

    #region Validation

    protected override void GetErrors(System.Collections.ObjectModel.Collection<string> errors, string propertyName)
    {
      if (string.IsNullOrWhiteSpace(propertyName) || propertyName == "GroupName")
      {
        if (string.IsNullOrWhiteSpace(GroupName)) errors.Add("Group Name is a mandatory field");
      }

      base.GetErrors(errors, propertyName);
    }

    #endregion
  }
}
