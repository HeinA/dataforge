﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Constants;
using Dataforge.Business.Data;
using Dataforge.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Activities
{
  [DataContract(IsReference = true, Namespace = Namespaces.Dataforge)]
  [Table(Schema = "Dataforge", OwnerColumn = "Activity", OwnerType = typeof(Activity), ReferenceColumn = "Role")]
  public class ActivityRole : AssociativeObject<Role>
  {
    #region DataMember bool EditPermission

    public const string PN_EDITPERMISSION = "EditPermission";
    [DataMember(Name = PN_EDITPERMISSION, EmitDefaultValue = false)]
    bool _editPermission;
    [Column]
    public bool EditPermission
    {
      get { return _editPermission; }
      set { SetValue<bool>(ref _editPermission, value, PN_EDITPERMISSION); }
    }

    #endregion
  }
}
