﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using Dataforge.Business.Data;
using Dataforge.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Activities
{
  [Cache]
  [DataContract(IsReference = true, Namespace = Namespaces.Dataforge)]
  [Table(Schema = "Dataforge")]
  public class Activity : IdentifiableObject, IGlobalAware
  {
    #region Constructors

    public Activity()
    {
      using (new EventStateSuppressor(this))
      {
        Id = 0;
      }
    }

    #endregion

    #region DataMember string Name

    public const string PN_NAME = "Name";
    [DataMember(Name = PN_NAME, EmitDefaultValue = false)]
    string _name;
    [Column]
    public string Name
    {
      get { return _name; }
      set { SetValue<string>(ref _name, value, PN_NAME); }
    }

    #endregion

    #region DataMember string Description

    public const string PN_DESCRIPTION = "Description";
    [DataMember(Name = PN_DESCRIPTION, EmitDefaultValue = false)]
    string _description;
    [Column]
    public string Description
    {
      get { return _description; }
      set { SetValue<string>(ref _description, value, PN_DESCRIPTION); }
    }

    #endregion

    #region DataMember AssociativeObjectCollection<ActivityOperation, Operation> Operations

    AssociativeObjectCollection<ActivityOperation, Operation> _operations;
    [Cache]
    public AssociativeObjectCollection<ActivityOperation, Operation> Operations
    {
      get
      {
        if (_operations == null) using (new EventStateSuppressor(this)) { _operations = new AssociativeObjectCollection<ActivityOperation, Operation>(this); }
        return _operations;
      }
    }

    #endregion

    #region DataMember AssociativeObjectCollection<ActivityRole, Role> Roles

    AssociativeObjectCollection<ActivityRole, Role> _roles;
    [Cache]
    public AssociativeObjectCollection<ActivityRole, Role> Roles
    {
      get
      {
        if (_roles == null) using (new EventStateSuppressor(this)) { _roles = new AssociativeObjectCollection<ActivityRole, Role>(this); }
        return _roles;
      }
    }

    #endregion

    #region bool CanEdit

    public bool CanEdit
    {
      get
      {
        foreach (Role r in SecurityContext.User.Roles)
        {
          if (Roles.Contains(r) && this.GetAssociativeObject<ActivityRole>(r).EditPermission) return true;
        }
        return false;
      }
    }

    #endregion

    #region bool CanView

    public bool CanView
    {
      get
      {
        foreach (Role r in SecurityContext.User.Roles)
        {
          if (Roles.Contains(r)) return true;
        }
        return false;
      }
    }

    #endregion

    public bool CanExecute(Operation op)
    {
      ActivityOperation aop = this.GetAssociativeObject<ActivityOperation>(op);
      foreach (Role r in SecurityContext.User.Roles)
      {
        if (aop.Roles.Contains(r)) return true;
      }
      return false;
    }

    #region DataMember bool IsGlobal

    const string PN_ISGLOBAL = "IsGlobal";
    [DataMember(Name = PN_ISGLOBAL, EmitDefaultValue = false)]
    bool _isGlobal;
    public bool IsGlobal
    {
      get { return _isGlobal; }
      set { SetValue<bool>(ref _isGlobal, value, PN_ISGLOBAL); }
    }

    #endregion

    #region Validation

    protected override void GetErrors(System.Collections.ObjectModel.Collection<string> errors, string propertyName)
    {
      if (string.IsNullOrWhiteSpace(propertyName) || propertyName == "Id")
      {
        if (Id <= 0) errors.Add("Id is a mandatory field.");
      }

      if (string.IsNullOrWhiteSpace(propertyName) || propertyName == "Name")
      {
        if (string.IsNullOrWhiteSpace(Name)) errors.Add("Name is a mandatory field.");
      }

      base.GetErrors(errors, propertyName);
    }

    #endregion
  }
}
