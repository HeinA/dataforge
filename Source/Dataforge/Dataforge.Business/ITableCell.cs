﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business
{
  public interface ITableCell
  {
    int RowId
    {
      get;
      //set;
    }

    void SetRowId(int id);

    BusinessObject BusinessObjectRow
    {
      get;
      set;
    }

    int ColumnId
    {
      get;
      //set;
    }

    void SetColumnId(int id);

    BusinessObject BusinessObjectColumn
    {
      get;
      set;
    }
  }
}
