﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Constants;
using Dataforge.Business.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Documents
{
  [Table(Schema="Dataforge")]
  [DataContract(IsReference = true, Namespace = Namespaces.Dataforge)]
  public abstract class Document : BusinessObject
  {
    #region int DocumentTypeId

    [Column(Name = "DocumentType")]
    public abstract int DocumentTypeId
    {
      get;
    }

    #endregion

    #region DataMember DateTime DocumentDate

    public const string PN_DOCUMENTDATE = "DocumentDate";
    [DataMember(Name = PN_DOCUMENTDATE, EmitDefaultValue = false)]
    DateTime _documentDate = DateTime.Now;
    [Search(Alias = "Date", Format = "dd MMM yyyy")]
    [Column]
    public DateTime DocumentDate
    {
      get { return _documentDate; }
      set { SetValue<DateTime>(ref _documentDate, value, PN_DOCUMENTDATE); }
    }

    #endregion

    #region DataMember string DocumentNumber

    public const string PN_DOCUMENTNUMBER = "DocumentNumber";
    [DataMember(Name = PN_DOCUMENTNUMBER, EmitDefaultValue = false)]
    string _documentNumber;
    [Column(SortOrder = 0, SortDirection = SortDirection.Descending)]
    [Search(Alias = "Document Number")]
    public string DocumentNumber
    {
      get { return _documentNumber; }
      set { SetValue<string>(ref _documentNumber, value, PN_DOCUMENTNUMBER); }
    }

    #endregion
  }
}
