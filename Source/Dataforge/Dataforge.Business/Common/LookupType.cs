﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Business.Common
{
  [DataContract(IsReference = true, Namespace = Namespaces.Dataforge)]
  [Table(Schema = "Dataforge", Global = true)]
  [Cache]
  public class LookupType : BusinessObject
  {
    #region DataMember string Text

    public const string PN_TEXT = "Text";
    [DataMember(Name = PN_TEXT, EmitDefaultValue = false)]
    string _text;
    [Column]
    public string Text
    {
      get { return _text; }
      set { SetValue<string>(ref _text, value, PN_TEXT); }
    }

    #endregion
  }
}
