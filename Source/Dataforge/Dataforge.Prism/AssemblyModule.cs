﻿using Dataforge.Business.Security;
using Dataforge.Prism.Events;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism
{
  public abstract class AssemblyModule : IModule
  {
    #region IEventAggregator EventAggregator

    [Import]
    IEventAggregator _eventAggregator = null;
    protected IEventAggregator EventAggregator
    {
      get { return _eventAggregator; }
    }

    #endregion

    #region IRegionManager RegionManager

    [Import]
    IRegionManager _regionManager = null;
    protected IRegionManager RegionManager
    {
      get { return _regionManager; }
    }

    #endregion

    #region void Initialize()

    public virtual void Initialize()
    {
      #region Event Subscriptions

      EventAggregator.GetEvent<SecurityContextChangedEvent>().Subscribe(OnSecurityContextChanged, ThreadOption.UIThread);

      #endregion
    }

    #endregion

    #region void OnSecurityContextChanged(User user)

    void OnSecurityContextChanged(User user)
    {
      if (user != null)
      {
        OnLogin(user);
      }
      else
      {
        OnLogout();
      }
    }

    #endregion

    #region void OnLogin(User user)

    protected virtual void OnLogin(User user)
    {
    }

    #endregion

    #region void OnLogout()

    protected virtual void OnLogout()
    {
    }

    #endregion
  }
}
