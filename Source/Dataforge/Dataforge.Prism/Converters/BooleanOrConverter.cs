﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Dataforge.Prism.Converters
{
  public class BooleanOrConverter : IMultiValueConverter
  {
    //public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    //{
    //  foreach (object value in values)
    //  {
    //    if ((bool)value == true)
    //    {
    //      return true;
    //    }
    //  }
    //  return false;
    //}

    public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
    {
      return values.OfType<bool>().Any(value => value);
    }

    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
    {
      throw new NotSupportedException();
    }
  }
}
