﻿using Dataforge.Prism.Views;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace Dataforge.Prism.Converters
{
  public class OperationsCollectionToHiddenConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      IEnumerable<OperationViewModel> col = value as IEnumerable<OperationViewModel>;
      int count = col.Count(o => o.Visibility == Visibility.Visible);

      if (count == 0)
      {
        return Visibility.Collapsed;
      }
      else
      {
        return Visibility.Visible;
      }
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      throw new NotImplementedException();
    }
  }
}
