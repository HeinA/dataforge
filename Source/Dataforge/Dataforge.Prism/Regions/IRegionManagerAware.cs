﻿using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Regions
{
  public interface IRegionManagerAware
  {
    IRegionManager LocalRegionManager { get; set; }
  }
}
