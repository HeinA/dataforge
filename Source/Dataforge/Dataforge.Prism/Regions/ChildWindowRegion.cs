﻿using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Dataforge.Prism.Regions
{
  public class ChildWindowRegion : SingleActiveRegion
  {
    public override IRegionManager Add(object view, string viewName, bool createRegionManagerScope)
    {
      if (this.Views.Count() != 0) throw new InvalidOperationException("Only one ChildWindow allowed at a time. ");
      return base.Add(view, viewName, createRegionManagerScope);
    }
  }
}
