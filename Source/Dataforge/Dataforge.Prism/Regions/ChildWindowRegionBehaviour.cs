﻿using Dataforge.Prism.Extensions;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.Prism.Regions.Behaviors;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Xceed.Wpf.Toolkit;

namespace Dataforge.Prism.Regions
{
  public class ChildWindowRegionBehaviour : RegionBehavior, IHostAwareRegionBehavior
  {
    public const string BehaviourKey = "ChildWindowRegionBehaviour";

    #region ChildWindow ChildWindow

    ChildWindow _childWindow;
    ChildWindow ChildWindow
    {
      get { return _childWindow; }
      set { _childWindow = value; }
    }

    #endregion

    #region DependencyObject HostControl

    public DependencyObject HostControl
    {
      get { return ChildWindow; }

      set { ChildWindow = value as ChildWindow; }
    }

    #endregion

    protected override void OnAttach()
    {
      Region.Views.CollectionChanged += Views_CollectionChanged;
      ChildWindow.Closed += ChildWindow_Closed;
    }

    void ChildWindow_Closed(object sender, EventArgs e)
    {
      if (Region.Views.Contains(Dialog)) Region.Remove(Dialog);
    }

    Control _dialog;
    Control Dialog
    {
      get { return _dialog; }
      set { _dialog = value; }
    }

    void Views_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      switch (e.Action)
      {
        case NotifyCollectionChangedAction.Add:
          Dialog = (Control)e.NewItems[0];
          //ChildWindow.Focus();
          //Dialog.Focus();
          //FrameworkElement uie = GetLogicalChildCollection<FrameworkElement>(Dialog).Where(p => FocusExtension.GetIsFocused(p) == true).FirstOrDefault();
          //ChildWindow.FocusedElement = uie;
          ChildWindow.Content = Dialog;
          ChildWindow.DataContext = Dialog.DataContext;
          ChildWindow.WindowState = Xceed.Wpf.Toolkit.WindowState.Open;
          break;

        case NotifyCollectionChangedAction.Remove:
          ChildWindow.Content = null;
          ChildWindow.DataContext = null;
          break;
      }
    }

    public List<T> GetLogicalChildCollection<T>(object parent) where T : DependencyObject
    {
      List<T> logicalCollection = new List<T>();
      GetLogicalChildCollection(parent as DependencyObject, logicalCollection);
      return logicalCollection;
    }

    private void GetLogicalChildCollection<T>(DependencyObject parent, List<T> logicalCollection) where T : DependencyObject
    {
      IEnumerable children = LogicalTreeHelper.GetChildren(parent);
      foreach (object child in children)
      {
        if (child is DependencyObject)
        {
          DependencyObject depChild = child as DependencyObject;
          if (child is T)
          {
            logicalCollection.Add(child as T);
          }
          GetLogicalChildCollection(depChild, logicalCollection);
        }
      }
    }
  }
}
