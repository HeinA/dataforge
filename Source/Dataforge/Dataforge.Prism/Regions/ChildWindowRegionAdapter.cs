﻿using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Xceed.Wpf.Toolkit;

namespace Dataforge.Prism.Regions
{
  public class ChildWindowRegionAdapter : RegionAdapterBase<ChildWindow>
  {
    public ChildWindowRegionAdapter(IRegionBehaviorFactory regionBehaviorFactory)
      :base(regionBehaviorFactory)
    {
    }

    protected override void Adapt(IRegion region, ChildWindow regionTarget)
    {
    }

    protected override IRegion CreateRegion()
    {
      return new ChildWindowRegion();
    }

    protected override void AttachBehaviors(IRegion region, ChildWindow regionTarget)
    {
      if (region == null) throw new System.ArgumentNullException("region");
      IRegionBehaviorFactory factory = ServiceLocator.Current.GetInstance<IRegionBehaviorFactory>();
      region.Behaviors.Add(ChildWindowRegionBehaviour.BehaviourKey, new ChildWindowRegionBehaviour() { HostControl = regionTarget });
      base.AttachBehaviors(region, regionTarget);
    }
  }
}
