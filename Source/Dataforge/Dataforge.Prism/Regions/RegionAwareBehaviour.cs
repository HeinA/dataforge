﻿using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Dataforge.Prism.Regions
{
  [Export]
  [PartCreationPolicy(CreationPolicy.NonShared)]
  public class RegionAwareBehaviour : RegionBehavior
  {
    /// <summary>
    /// The key to identify this behaviour.
    /// </summary>
    public const string RegionAwareBehaviourKey = "RegionAwareBehaviour";

    /// <summary>
    /// Override this method to perform the logic after the behaviour has been attached.
    /// </summary>
    protected override void OnAttach()
    {
      Region.Views.CollectionChanged += RegionViewsChanged;
    }

    private void RegionViewsChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      if (e.NewItems != null)
        foreach (var item in e.NewItems)
          MakeViewAware((FrameworkElement)item);
    }

    private void MakeViewAware(FrameworkElement view)
    {
      var scope = view.DataContext as IRegionManagerAware;
      if (scope != null)
      {
        IRegionManager regionManager = Microsoft.Practices.Prism.Regions.RegionManager.GetRegionManager(view);
        scope.LocalRegionManager = regionManager;
      }
    }
  }
}
