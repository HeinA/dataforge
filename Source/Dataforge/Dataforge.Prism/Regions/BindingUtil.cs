﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace Dataforge.Prism.Regions
{
  internal static class BindingUtil
  {
    /// <summary>
    /// Recursively resets all ElementName bindings on the submitted object
    /// and its children.
    /// </summary>
    public static void ResetElementNameBindings(this DependencyObject obj)
    {
      IEnumerable boundProperties = obj.GetDataBoundProperties();
      foreach (DependencyProperty dp in boundProperties)
      {
        Binding binding = BindingOperations.GetBinding(obj, dp);
        if (binding != null && !String.IsNullOrEmpty(binding.ElementName)) //binding itself should never be null, but anyway
        {
          //just updating source and/or target doesn’t do the trick – reset the binding
          BindingOperations.ClearBinding(obj, dp);
          BindingOperations.SetBinding(obj, dp, binding);
        }
      }

      int count = VisualTreeHelper.GetChildrenCount(obj);
      for (int i = 0; i < count; i++)
      {
        //process child items recursively
        DependencyObject childObject = VisualTreeHelper.GetChild(obj, i);
        ResetElementNameBindings(childObject);
      }
    }


    public static IEnumerable GetDataBoundProperties(this DependencyObject element)
    {
      LocalValueEnumerator lve = element.GetLocalValueEnumerator();

      while (lve.MoveNext())
      {
        LocalValueEntry entry = lve.Current;
        if (BindingOperations.IsDataBound(element, entry.Property))
        {
          yield return entry.Property;
        }
      }
    }

    //public List<T> GetLogicalChildCollection<T>(object parent) where T : DependencyObject
    //{
    //  List<T> logicalCollection = new List<T>();
    //  GetLogicalChildCollection(parent as DependencyObject, logicalCollection);
    //  return logicalCollection;
    //}

    //private void GetLogicalChildCollection<T>(DependencyObject parent, List<T> logicalCollection) where T : DependencyObject
    //{
    //  IEnumerable children = LogicalTreeHelper.GetChildren(parent);
    //  foreach (object child in children)
    //  {
    //    if (child is DependencyObject)
    //    {
    //      DependencyObject depChild = child as DependencyObject;
    //      if (child is T)
    //      {
    //        logicalCollection.Add(child as T);
    //      }
    //      GetLogicalChildCollection(depChild, logicalCollection);
    //    }
    //  }
    //}
  }
}
