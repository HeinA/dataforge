﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Globalization;
using System.Net.NetworkInformation;
using System.Net;
using System.Threading;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Dataforge.Business.Security;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Prism.PubSubEvents;
using Dataforge.Business.Service;
using Dataforge.Business;
using Dataforge.Prism.Events;

namespace Dataforge.Prism
{
  public delegate void ActionDelegate<T>(T proxy);
  public delegate TReturn FunctionDelegate<T, TReturn>(T proxy);

  public interface IDisposableWrapper<T> : IDisposable
  {
    //T Instance { get; }

    /// <summary>
    /// Callse the server with transient error handeling
    /// </summary>
    /// <param name="codeBlock">Delegate to execute</param>
    void ExecuteAction(ActionDelegate<T> codeBlock);

    /// <summary>
    /// Callse the server with transient error handeling
    /// </summary>
    /// <typeparam name="TReturn">Return Type</typeparam>
    /// <param name="codeBlock">Delegate to execute</param>
    /// <returns>Results of the server call</returns>
    TReturn ExecuteFunction<TReturn>(FunctionDelegate<T, TReturn> codeBlock);
  }

  public class DisposableWrapper<T> : IDisposableWrapper<T> where T : class
  {
    internal T Instance { get; private set; }
    ChannelFactory<T> _factory;
    public DisposableWrapper(ChannelFactory<T> factory)
    {
      _factory = factory;
      if (_factory != null) Instance = _factory.CreateChannel();
    }

    public void ExecuteAction(ActionDelegate<T> codeBlock)
    {
      IClientChannel client = this.Instance as IClientChannel;
      int retries = 5;
      Exception lastException = null;

      while (retries-- > 0)
      {
        try
        {
          lastException = null;
          codeBlock(Instance);
          return;
        }
        catch (TimeoutException ex)
        {
          lastException = ex;
          Thread.Sleep(500);
        }
        catch (FaultException<AuthorizationFault>)
        {
          HandleAuthorizationRequired(codeBlock);
          return;
        }
        catch (FaultException<SessionExpiredFault>)
        {
          HandleSessionExpired();
          return;
        }
        catch (FaultException)
        {
          throw;
        }
        catch (CommunicationException ex)
        {
          lastException = ex;
          Thread.Sleep(500);
        }
        catch
        {
          throw;
        }
      }

      throw lastException;
    }

    public TReturn ExecuteFunction<TReturn>(FunctionDelegate<T, TReturn> codeBlock)
    {
      IClientChannel client = this.Instance as IClientChannel;
      int retries = 5;
      Exception lastException = null;

      while (retries-- > 0)
      {
        try
        {
          lastException = null;
          return codeBlock(Instance);
        }
        catch (TimeoutException ex)
        {
          lastException = ex;
          Thread.Sleep(500);
        }
        catch (FaultException<AuthorizationFault>)
        {
          return HandleAuthorizationRequired(codeBlock);
        }
        catch (FaultException<SessionExpiredFault>)
        {
          HandleSessionExpired();
        }
        catch (FaultException)
        {
          throw;
        }
        catch (CommunicationException ex)
        {
          lastException = ex;
          Thread.Sleep(500);
        }
        catch
        {
          throw;
        }
      }

      throw lastException;
    }

    void HandleSessionExpired()
    {
      Guid session = SecurityContext.SessionToken;

      IEventAggregator ea = ServiceLocator.Current.GetInstance<IEventAggregator>();
      ea.GetEvent<SessionExpiredEvent>().Publish(EventArgs.Empty);

      if (SecurityContext.SessionToken == session)
      {
        Dataforge.Business.Security.SecurityContext.RemoveAuthentication(SecurityContext.SessionToken);
        ea.GetEvent<SecurityContextChangedEvent>().Publish(null);
        throw new MessageException("You have been logged out.");
      }
    }

    TReturn HandleAuthorizationRequired<TReturn>(FunctionDelegate<T, TReturn> codeBlock)
    {
      try
      {
        AuthorizationRequiredEventArgs args = new AuthorizationRequiredEventArgs();
        IEventAggregator ea = ServiceLocator.Current.GetInstance<IEventAggregator>();
        ea.GetEvent<AuthorizationRequiredEvent>().Publish(args);

        if (args.SecurityToken == Guid.Empty)
          throw new MessageException("Authorization failed.");

        SecurityContext.SetImpersonationToken(new AuthenticationResult(args.SecurityToken, args.User));

        //retry action
        return codeBlock(Instance);
      }
      finally
      {
        SecurityContext.RemoveImpersonation(SecurityContext.ImpersonationToken);
      }
    }

    void HandleAuthorizationRequired(ActionDelegate<T> codeBlock)
    {
      try
      {
        AuthorizationRequiredEventArgs args = new AuthorizationRequiredEventArgs();
        IEventAggregator ea = ServiceLocator.Current.GetInstance<IEventAggregator>();
        ea.GetEvent<AuthorizationRequiredEvent>().Publish(args);

        if (args.SecurityToken == Guid.Empty)
          throw new MessageException("Authorization failed.");

        SecurityContext.SetImpersonationToken(new AuthenticationResult(args.SecurityToken, args.User));

        //retry action
        codeBlock(Instance);
      }
      finally
      {
        SecurityContext.RemoveImpersonation(SecurityContext.ImpersonationToken);
      }
    }

    protected virtual void Dispose(bool disposing)
    {
      if (disposing)
      {
        (Instance as IDisposable).Dispose();
      }
    }

    public void Dispose()
    {
      try
      {
        Dispose(true);
        GC.SuppressFinalize(this);
      }
      catch
      {
      }
      Instance = default(T);
    }
  }

  public class ClientWrapper<TProxy> : DisposableWrapper<TProxy>
      where TProxy : class
  {
    public ClientWrapper(ChannelFactory<TProxy> proxy) : base(proxy) { }
    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        if (this.Instance != null)
        {
          if ((this.Instance as IClientChannel).State == CommunicationState.Faulted)
          {
            (this.Instance as IClientChannel).Abort();
          }
          else
          {
            (this.Instance as IClientChannel).Close();
          }
        }
      }

      base.Dispose(disposing);
    }
  }

  public static class WCFExtensions
  {
    public static IDisposableWrapper<TProxy> Wrap<TProxy>(
        this ChannelFactory<TProxy> proxy)
        where TProxy : class
    {

      return new ClientWrapper<TProxy>(proxy);
    }
  }

  public static class ServiceFactory
  {
    static object _lock = new object();

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")]
    public static IDisposableWrapper<T> GetService<T>()
      where T : class
    {
      lock (_lock)
      {
        ChannelFactory<T> factory = null;
        if (!FactoryDictionary.ContainsKey(typeof(T).FullName))
        {
          factory = new ChannelFactory<T>(typeof(T).FullName);
          factory.Open();
          FactoryDictionary.Add(typeof(T).FullName, factory);
        }
        else
        {
          factory = (ChannelFactory<T>)FactoryDictionary[typeof(T).FullName];
        }
        IDisposableWrapper<T> wrapper = WCFExtensions.Wrap<T>(factory);
        return wrapper;
      }
    }

    static Dictionary<string, object> _factoryDictionary = new Dictionary<string, object>();
    static Dictionary<string, object> FactoryDictionary
    {
      get { return ServiceFactory._factoryDictionary; }
    }
  }
}
