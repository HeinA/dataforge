﻿using Dataforge.Business;
using Dataforge.Business.Activities;
using Dataforge.Business.Collections;
using Dataforge.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Events
{
  public class OperationOverrideEventArgs
  {
    public OperationOverrideEventArgs(Operation operation, BasicCollection<BusinessObject> operationTargets, bool visible)
    {
      _operation = operation;
      _operationTargets = operationTargets;
      _visible = visible;
    }

    Operation _operation;
    public Operation Operation
    {
      get { return _operation; }
    }

    BasicCollection<BusinessObject> _operationTargets;
    public BasicCollection<BusinessObject> OperationTargets
    {
      get { return _operationTargets; }
    }

    string _text = string.Empty;
    public string Text
    {
      get { return _text; }
      set { _text = value; }
    }

    bool _visible;
    public bool Visible
    {
      get { return _visible; }
      set { _visible = value; }
    }
  }
}
