﻿using Dataforge.Business.Activities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Events
{
  public class OperationReplaceEventArgs
  {
    public OperationReplaceEventArgs(Operation operation)
    {
      _operation = operation;
    }

    Operation _operation;
    public Operation Operation
    {
      get { return _operation; }
    }

    Collection<Operation> _replacementOperations;
    public Collection<Operation> ReplacementOperations
    {
      get { return _replacementOperations ?? (_replacementOperations = new Collection<Operation>()); }
    }
  }
}
