﻿using Microsoft.Practices.Prism.PubSubEvents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Events
{
  public class OperationOverrideEvent : PubSubEvent<OperationOverrideEventArgs>
  {
  }
}
