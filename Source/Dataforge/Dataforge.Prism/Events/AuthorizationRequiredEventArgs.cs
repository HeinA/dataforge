﻿using Dataforge.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Events
{
  public class AuthorizationRequiredEventArgs
  {
    Guid _securityToken = Guid.Empty;
    public Guid SecurityToken
    {
      get { return _securityToken; }
      set { _securityToken = value; }
    }

    User _user = null;
    public User User
    {
      get { return _user; }
      set { _user = value; }
    }
  }
}
