﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Logging
{
  public class XceedFilter : EventTypeFilter
  {
    private const string Xceed = "Xceed.";

    public XceedFilter(SourceLevels level)
      : base(level)
    {
    }

    public override bool ShouldTrace(TraceEventCache cache, string source, TraceEventType eventType, int id, string formatOrMessage, object[] args, object data1, object[] data)
    {
      if (IsXceedCall()) return false;

      return base.ShouldTrace(cache, source, eventType, id, formatOrMessage, args, data1, data);
    }

    protected virtual bool IsXceedCall()
    {
      var stackTrace = new StackTrace();
      foreach (var frame in stackTrace.GetFrames())
      {
        try
        {
          if (frame.GetMethod().ReflectedType.FullName.StartsWith(Xceed))
          {
            return true;
          }
        }
        catch
        {
        }
      }

      return false;
    }
  }
}
