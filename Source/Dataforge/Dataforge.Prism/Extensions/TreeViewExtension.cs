﻿using Dataforge.Prism.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace Dataforge.Prism.Extensions
{
  public static class TreeViewExtension
  {
    public static readonly DependencyProperty SelectItemOnRightClickProperty = DependencyProperty.RegisterAttached(
               "SelectItemOnRightClick",
               typeof(bool),
               typeof(TreeViewExtension),
               new UIPropertyMetadata(false, OnSelectItemOnRightClickChanged));

    public static bool GetSelectItemOnRightClick(DependencyObject d)
    {
      return (bool)d.GetValue(SelectItemOnRightClickProperty);
    }

    public static void SetSelectItemOnRightClick(DependencyObject d, bool value)
    {
      d.SetValue(SelectItemOnRightClickProperty, value);
    }

    private static void OnSelectItemOnRightClickChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      bool selectItemOnRightClick = (bool)e.NewValue;

      TreeView treeView = d as TreeView;
      if (treeView != null)
      {
        if (selectItemOnRightClick)
        {
          treeView.PreviewMouseRightButtonDown += OnPreviewMouseRightButtonDown;
          treeView.SelectedItemChanged += TreeView_SelectedItemChanged;
        }
        else
        {
          treeView.PreviewMouseRightButtonDown -= OnPreviewMouseRightButtonDown;
          treeView.SelectedItemChanged -= TreeView_SelectedItemChanged;
        }
      }
    }

    static void TreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
    {
      TreeView treeView = sender as TreeView;
      IFrameViewModel vm = treeView.DataContext as IFrameViewModel;
      vm.ActiveViewModel = e.NewValue as ITreeNodeViewModel;
      vm.SelectedViewModels.Add(e.NewValue as ITreeNodeViewModel);
      for (int i = vm.SelectedViewModels.Count - 1; i >= 0; i--)
      {
        var svm = vm.SelectedViewModels[i];
        if (!svm.Equals(e.NewValue)) 
          vm.SelectedViewModels.RemoveAt(i);
      }
    }

    private static void OnPreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
    {
      TreeViewItem treeViewItem = VisualUpwardSearch(e.OriginalSource as DependencyObject);
      TreeView treeView = sender as TreeView;


      if (treeViewItem != null)
      {
        treeView.Focus();
        treeViewItem.Focus();
        e.Handled = true;
      }

      IOperationTarget ot = treeView.DataContext as IOperationTarget;
      ot.RaiseCanExecuteChanged();
    }

    public static TreeViewItem VisualUpwardSearch(DependencyObject source)
    {
      while (source != null && !(source is TreeViewItem))
        source = VisualTreeHelper.GetParent(source);

      return source as TreeViewItem;
    }
  }
}


