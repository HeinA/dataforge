﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Dataforge.Prism.Extensions
{
  public static class UpdateSourceOnKeypressExtension
  {
    public static bool GetIsActive(DependencyObject obj)
    {
      return (bool)obj.GetValue(SetIsActiveProperty);
    }
    public static void SetIsActive(DependencyObject obj, bool value)
    {
      obj.SetValue(SetIsActiveProperty, value);
    }

    public static readonly DependencyProperty SetIsActiveProperty = DependencyProperty.RegisterAttached("IsActive", typeof(bool), typeof(UpdateSourceOnKeypressExtension), new PropertyMetadata(false, OnIsActive));

    private static void OnIsActive(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      bool isActive = (bool)e.NewValue;
      if (isActive)
      {
        TextBox tb = (TextBox)d;
        tb.TextChanged += (sender, args) =>
          {
            BindingExpression ex = tb.GetBindingExpression(TextBox.TextProperty);
            ex.UpdateSource();
          };
      }
    }
  }
}
