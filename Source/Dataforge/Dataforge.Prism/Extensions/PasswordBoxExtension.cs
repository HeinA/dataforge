﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Dataforge.Prism.Extensions
{
  public static class PasswordBoxExtension
  {
    public static string GetEncryptedPassword(DependencyObject obj)
    {
      return (string)obj.GetValue(EncryptedPasswordProperty);
    }

    public static void SetEncryptedPassword(DependencyObject obj, string value)
    {
      obj.SetValue(EncryptedPasswordProperty, value);
    }

    public static readonly DependencyProperty EncryptedPasswordProperty =
        DependencyProperty.RegisterAttached("EncryptedPassword", typeof(string), typeof(PasswordBoxExtension));
  }
}
