﻿using Dataforge.Prism.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Dataforge.Prism.Extensions
{
  public static class FocusExtension
  {
    public static bool GetIsFocused(DependencyObject obj)
    {
      return (bool)obj.GetValue(IsFocusedProperty);
    }

    public static void SetIsFocused(DependencyObject obj, bool value)
    {
      obj.SetValue(IsFocusedProperty, value);
    }

    public static readonly DependencyProperty IsFocusedProperty = DependencyProperty.RegisterAttached("IsFocused", typeof(bool), typeof(FocusExtension), new UIPropertyMetadata(false, OnIsFocusedPropertyChanged));

    private static void OnIsFocusedPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var fwe = (FrameworkElement)d;
      //ISelectableViewModel svm = fwe.DataContext as ISelectableViewModel;
      if ((bool)e.NewValue)
      {
        //if (svm != null) svm.IsFocused = true;
        fwe.Focus();
        SetIsFocused(d, false);
      }
      //else
      //{
      //  if (svm != null) svm.IsFocused = false;
      //}
    }

    public static bool GetIsFocusedAsync(DependencyObject obj)
    {
      return (bool)obj.GetValue(IsFocusedAsyncProperty);
    }

    public static void SetIsFocusedAsync(DependencyObject obj, bool value)
    {
      obj.SetValue(IsFocusedAsyncProperty, value);
    }

    public static readonly DependencyProperty IsFocusedAsyncProperty = DependencyProperty.RegisterAttached("IsFocusedAsync", typeof(bool), typeof(FocusExtension), new UIPropertyMetadata(false, OnIsFocusedAsyncPropertyChanged));

    private static void OnIsFocusedAsyncPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var fwe = (FrameworkElement)d;
      if ((bool)e.NewValue)
      {
        fwe.Dispatcher.BeginInvoke(new System.Action(() =>
        {
          fwe.Focus();
          SetIsFocusedAsync(d, false);
        }), DispatcherPriority.Background, null);
      }
    }
  }

}
