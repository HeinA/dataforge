﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Dataforge.Prism.Controls
{
  public class OperationToolbar : ToolBar
  {
    public OperationToolbar()
    {
      this.Style = System.Windows.Application.Current.Resources["OperationsToolBar"] as System.Windows.Style;
    }
  }
}
