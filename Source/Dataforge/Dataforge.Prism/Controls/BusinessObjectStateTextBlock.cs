﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;

namespace Dataforge.Prism.Controls
{
  public class BusinessObjectStateTextBlock : TextBlock
  {
    public BusinessObjectStateTextBlock()
    {
      BindingOperations.SetBinding(this, TextBlock.ForegroundProperty, new Binding("Foreground"));
      BindingOperations.SetBinding(this, TextBlock.TextDecorationsProperty, new Binding("TextDecoration"));
      BindingOperations.SetBinding(this, TextBlock.FontWeightProperty, new Binding("FontWeight"));
      this.Padding = new System.Windows.Thickness(0, 0, 3, 0);
    }
  }
}
