﻿using Dataforge.Prism.Commands;
using Dataforge.Prism.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Interactivity;

namespace Dataforge.Prism.Controls
{
  public class OperationsListView : ListView
  {
    public OperationsListView()
    {
      System.Windows.Interactivity.TriggerCollection triggerCollection = Interaction.GetTriggers(this);
      System.Windows.Interactivity.EventTrigger trigger = new System.Windows.Interactivity.EventTrigger("ContextMenuOpening");
      InteractiveCommand commandAction = new InteractiveCommand();
      BindingOperations.SetBinding(commandAction, InteractiveCommand.CommandProperty, new Binding("ContextMenuOpening"));
      trigger.Actions.Add(commandAction);
      triggerCollection.Add(trigger);

      this.Style = System.Windows.Application.Current.Resources["OperationsListView"] as System.Windows.Style;
    }
  }
}
