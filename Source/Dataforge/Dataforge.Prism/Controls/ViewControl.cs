﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Interactivity;

namespace Dataforge.Prism.Controls
{
  public class ViewControl : System.Windows.Controls.UserControl
  {
    public ViewControl()
    {
      TriggerCollection triggerCollection = Interaction.GetTriggers(this);
      EventTrigger trigger = new EventTrigger("Loaded");
      InvokeCommandAction commandAction = new InvokeCommandAction();
      BindingOperations.SetBinding(commandAction, InvokeCommandAction.CommandProperty, new Binding("OnLoadedCommand"));
      trigger.Actions.Add(commandAction);
      triggerCollection.Add(trigger);
    }
  }
}
