﻿using System.Windows.Input;
using Dataforge.Prism.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Interactivity;
using System.Windows.Media;
using System.Windows;
using Dataforge.Prism.Commands;

namespace Dataforge.Prism.Controls
{
  public class OperationsTreeView : TreeView
  {
    public OperationsTreeView()
    {
      System.Windows.Interactivity.TriggerCollection triggerCollection = Interaction.GetTriggers(this);
      System.Windows.Interactivity.EventTrigger trigger = new System.Windows.Interactivity.EventTrigger("ContextMenuOpening");
      InteractiveCommand commandAction = new InteractiveCommand();
      BindingOperations.SetBinding(commandAction, InteractiveCommand.CommandProperty, new Binding("ContextMenuOpening")); 
      trigger.Actions.Add(commandAction);
      triggerCollection.Add(trigger);

      this.Style = System.Windows.Application.Current.Resources["OperationsTreeView"] as System.Windows.Style;
    }

    protected override void OnSelectedItemChanged(System.Windows.RoutedPropertyChangedEventArgs<object> e)
    {
      var vm = this.DataContext as IMultiModelViewModel;
      if (vm != null)
      {
        vm.SelectedViewModels.Add(e.NewValue as ITreeNodeViewModel);
        vm.SelectedViewModels.Remove(e.OldValue as ITreeNodeViewModel);
        vm.FocusedViewModel = e.NewValue as ITreeNodeViewModel;
      }
      base.OnSelectedItemChanged(e);
    }

    protected override void OnPreviewMouseRightButtonDown(MouseButtonEventArgs e)
    {
      TreeViewItem treeViewItem = VisualUpwardSearch(e.OriginalSource as DependencyObject);
      if (treeViewItem != null)
      { 
        this.Focus();
        treeViewItem.Focus();
        e.Handled = true;
      }

      var ot = this.DataContext as IOperationTarget;
      if (ot != null) ot.RaiseCanExecuteChanged();

      base.OnPreviewMouseRightButtonDown(e);
    }

    public static TreeViewItem VisualUpwardSearch(DependencyObject source)
    {
      while (source != null && !(source is TreeViewItem))
        source = VisualTreeHelper.GetParent(source);

      return source as TreeViewItem;
    }
  }
}
