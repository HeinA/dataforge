﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Dataforge.Prism.Controls
{
  public class VerticalGridSplitter : GridSplitter
  {
    const string RegistryKey = @"GridSplitters\{0}\";

    public VerticalGridSplitter()
    {
      this.Style = System.Windows.Application.Current.Resources["VerticalSplitterStyle"] as System.Windows.Style;

      if (!DesignerProperties.GetIsInDesignMode(this))
      {
        this.Unloaded += (sender, e) =>
        {
          SaveWidth();
        };

        this.Loaded += (sender, e) =>
        {
          LoadWidth();
        };
      }
    }

    void LoadWidth()
    {
      Grid gv = (Grid)this.Parent;
      foreach (var c in gv.ColumnDefinitions)
      {
        if (!string.IsNullOrWhiteSpace(Identifier))
        {
          int header = gv.ColumnDefinitions.IndexOf(c);
          int width = 0;
          if (RegistryHelper.GetRegistryValue<int>(Microsoft.Win32.RegistryHive.CurrentUser, string.Format(RegistryHelper.DataforgeKey, string.Format(RegistryKey, Identifier)), header.ToString(), Microsoft.Win32.RegistryValueKind.DWord, out width))
          {
            c.Width = new GridLength(width);
          }
        }
      }
    }

    void SaveWidth()
    {
      Grid gv = (Grid)this.Parent;
      foreach (var c in gv.ColumnDefinitions)
      {
        if (!string.IsNullOrWhiteSpace(Identifier))
        {
          int header = gv.ColumnDefinitions.IndexOf(c);
          if (!c.Width.IsAuto && !c.Width.IsStar)
          {
            int width = (int)c.Width.Value;
            RegistryHelper.SetRegistryValue<int>(Microsoft.Win32.RegistryHive.CurrentUser, string.Format(RegistryHelper.DataforgeKey, string.Format(RegistryKey, Identifier)), header.ToString(), Microsoft.Win32.RegistryValueKind.DWord, width);
          }
        }
      }
    }

    string _identifier;
    public string Identifier
    {
      get { return _identifier; }
      set
      {
        if (_identifier == value) return;
        _identifier = value;
      }
    }

    public static readonly DependencyProperty IdentifierProperty = DependencyProperty.Register("Identifier", typeof(string), typeof(VerticalGridSplitter), new PropertyMetadata(string.Empty, OnIdentifierChanged));

    private static void OnIdentifierChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      VerticalGridSplitter vgs = d as VerticalGridSplitter;
      vgs.Identifier = (string)e.NewValue;
    }
  }
}
