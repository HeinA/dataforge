﻿using Dataforge.Prism.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Dataforge.Prism.Controls
{
  public class ListBox : System.Windows.Controls.ListBox
  {
    protected override void OnSelectionChanged(SelectionChangedEventArgs e)
    {
      var vm = this.DataContext as IMultiModelViewModel;
      if (vm != null)
      {
        foreach (ISelectableViewModel item in e.RemovedItems)
        {
          vm.SelectedViewModels.Remove(item);
        }
        foreach (ISelectableViewModel item in e.AddedItems)
        {
          if (!vm.SelectedViewModels.Contains(item)) vm.SelectedViewModels.Add(item);
        }

        foreach (var item in this.Items)
        {
          ListBoxItem lbi = (ListBoxItem)this.ItemContainerGenerator.ContainerFromItem(item);
          if (lbi != null && lbi.IsFocused)
          {
            vm.FocusedViewModel = (ISelectableViewModel)item;
            break;
          }
        }
      }
      base.OnSelectionChanged(e);
    }
  }
}
