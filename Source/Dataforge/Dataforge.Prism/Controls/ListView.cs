﻿using Dataforge.Prism.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Dataforge.Prism.Controls
{
  public class ListView : System.Windows.Controls.ListView
  {
    const string RegistryKey = @"ListViews\{0}";

    public ListView()
    {
      if (!DesignerProperties.GetIsInDesignMode(this))
      {
        this.Unloaded += (sender, e) =>
        {
          if (!string.IsNullOrWhiteSpace(Identifier))
          {
            foreach (string header in ColumnDictionary.Keys)
            {
              RegistryHelper.SetRegistryValue<int>(Microsoft.Win32.RegistryHive.CurrentUser, string.Format(RegistryHelper.DataforgeKey, string.Format(RegistryKey, Identifier)), header, Microsoft.Win32.RegistryValueKind.DWord, (int)ColumnDictionary[header].Width);
            }
          }
        };

        this.Loaded += (sender, e) =>
        {
          LoadColumnWidths();
        };
      }
    }

    Dictionary<string, GridViewColumn> _columnDictionary = new Dictionary<string, GridViewColumn>();
    public Dictionary<string, GridViewColumn> ColumnDictionary
    {
      get { return _columnDictionary; }
    }

    void LoadColumnWidths()
    {
      GridView gv = (GridView)this.View;
      foreach (var c in gv.Columns)
      {
        if (!string.IsNullOrWhiteSpace(Identifier))
        {
          string header = (string)c.Header;
          int width = 0;
          if (RegistryHelper.GetRegistryValue<int>(Microsoft.Win32.RegistryHive.CurrentUser, string.Format(RegistryHelper.DataforgeKey, string.Format(RegistryKey, Identifier)), header, Microsoft.Win32.RegistryValueKind.DWord, out width))
          {
            c.Width = width;
          }
        }

        ((INotifyPropertyChanged)c).PropertyChanged += (sender, e) =>
        {
          GridViewColumn gvc = (GridViewColumn)sender;
          if (e.PropertyName == "Width")
          {
            string header1 = (string)gvc.Header;
            if (!ColumnDictionary.ContainsKey(header1)) ColumnDictionary.Add(header1, gvc);
            else ColumnDictionary[header1] = gvc;
          }
        };
      }
    }

    string _identifier;
    public string Identifier
    {
      get { return _identifier; }
      set
      {
        if (_identifier == value) return;
        _identifier = value;
      }
    }

    public static readonly DependencyProperty IdentifierProperty = DependencyProperty.Register("Identifier", typeof(string), typeof(OperationsListView), new PropertyMetadata(string.Empty, OnIdentifierChanged));

    private static void OnIdentifierChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      OperationsListView olv = d as OperationsListView;
      olv.Identifier = (string)e.NewValue;
    }

    protected override void OnSelectionChanged(SelectionChangedEventArgs e)
    {
      var vm = this.DataContext as IMultiModelViewModel;
      if (vm != null)
      {
        foreach (ISelectableViewModel item in e.RemovedItems)
        {
          vm.SelectedViewModels.Remove(item);
        }
        foreach (ISelectableViewModel item in e.AddedItems)
        {
          if (!vm.SelectedViewModels.Contains(item)) vm.SelectedViewModels.Add(item);
        }

        foreach (var item in this.Items)
        {
          ListViewItem lvi = (ListViewItem)this.ItemContainerGenerator.ContainerFromItem(item);
          if (lvi != null && lvi.IsFocused)
          {
            vm.FocusedViewModel = (ISelectableViewModel)item;
            break;
          }
        }
      }
      base.OnSelectionChanged(e);
    }
  }
}
