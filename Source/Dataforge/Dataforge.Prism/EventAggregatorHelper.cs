﻿using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism
{
  class EventAggregatorHelper
  {
    static Dictionary<IRegionManager, IEventAggregator> _localEventAggregatorDictionary = new Dictionary<IRegionManager, IEventAggregator>();

    public static IEventAggregator GetLocalEventAggregator(IRegionManager localRegionManager)
    {
      if (localRegionManager == null) return null;
      if (_localEventAggregatorDictionary.ContainsKey(localRegionManager)) return _localEventAggregatorDictionary[localRegionManager];
      _localEventAggregatorDictionary.Add(localRegionManager, new EventAggregator());
      return _localEventAggregatorDictionary[localRegionManager];
    }
  }
}
