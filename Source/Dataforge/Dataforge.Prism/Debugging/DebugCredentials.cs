﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Debugging
{
#if DEBUG
  public abstract class DebugCredentials
  {
    public abstract string Username { get; }
    public abstract string Password { get; }
  }
#endif
}
