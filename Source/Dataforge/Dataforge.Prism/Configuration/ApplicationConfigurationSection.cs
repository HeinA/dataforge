﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Configuration
{
  public class ApplicationConfigurationSection : ConfigurationSection
  {
    public const string SectionName = "applicationConfiguration";

    [ConfigurationProperty("title", IsRequired = true)]
    public string Title
    {
      get { return (string)this["title"]; }
      set { this["title"] = value; }
    }

    [ConfigurationProperty("id", IsRequired = true)]
    public int Id
    {
      get { return (int)this["id"]; }
      set { this["id"] = value; }
    }
  }
}
