﻿using Dataforge.Business;
using Dataforge.Business.Activities;
using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using Dataforge.Business.Documents;
using Dataforge.Business.Security;
using Dataforge.Business.Service;
using Dataforge.Prism.Configuration;
using Dataforge.Prism.Contants;
using Dataforge.Prism.Debugging;
using Dataforge.Prism.Events;
using Dataforge.Prism.Views;
using Dataforge.Prism.Views.Dialogs;
using Dataforge.Prism.Views.Ribbon;
using Dataforge.PrismAvalonExtensions;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.MefExtensions.Modularity;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Dataforge.Prism
{
  [ModuleExport(typeof(MainModule))]
  public class MainModule : AssemblyModule
  {
    #region LoginView LoginView

    LoginView LoginView
    {
      get { return ServiceLocator.Current.GetInstance<LoginView>(); }
    }

    #endregion

    #region SessionExpiredView SessionExpiredView

    SessionExpiredView SessionExpiredView
    {
      get { return ServiceLocator.Current.GetInstance<SessionExpiredView>(); }
    }

    #endregion

    #region LoginTab LoginTab

    [Import]
    LoginTab _loginTab = null;
    LoginTab LoginTab
    {
      get { return _loginTab; }
    }


    #endregion

    //#region Import OrganizationalStructureView OrganizationalStructureView

    //[Import]
    //OrganizationalStructureView _organizationalStructureView = null;
    //public OrganizationalStructureView OrganizationalStructureView
    //{
    //  get { return _organizationalStructureView; }
    //}

    //#endregion

    //#region Import OrganizationalStructureViewModel OrganizationalStructureViewModel

    //[Import]
    //OrganizationalStructureViewModel _organizationalStructureViewModel = null;
    //public OrganizationalStructureViewModel OrganizationalStructureViewModel
    //{
    //  get { return _organizationalStructureViewModel; }
    //}

    //#endregion

    #region Import WaitAnimation WaitAnimation

    [Import]
    WaitAnimation _waitAnimation = null;
    public WaitAnimation WaitAnimation
    {
      get { return _waitAnimation; }
    }

    #endregion

    #region void Initialize()

    public override void Initialize()
    {
      base.Initialize();

      #region Event Subscriptions

      EventAggregator.GetEvent<LoadCacheEvent>().Subscribe(LoadCache, ThreadOption.PublisherThread);
      EventAggregator.GetEvent<SessionExpiredEvent>().Subscribe(OnSessionExpired, ThreadOption.PublisherThread);
      EventAggregator.GetEvent<AuthorizationRequiredEvent>().Subscribe(OnAuthorizationRequired, ThreadOption.PublisherThread);
      
      #endregion

      //IPAddress ip = System.Net.Dns.GetHostEntry(Environment.MachineName).AddressList.Where(i => i.AddressFamily == AddressFamily.InterNetwork).FirstOrDefault();

      //NATUPNPLib.UPnPNATClass upnpnat = new NATUPNPLib.UPnPNATClass();
      //NATUPNPLib.IStaticPortMappingCollection mappings = upnpnat.StaticPortMappingCollection;

      //foreach (NATUPNPLib.IStaticPortMapping portMapping in mappings)
      //{
      //  // do something with the port mapping, such as displaying it in a listbox
      //}

      #region Login

#if (DEBUG)
      if (!RegionManager.Regions[RegionNames.WaitAnimationRegion].Views.Contains(WaitAnimation))
        RegionManager.Regions[RegionNames.WaitAnimationRegion].Add(WaitAnimation);

      BackgroundWorker bw = new BackgroundWorker();
      bw.RunWorkerCompleted += (s, e) =>
      {
        if (SecurityContext.User != null)
        {
          EventAggregator.GetEvent<LoadCacheEvent>().Publish(new EventArgs());
          EventAggregator.GetEvent<SecurityContextChangedEvent>().Publish(SecurityContext.User);
        }
        else
        {
          if (RegionManager.Regions[RegionNames.WaitAnimationRegion].Views.Contains(WaitAnimation))
            RegionManager.Regions[RegionNames.WaitAnimationRegion].Remove(WaitAnimation);
        }

        bw.Dispose();
      };
      bw.DoWork += (s, e) =>
      {
        try
        {
          DebugCredentials user = ServiceLocator.Current.GetInstance<DebugCredentials>();
          var appConfig = ConfigurationManager.GetSection(ApplicationConfigurationSection.SectionName) as ApplicationConfigurationSection;
          HashAlgorithm algoritm = new SHA256Managed();
          using (var svc = ServiceFactory.GetService<IDataforgeService>())
          {
            AuthenticationResult ar = svc.ExecuteFunction<AuthenticationResult>(i => i.Login(appConfig.Id, user.Username, ASCIIEncoding.ASCII.GetString(algoritm.ComputeHash(ASCIIEncoding.ASCII.GetBytes(user.Password)))));
            Dataforge.Business.Security.SecurityContext.AddAuthentication(ar);
          }
        }
        catch
        {
          EventAggregator.GetEvent<SecurityContextChangedEvent>().Publish(null);
        }
      };
      bw.RunWorkerAsync();
#else
      RegionManager.Regions[RegionNames.RibbonRegion].Add(LoginTab);
      RegionManager.Regions[RegionNames.DialogRegion].Add(LoginView);
#endif

      #endregion
    }

    private void OnAuthorizationRequired(AuthorizationRequiredEventArgs args)
    {
    }

    private void OnSessionExpired(EventArgs obj)
    {
      SessionExpiredView sev = SessionExpiredView;
      LoginViewModel lvm = (LoginViewModel)sev.DataContext;
      lvm.SessionExpired = true;
      lvm.Username = SecurityContext.User.Username;
      ThreadBlockingDialogHelper.Show(sev);
    }

    #region LoadCache(EventArgs obj)

    private void LoadCache(EventArgs obj)
    {
      if (!RegionManager.Regions[RegionNames.WaitAnimationRegion].Views.Contains(WaitAnimation))
        RegionManager.Regions[RegionNames.WaitAnimationRegion].Add(WaitAnimation);

      BasicCollection<BusinessObject> cache = null;
      BackgroundWorker bw = new BackgroundWorker();
      bw.DoWork += (s, e) =>
      {
        try
        {
          using (var svc = ServiceFactory.GetService<IDataforgeService>())
          {
            cache = svc.ExecuteFunction<BasicCollection<BusinessObject>>(i => i.LoadCache());
          }
        }
        catch (Exception ex)
        {
          if (ExceptionPolicy.HandleException(ex, "Default Policy"))
            throw;
        }
      };
      bw.RunWorkerCompleted += (s, e) =>
      {
        if (cache != null) Cache.Instance.LoadCache(cache);

        if (RegionManager.Regions[RegionNames.WaitAnimationRegion].Views.Contains(WaitAnimation))
          RegionManager.Regions[RegionNames.WaitAnimationRegion].Remove(WaitAnimation);

        bw.Dispose();
      };
      bw.RunWorkerAsync();

      while (RegionManager.Regions[RegionNames.WaitAnimationRegion].Views.Contains(WaitAnimation))
      {
        LoginView.Dispatcher.Invoke(DispatcherPriority.Background, new ThreadStart(delegate { }));
        Thread.Sleep(20);
      }
    }

    #endregion

    #endregion

    #region void OnLogin(User user)

    protected override void OnLogin(User user)
    {
      if (RegionManager.Regions[RegionNames.RibbonRegion].Views.Contains(LoginTab))
        RegionManager.Regions[RegionNames.RibbonRegion].Remove(LoginTab);

      base.OnLogin(user);
    }

    #endregion

    #region void OnLogout()

    protected override void OnLogout()
    {
      foreach (var view in RegionManager.Regions[RegionNames.DockingRegion].Views)
      {
        RegionManager.Regions[RegionNames.DockingRegion].Remove(view);
      }
      foreach (var view in RegionManager.Regions[RegionNames.DialogRegion].Views)
      {
        RegionManager.Regions[RegionNames.DialogRegion].Remove(view);
      }
      foreach (var view in RegionManager.Regions[RegionNames.RibbonRegion].Views)
      {
        RegionManager.Regions[RegionNames.RibbonRegion].Remove(view);
      }

      RegionManager.Regions[RegionNames.RibbonRegion].Add(LoginTab);
      RegionManager.Regions[RegionNames.RibbonRegion].Activate(LoginTab);

      //RegionManager.Regions[RegionNames.DialogRegion].Add(LoginView);
      base.OnLogout();
    }

    #endregion
  }
}
