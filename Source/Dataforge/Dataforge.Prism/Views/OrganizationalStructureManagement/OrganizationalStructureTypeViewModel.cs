﻿using Dataforge.Business.Documents;
using Dataforge.Business.Security;
using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Dataforge.Prism.Views.OrganizationalStructureManagement
{
  [Export]
  [PartCreationPolicy(CreationPolicy.Shared)]
  public class OrganizationalStructureTypeViewModel : ViewModel<OrganizationalStructureType>
  {
    #region Model Propertry string Name

    public string Name
    {
      get
      {
        if (Model == null) return default(string);
        else return Model.Name;
      }
      set { Model.Name = value; }
    }

    #endregion

    #region Model Propertry string SystemIdentifier

    public string SystemIdentifier
    {
      get
      {
        if (Model == null) return default(string);
        return Model.SystemIdentifier;
      }
      set { Model.SystemIdentifier = value; }
    }

    #endregion
  }
}
