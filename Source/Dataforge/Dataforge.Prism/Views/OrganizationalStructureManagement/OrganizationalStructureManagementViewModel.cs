﻿using Dataforge.Business.Activities;
using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using Dataforge.Business.Documents;
using Dataforge.Business.Security;
using Dataforge.Business.Service;
using Dataforge.Prism.Contants;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views.OrganizationalStructureManagement
{
  [Export]
  [PartCreationPolicy(CreationPolicy.NonShared)]
  public class OrganizationalStructureManagementViewModel : ManagementViewModel<OrganizationalStructureType, OrganizationalStructureTypeNodeViewModel>
  {
    public const string ContextRegion = "ContextRegion";

    #region Constructors

    public OrganizationalStructureManagementViewModel()
    {
      Title = "Organizational Structure";
      IsFocused = true;
    }

    public OrganizationalStructureManagementViewModel(BasicCollection<OrganizationalStructureType> os)
      : base(os)
    {
      Title = "Organizational Structure";
      IsFocused = true;
    }

    #endregion

    #region Import OrganizationalStructureTypeView OrganizationalStructureTypeView

    [Import]
    OrganizationalStructureTypeView _organizationalStructureTypeView = null;
    public OrganizationalStructureTypeView OrganizationalStructureTypeView
    {
      get { return _organizationalStructureTypeView; }
    }

    #endregion

    #region Import OrganizationalStructureTypeViewModel OrganizationalStructureTypeViewModel

    [Import]
    OrganizationalStructureTypeViewModel _organizationalStructureTypeViewModel = null;
    public OrganizationalStructureTypeViewModel OrganizationalStructureTypeViewModel
    {
      get { return _organizationalStructureTypeViewModel; }
    }

    #endregion

    #region Overrides

    #region int ActivityId

    public override int ActivityId
    {
      get { return Activities.OrganizationalStructureManagement; }
    }

    #endregion

    #region void Load()

    public override void Load()
    {
      using (var svc = ServiceFactory.GetService<IDataforgeService>())
      {
        ModelCollection = svc.ExecuteFunction<BasicCollection<OrganizationalStructureType>>(i => i.LoadOrganizationalStructure());
      }
    }

    #endregion

    #region void Save()

    public override void Save()
    {
      using (var svc = ServiceFactory.GetService<IDataforgeService>())
      {
        ModelCollection = svc.ExecuteFunction<BasicCollection<OrganizationalStructureType>>(i => i.SaveOrganizationalStructure(ModelCollection));
      }
    }

    #endregion

    #region OrganizationalStructureTypeNodeViewModel CreateViewModel(OrganizationalStructureType model)

    public override OrganizationalStructureTypeNodeViewModel CreateViewModel(OrganizationalStructureType model)
    {
      return new OrganizationalStructureTypeNodeViewModel(model, this);
    }

    #endregion

    #region void OnFocusedViewModelChanged(ISelectableViewModel viewModel)

    protected override void OnFocusedViewModelChanged(ISelectableViewModel viewModel)
    {
      if (viewModel is OrganizationalStructureTypeNodeViewModel)
      {
        OrganizationalStructureTypeNodeViewModel vm = viewModel as OrganizationalStructureTypeNodeViewModel;
        ShowInRegion(ContextRegion, OrganizationalStructureTypeView);
        OrganizationalStructureTypeViewModel.IsReadOnly = !Activity.CanEdit;
        OrganizationalStructureTypeViewModel.Model = vm.Model;
      }
      else
      {
        RemoveFromRegion(ContextRegion, OrganizationalStructureTypeView);
      }

      base.OnFocusedViewModelChanged(viewModel);
    }

    #endregion

    #region bool OnCanExecuteOperation(Operation operation)

    protected override bool OnCanExecuteOperation(Operation operation)
    {
      switch (operation.Id)
      {
        case Operations.OrganizationalStructure_AddRootType:
          if (ModelCollection.Count != 0) operation.Visible = false;
          else operation.Visible = true;
          return ModelCollection.Count == 0;
      }

      return base.OnCanExecuteOperation(operation);
    }

    #endregion

    #region void OnExecuteOperation(Operation operation)

    int _iCount = 1;
    protected override void OnExecuteOperation(Operation operation)
    {
      switch (operation.Id)
      {
        case Operations.OrganizationalStructure_AddRootType:
          {
            var e = new OrganizationalStructureType {Name = string.Format("New Element {0}", _iCount++)};
            ModelCollection.Add(e);
            ISelectableViewModel node = GetViewModel(e);
            node.IsSelected = true;
            IsFocused = true;
          }
          break;

        case Operations.OrganizationalStructure_AddSubType:
          {
            var e = new OrganizationalStructureType {Name = string.Format("New Element {0}", _iCount++)};
            ((OrganizationalStructureType)FocusedModel).SubTypes.Add(e);
            ISelectableViewModel node = GetViewModel(e);
            ((ITreeNodeViewModel)FocusedViewModel).IsExpanded = true;
            node.IsSelected = true;
            OrganizationalStructureTypeViewModel.IsFocused = true;
          }
          break;
      }

      base.OnExecuteOperation(operation);
    }

    #endregion

    #endregion
  }
}
