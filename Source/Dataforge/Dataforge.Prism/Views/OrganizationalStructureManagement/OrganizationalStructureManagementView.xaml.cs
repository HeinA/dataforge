﻿using Dataforge.Prism.Controls;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Dataforge.Prism.Views.OrganizationalStructureManagement
{
  [Export]
  [PartCreationPolicy(CreationPolicy.Shared)]
  public partial class OrganizationalStructureManagementView : ViewControl
  {
    public OrganizationalStructureManagementView()
    {
      InitializeComponent();
    }
  }
}
