﻿using Dataforge.Business.Collections;
using Dataforge.Business.Documents;
using Dataforge.Business.Security;
using Dataforge.Business.Service;
using Dataforge.Prism.Views.Operations;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views.OrganizationalStructure
{
  [Export]
  public class OrganizationalStructureViewModel : TreeViewViewModel<OrganizationalStructureElement, OrganizationalStructureElementViewModel> 
  {
    #region Constructors

    public OrganizationalStructureViewModel(BasicCollection<OrganizationalStructureElement> modelCollection)
      : base(modelCollection)
    {
      Operation op = new Operation();
      op.Name = "Dataforge.AddOrganizationalStructureRootElement";
      op.Text = "Add Root Element";
      op.Description = "Adds a root organizational structure element.";
      OperationCollectionProvider.ModelCollection.Add(op);

      op = new Operation();
      op.Name = "Dataforge.AddOrganizationalStructureSubElement";
      op.Text = "Add Sub Element";
      op.Description = "Adds a sub element onto the currently selected organizational structure element.";
      OperationCollectionProvider.ModelCollection.Add(op);

      op = new Operation();
      op.Name = "Dataforge.ResetOrganizationalStructure";
      op.Text = "Reset";
      op.Description = "Resets the organizational structure.";
      OperationCollectionProvider.ModelCollection.Add(op);
    }

    #endregion

    #region OrganizationalStructureElementViewModel CreateViewModel(OrganizationalStructureElement model)

    public override OrganizationalStructureElementViewModel CreateViewModel(OrganizationalStructureElement model)
    {
      //if (SelectFirstRootNode) return new OrganizationalStructureElementViewModel(model, this) { IsSelected = CollectionProvider.ModelCollection.IndexOf(model) == 0 };
      return new OrganizationalStructureElementViewModel(model, this);
    }

    #endregion

    #region bool CanExecuteOperation(Operation operation)

    public override bool CanExecuteOperation(Operation operation)
    {
      switch (operation.Name)
      {
        //case "Dataforge.AddOrganizationalStructureRootElement":
        //  return OperationViewModels.Count == 0;

        case "Dataforge.AddOrganizationalStructureSubElement":
          return SelectedModel != null;
      }

      return true;
    }

    #endregion

    #region void ExecuteOperation(Operation operation)

    public override void ExecuteOperation(Operation operation)
    {
      switch (operation.Name)
      {
        case "Dataforge.AddOrganizationalStructureRootElement":
          {
            OrganizationalStructureElement e = new OrganizationalStructureElement();
            e.Name = string.Format("New Element {0}", i++);
            ModelCollection.Add(e);
            OrganizationalStructureElementViewModel evm = ViewModelCollection.FirstOrDefault(n => n.Model == e);
            evm.IsSelected = true;
            IsFocused = true;
          }
          break;

        case "Dataforge.AddOrganizationalStructureSubElement":
          {
            OrganizationalStructureElement e = new OrganizationalStructureElement();
            e.Name = string.Format("New Element {0}", i++);
            SelectedModel.SubElements.Add(e);
            OrganizationalStructureElementViewModel evm = SelectedNodeViewModel.NodeViewModels.FirstOrDefault(n => n.Model == e);
            SelectedNodeViewModel.IsExpanded = true;
            evm.IsSelected = true;
            IsFocused = true;
          }
          break;

        case "Dataforge.ResetOrganizationalStructure":
          {
            using (var svc = ServiceFactory.GetService<IDataforgeService>())
            {
              BasicCollection<OrganizationalStructureElement> structure = svc.ExecuteFunction<BasicCollection<OrganizationalStructureElement>>(i => i.LoadOrganizationalStructure());
              CollectionProvider.ModelCollection = structure;
            }
          }
          break;
      }
    }

    int i = 1;

    #endregion
  }
}