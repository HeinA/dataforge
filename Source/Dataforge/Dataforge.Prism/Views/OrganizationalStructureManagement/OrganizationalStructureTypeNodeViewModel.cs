﻿using Dataforge.Business.Collections;
using Dataforge.Business.Documents;
using Dataforge.Business.Security;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Dataforge.Prism.Views.OrganizationalStructureManagement
{
  public class OrganizationalStructureTypeNodeViewModel : TreeNodeViewModel<OrganizationalStructureType>
  {
    #region Constructors

    public OrganizationalStructureTypeNodeViewModel(OrganizationalStructureType ose, IMultiModelViewModel owner)
      : base(ose, owner)
    {
      StructureTypeModelProvider = new ReadOnlyCollectionProvider<OrganizationalStructureType, OrganizationalStructureTypeNodeViewModel>(this, Model.SubTypes, CreateOrganizationalStructureElementViewModel, OnOrganizationalStructureElementViewModelCreated);
    }

    #endregion

    #region string Name

    public string Name
    {
      get
      {
        if (Model == null) return default(string);
        else if (string.IsNullOrWhiteSpace(Model.Name)) return "** Structure Element **";
        else return Model.Name;
      }
      set { Model.Name = value; }
    }

    #endregion

    #region ReadOnlyCollectionProvider StructureTypeViewModels

    ////////////////////////////////////////////////////////
    // Add to Constructor:
    // StructureTypeModelProvider = new ReadOnlyCollectionProvider<OrganizationalStructureElement, OrganizationalStructureElementViewModel>(CreateOrganizationalStructureElementViewModel, OnOrganizationalStructureElementViewModelCreated);
    ////////////////////////////////////////////////////////

    ReadOnlyCollectionProvider<OrganizationalStructureType, OrganizationalStructureTypeNodeViewModel> StructureTypeModelProvider;

    public ReadOnlyObservableCollection<OrganizationalStructureTypeNodeViewModel> StructureTypeViewModels
    {
      get { return StructureTypeModelProvider.ViewModelCollection; }
    }

    public BasicCollection<OrganizationalStructureType> StructureTypes
    {
      get { return StructureTypeModelProvider.ModelCollection; }
      set { StructureTypeModelProvider.ModelCollection = value; }
    }

    OrganizationalStructureTypeNodeViewModel CreateOrganizationalStructureElementViewModel(OrganizationalStructureType model)
    {
      return new OrganizationalStructureTypeNodeViewModel(model, Owner);
    }

    void OnOrganizationalStructureElementViewModelCreated(OrganizationalStructureTypeNodeViewModel viewModel, OrganizationalStructureType model)
    {
    }

    #endregion
  }
}
