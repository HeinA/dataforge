﻿using Dataforge.Business.Collections;
using Dataforge.Business.Documents;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views.OrganizationalStructure
{
  public class OrganizationalStructureElementViewModel : TreeViewNodeViewModel<OrganizationalStructureElement, OrganizationalStructureElementViewModel>
  {
    #region Constructors

    public OrganizationalStructureElementViewModel(OrganizationalStructureElement osElement, OrganizationalStructureViewModel owner)
      : base(osElement, owner)
    {
    }

    #endregion

    #region string Name

    public string Name
    {
      get { return Model.Name; }
      set
      {
        Model.Name = value;
        OnPropertyChanged(OrganizationalStructureElement.PN_NAME);
      }
    }

    #endregion

    #region string SystemIdentifier

    public string SystemIdentifier
    {
      get { return Model.SystemIdentifier; }
      set
      {
        Model.SystemIdentifier = value;
        OnPropertyChanged(OrganizationalStructureElement.PN_SYSTEMIDENTIFIER);
      }
    }

    #endregion

    #region BasicCollection<OrganizationalStructureElement> GetNodesCollection()

    protected override BasicCollection<OrganizationalStructureElement> GetNodesCollection()
    {
      return Model.SubElements;
    }

    #endregion

    #region OrganizationalStructureElementViewModel CreateNodeViewModel(OrganizationalStructureElement model)

    protected override OrganizationalStructureElementViewModel CreateNodeViewModel(OrganizationalStructureElement model)
    {
      //if (Owner.SelectNewNode)
      //{
      //  IsExpanded = true;
      //  return new OrganizationalStructureElementViewModel(model, (OrganizationalStructureViewModel)Owner) { IsSelected = true };
      //}
      return new OrganizationalStructureElementViewModel(model, (OrganizationalStructureViewModel)Owner);
    }

    #endregion
  }
}
