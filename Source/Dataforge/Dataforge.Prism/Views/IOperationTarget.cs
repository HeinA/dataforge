﻿using Dataforge.Business.Activities;
using Dataforge.Business.Security;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views
{
  public interface IOperationTarget
  {
    ReadOnlyObservableCollection<OperationViewModel> GlobalOperationViewModels { get; }
    ReadOnlyObservableCollection<OperationViewModel> ContextOperationViewModels { get; }
    bool CanExecuteOperation(Operation operation);
    void ExecuteOperation(Operation operation);
    void RaiseCanExecuteChanged();
    void VisibilityChanged(OperationViewModel vm);
  }
}
