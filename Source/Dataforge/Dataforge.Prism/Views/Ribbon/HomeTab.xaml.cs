﻿using Dataforge.Prism.Views.Ribbon;
using Fluent;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Dataforge.Prism.Views.Ribbon
{
  /// <summary>
  /// Interaction logic for HomeTab.xaml
  /// </summary>
  [Export]
  [PartCreationPolicy(CreationPolicy.Shared)]
  public partial class HomeTab : RibbonTabItem
  {
    public HomeTab()
    {
      InitializeComponent();
      if (ServiceLocator.Current == null) return;
      this.DataContext = ServiceLocator.Current.GetInstance<HomeTabViewModel>();
    }
  }
}
