﻿using Dataforge.Prism.Contants;
using Dataforge.Prism.Views.Dialogs;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Dataforge.Prism.Views.Ribbon
{
  [Export]
  [PartCreationPolicy(CreationPolicy.Shared)]
  public class LoginTabViewModel : ViewModel
  {
    #region LoginView LoginView

    LoginView LoginView
    {
      get { return ServiceLocator.Current.GetInstance<LoginView>(); }
    }

    #endregion

    #region Commands

    public ICommand LoginCommand { get { return new DelegateCommand(Login); } }

    private void Login()
    {
      GlobalRegionManager.Regions[RegionNames.DialogRegion].Add(LoginView);
    }

    #endregion
  }
}
