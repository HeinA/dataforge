﻿using Dataforge.Business.Activities;
using Dataforge.Business.Collections;
using Dataforge.Business.Documents;
using Dataforge.Business.Security;
using Dataforge.Business.Service;
using Dataforge.Prism.Contants;
using Dataforge.Prism.Views.ActivityManagement;
using Dataforge.Prism.Views.OperationManagement;
using Dataforge.Prism.Views.OrganizationalStructureManagement;
using Dataforge.Prism.Views.OrganizationalUnitManagement;
using Dataforge.Prism.Views.UserManagement;
using Dataforge.PrismAvalonExtensions;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views.Ribbon
{
  [Export]
  [PartCreationPolicy(CreationPolicy.Shared)]
  public class ManagementTabViewModel : ViewModel
  {
    #region DelegateCommand ManageOrganizationalStructureCommand

    DelegateCommand _manageOrganizationalStructureCommand;
    public DelegateCommand ManageOrganizationalStructureCommand
    {
      get { return _manageOrganizationalStructureCommand ?? (_manageOrganizationalStructureCommand = new DelegateCommand(ExecuteManageOrganizationalStructure, CanExecuteManageOrganizationalStructure)); }
    }

    bool CanExecuteManageOrganizationalStructure()
    {
      return true;
    }

    void ExecuteManageOrganizationalStructure()
    {
      DockingHelper.ShowManagementView<OrganizationalStructureManagementView, OrganizationalStructureManagementViewModel>();
    }

    #endregion

    #region DelegateCommand ManageOrganizationalUnitsCommand

    DelegateCommand _manageOrganizationalUnitsCommand;
    public DelegateCommand ManageOrganizationalUnitsCommand
    {
      get { return _manageOrganizationalUnitsCommand ?? (_manageOrganizationalUnitsCommand = new DelegateCommand(ExecuteManageOrganizationalUnits, CanExecuteManageOrganizationalUnits)); }
    }

    bool CanExecuteManageOrganizationalUnits()
    {
      return true;
    }

    void ExecuteManageOrganizationalUnits()
    {
      DockingHelper.ShowManagementView<OrganizationalUnitManagementView, OrganizationalUnitManagementViewModel>();
    }

    #endregion

    #region DelegateCommand UserManagementCommand

    DelegateCommand _userManagementCommand;
    public DelegateCommand UserManagementCommand
    {
      get { return _userManagementCommand ?? (_userManagementCommand = new DelegateCommand(ExecuteUserManagement, CanExecuteUserManagement)); }
    }

    bool CanExecuteUserManagement()
    {
      return true;
    }

    void ExecuteUserManagement()
    {
      DockingHelper.ShowManagementView<UserManagementView, UserManagementViewModel>();
    }

    #endregion

    #region DelegateCommand OperationManagementCommand

    DelegateCommand _operationManagementCommand;
    public DelegateCommand OperationManagementCommand
    {
      get { return _operationManagementCommand ?? (_operationManagementCommand = new DelegateCommand(ExecuteOperationManagement, CanExecuteOperationManagement)); }
    }

    bool CanExecuteOperationManagement()
    {
      return true;
    }

    void ExecuteOperationManagement()
    {
      DockingHelper.ShowManagementView<OperationManagementView, OperationManagementViewModel>();
    }

    #endregion

    #region DelegateCommand ActivityManagementCommand

    DelegateCommand _activityManagementCommand;
    public DelegateCommand ActivityManagementCommand
    {
      get { return _activityManagementCommand ?? (_activityManagementCommand = new DelegateCommand(ExecuteActivityManagement, CanExecuteActivityManagement)); }
    }

    bool CanExecuteActivityManagement()
    {
      return true;
    }

    void ExecuteActivityManagement()
    {
      DockingHelper.ShowManagementView<ActivityManagementView, ActivityManagementViewModel>();
    }

    #endregion

  }
}
