﻿using Dataforge.Business.Security;
using Dataforge.Prism.Contants;
using Dataforge.Prism.Events;
using Dataforge.Prism.Views.Tools;
using Dataforge.PrismAvalonExtensions;
using Dataforge.PrismAvalonExtensions.Events;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Dataforge.Prism.Views.Ribbon
{
  [Export]
  [PartCreationPolicy(CreationPolicy.Shared)]
  public class HomeTabViewModel : ViewModel
  {
    #region void OnInitialized()

    protected override void OnInitialized()
    {
      #region Subscribe to RegionManager.Regions[RegionNames.DockingRegion].Views.CollectionChanged

      GlobalRegionManager.Regions[RegionNames.DockingRegion].Views.CollectionChanged += DockingRegionViews_CollectionChanged;

      #endregion

      base.OnInitialized();
    }

    #endregion

    //protected override void OnRegionManagerUpdated()
    //{
    //  #region Subscribe to RegionManager.Regions[RegionNames.DockingRegion].Views.CollectionChanged

    //  RegionManager.Regions[RegionNames.DockingRegion].Views.CollectionChanged += DockingRegionViews_CollectionChanged;

    //  #endregion

    //  base.OnRegionManagerUpdated();
    //}

    #region DockingRegionViews_CollectionChanged

    void DockingRegionViews_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      switch (e.Action)
      {
        case NotifyCollectionChangedAction.Add:
          foreach (var o in e.NewItems)
          {
            if (o == DocumentStateView) DocumentStateViewToggled = true;
          }
          break;

        case NotifyCollectionChangedAction.Remove:
          foreach (var o in e.OldItems)
          {
            if (o == DocumentStateView) DocumentStateViewToggled = false;
          }
          break;
      }
    }

    #endregion

    #region Views

    #region Import DocumentStateView DocumentStateView

    [Import]
    DocumentStateView _documentStateView = null;
    public DocumentStateView DocumentStateView
    {
      get { return _documentStateView; }
    }

    #region bool DocumentStateViewToggled

    bool _documentStateViewToggled;
    public bool DocumentStateViewToggled
    {
      get { return _documentStateViewToggled; }
      set
      {
        if (_documentStateViewToggled != value)
        {
          _documentStateViewToggled = value;
          OnPropertyChanged("DocumentStateViewToggled");
        }
      }
    }

    #endregion

    #endregion

    #endregion

    #region Commands

    #region DelegateCommand ToggleDocumentStateViewCommand

    DelegateCommand _toggleDocumentStateViewCommand;
    public DelegateCommand ToggleDocumentStateViewCommand
    {
      get
      {
        if (_toggleDocumentStateViewCommand == null) _toggleDocumentStateViewCommand = new DelegateCommand(ExecuteToggleDocumentStateView, CanExecuteToggleDocumentStateView);
        return _toggleDocumentStateViewCommand;
      }
    }

    bool CanExecuteToggleDocumentStateView()
    {
      return true;
    }

    void ExecuteToggleDocumentStateView()
    {
      IRegion region = GlobalRegionManager.Regions[RegionNames.DockingRegion];
      if (!region.Views.Contains(DocumentStateView)) region.Add(new DockingMetadata(DocumentStateView, new SideDockStrategy(DockSide.Right, new GridLength(200))) { AutoHide = true, Title = "Document State View" });
      else region.Remove(DocumentStateView);
    }

    #endregion

    #region DelegateCommand LogoutCommand

    DelegateCommand _logoutCommand;
    public DelegateCommand LogoutCommand
    {
      get
      {
        if (_logoutCommand == null) _logoutCommand = new DelegateCommand(ExecuteLogout, CanExecuteLogout);
        return _logoutCommand;
      }
    }

    bool CanExecuteLogout()
    {
      return true;
    }

    void ExecuteLogout()
    {
      CloseAllDocumentsEventArgs args = new CloseAllDocumentsEventArgs();
      GlobalEventAggregator.GetEvent<CloseAllDocumentsEvent>().Publish(args);
      if (!args.Cancel)
      {
        Dataforge.Business.Security.SecurityContext.RemoveAuthentication(SecurityContext.SessionToken);
        GlobalEventAggregator.GetEvent<SecurityContextChangedEvent>().Publish(null);
      }
    }

    #endregion

    #endregion
  }
}
