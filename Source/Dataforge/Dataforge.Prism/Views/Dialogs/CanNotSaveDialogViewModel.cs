﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views.Dialogs
{
  [Export]
  [PartCreationPolicy(CreationPolicy.Shared)]
  public class CanNotSaveDialogViewModel : DialogViewModel
  {
    public override string Title
    {
      get { return "Discard Changes?"; }
    }
  }
}
