﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xceed.Wpf.Toolkit;

namespace Dataforge.Prism.Views.Dialogs
{
  public interface IThreadBlockingDialogViewModel
  {
    WindowState WindowState { get; }
    DialogResult Result { get; set; }
  }
}
