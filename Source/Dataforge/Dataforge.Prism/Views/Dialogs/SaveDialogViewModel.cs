﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xceed.Wpf.Toolkit;

namespace Dataforge.Prism.Views.Dialogs
{
  [Export]
  [PartCreationPolicy(CreationPolicy.Shared)]
  public class SaveDialogViewModel : DialogViewModel
  {
    public override string Title
    {
      get { return "Save Changes?"; }
    }
  }
}
