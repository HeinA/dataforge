﻿using Microsoft.Practices.Prism.Commands;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views.Dialogs
{
  public class ExceptionViewModel : DialogViewModel
  {
    public ExceptionViewModel(Exception ex)
    {
      _exception = ex;
    }

    Exception _exception;
    public Exception Exception
    {
      get { return _exception; }
    }

    public override string Title
    {
      get { return "Error"; }
    }

    #region DelegateCommand CopyDetailsCommand

    DelegateCommand _copyDetailsCommand;
    public DelegateCommand CopyDetailsCommand
    {
      get { return _copyDetailsCommand ?? (_copyDetailsCommand = new DelegateCommand(ExecuteCopyDetails, CanExecuteCopyDetails)); }
    }

    bool CanExecuteCopyDetails()
    {
      return true;
    }

    const int indentSize = 4;
    
    void ExecuteCopyDetails()
    {
      string error = string.Empty;
      using (StringWriter sw = new StringWriter())
      using (IndentedTextWriter tw = new IndentedTextWriter(sw, string.Empty.PadRight(indentSize, ' ')))
      {
        WriteInner(Exception, tw, '*');
        error = sw.ToString();
        System.Windows.Clipboard.SetText(error);
      }
    }

    void WriteInner(Exception ex, IndentedTextWriter tw, char seperator)
    {
      try
      {
        tw.WriteLine(string.Empty.PadRight(100 - (tw.Indent * indentSize), seperator));
        tw.WriteLine(ex.GetType().ToString());
        tw.WriteLine(string.Empty.PadRight(100 - (tw.Indent * indentSize), seperator));
        tw.WriteLine(ex.Message.Replace("\n", "\n".PadRight(tw.Indent * indentSize, ' ')));
        tw.WriteLine(string.Empty.PadRight(100 - (tw.Indent * indentSize), seperator));
        tw.WriteLine(ex.StackTrace.Replace("\n", "\n".PadRight(tw.Indent * indentSize, ' ')));
        tw.WriteLine(string.Empty.PadRight(100 - (tw.Indent * indentSize), seperator));
        tw.WriteLine();
      }
      catch
      {
        throw;
      }

      if (ex.InnerException != null)
      {
        tw.Indent++;
        WriteInner(ex.InnerException, tw, '-');
        tw.Indent--;
      }
    }

    #endregion

  }
}
