﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views.Dialogs
{
  public class DataPersistanceExceptionViewModel : DialogViewModel
  {
    public DataPersistanceExceptionViewModel(Exception ex)
    {
      _exception = ex;
    }

    Exception _exception;
    public Exception Exception
    {
      get { return _exception; }
    }

    public string Message
    {
      get { return Exception.Message; }
    }

    public override string Title
    {
      get { return "Persistance Error"; }
    }
  }
}
