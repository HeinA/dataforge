﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Dataforge.Prism.Views.Dialogs
{
  [Export]
  [PartCreationPolicy(CreationPolicy.NonShared)]
  public class SetPasswordDialogViewModel : DialogViewModel
  {
    string EmptyPassword { get; set; }

    public SetPasswordDialogViewModel()
    {
      HashAlgorithm algoritm = new SHA256Managed();
      EmptyPassword = ASCIIEncoding.ASCII.GetString(algoritm.ComputeHash(ASCIIEncoding.ASCII.GetBytes(string.Empty)));
      ValidatePasswords();
    }

    #region Title

    public override string Title
    {
      get { return "Set Password"; }
    }

    #endregion

    #region string Error

    string _error;
    public string Error
    {
      get { return _error; }
      set { SetValue<string>(ref _error, value, "Error", "ErrorVisibility"); }
    }

    #endregion

    #region Visibility ErrorVisibility

    public override Visibility ErrorVisibility
    {
      get
      {
        if (string.IsNullOrWhiteSpace(Error)) return Visibility.Collapsed;
        return Visibility.Visible;
      }
    }

    #endregion

    protected override bool CanApply()
    {
      return Error == null;
    }

    string _passwordHash;
    public string PasswordHash
    {
      get { return _passwordHash; }
      set
      {
        if (_passwordHash != value)
        {
          _passwordHash = value;
          ValidatePasswords();
        }
      }
    }

    string _passwordVerificationHash;
    public string PasswordVerificationHash
    {
      get { return _passwordVerificationHash; }
      set
      {
        if (_passwordVerificationHash != value)
        {
          _passwordVerificationHash = value;
          ValidatePasswords();
        }
      }
    }

    void ValidatePasswords()
    {
      if (PasswordHash == PasswordVerificationHash)
      {
        if (PasswordHash == null) Error = "Invalid password.";
        else Error = null;
      }
      else
      {
        Error = "Passwords do not match.";
      }
      RaiseCanExecuteChanged();
    }
  }
}
