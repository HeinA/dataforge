﻿using Dataforge.Prism.Extensions;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Dataforge.Prism.Views.Dialogs
{
  /// <summary>
  /// Interaction logic for LoginView.xaml
  /// </summary>
  [Export]
  [PartCreationPolicy(CreationPolicy.NonShared)]
  public partial class SessionExpiredView : UserControl
  {
    public SessionExpiredView()
    {
      InitializeComponent();
      if (ServiceLocator.Current == null) return;
      this.DataContext = ServiceLocator.Current.GetInstance<LoginViewModel>();
    }

    private void Password_GotFocus(object sender, RoutedEventArgs e)
    {
      Password.SelectAll();
    }
  }
}
