﻿using Dataforge.Business.Security;
using Dataforge.Business.Service;
using Dataforge.Prism.Configuration;
using Dataforge.Prism.Contants;
using Dataforge.Prism.Events;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Linq;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Dataforge.Prism.Views.Dialogs
{
  [Export]
  [PartCreationPolicy(CreationPolicy.NonShared)]
  public class LoginViewModel : DialogViewModel
  {
    #region DelegateCommand<PasswordBox> LoginCommand

    DelegateCommand<PasswordBox> _loginCommand;
    public DelegateCommand<PasswordBox> LoginCommand
    {
      get
      {
        if (_loginCommand == null) _loginCommand = new DelegateCommand<PasswordBox>(OnLogin, CanLogin);
        return _loginCommand;
      }
    }

    #endregion

    #region string Username

    string _username;
    public string Username
    {
      get { return _username; }
      set { SetValue<string>(ref _username, value, "Username"); }
    }

    #endregion

    #region string Error

    string _error;
    public string Error
    {
      get { return _error; }
      set { SetValue<string>(ref _error, value, "Error", "ErrorVisibility"); }
    }

    #endregion

    #region Visibility ErrorVisibility

    public override Visibility ErrorVisibility
    {
      get
      {
        if (string.IsNullOrWhiteSpace(Error)) return Visibility.Collapsed;
        return Visibility.Visible;
      }
    }

    #endregion

    #region string Title

    public override string Title
    {
      get { return "Login"; }
    }

    #endregion

    #region void OnApply()

    protected override void OnApply()
    {
      throw new NotImplementedException();
    }

    #endregion

    #region bool CanLogin()

    bool CanLogin(PasswordBox pwb)
    {
      return !string.IsNullOrWhiteSpace(Username);
    }

    #endregion

    #region Import WaitAnimation WaitAnimation

    [Import]
    WaitAnimation _waitAnimation = null;
    public WaitAnimation WaitAnimation
    {
      get { return _waitAnimation; }
    }

    #endregion

    #region bool SessionExpired

    public bool SessionExpired { get; set; }

    #endregion

    #region void OnLogin(PasswordBox pwb)

    void OnLogin(PasswordBox pwb)
    {
      using (new WaitCursor())
      {
        var appConfig = ConfigurationManager.GetSection(ApplicationConfigurationSection.SectionName) as ApplicationConfigurationSection;
        HashAlgorithm algoritm = new SHA256Managed();
        string hash = ASCIIEncoding.ASCII.GetString(algoritm.ComputeHash(ASCIIEncoding.ASCII.GetBytes(pwb.Password)));

        using (var svc = ServiceFactory.GetService<IDataforgeService>())
        {
          AuthenticationResult ar = svc.ExecuteFunction<AuthenticationResult>(i => i.Login(appConfig.Id, Username, hash));
          if (!string.IsNullOrWhiteSpace(ar.Error))
          {
            Error = ar.Error;
            pwb.SelectAll();
          }
          else
          {
            Dataforge.Business.Security.SecurityContext.AddAuthentication(ar);
            WindowState = Xceed.Wpf.Toolkit.WindowState.Closed;
            if (!SessionExpired)
            {
              GlobalEventAggregator.GetEvent<LoadCacheEvent>().Publish(new EventArgs());
              GlobalEventAggregator.GetEvent<SecurityContextChangedEvent>().Publish(ar.User);
            }
          }
        }
      }
    }

    #endregion

    #region void OnPropertyChanged(string propertyName)

    public override void OnPropertyChanged(string propertyName)
    {
      base.OnPropertyChanged(propertyName);
      LoginCommand.RaiseCanExecuteChanged();
    }

    #endregion
  }
}
