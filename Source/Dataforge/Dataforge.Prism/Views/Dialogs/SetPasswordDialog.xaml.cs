﻿using Dataforge.Prism.Extensions;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Dataforge.Prism.Views.Dialogs
{
  /// <summary>
  /// Interaction logic for SetPasswordDialog.xaml
  /// </summary>
  [Export]
  [PartCreationPolicy(CreationPolicy.Shared)]
  public partial class SetPasswordDialog : UserControl
  {
    public SetPasswordDialog()
    {
      InitializeComponent();
      if (ServiceLocator.Current == null) return;
      this.DataContext = ServiceLocator.Current.GetInstance<SetPasswordDialogViewModel>();

      Password.PasswordChanged += Password_PasswordChanged;
      Password.GotFocus += Password_GotFocus;
      PasswordVerification.PasswordChanged += PasswordVerification_PasswordChanged;
      PasswordVerification.GotFocus += PasswordVerification_GotFocus;
    }

    void PasswordVerification_GotFocus(object sender, RoutedEventArgs e)
    {
      PasswordVerification.SelectAll();
    }

    void Password_GotFocus(object sender, RoutedEventArgs e)
    {
      Password.SelectAll();
    }

    void PasswordVerification_PasswordChanged(object sender, RoutedEventArgs e)
    {
      if (string.IsNullOrWhiteSpace(PasswordVerification.Password)) PasswordBoxExtension.SetEncryptedPassword(PasswordVerification, null);
      else
      {
        HashAlgorithm algoritm = new SHA256Managed();
        PasswordBoxExtension.SetEncryptedPassword(PasswordVerification, ASCIIEncoding.ASCII.GetString(algoritm.ComputeHash(ASCIIEncoding.ASCII.GetBytes(PasswordVerification.Password))));
      }
    }

    void Password_PasswordChanged(object sender, RoutedEventArgs e)
    {
      if (string.IsNullOrWhiteSpace(Password.Password)) PasswordBoxExtension.SetEncryptedPassword(Password, null);
      else
      {
        HashAlgorithm algoritm = new SHA256Managed();
        PasswordBoxExtension.SetEncryptedPassword(Password, ASCIIEncoding.ASCII.GetString(algoritm.ComputeHash(ASCIIEncoding.ASCII.GetBytes(Password.Password))));
      }
    }

    public SetPasswordDialogViewModel ViewModel
    {
      get { return (SetPasswordDialogViewModel)this.DataContext; }
    }
  }
}
