﻿using Dataforge.Business;
using Dataforge.Business.Activities;
using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views
{
  public abstract class ManagementViewModel<TModel, TViewModel> : MultiModelOperationsAwareViewModel<TModel, TViewModel>, IManagementViewModel
    where TModel : BusinessObject
    where TViewModel : ViewModel<TModel>, ISelectableViewModel
  {
    #region Constructors

    public ManagementViewModel()
    {
    }

    public ManagementViewModel(BasicCollection<TModel> models)
      : base(models)
    {
    }

    #endregion

    public abstract void Load();
    public abstract void Save();

    protected override void OnExecuteOperation(Operation operation)
    {
      switch (operation.Id)
      {
        case Operations.Refresh:
          Load();
          break;

        case Operations.Save:
          Save();
          break;
      }

      base.OnExecuteOperation(operation);
    }

    protected override void OnLoaded()
    {
      if (ViewModelCollection.Count > 0) ViewModelCollection[0].IsFocused = true;
      base.OnLoaded();
    }
  }
}
