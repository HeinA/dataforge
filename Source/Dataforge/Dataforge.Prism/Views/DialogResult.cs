﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views
{
  public enum DialogResult
  {
    Yes,
    No,
    Ok,
    Cancel
  }
}
