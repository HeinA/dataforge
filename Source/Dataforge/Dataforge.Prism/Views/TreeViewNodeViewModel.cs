﻿using Dataforge.Business;
using Dataforge.Business.Collections;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views
{
  public abstract class TreeViewNodeViewModel<TModel, TViewModel> : StatefullViewModel<TModel>, ITreeViewNodeViewModel
    where TModel : IdentifiableObject
    where TViewModel : ITreeViewNodeViewModel
  {
    public TreeViewNodeViewModel(TModel model, TreeViewViewModel<TModel, TViewModel> owner)
      : base(model)
    {
      _owner = owner;
      Owner.RegisterNodeViewModel(this);
    }

    TreeViewViewModel<TModel, TViewModel> _owner;
    public TreeViewViewModel<TModel, TViewModel> Owner
    {
      get { return _owner; }
    }

    #region bool IsSelected

    const string PN_ISSELECTED = "IsSelected";
    public virtual bool IsSelected
    {
      get
      {
        bool selected = GetState<bool>(PN_ISSELECTED);
        if (selected) Owner.SelectedNodeViewModel = this;
        return selected;
      }
      set
      {
        SetState<bool>(PN_ISSELECTED, value);
        if (value)
          Owner.SelectedNodeViewModel = this;
        else if (this == Owner.SelectedNodeViewModel)
          Owner.SelectedNodeViewModel = null;
      }
    }

    #endregion

    #region bool IsExpanded

    const string PN_ISEXPANDED = "IsExpanded";
    public bool IsExpanded
    {
      get { return GetState<bool>(PN_ISEXPANDED); }
      set { SetState<bool>(PN_ISEXPANDED, value); }
    }

    #endregion

    #region Nodes

    #region ReadOnlyCollectionProvider<TModel, TViewModel> NodeProvider

    ReadOnlyCollectionProvider<TModel, TViewModel> _nodeProvider;
    public ReadOnlyCollectionProvider<TModel, TViewModel> NodeProvider
    {
      get
      {
        if (_nodeProvider == null) 
          _nodeProvider = new ReadOnlyCollectionProvider<TModel, TViewModel>(GetNodesCollection(), CreateNodeViewModel, OnNodeViewModelCreated);
        return _nodeProvider;
      }
    }

    #endregion

    protected virtual void OnNodeViewModelCreated(TViewModel viewModel)
    {
    }

    protected abstract BasicCollection<TModel> GetNodesCollection();
    protected abstract TViewModel CreateNodeViewModel(TModel model);

    #region ReadOnlyObservableCollection<TViewModel> NodeViewModels

    public ReadOnlyObservableCollection<TViewModel> NodeViewModels
    {
      get { return NodeProvider.ViewModelCollection; }
    }

    #endregion

    #endregion

    ITreeViewViewModel ITreeViewNodeViewModel.Owner
    {
      get { return (ITreeViewViewModel)Owner; }
    }
  }
}
