﻿using Dataforge.Business.Activities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Dataforge.Prism.Views.OperationManagement
{
  public class OperationNodeViewModel : TreeNodeViewModel<Operation>
  {
    #region Constructors

    public OperationNodeViewModel(Operation op, IMultiModelViewModel owner)
      : base(op, owner)
    {
    }

    #endregion

    #region Model Propertry string Text

    public string Text
    {
      get
      {
        if (Model == null) return default(string);
        return string.IsNullOrWhiteSpace(Model.Text) ? "** Operation **" : Model.Text;
      }
      set { Model.Text = value; }
    }

    #endregion

    #region Visibility ImageVisibility

    public Visibility ImageVisibility
    {
      get
      {
        if (Model == null) return Visibility.Collapsed;
        if (string.IsNullOrWhiteSpace(Model.Base64Image)) return Visibility.Collapsed;
        return Visibility.Visible;
      }
    }

    #endregion

    #region ImageSource ImageSource

    ImageSource _imageSource;
    public ImageSource ImageSource
    {
      get
      {
        if (Model == null) return default(ImageSource);
        if (string.IsNullOrWhiteSpace(Model.Base64Image)) return null;
        if (_imageSource == null)
        {
          byte[] imgData = Convert.FromBase64String(Model.Base64Image);
          BitmapImage img = new BitmapImage();
          using (MemoryStream ms = new MemoryStream(imgData))
          {
            img.BeginInit();
            img.CacheOption = BitmapCacheOption.OnLoad;
            img.StreamSource = ms;
            img.EndInit();
          }
          _imageSource = img as ImageSource;
        }
        return _imageSource;
      }
    }

    public void SetImageFromFile(string filename)
    {
      Model.SetImageFromFile(filename);
      _imageSource = null;
      OnPropertyChanged("ImageSource");
      OnPropertyChanged("ImageVisibility");
    }

    #endregion
  }
}
