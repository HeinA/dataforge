﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views.OperationManagement
{
  public class ContextTypeViewModel
  {
    public ContextTypeViewModel()
    {
    }

    public ContextTypeViewModel(Type contextType)
    {
      ContextType = contextType;
    }

    Type _contextType;
    public Type ContextType
    {
      get { return _contextType; }
      private set
      {
        _contextType = value;
        ContextTypeName = string.Format("{0}, {1}", value.FullName, value.Assembly.GetName().Name);
      }
    }

    string _contextTypeName;
    public string ContextTypeName
    {
      get { return _contextTypeName; }
      private set { _contextTypeName = value; }
    }
  }
}
