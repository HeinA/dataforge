﻿using Dataforge.Business.Activities;
using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using Dataforge.Business.Service;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views.OperationManagement
{
  [Export]
  [PartCreationPolicy(CreationPolicy.NonShared)]
  public class OperationManagementViewModel : ManagementViewModel<OperationGroup, OperationGroupNodeViewModel>
  {
    public const string ContextRegion = "ContextRegion";

    #region Constructors

    public OperationManagementViewModel()
    {
      Title = "Operation Management";
      IsFocused = true;
    }

    public OperationManagementViewModel(BasicCollection<OperationGroup> os)
      : base(os)
    {
      Title = "Operation Management";
      IsFocused = true;
    }

    #endregion

    #region Import OperationGroupView OperationGroupView & OperationGroupViewModel OperationGroupViewModel

    #region Import OperationGroupView OperationGroupView

    [Import]
    OperationGroupView _operationGroupView = null;
    public OperationGroupView OperationGroupView
    {
      get { return _operationGroupView; }
    }

    #endregion

    #region Import OperationGroupViewModel OperationGroupViewModel

    [Import]
    OperationGroupViewModel _operationGroupViewModel = null;
    public OperationGroupViewModel OperationGroupViewModel
    {
      get { return _operationGroupViewModel; }
    }

    #endregion

    #endregion

    #region Import OperationView OperationView & OperationViewModel OperationViewModel

    #region Import OperationView OperationView

    [Import]
    OperationView _operationView = null;
    public OperationView OperationView
    {
      get { return _operationView; }
    }

    #endregion

    #region Import OperationViewModel OperationViewModel

    [Import]
    OperationViewModel _operationViewModel = null;
    public OperationViewModel OperationViewModel
    {
      get { return _operationViewModel; }
    }

    #endregion

    #endregion

    #region Overrides

    #region int ActivityId

    public override int ActivityId
    {
      get { return Activities.OperationManagement; }
    }

    #endregion

    #region void Load()

    public override void Load()
    {
      using (var svc = ServiceFactory.GetService<IDataforgeService>())
      {
        ModelCollection = svc.ExecuteFunction<BasicCollection<OperationGroup>>(i => i.LoadOperationGroups());
      }
    }

    #endregion

    #region void Save()

    public override void Save()
    {
      using (var svc = ServiceFactory.GetService<IDataforgeService>())
      {
        ModelCollection = svc.ExecuteFunction<BasicCollection<OperationGroup>>(i => i.SaveOperationGroups(ModelCollection));
      }
    }

    #endregion

    #region OperationGroupNodeViewModel CreateViewModel(OperationGroup model)

    public override OperationGroupNodeViewModel CreateViewModel(OperationGroup model)
    {
      return new OperationGroupNodeViewModel(model, this);
    }

    #endregion

    #region void OnFocusedViewModelChanged(ISelectableViewModel viewModel)

    protected override void OnFocusedViewModelChanged(ISelectableViewModel viewModel)
    {
      if (viewModel is OperationGroupNodeViewModel)
      {
        OperationGroupNodeViewModel vm = viewModel as OperationGroupNodeViewModel;
        ShowInRegion(ContextRegion, OperationGroupView);
        OperationGroupViewModel.Model = vm.Model;
        OperationGroupViewModel.IsReadOnly = !Activity.CanEdit;
      }
      else
      {
        RemoveFromRegion(ContextRegion, OperationGroupView);
      }

      if (viewModel is OperationNodeViewModel)
      {
        OperationNodeViewModel vm = viewModel as OperationNodeViewModel;
        ShowInRegion(ContextRegion, OperationView);
        OperationViewModel.Model = vm.Model;
        OperationViewModel.IsReadOnly = !Activity.CanEdit;
      }
      else
      {
        RemoveFromRegion(ContextRegion, OperationView);
      }

      base.OnFocusedViewModelChanged(viewModel);
    }

    #endregion

    #region bool OnCanExecuteOperation(Operation operation)

    protected override bool OnCanExecuteOperation(Operation operation)
    {
      return base.OnCanExecuteOperation(operation);
    }

    #endregion

    #region void OnExecuteOperation(Operation operation)

    int _iCount = 1;
    protected override void OnExecuteOperation(Operation operation)
    {
      switch (operation.Id)
      {
        case Operations.Operations_AddGroup:
          {
            OperationGroup opg = new OperationGroup();
            opg.GroupName = string.Format("New Operation Group {0}", _iCount++);
            ModelCollection.Add(opg);
            ISelectableViewModel node = GetViewModel(opg);
            node.IsSelected = true;
            OperationGroupViewModel.IsFocused = true;
          }
          break;

        case Operations.Operations_AddOperation:
          {
            Operation op = new Operation();
            op.Text = string.Format("New Operation {0}", _iCount++);
            ((OperationGroup)FocusedModel).Operations.Add(op);
            ISelectableViewModel node = GetViewModel(op);
            ((ITreeNodeViewModel)FocusedViewModel).IsExpanded = true;
            node.IsSelected = true;
            OperationViewModel.IsFocused = true;
          }
          break;

        case Operations.Operations_SetIcon:
          {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = false;
            ofd.Filter = "Icon Files|*.ico;*.png;*.jpg|All Files|*.*";
            if (ofd.ShowDialog().Value)
            {
              OperationNodeViewModel node = (OperationNodeViewModel)FocusedViewModel;
              node.SetImageFromFile(ofd.FileName);
            }
          }
          break;
      }

      base.OnExecuteOperation(operation);
    }

    #endregion

    #endregion
  }
}
