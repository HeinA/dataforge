﻿using Dataforge.Business.Activities;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views.OperationManagement
{
  [Export]
  [PartCreationPolicy(CreationPolicy.Shared)]
  public class OperationGroupViewModel : ViewModel<OperationGroup>
  {
    #region Model Propertry string GroupName

    public string GroupName
    {
      get
      {
        if (Model == null) return default(string);
        return Model.GroupName;
      }
      set { Model.GroupName = value; }
    }

    #endregion

    #region Model Propertry bool IsGlobal

    public bool IsGlobal
    {
      get
      {
        if (Model == null) return default(bool);
        return Model.IsGlobal;
      }
      set { Model.IsGlobal = value; }
    }

    #endregion
  }
}
