﻿using Dataforge.Business.Activities;
using Dataforge.Business.Collections;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views.OperationManagement
{
  public class OperationGroupNodeViewModel : TreeNodeViewModel<OperationGroup>
  {
    #region Constructors

    public OperationGroupNodeViewModel(OperationGroup og, IMultiModelViewModel owner)
      : base(og, owner)
    {
      OperationNodeViewModelsProvider = new ReadOnlyCollectionProvider<Operation, OperationNodeViewModel>(this, og.Operations, CreateOperationNodeViewModel, OnOperationNodeViewModelCreated);
    }

    #endregion

    #region Model Propertry string GroupName

    public string GroupName
    {
      get
      {
        if (Model == null) return default(string);
        return string.IsNullOrWhiteSpace(Model.GroupName) ? "** Operation Group **" : Model.GroupName;
      }
      set { Model.GroupName = value; }
    }

    #endregion

    #region ReadOnlyCollectionProvider OperationNodeViewModels

    ////////////////////////////////////////////////////////
    // Add to Constructor:
    // OperationNodeViewModelsProvider = new ReadOnlyCollectionProvider<Operation, OperationNodeViewModel>(this, /* Optionally add model collection here */ CreateOperationNodeViewModel, OnOperationNodeViewModelCreated);
    ////////////////////////////////////////////////////////

    ReadOnlyCollectionProvider<Operation, OperationNodeViewModel> OperationNodeViewModelsProvider;

    public ReadOnlyObservableCollection<OperationNodeViewModel> OperationNodeViewModels
    {
      get { return OperationNodeViewModelsProvider.ViewModelCollection; }
    }

    public BasicCollection<Operation> Operations
    {
      get { return OperationNodeViewModelsProvider.ModelCollection; }
      set { OperationNodeViewModelsProvider.ModelCollection = value; }
    }

    OperationNodeViewModel CreateOperationNodeViewModel(Operation model)
    {
      return new OperationNodeViewModel(model, Owner);
    }

    void OnOperationNodeViewModelCreated(OperationNodeViewModel viewModel, Operation model)
    {
    }

    #endregion
  }
}
