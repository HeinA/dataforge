﻿using Dataforge.Business;
using Dataforge.Business.Activities;
using Dataforge.Business.Attributes;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views.OperationManagement
{
  [Export]
  [PartCreationPolicy(CreationPolicy.Shared)]
  public class OperationViewModel : ViewModel<Operation>
  {
    public OperationViewModel()
    {
      AddModelPropertyMapping("IsContextOperation", "IsContextOperationEnabled");
    }

    #region Model Propertry int Id

    public int Id
    {
      get
      {
        if (Model == null) return default(int);
        return Model.Id;
      }
      set { Model.Id = value; }
    }

    #endregion

    #region Model Propertry string Text

    public string Text
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Text;
      }
      set { Model.Text = value; }
    }

    #endregion

    #region Model Propertry string Description

    public string Description
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Description;
      }
      set { Model.Description = value; }
    }

    #endregion

    #region Model Propertry bool IsContextOperation

    public bool IsContextOperation
    {
      get
      {
        if (Model == null) return default(bool);
        return Model.IsContextOperation;
      }
      set
      {
        if (Model.IsContextOperation == value) return;
        Model.IsContextOperation = value;
        if (!value) ContextType = null;
      }
    }

    #endregion

    #region Model Propertry Type ContextType

    public Type ContextType
    {
      get
      {
        if (Model == null) return default(Type);
        return Model.ContextType;
      }
      set { Model.ContextType = value; }
    }

    #endregion

    #region Model Propertry bool IsGlobal

    public bool IsGlobal
    {
      get
      {
        if (Model == null) return default(bool);
        return Model.IsGlobal;
      }
      set { Model.IsGlobal = value; }
    }

    #endregion

    #region Collection<ContextTypeViewModel> ContextTypes

    Collection<ContextTypeViewModel> _contextTypes;
    public Collection<ContextTypeViewModel> ContextTypes
    {
      get
      {
        if (_contextTypes != null) return _contextTypes;
        Collection<ContextTypeViewModel> types = new Collection<ContextTypeViewModel>();
        types.Add(new ContextTypeViewModel());
        foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
        {
          try
          {
            foreach (Type t in a.GetTypes().Where(t => !t.IsAbstract && t.IsSubclassOf(typeof(BusinessObject))))
            {
              types.Add(new ContextTypeViewModel(t));
            }
          }
          catch
          {
            throw;
          }
        }
        _contextTypes = new Collection<ContextTypeViewModel>(types.OrderBy(vm => vm.ContextTypeName).ToList());
        return _contextTypes;
      }
    }

    #endregion

    public bool IsContextOperationEnabled
    {
      get { return Model.IsContextOperation && IsEditable; }
    }
  }
}
