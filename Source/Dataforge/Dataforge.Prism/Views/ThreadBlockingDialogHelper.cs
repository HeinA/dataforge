﻿using Dataforge.Prism.Contants;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Dataforge.Prism.Views
{
  public class ThreadBlockingDialogHelper
  {
    public static DialogResult Show(FrameworkElement view)
    {
      return Show(view, RegionNames.DialogRegion);
    }

    public static DialogResult Show(FrameworkElement view, string region)
    {
      DialogViewModel vm = view.DataContext as DialogViewModel;
      if (vm == null) throw new ArgumentException("View DataContext must inherit from DialogViewModel");
      vm.Result = DialogResult.Cancel;

      IRegionManager rm = ServiceLocator.Current.GetInstance<IRegionManager>();
      rm.Regions[region].Add(view);

      while (vm.WindowState == Xceed.Wpf.Toolkit.WindowState.Open)
      {
        if (view.Dispatcher.HasShutdownStarted || view.Dispatcher.HasShutdownFinished)
        {
          break;
        }

        view.Dispatcher.Invoke(DispatcherPriority.Background, new ThreadStart(delegate { }));
        Thread.Sleep(20);
      }

      return vm.Result;
    }
  }
}
