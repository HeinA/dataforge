﻿using Dataforge.Business;
using Dataforge.Business.Security;
using Dataforge.Prism.Configuration;
using Dataforge.Prism.Contants;
using Dataforge.Prism.Events;
using Dataforge.Prism.Regions;
using Dataforge.Prism.Views.Ribbon;
using Dataforge.PrismAvalonExtensions;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Dataforge.Prism.Views
{
  public abstract class ViewModel<T> : ViewModel, IViewModel, IDataErrorInfo
    where T : BusinessObject
  {
    #region Constructors

    public ViewModel()
    {
      AddModelPropertyMapping(ViewModel.PN_HASERRROR, "ErrorVisibility");
      AddModelPropertyMapping(ViewModel.PN_HASERRROR, "Error");
    }

    public ViewModel(T model)
    {
      Model = model;
    }

    #endregion

    #region T Model

    T _model;
    public T Model
    {
      get { return _model; }
      set
      {
        if (_model != null)
        {
          _model.PropertyChanged -= Model_PropertyChanged;
          _model.CompositionChanged -= Model_CompositionChanged;
        }
        SetValue<T>(ref _model, value, "Model");
        if (Model != null)
        {
          Model.PropertyChanged += Model_PropertyChanged;
          Model.CompositionChanged += Model_CompositionChanged;
        }
        IsDirty = false;
        HasError = !string.IsNullOrWhiteSpace(Error);
        OnPropertyChanged(null);
        if (ModelChanged != null) ModelChanged(this, new EventArgs());
      }
    }

    public event EventHandler<EventArgs> ModelChanged;

    void Model_CompositionChanged(object sender, CompositionChangedEventArgs e)
    {
      IsDirty = true;
    }

    void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      OnPropertyChanged(e.PropertyName);
      HasError = Model.HasError;
    }

    #endregion

    #region IViewModel

    object IViewModel.ModelSource
    {
      get { return Model; }
      set { Model = (T)value; }
    }

    #endregion

    #region IDataErrorInfo

    public string Error
    {
      get { return Model == null ? null : Model.Error; }
    }

    public string this[string columnName]
    {
      get { return Model == null ? null : ((IDataErrorInfo)Model)[columnName]; }
    }

    //public string Error
    //{
    //  get
    //  {
    //    if (Model == null) return null;
    //    if (Model.IsDeleted) return null;
    //    Collection<string> errors = new Collection<string>();
    //    errors.Add(Model.Error);
    //    GetErrors(errors, null);
    //    string error = string.Join("\n", errors);
    //    if (string.IsNullOrWhiteSpace(error)) error = null;
    //    return error;
    //  }
    //}

    //public string this[string columnName]
    //{
    //  get
    //  {
    //    if (Model == null) return null;
    //    if (Model.IsDeleted) return null;
    //    Collection<string> errors = new Collection<string>();
    //    GetErrors(errors, columnName);
    //    string vme = string.Join("\n", errors);
    //    string me = ((IDataErrorInfo)Model)[columnName];
    //    if (!string.IsNullOrWhiteSpace(vme) && !string.IsNullOrWhiteSpace(vme))
    //    {
    //      return string.Format("{0}\n{1}", me, vme);
    //    }
    //    else if (!string.IsNullOrWhiteSpace(vme)) return vme;
    //    else return me;
    //  }
    //}

    //protected virtual void GetErrors(Collection<string> errors, string propertyName)
    //{
    //}

    #endregion

    public virtual void Initialize()
    {
      IsFocused = true;
    }
  }



  public abstract class ViewModel : INotifyPropertyChanged, IPartImportsSatisfiedNotification, IRegionManagerAware
  {
    #region IEventAggregator EventAggregator

    [Import]
    IEventAggregator _globalEventAggregator = null;
    public IEventAggregator GlobalEventAggregator
    {
      get { return _globalEventAggregator; }
    }

    #endregion

    #region IEventAggregator LocalEventAggregator

    //IEventAggregator _localEventAggregator;
    public IEventAggregator LocalEventAggregator
    {
      get
      {
        return EventAggregatorHelper.GetLocalEventAggregator(LocalRegionManager);
        //if (_localEventAggregator == null) _localEventAggregator = new EventAggregator();
        //return _localEventAggregator;
      }
      //set { _localEventAggregator = value; }
    }

    #endregion

    #region IRegionManager GlobalRegionManager

    [Import]
    IRegionManager _globalRegionManager = null;
    public IRegionManager GlobalRegionManager
    {
      get { return _globalRegionManager; }
    }

    #endregion

    #region IRegionManager LocalRegionManager

    IRegionManager _localRegionManager = null;
    public IRegionManager LocalRegionManager
    {
      get { return _localRegionManager; }
      set
      {
        if (_localRegionManager == value) return;
        _localRegionManager = value;
        OnLocalRegionManagerChanged();
      }
    }

    protected virtual void OnLocalRegionManagerChanged()
    {
    }

    #endregion

    #region bool IsFocused

    bool _isFocused = false;
    /// <summary>
    /// IsFocused will always return false. Set to true to set focus.
    /// </summary>
    public virtual bool IsFocused
    {
      get { return _isFocused; }
      set
      {
        if (SetValue<bool>(ref _isFocused, value, true, "IsFocused"))
        {
          if (value)
          {
            OnFocusGained();
            _isFocused = false;
          }
        }
      }
    }

    protected virtual void OnFocusGained()
    {
    }

    #endregion

    #region bool IsDirty

    public const string PN_ISDIRTY = "IsDirty";
    bool _isDirty = false;
    public bool IsDirty
    {
      get { return _isDirty; }
      set
      {
        if (SetValue<bool>(ref _isDirty, value, PN_ISDIRTY))
        {
          if (_isDirty)
          {
          }
          OnDirtyStateChanged();
        }
      }
    }

    protected virtual void OnDirtyStateChanged()
    {
    }

    #endregion

    #region bool HasError

    public const string PN_HASERRROR = "HasError";
    bool _hasError = false;
    public bool HasError
    {
      get { return _hasError; }
      set
      {
        if (SetValue<bool>(ref _hasError, value, PN_HASERRROR))
        {
          OnErrorStateChanged();
        }
      }
    }

    protected virtual void OnErrorStateChanged()
    {
    }

    #endregion

    #region bool IsReadOnly

    bool _readOnly = false;
    public bool IsReadOnly
    {
      get { return _readOnly; }
      set { SetValue<bool>(ref _readOnly, value, "IsReadOnly"); }
    }

    #endregion

    #region bool IsEditable

    public bool IsEditable
    {
      get { return !IsReadOnly; }
    }

    #endregion

    #region Visibility ErrorVisibility

    public virtual Visibility ErrorVisibility
    {
      get { return HasError ? Visibility.Visible : Visibility.Collapsed; }
    }

    #endregion
    
    #region bool SetValue<T>(ref T field, T value, string propertyName)

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "0#")]
    protected bool SetValue<T>(ref T field, T value, string propertyName, params string[] additionalProperyNotifications)
    {
      return SetValue<T>(ref field, value, false, propertyName, additionalProperyNotifications);
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1045:DoNotPassTypesByReference", MessageId = "0#")]
    protected bool SetValue<T>(ref T field, T value, bool forceNotification, string propertyName, params string[] additionalProperyNotifications) //
    {
      if (string.IsNullOrWhiteSpace(propertyName)) throw new ArgumentException("propertiesChanged");

      ObjectIDGenerator gen = new ObjectIDGenerator();
      bool ft = false;
      if ((field == null && value != null) || (field != null && value == null) || (field != null && gen.GetId(field, out ft) != gen.GetId(value, out ft)) || forceNotification) //
      {
        field = value;
        OnPropertyChanged(propertyName);
        foreach (string pn in additionalProperyNotifications)
        {
          OnPropertyChanged(pn);
        }
        return true;
      }

      return false;
    }

    #endregion

    #region void OnPropertyChanged(string propertyName)

    public event PropertyChangedEventHandler PropertyChanged;
    public virtual void OnPropertyChanged(string propertyName)
    {
      if (PropertyChanged != null)
      {
        PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        if (!string.IsNullOrWhiteSpace(propertyName) && PropertyMappingDictionary.ContainsKey(propertyName))
        {
          foreach (string p in PropertyMappingDictionary[propertyName])
          {
            PropertyChanged(this, new PropertyChangedEventArgs(p));
          }
        }
      }
    }

    #endregion

    #region IPartImportsSatisfiedNotification

    void IPartImportsSatisfiedNotification.OnImportsSatisfied()
    {
      OnInitialized();
    }

    #endregion

    #region void ShowInRegion(string regionName, FrameworkElement view)

    protected virtual void ShowInRegion(string regionName, FrameworkElement view)
    {
      if (LocalRegionManager == null) return;
      if (!LocalRegionManager.Regions[regionName].Views.Contains(view))
        LocalRegionManager.Regions[regionName].Add(view);
    }

    #endregion

    #region void RemoveFromRegion(string regionName, FrameworkElement view)

    protected virtual void RemoveFromRegion(string regionName, FrameworkElement view)
    {
      if (LocalRegionManager.Regions[regionName].Views.Contains(view))
        LocalRegionManager.Regions[regionName].Remove(view);
    }

    #endregion

    #region void AddModelPropertyMapping(string modelPropertyName, string viewModelPropertyName)

    protected void AddModelPropertyMapping(string modelPropertyName, string viewModelPropertyName)
    {
      if (!PropertyMappingDictionary.ContainsKey(modelPropertyName)) PropertyMappingDictionary.Add(modelPropertyName, new Collection<string>());
      Collection<string> mappings = PropertyMappingDictionary[modelPropertyName];
      mappings.Add(viewModelPropertyName);
    }

    Dictionary<string, Collection<string>> _propertyMappingDictionary = new Dictionary<string, Collection<string>>();
    public Dictionary<string, Collection<string>> PropertyMappingDictionary
    {
      get { return _propertyMappingDictionary; }
    }

    #endregion

    #region DelegateCommand OnLoadedCommand

    DelegateCommand _onLoadedCommand;
    public DelegateCommand OnLoadedCommand
    {
      get { return _onLoadedCommand ?? (_onLoadedCommand = new DelegateCommand(ExecuteOnLoaded, CanExecuteOnLoaded)); }
    }

    bool CanExecuteOnLoaded()
    {
      return true;
    }

    bool _loaded = false;
    void ExecuteOnLoaded()
    {
      if (_loaded) return;
      _loaded = true;
      OnLoaded();
    }

    protected virtual void OnLoaded()
    {
    }

    #endregion

    #region OnInitialized

    protected virtual void OnInitialized()
    {
    }

    #endregion
  }
}
