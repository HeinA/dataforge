﻿using Dataforge.Business;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Dataforge.Prism.Views
{
  public abstract class SelectableViewModel<T> : StatefullViewModel<T>, ISelectableViewModel
    where T : BusinessObject, IIdentifiableObject
  {
    #region Constructors

    public SelectableViewModel(T model, IMultiModelViewModel owner)
      : base(model, owner)
    {
      _owner = owner;
      Owner.RegisterItem(this);
      AddModelPropertyMapping(BusinessObject.PN_HASERROR, "Foreground");
      AddModelPropertyMapping(BusinessObject.PN_HASERROR, "FontWeight");
      AddModelPropertyMapping(BusinessObject.PN_ISDELETED, "TextDecoration");
    }

    #endregion

    #region IMultiModelViewModel Owner

    IMultiModelViewModel _owner;
    public IMultiModelViewModel Owner
    {
      get { return _owner; }
    }

    #endregion

    #region bool IsSelected

    const string PN_ISSELECTED = "IsSelected";
    public virtual bool IsSelected
    {
      get { return GetState<bool>(PN_ISSELECTED); }
      set { SetState<bool>(PN_ISSELECTED, value); }
    }

    #endregion

    #region Brush Foreground

    public SolidColorBrush Foreground
    {
      get
      {
        if (Model == null) return new SolidColorBrush(Colors.Black);
        if (Model.HasError) return new SolidColorBrush(Colors.Red);
        return new SolidColorBrush(Colors.Black);
      }
    }

    #endregion

    #region FontWeight FontWeight

    public FontWeight FontWeight
    {
      get
      {
        if (Model == null) return FontWeights.Normal;
        if (Model.HasError) return FontWeights.Bold;
        return FontWeights.Normal;
      }
    }

    #endregion

    #region TextDecorationCollection TextDecoration

    public TextDecorationCollection TextDecoration
    {
      get
      {
        if (Model.IsDeleted) return TextDecorations.Strikethrough;
        else return null;
      }
    }

    #endregion

    #region bool MayDelete

    public virtual bool MayDelete
    {
      get { return true; }
    }

    #endregion
  }
}
