﻿using Dataforge.Business;
using Dataforge.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views
{
  public interface IMultiModelViewModel : IStateProvider, IViewModel
  {
    BusinessObject FocusedModel { get; }
    ISelectableViewModel FocusedViewModel { get; set; }
    BasicCollection<ISelectableViewModel> SelectedViewModels { get; }
    void SetSingleSelection(ISelectableViewModel vm);
    void RegisterItem(ISelectableViewModel viewModel);
    ISelectableViewModel GetViewModel(BusinessObject obj);
  }
}
