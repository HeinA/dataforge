﻿using Dataforge.Business;
using Dataforge.Business.Activities;
using Dataforge.Business.Constants;
using Dataforge.Business.Security;
using Dataforge.Prism.Events;
using Dataforge.Prism.Views.Dialogs;
using Dataforge.PrismAvalonExtensions;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.ServiceLocation;
using System.Collections.ObjectModel;
using System.Linq;

namespace Dataforge.Prism.Views
{
  public abstract class OperationsAwareViewModel<T> : ViewModel<T>, IOperationTarget, IClosingValidator, ITitledControl
    where T : Operation
  {
    #region Constructors

    public OperationsAwareViewModel(T model)
      : base(model)
    {
      OperationsContainer = new OperationsContainer(this);
    }


    OperationViewModel CreateOperationViewModel(Operation op)
    {
      return new OperationViewModel(this, op);
    }

    #endregion

    #region OperationsContainer OperationsContainer

    OperationsContainer _operationsContainer;
    OperationsContainer OperationsContainer
    {
      get { return _operationsContainer; }
      set { _operationsContainer = value; }
    }

    const string PN_GLOBALOPERATIONVIEWMODELS = "GlobalOperationViewModels";
    public ReadOnlyObservableCollection<OperationViewModel> GlobalOperationViewModels
    {
      get { return OperationsContainer.GlobalOperationViewModels; }
    }

    const string PN_CONTEXTOPERATIONVIEWMODELS = "ContextOperationViewModels";
    public ReadOnlyObservableCollection<OperationViewModel> ContextOperationViewModels
    {
      get { return OperationsContainer.ContextOperationViewModels; }
    }

    protected OperationViewModel GetOperationViewModel(Operation operation)
    {
      return OperationsContainer.GetOperationViewModel(operation);
    }

    #endregion

    #region IOperationTarget

    #region CanExecuteOperation(Operation operation)

    public virtual bool CanExecuteOperation(Operation operation)
    {
      return true;
    }

    #endregion

    #region void ExecuteOperation(Operation operation)

    public virtual void ExecuteOperation(Operation operation)
    {
      RaiseCanExecuteChanged();
    }

    #endregion

    #region void RaiseCanExecuteChanged()

    public void RaiseCanExecuteChanged()
    {
      foreach (var opvm in OperationsContainer.GlobalOperationViewModels)
      {
        opvm.RaiseCanExecuteChanged();
      }
      foreach (var opvm in OperationsContainer.ContextOperationViewModels)
      {
        opvm.RaiseCanExecuteChanged();
      }
    }

    #endregion

    #endregion

    #region ITitledControl

    string _title = null;
    public string Title
    {
      get { return _title; }
      set { SetValue<string>(ref _title, value, "Title"); }
    }

    protected override void OnDirtyStateChanged()
    {
      if (Title != null && IsDirty && Title.Substring(0, 1) != "*")
        Title = string.Format("*{0}", Title);
      if (Title != null && !IsDirty && Title.Substring(0, 1) == "*")
        Title = Title.Substring(1, Title.Length - 1);
      base.OnDirtyStateChanged();
    }

    #endregion

    #region Activity Activity

    protected abstract int ActivityId { get; }

    protected Activity Activity
    {
      get { return Cache.Instance.GetObject<Activity>(ActivityId); }
    }

    #endregion

    #region DelegateCommand OnLoadedCommand

    DelegateCommand _onLoadedCommand;
    public DelegateCommand OnLoadedCommand
    {
      get { return _onLoadedCommand ?? (_onLoadedCommand = new DelegateCommand(ExecuteOnLoaded, CanExecuteOnLoaded)); }
    }

    bool CanExecuteOnLoaded()
    {
      return true;
    }

    void ExecuteOnLoaded()
    {
      OperationsContainer.Activity = Activity; ;
      OnPropertyChanged(PN_GLOBALOPERATIONVIEWMODELS);
      OnPropertyChanged(PN_CONTEXTOPERATIONVIEWMODELS);
    }

    #endregion

    #region IClosingValidator

    public bool OnClosing()
    {
      if (SecurityContext.User == null) return true;

      Operation op = Activity.Operations.FirstOrDefault(o => o.Id == Operations.Save && o.Enabled);
      if (op == null) return true;

      if (HasError)
      {
        DialogResult result = ThreadBlockingDialogHelper.Show(ServiceLocator.Current.GetInstance<CanNotSaveDialog>());
        if (result == DialogResult.Cancel) return false;
      }
      else
      {
        DialogResult result = ThreadBlockingDialogHelper.Show(ServiceLocator.Current.GetInstance<SaveDialog>());
        if (result == DialogResult.Cancel) return false;
        if (result == DialogResult.Yes)
        {
          OperationViewModel opvm = GetOperationViewModel(op);
          if (opvm == null) return true;
          opvm.OperationCommand.Execute();
          return true;
        }
      }

      return true;
    }

    #endregion

    #region void OnOperationNameOverrideEvent(OperationNameOverrideEventArgs args)

    void OnOperationNameOverrideEvent(OperationOverrideEventArgs args)
    {
      switch (args.Operation.Id)
      {
        case Operations.ToggleDelete:
          if (args.OperationTargets.Count == 0)
          {
            args.Visible = false;
            return;
          }
          bool b = args.OperationTargets[0].IsDeleted;
          foreach (var ot in args.OperationTargets)
          {
            if (ot.IsDeleted != b) return;
          }
          if (!b) args.Text = "Delete";
          else args.Text = "Undelete";
          break;
      }
    }

    #endregion

    public void VisibilityChanged(OperationViewModel vm)
    {
    }
  }
}
