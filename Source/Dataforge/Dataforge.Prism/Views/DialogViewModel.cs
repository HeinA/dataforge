﻿using Dataforge.Prism.Views;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using Xceed.Wpf.Toolkit;

namespace Dataforge.Prism.Views
{
  public abstract class DialogViewModel : ViewModel
  {
    #region DialogResult Result

    DialogResult _result = DialogResult.Cancel;
    public DialogResult Result
    {
      get { return _result; }
      set { SetValue<DialogResult>(ref _result, value, "Result"); }
    }

    #endregion

    #region bool MayCloseWindow

    bool _mayCloseWindow = true;
    protected bool MayCloseWindow
    {
      get { return _mayCloseWindow; }
      set { _mayCloseWindow = value; }
    }

    #endregion

    #region Commands

    #region ICommand OkCommand

    DelegateCommand _okCommand;
    public ICommand OkCommand
    {
      get
      {
        if (_okCommand == null) _okCommand = new DelegateCommand(OnOk, CanApply);
        return _okCommand;
      }
    }

    void OnOk()
    {
      OnApply();
      if (MayCloseWindow)
      {
        Result = DialogResult.Ok;
        WindowState = Xceed.Wpf.Toolkit.WindowState.Closed;
      }
    }

    #endregion

    #region ICommand ApplyCommand

    DelegateCommand _applyCommand;
    public ICommand ApplyCommand
    {
      get
      {
        if (_applyCommand == null) _applyCommand = new DelegateCommand(OnApply, CanApply);
        return _applyCommand;
      }
    }

    protected virtual bool CanApply()
    {
      return true;
    }

    protected virtual void OnApply()
    {
    }

    #endregion

    #region ICommand CancelCommand

    DelegateCommand _cancelCommand;
    public ICommand CancelCommand
    {
      get
      {
        if (_cancelCommand == null) _cancelCommand = new DelegateCommand(OnCancel);
        return _cancelCommand;
      }
    }

    protected virtual void OnCancel()
    {
      Result = DialogResult.Cancel;
      WindowState = Xceed.Wpf.Toolkit.WindowState.Closed;
    }

    #endregion

    #region ICommand YesCommand

    DelegateCommand _yesCommand;
    public ICommand YesCommand
    {
      get
      {
        if (_yesCommand == null) _yesCommand = new DelegateCommand(OnYes);
        return _yesCommand;
      }
    }

    protected virtual void OnYes()
    {
      Result = DialogResult.Yes;
      WindowState = Xceed.Wpf.Toolkit.WindowState.Closed;
    }

    #endregion

    #region ICommand NoCommand

    DelegateCommand _noCommand;
    public ICommand NoCommand
    {
      get
      {
        if (_noCommand == null) _noCommand = new DelegateCommand(OnNo);
        return _noCommand;
      }
    }

    protected virtual void OnNo()
    {
      Result = DialogResult.No;
      WindowState = Xceed.Wpf.Toolkit.WindowState.Closed;
    }

    #endregion

    protected void RaiseCanExecuteChanged()
    {
      ((DelegateCommand)ApplyCommand).RaiseCanExecuteChanged();
    }

    #endregion

    #region Command Handlers

    #endregion

    #region WindowState WindowState

    WindowState _windowState = WindowState.Open;
    public WindowState WindowState
    {
      get { return _windowState; }
      set { SetValue<WindowState>(ref _windowState, value, "WindowState"); }
    }

    #endregion

    #region string Title
    
    public abstract string Title
    {
      get;
    }

    #endregion

    #region void OnPropertyChanged(string propertyName)

    public override void OnPropertyChanged(string propertyName)
    {
      base.OnPropertyChanged(propertyName);
      if (_okCommand != null) _okCommand.RaiseCanExecuteChanged();
      if (_applyCommand != null) _applyCommand.RaiseCanExecuteChanged();
    }

    #endregion
  }
}
