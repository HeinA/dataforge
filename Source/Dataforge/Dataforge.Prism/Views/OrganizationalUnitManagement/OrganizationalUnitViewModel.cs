﻿using Dataforge.Business.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views.OrganizationalUnitManagement
{
  [Export]
  [PartCreationPolicy(CreationPolicy.Shared)]
  public class OrganizationalUnitViewModel : ViewModel<OrganizationalUnit>
  {
    #region Model Propertry string Name

    public string Name
    {
      get
      {
        if (Model == null) return default(string);
        else return Model.Name;
      }
      set { Model.Name = value; }
    }

    #endregion

    #region Model Propertry bool UserAssignable

    public bool UserAssignable
    {
      get
      {
        if (Model == null) return default(bool);
        return Model.UserAssignable;
      }
      set { Model.UserAssignable = value; }
    }

    #endregion

    public string StructureTypeText
    {
      get { return Model.StructureType.Name; }
    }
  }
}
