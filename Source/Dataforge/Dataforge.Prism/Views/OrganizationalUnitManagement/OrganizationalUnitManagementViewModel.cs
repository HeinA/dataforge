﻿using Dataforge.Business;
using Dataforge.Business.Activities;
using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using Dataforge.Business.Security;
using Dataforge.Business.Service;
using Dataforge.Prism.Events;
using Microsoft.Practices.Prism.PubSubEvents;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views.OrganizationalUnitManagement
{
  [Export]
  [PartCreationPolicy(CreationPolicy.NonShared)]
  public class OrganizationalUnitManagementViewModel : ManagementViewModel<OrganizationalUnit, OrganizationalUnitNodeViewModel>
  {
    public const string ContextRegion = "ContextRegion";

    #region Constructors

    public OrganizationalUnitManagementViewModel()
    {
      Title = "Organizational Units";
      IsFocused = true;
    }

    public OrganizationalUnitManagementViewModel(BasicCollection<OrganizationalUnit> ous)
      : base(ous)
    {
      Title = "Organizational Units";
      IsFocused = true;
    }

    #endregion

    #region Import OrganizationalUnitView OrganizationalUnitView

    [Import]
    OrganizationalUnitView _organizationalUnitView = null;
    public OrganizationalUnitView OrganizationalUnitView
    {
      get { return _organizationalUnitView; }
    }

    #endregion

    #region Import OrganizationalUnitViewModel OrganizationalUnitViewModel

    [Import]
    OrganizationalUnitViewModel _organizationalUnitViewModel = null;
    public OrganizationalUnitViewModel OrganizationalUnitViewModel
    {
      get { return _organizationalUnitViewModel; }
    }

    #endregion

    #region Overrides

    #region int ActivityId

    public override int ActivityId
    {
      get { return Activities.OrganizationalUnitManagement; }
    }

    #endregion

    #region void Load()

    public override void Load()
    {
      using (var svc = ServiceFactory.GetService<IDataforgeService>())
      {
        ModelCollection = svc.ExecuteFunction<BasicCollection<OrganizationalUnit>>(i => i.LoadOrganizationalUnits());
      }
    }

    #endregion

    #region void Save()

    public override void Save()
    {
      using (var svc = ServiceFactory.GetService<IDataforgeService>())
      {
        ModelCollection = svc.ExecuteFunction<BasicCollection<OrganizationalUnit>>(i => i.SaveOrganizationalUnits(ModelCollection));
      }
    }

    #endregion

    #region OrganizationalUnitNodeViewModel CreateViewModel(OrganizationalUnit model)

    public override OrganizationalUnitNodeViewModel CreateViewModel(OrganizationalUnit model)
    {
      return new OrganizationalUnitNodeViewModel(model, this);
    }

    #endregion

    #region void OnFocusedViewModelChanged(ITreeNodeViewModel viewModel)

    protected override void OnFocusedViewModelChanged(ISelectableViewModel viewModel)
    {
      if (viewModel is OrganizationalUnitNodeViewModel)
      {
        OrganizationalUnitNodeViewModel vm = viewModel as OrganizationalUnitNodeViewModel;
        ShowInRegion(ContextRegion, OrganizationalUnitView);
        OrganizationalUnitViewModel.Model = vm.Model;
        OrganizationalUnitViewModel.IsReadOnly = !Activity.CanEdit;
      }
      else
      {
        RemoveFromRegion(ContextRegion, OrganizationalUnitView);
      }

      base.OnFocusedViewModelChanged(viewModel);
    }

    #endregion

    #region bool OnCanExecuteOperation(Operation operation)

    protected override bool OnCanExecuteOperation(Operation operation)
    {
      return base.OnCanExecuteOperation(operation);
    }

    #endregion

    #region void OnExecuteOperation(Operation operation)

    int _iCount = 1;
    protected override void OnExecuteOperation(Operation operation)
    {
      switch (operation.Id)
      {
        case Operations.OrganizationalUnit_AddRoot:
          {
            OrganizationalUnit ou = new OrganizationalUnit();
            ou.StructureType = (OrganizationalStructureType)operation.Tag;
            ou.Name = string.Format("New Organizational Unit {0}", _iCount++);
            ModelCollection.Add(ou);
            ISelectableViewModel node = GetViewModel(ou);
            node.IsSelected = true;
            OrganizationalUnitViewModel.IsFocused = true;
          }
          break;

        case Operations.OrganizationalUnit_AddSubUnit:
          {
            OrganizationalUnit ou = new OrganizationalUnit();
            ou.StructureType = (OrganizationalStructureType)operation.Tag;
            ou.Name = string.Format("New Organizational Unit {0}", _iCount++);
            ((OrganizationalUnit)FocusedModel).SubUnits.Add(ou);
            ISelectableViewModel node = GetViewModel(ou);
            ((ITreeNodeViewModel)FocusedViewModel).IsExpanded = true;
            node.IsSelected = true;
            OrganizationalUnitViewModel.IsFocused = true;
          }
          break;
      }

      base.OnExecuteOperation(operation);
    }

    #endregion

    #region void OnInitialized()

    protected override void OnInitialized()
    {
      base.OnInitialized();
    }

    protected override void OnLocalRegionManagerChanged()
    {
      LocalEventAggregator.GetEvent<OperationOverrideEvent>().Subscribe(OnOperationOverride, ThreadOption.PublisherThread);
      LocalEventAggregator.GetEvent<OperationReplaceEvent>().Subscribe(OnOperationReplace, ThreadOption.PublisherThread);
      base.OnLocalRegionManagerChanged();
    }

    private void OnOperationReplace(OperationReplaceEventArgs e)
    {
      switch (e.Operation.Id)
      {
        case Operations.OrganizationalUnit_AddSubUnit:
          BasicCollection<OrganizationalStructureType> types = Cache.Instance.GetObjects<OrganizationalStructureType>(ost => ost.Owner == null);
          if (types.Count > 0) CreateSubUnitOperationsRecursive(types[0], e.ReplacementOperations);
          break;
      }
    }

    private void OnOperationOverride(OperationOverrideEventArgs e)
    {
      switch (e.Operation.Id)
      {
        case Operations.OrganizationalUnit_AddRoot:
          BasicCollection<OrganizationalStructureType> types = Cache.Instance.GetObjects<OrganizationalStructureType>(ost => ost.Owner == null);
          if (types.Count > 0)
          {
            e.Text = string.Format("Add {0}", types[0].Name);
            e.Operation.Tag = types[0];
          }
          else e.Visible = false;
          break;

        case Operations.OrganizationalUnit_AddSubUnit:
          OrganizationalUnit ou = e.OperationTargets[0] as OrganizationalUnit;
          if (ou == null) e.Visible = false;
          else if (!ou.StructureType.SubTypes.Contains(e.Operation.Tag)) e.Visible = false;
          else e.Visible = true;
          break;
      }
    }

    void CreateSubUnitOperationsRecursive(OrganizationalStructureType st, Collection<Operation> operations)
    {
      foreach (OrganizationalStructureType subType in st.SubTypes)
      {
        Operation op = new Operation() { Id = Operations.OrganizationalUnit_AddSubUnit, IsContextOperation = true, ContextType = typeof(OrganizationalUnit), Text = string.Format("Add {0}", subType.Name), Tag = subType };
        operations.Add(op);
        CreateSubUnitOperationsRecursive(subType, operations);
      }
    }

    #endregion

    #endregion
  }
}
