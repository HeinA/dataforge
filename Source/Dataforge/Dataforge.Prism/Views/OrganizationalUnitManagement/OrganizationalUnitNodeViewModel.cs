﻿using Dataforge.Business.Collections;
using Dataforge.Business.Security;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views.OrganizationalUnitManagement
{
  public class OrganizationalUnitNodeViewModel : TreeNodeViewModel<OrganizationalUnit>
  {
    #region Constructors

    public OrganizationalUnitNodeViewModel(OrganizationalUnit ou, IMultiModelViewModel owner)
      : base(ou, owner)
    {
      SubUnitViewModelsProvider = new ReadOnlyCollectionProvider<OrganizationalUnit, OrganizationalUnitNodeViewModel>(this, Model.SubUnits, CreateOrganizationalUnitNodeViewModel, OnOrganizationalUnitNodeViewModelCreated);
    }

    #endregion

    #region string Name

    public string Name
    {
      get
      {
        if (Model == null) return default(string);
        else if (string.IsNullOrWhiteSpace(Model.Name)) return "** Organizational Unit **";
        else return Model.Name;
      }
      set { Model.Name = value; }
    }

    #endregion

    #region ReadOnlyCollectionProvider SubUnitViewModels

    ////////////////////////////////////////////////////////
    // Add to Constructor:
    // SubUnitViewModelsProvider = new ReadOnlyCollectionProvider<OrganizationalUnit, OrganizationalUnitNodeViewModel>(this, /* Optionally add model collection here */ CreateOrganizationalUnitNodeViewModel, OnOrganizationalUnitNodeViewModelCreated);
    ////////////////////////////////////////////////////////

    ReadOnlyCollectionProvider<OrganizationalUnit, OrganizationalUnitNodeViewModel> SubUnitViewModelsProvider;

    public ReadOnlyObservableCollection<OrganizationalUnitNodeViewModel> SubUnitViewModels
    {
      get { return SubUnitViewModelsProvider.ViewModelCollection; }
    }

    public BasicCollection<OrganizationalUnit> SubUnits
    {
      get { return SubUnitViewModelsProvider.ModelCollection; }
      set { SubUnitViewModelsProvider.ModelCollection = value; }
    }

    OrganizationalUnitNodeViewModel CreateOrganizationalUnitNodeViewModel(OrganizationalUnit model)
    {
      return new OrganizationalUnitNodeViewModel(model, Owner);
    }

    void OnOrganizationalUnitNodeViewModelCreated(OrganizationalUnitNodeViewModel viewModel, OrganizationalUnit model)
    {
    }

    #endregion
  }
}
