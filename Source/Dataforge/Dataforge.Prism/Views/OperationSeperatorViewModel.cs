﻿using Dataforge.Business.Activities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Dataforge.Prism.Views
{
  public class OperationSeperatorViewModel : ViewModel<OperationBase>
  {
    public OperationSeperatorViewModel(ReadOnlyObservableCollection<OperationViewModel> operations)
    {
      Operations = operations;
    }
  }
}
