﻿using Dataforge.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Dataforge.Prism.Views
{
  public abstract class TreeNodeViewModel<T> : SelectableViewModel<T>, ITreeNodeViewModel
    where T : BusinessObject, IIdentifiableObject
  {
    #region Constructors

    public TreeNodeViewModel(T model, IMultiModelViewModel owner)
      : base(model, owner)
    {
    }

    #endregion

    #region bool IsExpanded

    const string PN_ISEXPANDED = "IsExpanded";
    public bool IsExpanded
    {
      get { return GetState<bool>(PN_ISEXPANDED); }
      set { SetState<bool>(PN_ISEXPANDED, value); }
    }

    #endregion
  }
}
