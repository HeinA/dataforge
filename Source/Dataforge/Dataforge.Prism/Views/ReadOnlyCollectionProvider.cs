﻿using Dataforge.Business;
using Dataforge.Business.Collections;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Dataforge.Prism.Views
{
  public class ReadOnlyCollectionProvider<TModel, TViewModel>
    where TModel : BusinessObject
    where TViewModel : ViewModel<TModel>
  {
    #region Constructors

    public ReadOnlyCollectionProvider(ViewModel viewModel, Func<TModel, TViewModel> createDelegate)
    {
      _viewModel = viewModel;
      _createDelegate = createDelegate;
    }

    public ReadOnlyCollectionProvider(ViewModel viewModel, Func<TModel, TViewModel> createDelegate, Action<TViewModel, TModel> onViewModelCreatedDelegate)
    {
      _viewModel = viewModel;
      _createDelegate = createDelegate;
      _onViewModelCreatedDelegate = onViewModelCreatedDelegate;
    }

    public ReadOnlyCollectionProvider(ViewModel viewModel, BasicCollection<TModel> modelCollection, Func<TModel, TViewModel> createDelegate)
      : this(viewModel, createDelegate)
    {
      ModelCollection = modelCollection;
    }

    public ReadOnlyCollectionProvider(ViewModel viewModel, BasicCollection<TModel> modelCollection, Func<TModel, TViewModel> createDelegate, Action<TViewModel, TModel> onViewModelCreatedDelegate)
      : this(viewModel, createDelegate, onViewModelCreatedDelegate)
    {
      ModelCollection = modelCollection;
    }

    #endregion

    #region ViewModel ViewModel

    ViewModel _viewModel;
    public ViewModel ViewModel
    {
      get { return _viewModel; }
    }

    #endregion

    #region ReadOnlyObservableCollection<TViewModel> ViewModelCollection

    ReadOnlyObservableCollection<TViewModel> _viewModelCollection;
    public ReadOnlyObservableCollection<TViewModel> ViewModelCollection
    {
      get { return _viewModelCollection ?? (_viewModelCollection = new ReadOnlyObservableCollection<TViewModel>(InternalViewModelCollection)); }
    }

    #endregion

    #region BasicCollection<TModel> ModelCollection

    BasicCollection<TModel> _modelCollection;
    public BasicCollection<TModel> ModelCollection
    {
      get
      {
        if (_modelCollection == null)
        {
          _modelCollection = new BasicCollection<TModel>();
          ModelCollection.CollectionChanged += ModelCollection_CollectionChanged;
        }
        return _modelCollection;
      }
      set
      {
        try
        {
          if (_modelCollection != value)
          {
            if (ModelCollection != null) ModelCollection.CollectionChanged -= ModelCollection_CollectionChanged;
            Stack<TViewModel> stack = new Stack<TViewModel>(InternalViewModelCollection);
            _modelCollection = value;
            InternalViewModelCollection.Clear();
            if (value != null)
            {
              foreach (var vm in (from child in ModelCollection select CreateViewModel(child))) InternalViewModelCollection.Add(vm);
              ModelCollection.CollectionChanged += ModelCollection_CollectionChanged;
            }
            while (stack.Count > 0)
            {
              TViewModel vm = stack.Pop();
              InternalViewModelCollection.Remove(vm);
            }
            ViewModel.OnPropertyChanged(null);
            if (ModelCollectionChanged != null) ModelCollectionChanged(this, new EventArgs());
          }
        }
        catch (Exception ex)
        {
          if (ExceptionPolicy.HandleException(ex, "Default Policy"))
            throw;
        }
      }
    }

    public event EventHandler<EventArgs> ModelCollectionChanged;

    #endregion



    #region Private

    #region TViewModel CreateViewModel(TModel model)

    TViewModel CreateViewModel(TModel model)
    {
      try
      {
        TViewModel vm = CreateDelegate(model);
        if (OnViewModelCreatedDelegate != null) OnViewModelCreatedDelegate(vm, model);
        return vm;
      }
      catch (Exception ex)
      {
        if (ExceptionPolicy.HandleException(ex, "Default Policy"))
          throw;

        return null;
      }
    }

    #endregion

    #region Func<TModel, TViewModel> _createDelegate

    Func<TModel, TViewModel> _createDelegate;
    Func<TModel, TViewModel> CreateDelegate
    {
      get { return _createDelegate; }
    }

    #endregion

    #region Action<TViewModel> OnViewModelCreatedDelegate

    Action<TViewModel, TModel> _onViewModelCreatedDelegate;
    Action<TViewModel, TModel> OnViewModelCreatedDelegate
    {
      get { return _onViewModelCreatedDelegate; }
    }

    #endregion

    #region BasicCollection<TViewModel> InternalViewModelCollection

    BasicCollection<TViewModel> _internalViewModelCollection = new BasicCollection<TViewModel>();
    BasicCollection<TViewModel> InternalViewModelCollection
    {
      get { return _internalViewModelCollection; }
    }

    #endregion

    #region void ModelCollection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)

    void ModelCollection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      //Application.Current.Dispatcher.Invoke(() =>
      //{
        try
        {
          switch (e.Action)
          {
            case NotifyCollectionChangedAction.Add:
              int i = e.NewStartingIndex;
              foreach (TModel o in e.NewItems)
              {
                TViewModel vm = CreateViewModel(o);
                InternalViewModelCollection.Insert(i++, vm);
              }
              break;

            case NotifyCollectionChangedAction.Remove:
              foreach (TModel o in e.OldItems)
              {
                TViewModel vm = InternalViewModelCollection.FirstOrDefault(p => p.Model == o);
                if (vm != null) InternalViewModelCollection.Remove(vm);
              }
              break;

            case NotifyCollectionChangedAction.Reset:
              if (ModelCollection.Count == 0) InternalViewModelCollection.Clear();
              InternalViewModelCollection.FireReset();
              break;
          }
        }
        catch (Exception ex)
        {
          if (ExceptionPolicy.HandleException(ex, "Default Policy"))
            throw;
        }
      //});
    }

    #endregion

    #endregion
  }
}
