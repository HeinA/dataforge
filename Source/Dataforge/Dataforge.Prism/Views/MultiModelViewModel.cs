﻿using Dataforge.Business;
using Dataforge.Business.Collections;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views
{
  public abstract class MultiModelViewModel<TModel, TViewModel> : ViewModel, IMultiModelViewModel, IViewModel
    where TModel : BusinessObject
    where TViewModel : ViewModel<TModel>, ISelectableViewModel
  {
    #region Constructors

    public MultiModelViewModel()
    {
      CollectionProvider = new ReadOnlyCollectionProvider<TModel, TViewModel>(this, CreateViewModel, OnViewModelCreated);
      SelectedViewModels.CollectionChanged += SelectedViewModels_CollectionChanged;
    }

    public MultiModelViewModel(BasicCollection<TModel> modelCollection)
    {
      CollectionProvider = new ReadOnlyCollectionProvider<TModel, TViewModel>(this, modelCollection, CreateViewModel, OnViewModelCreated);
      OnModelCollectionUpdated();
      SelectedViewModels.CollectionChanged += SelectedViewModels_CollectionChanged;
    }

    public MultiModelViewModel(TModel model)
    {
      CollectionProvider = new ReadOnlyCollectionProvider<TModel, TViewModel>(this, new BasicCollection<TModel>(new TModel[] { model }), CreateViewModel, OnViewModelCreated);
      OnModelCollectionUpdated();
      SelectedViewModels.CollectionChanged += SelectedViewModels_CollectionChanged;
    }

    #endregion

    void SelectedViewModels_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      OnSelectionChanged();
    }

    protected virtual void OnSelectionChanged()
    {
    }

    #region ReadOnlyCollectionProvider ViewModels

    public abstract TViewModel CreateViewModel(TModel model);

    protected ReadOnlyCollectionProvider<TModel, TViewModel> CollectionProvider;
    protected virtual void OnViewModelCreated(TViewModel obj, TModel model)
    {
      model.CompositionChanged += Model_CompositionChanged;
      model.PropertyChanged += Model_PropertyChanged;
      IsDirty = true;
    }

    public ReadOnlyObservableCollection<TViewModel> ViewModelCollection
    {
      get { return CollectionProvider.ViewModelCollection; }
    }

    public BasicCollection<TModel> ModelCollection
    {
      get { return CollectionProvider.ModelCollection; }
      set
      {
        if (CollectionProvider.ModelCollection != null)
        {
          foreach (BusinessObject bo in CollectionProvider.ModelCollection)
          {
            bo.CompositionChanged -= Model_CompositionChanged;
            bo.PropertyChanged -= Model_PropertyChanged;
          }
        }
        OnModelCollectionUpdating();

        //TODO: Check!  Causing Activity Folerd Operations Listbox to show a selected value, but have none selected in the viewmodel.
        //SelectedViewModels.Clear();

        CollectionProvider.ModelCollection = value;

        OnModelCollectionUpdated();
        IsDirty = false;
        IsFocused = true;
        OnPropertyChanged(null);
      }
    }

    void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      if (e.PropertyName == BusinessObject.PN_HASERROR)
      {
        foreach (BusinessObject bo in ModelCollection)
        {
          if (bo.HasError)
          {
            HasError = true;
            return;
          }
        }
        HasError = false;
      }
    }

    void Model_CompositionChanged(object sender, CompositionChangedEventArgs e)
    {
      IsDirty = true;
    }

    protected virtual void OnModelCollectionUpdated()
    {
      if (!ViewModelCollection.Contains(FocusedViewModel))
      {
        if (ViewModelCollection.Count > 0)
        {
          foreach (ISelectableViewModel vm in this.ViewModelCollection.Where(v => v.IsSelected))
          {
            SelectedViewModels.Add(vm);
          }
          if (SelectedViewModels.Count > 0) FocusedViewModel = SelectedViewModels[0];
          else FocusedViewModel = ViewModelCollection[0];
          FocusedViewModel.IsSelected = true;
        }
        else
        {
          FocusedViewModel = null;
        }
      }
    }

    protected void OnModelCollectionUpdating()
    {
      NodeDictionary.Clear();
    }

    #endregion

    #region IMultiModelViewModel

    public void RegisterItem(ISelectableViewModel viewModel)
    {
      if (NodeDictionary.ContainsKey((BusinessObject)viewModel.ModelSource)) NodeDictionary.Remove((BusinessObject)viewModel.ModelSource);
      NodeDictionary.Add((BusinessObject)viewModel.ModelSource, viewModel);
      viewModel.LocalRegionManager = this.LocalRegionManager;
    }

    readonly Dictionary<BusinessObject, ISelectableViewModel> _nodeDictionary = new Dictionary<BusinessObject, ISelectableViewModel>();
    private Dictionary<BusinessObject, ISelectableViewModel> NodeDictionary
    {
      get { return _nodeDictionary; }
    }

    public BusinessObject FocusedModel
    {
      get { return FocusedViewModel == null ? null : (BusinessObject)FocusedViewModel.ModelSource; }
    }

    ISelectableViewModel _focusedViewModel;
    public ISelectableViewModel FocusedViewModel
    {
      get { return _focusedViewModel ?? (ISelectableViewModel)(ViewModelCollection.Count > 0 ? ViewModelCollection[0] : null); }
      set
      {
        if (_focusedViewModel == value) return;
        _focusedViewModel = value;
        //if (FocusedViewModel != null)
        //{
        //    FocusedViewModel.IsFocused = true;
        //}
        if (LocalRegionManager != null)
          OnFocusedViewModelChanged(FocusedViewModel);
      }
    }

    //protected override void OnFocusGained()
    //{
    //  if (FocusedViewModel != null)
    //    FocusedViewModel.IsFocused = true;
    //  base.OnFocusGained();
    //}

    public void SetSingleSelection(ISelectableViewModel vm)
    {
      Stack<ISelectableViewModel> stack = new Stack<ISelectableViewModel>(ViewModelCollection.Cast<ISelectableViewModel>());
      while (stack.Count > 0)
      {
        ISelectableViewModel svm = stack.Pop();
        if (!svm.Equals(vm)) svm.IsSelected = false;
      }
      vm.IsSelected = true;
      FocusedViewModel = vm;
    }

    protected override void OnLocalRegionManagerChanged()
    {
      OnFocusedViewModelChanged(FocusedViewModel);
      foreach (IViewModel vm in NodeDictionary.Values)
      {
        vm.LocalRegionManager = this.LocalRegionManager;
      }
      base.OnLocalRegionManagerChanged();
    }

    protected virtual void OnFocusedViewModelChanged(ISelectableViewModel viewModel)
    {
    }

    private BasicCollection<ISelectableViewModel> _selectedViewModels;
    public BasicCollection<ISelectableViewModel> SelectedViewModels
    {
      get { return _selectedViewModels ?? (_selectedViewModels = new BasicCollection<ISelectableViewModel>()); }
    }

    public BasicCollection<BusinessObject> SelectedModels
    {
      get 
      {
        var ret = new  BasicCollection<BusinessObject>();
        foreach (var vm in (from child in SelectedViewModels select (BusinessObject)child.ModelSource)) ret.Add(vm);
        return ret;
      }
    }

    public ISelectableViewModel GetViewModel(BusinessObject obj)
    {
      return !NodeDictionary.ContainsKey(obj) ? null : NodeDictionary[obj];
    }

    #endregion

    #region IStateProvider

    public void SetState<TPropertyType>(IIdentifiableObject io, string propertyName, TPropertyType value)
    {
      if (!StateDictionary.ContainsKey(io.StringIdentifier)) StateDictionary.Add(io.StringIdentifier, new Dictionary<string, object>());
      Dictionary<string, object> objectStateDictionary = StateDictionary[io.StringIdentifier];
      if (!objectStateDictionary.ContainsKey(propertyName))
        objectStateDictionary.Add(propertyName, value);
      else
        objectStateDictionary[propertyName] = value;
      OnPropertyChanged(propertyName);
    }

    public TPropertyType GetState<TPropertyType>(IIdentifiableObject io, string propertyName)
    {
      if (!StateDictionary.ContainsKey(io.StringIdentifier)) return default(TPropertyType);
      Dictionary<string, object> objectStateDictionary = StateDictionary[io.StringIdentifier];
      if (!objectStateDictionary.ContainsKey(propertyName))
        return default(TPropertyType);
      else
        return (TPropertyType)objectStateDictionary[propertyName];
    }

    Dictionary<string, Dictionary<string, object>> _stateDictionary;
    Dictionary<string, Dictionary<string, object>> StateDictionary
    {
      get
      {
        if (_stateDictionary == null) _stateDictionary = new Dictionary<string, Dictionary<string, object>>();
        return _stateDictionary;
      }
      set { _stateDictionary = value; }
    }

    #endregion

    #region IViewModel

    public virtual void Initialize()
    {
      IsFocused = true;
      if (ViewModelCollection.Count > 0) SetSingleSelection((ISelectableViewModel)ViewModelCollection[0]);
    }

    object IViewModel.ModelSource
    {
      get { return ModelCollection; }
      set { ModelCollection = (BasicCollection<TModel>)value; }
    }

    #endregion
  }
}
