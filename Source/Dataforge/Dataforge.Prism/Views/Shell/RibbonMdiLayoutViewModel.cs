﻿using Dataforge.Business;
using Dataforge.Business.Security;
using Dataforge.Business.Service;
using Dataforge.Prism.Configuration;
using Dataforge.Prism.Contants;
using Dataforge.Prism.Events;
using Dataforge.Prism.Regions;
using Dataforge.Prism.Views.Ribbon;
using Dataforge.PrismAvalonExtensions.Events;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Xceed.Wpf.Toolkit;

namespace Dataforge.Prism.Views.Shell
{
  [Export(typeof(RibbonMdiLayoutViewModel))]
  [PartCreationPolicy(CreationPolicy.Shared)]
  public class RibbonMdiLayoutViewModel : ViewModel
  {
    #region DelegateCommand ClosingCommand

    DelegateCommand<CancelEventArgs> _closingCommand;
    public DelegateCommand<CancelEventArgs> ClosingCommand
    {
      get
      {
        if (_closingCommand == null) _closingCommand = new DelegateCommand<CancelEventArgs>(ExecuteClosing, CanExecuteClosing);
        return _closingCommand;
      }
    }

    bool CanExecuteClosing(CancelEventArgs args)
    {
      return true;
    }

    void ExecuteClosing(CancelEventArgs args)
    {
      CloseAllDocumentsEventArgs args1 = new CloseAllDocumentsEventArgs();
      GlobalEventAggregator.GetEvent<CloseAllDocumentsEvent>().Publish(args1);
      args.Cancel = args1.Cancel;
    }

    #endregion

    #region string DefaultTitle

    string _defaultTitle;
    string DefaultTitle
    {
      get { return _defaultTitle; }
      set { _defaultTitle = value; }
    }

    #endregion

    #region string Title

    string _title;
    public string Title
    {
      get { return _title; }
      set { SetValue<string>(ref _title, value, "Title"); }
    }

    #endregion

    #region bool RibbonEnabled

    bool _ribbonEnabled = true;
    public bool RibbonEnabled
    {
      get { return _ribbonEnabled; }
      set
      {
        if (_ribbonEnabled != value)
        {
          _ribbonEnabled = value;
          OnPropertyChanged("RibbonEnabled");
        }
      }
    }

    #endregion

    #region HomeTab HomeTab

    [Import]
    HomeTab _homeTab = null;
    HomeTab HomeTab
    {
      get { return _homeTab; }
    }

    #endregion

    #region Import ManagementTab ManagementTab

    [Import]
    ManagementTab _managementTab = null;
    public ManagementTab ManagementTab
    {
      get { return _managementTab; }
    }

    #endregion

    #region void DialogRegionViews_CollectionChanged

    void DialogRegionViews_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      if (GlobalRegionManager.Regions[RegionNames.DialogRegion].Views.Count() == 0 && GlobalRegionManager.Regions[RegionNames.WaitAnimationRegion].Views.Count() == 0) RibbonEnabled = true;
      else RibbonEnabled = false;
    }

    #endregion 

    #region void OnInitialized()

    protected override void OnInitialized()
    {
      #region Subscribe to RegionManager.Regions[RegionNames.DialogRegion].Views.CollectionChanged

      GlobalRegionManager.Regions[RegionNames.DialogRegion].Views.CollectionChanged += DialogRegionViews_CollectionChanged;
      GlobalRegionManager.Regions[RegionNames.WaitAnimationRegion].Views.CollectionChanged += DialogRegionViews_CollectionChanged;

      #endregion

      #region Event Subscriptions

      GlobalEventAggregator.GetEvent<SecurityContextChangedEvent>().Subscribe(OnSecurityContextChanged, ThreadOption.UIThread);

      #endregion

      var appConfig = ConfigurationManager.GetSection(ApplicationConfigurationSection.SectionName) as ApplicationConfigurationSection;
      Title = DefaultTitle = appConfig.Title;

      base.OnInitialized();
    }

    #endregion

    #region void OnSecurityContextChanged(User user)

    void OnSecurityContextChanged(User user)
    {
      if (user != null)
      {
        Title = string.Format("{0} - {1}", DefaultTitle, user.Username);

        GlobalRegionManager.Regions[RegionNames.RibbonRegion].Add(HomeTab);
        GlobalRegionManager.Regions[RegionNames.RibbonRegion].Add(ManagementTab);

        GlobalRegionManager.Regions[RegionNames.RibbonRegion].Activate(HomeTab);
      }
      else
      {
        Title = DefaultTitle;
      }
    }

    #endregion

    #region WindowState WindowState

    //Dummy Property to prevent binding errors
    public WindowState WindowState
    {
      get { return WindowState.Closed; }
      set {  }
    }

    #endregion
  }
}
