﻿using Dataforge.Prism.Contants;
using Dataforge.Prism.Views;
using Dataforge.Prism.Views.Shell;
using Dataforge.PrismAvalonExtensions;
using Fluent;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Xceed.Wpf.AvalonDock.Layout.Serialization;

namespace Dataforge.Prism.Views.Shell
{
  /// <summary>
  /// Interaction logic for RibbonMdiLayout.xaml
  /// </summary>
  [Export(typeof(RibbonMdiLayout))]
  [PartCreationPolicy(CreationPolicy.Shared)]
  public partial class RibbonMdiLayout : RibbonWindow
  {
    public RibbonMdiLayout()
    {
      InitializeComponent();
      this.Loaded += (sender, e) =>
      {
        if (this.DataContext == null)
        {
          if (ServiceLocator.Current == null) return;
          this.DataContext = ServiceLocator.Current.GetInstance<RibbonMdiLayoutViewModel>();
        }

        this.Activate();
      };
    }

    protected override void OnClosing(CancelEventArgs e)
    {
      base.OnClosing(e);
      //SerializationHelper.Serialize(DockingManager, "Layout.xml");
    }

    private void RibbonWindow_Closing(object sender, CancelEventArgs e)
    {

    }
  }
}
