﻿using Dataforge.Business;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views
{
  public interface IViewModel
  {
    IRegionManager GlobalRegionManager { get; }
    IRegionManager LocalRegionManager { get; set; }
    IEventAggregator GlobalEventAggregator { get; }
    IEventAggregator LocalEventAggregator { get; }
    object ModelSource { get; set; }
    bool IsFocused { get; set; }
    void Initialize();
    bool IsReadOnly { get; set; }
    bool IsEditable { get; }
  }
}
