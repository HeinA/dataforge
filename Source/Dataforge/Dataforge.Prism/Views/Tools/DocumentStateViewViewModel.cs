﻿using Dataforge.Prism.Contants;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views.Tools
{
  [Export(typeof(DocumentStateViewViewModel))]
  [PartCreationPolicy(CreationPolicy.Shared)]
  public class DocumentStateViewViewModel : ViewModel
  {
    public DocumentStateViewViewModel()
    {
    }
  }
}
