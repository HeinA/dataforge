﻿using Dataforge.Business;
using Dataforge.Business.Activities;
using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using Dataforge.Business.Security;
using Dataforge.Prism.Events;
using Dataforge.Prism.Views.Dialogs;
using Dataforge.PrismAvalonExtensions;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Dataforge.Prism.Views
{
  public abstract class MultiModelOperationsAwareViewModel<TModel, TViewModel> : MultiModelViewModel<TModel, TViewModel>, IOperationTarget, IClosingValidator, ITitledControl
    where TModel : BusinessObject
    where TViewModel : ViewModel<TModel>, ISelectableViewModel
  {
    #region Constructors

    public MultiModelOperationsAwareViewModel()
    {
      ContextMenuOpening = new DelegateCommand<ContextMenuEventArgs>(OnContextMenuOpening);
      CollectionProvider.ModelCollectionChanged += CollectionProvider_ModelCollectionChanged;
      OperationsContainer = new OperationsContainer(this);
      this.IsReadOnly = !Activity.CanEdit;
    }

    public MultiModelOperationsAwareViewModel(BasicCollection<TModel> models)
      : base(models)
    {
      ContextMenuOpening = new DelegateCommand<ContextMenuEventArgs>(OnContextMenuOpening);
      CollectionProvider.ModelCollectionChanged += CollectionProvider_ModelCollectionChanged;
      OperationsContainer = new OperationsContainer(this);
      this.IsReadOnly = !Activity.CanEdit;
    }

    void CollectionProvider_ModelCollectionChanged(object sender, EventArgs e)
    {
      RaiseCanExecuteChanged();
    }

    #endregion

    protected override void OnLocalRegionManagerChanged()
    {
      LocalEventAggregator.GetEvent<OperationOverrideEvent>().Subscribe(OnOperationOverrideEvent);
      base.OnLocalRegionManagerChanged();
    }

    #region Activity Activity

    public abstract int ActivityId { get; }

    protected Activity Activity
    {
      get { return Cache.Instance.GetObject<Activity>(ActivityId); }
    }

    #endregion

    protected override void OnLoaded()
    {
      if (Activity.CanEdit)
      {
        OperationsContainer.Activity = Activity;
        OnPropertyChanged(PN_GLOBALOPERATIONVIEWMODELS);
        OnPropertyChanged(PN_CONTEXTOPERATIONVIEWMODELS);
      }
      base.OnLoaded();
    }

    #region OperationsContainer OperationsContainer

    OperationsContainer _operationsContainer;
    OperationsContainer OperationsContainer
    {
      get { return _operationsContainer; }
      set { _operationsContainer = value; }
    }

    const string PN_GLOBALOPERATIONVIEWMODELS = "GlobalOperationViewModels";
    public ReadOnlyObservableCollection<OperationViewModel> GlobalOperationViewModels
    {
      get { return OperationsContainer.GlobalOperationViewModels; }
    }

    const string PN_CONTEXTOPERATIONVIEWMODELS = "ContextOperationViewModels";
    public ReadOnlyObservableCollection<OperationViewModel> ContextOperationViewModels
    {
      get { return OperationsContainer.ContextOperationViewModels; }
    }

    protected OperationViewModel GetOperationViewModel(Operation operation)
    {
      return OperationsContainer.GetOperationViewModel(operation);
    }

    #endregion

    #region IOperationTarget

    public bool CanExecuteOperation(Operation operation)
    {
      try
      {
        return OnCanExecuteOperation(operation);
      }
      catch (Exception ex)
      {
        if (ExceptionPolicy.HandleException(ex, "Default Policy"))
          throw;
      }

      return false;
    }

    protected virtual bool OnCanExecuteOperation(Operation operation)
    {
      return true;
    }

    public void ExecuteOperation(Operation operation)
    {
      try
      {
        using (new WaitCursor())
        {
          OnExecuteOperation(operation);
        }
        RaiseCanExecuteChanged();
      }
      catch (Exception ex)
      {
        if (ExceptionPolicy.HandleException(ex, "Default Policy"))
          throw;
      }
    }

    protected virtual void OnExecuteOperation(Operation operation)
    {
      switch (operation.Id)
      {
        case Operations.ToggleDelete:
          foreach (var obj in SelectedModels)
          {
            obj.IsDeleted = !obj.IsDeleted;
          }
          break;
      }
    }

    public void RaiseCanExecuteChanged()
    {
      foreach (var opvm in OperationsContainer.GlobalOperationViewModels)
      {
        opvm.RaiseCanExecuteChanged();
      }
      foreach (var opvm in OperationsContainer.ContextOperationViewModels)
      {
        opvm.RaiseCanExecuteChanged();
      }
    }

    public void VisibilityChanged(OperationViewModel vm)
    {
      if (GlobalOperationViewModels.Contains(vm)) ResetSeperators(GlobalOperationViewModels);
      else ResetSeperators(ContextOperationViewModels);
    }

    void ResetSeperators(ReadOnlyCollection<OperationViewModel> vmc)
    {
      foreach (var vm in vmc.Where(m => m.Model == null))
      {
        vm.Visibility = System.Windows.Visibility.Visible;
      }

      List<OperationViewModel> vvmc = vmc.Where(m => m.Visibility == Visibility.Visible).ToList();

      if (vvmc.Count > 0)
      {
        OperationViewModel last = vmc[0];
        foreach (OperationViewModel vm in vvmc)
        {
          if ((last.Model == null && vm.Model == null) || vvmc.IndexOf(vm) == vvmc.Count-1 || vvmc.IndexOf(vm) == 0) vm.Visibility = Visibility.Collapsed;
          last = vm;
        }
      }
    }

    #endregion

    #region ITitledControl

    string _title = null;
    public string Title
    {
      get { return _title; }
      set { SetValue<string>(ref _title, value, "Title"); }
    }

    protected override void OnDirtyStateChanged()
    {
      if (Title != null && IsDirty && Title.Substring(0, 1) != "*")
        Title = string.Format("*{0}", Title);
      if (Title != null && !IsDirty && Title.Substring(0, 1) == "*")
        Title = Title.Substring(1, Title.Length - 1);
      base.OnDirtyStateChanged();
    }

    #endregion

    #region DelegateCommand ContextMenuOpening

    public DelegateCommand<ContextMenuEventArgs> ContextMenuOpening { get; set; }

    void OnContextMenuOpening(ContextMenuEventArgs e)
    {
      foreach (Operation op in OperationsContainer.ContextOperations.Where(o => o is Operation))
      {
        if (op.Enabled)
        {
          if (op.Id == Operations.ToggleDelete && !FocusedViewModel.MayDelete) op.Visible = false;
          else if (op.ContextType == null) op.Visible = true;
          else if (FocusedModel == null) op.Visible = false;
          else if (op.ContextType == FocusedModel.GetType() || op.ContextType.IsSubclassOf(FocusedModel.GetType())) op.Visible = true;
          else if (!(op.ContextType == FocusedModel.GetType() || op.ContextType.IsSubclassOf(FocusedModel.GetType()))) op.Visible = false;

          var args = new OperationOverrideEventArgs(op, SelectedModels, op.Visible);
          LocalEventAggregator.GetEvent<OperationOverrideEvent>().Publish(args);
          OperationViewModel opvm = GetOperationViewModel(op);
          opvm.TextOverride = args.Text;
          op.Visible = args.Visible;
        }
      }

      int count = OperationsContainer.ContextOperations.Count(o => o.Visible && !(o is OperationSeperator));
      if (count == 0) e.Handled = true;

      ResetSeperators(OperationsContainer.ContextOperationViewModels);
      OperationsContainer.ContextOperations.FireReset();
    }

    #endregion

    #region IClosingValidator

    public bool OnClosing()
    {
      if (SecurityContext.User == null) return true;

      Operation op = Activity.Operations.FirstOrDefault(o => o.Id == Operations.Save && o.Enabled);
      if (op == null) return true;

      if (HasError)
      {
        DialogResult result = ThreadBlockingDialogHelper.Show(ServiceLocator.Current.GetInstance<CanNotSaveDialog>());
        if (result == DialogResult.Cancel) return false;
      }
      else
      {
        DialogResult result = ThreadBlockingDialogHelper.Show(ServiceLocator.Current.GetInstance<SaveDialog>());
        if (result == DialogResult.Cancel) return false;
        if (result == DialogResult.Yes)
        {
          OperationViewModel opvm = GetOperationViewModel(op);
          if (opvm == null) return true;
          opvm.OperationCommand.Execute();
          return true;
        }
      }

      return true;
    }

    #endregion

    #region void OnOperationOverrideEvent(OperationTextOverrideEventArgs args)

    void OnOperationOverrideEvent(OperationOverrideEventArgs args)
    {
      switch (args.Operation.Id)
      {
        case Operations.ToggleDelete:
          if (args.OperationTargets.Count == 0)
          {
            args.Visible = false;
            return;
          }
          bool b = args.OperationTargets[0].IsDeleted;
          foreach (var ot in args.OperationTargets)
          {
            if (ot.IsDeleted != b) return;
          }
          if (!b) args.Text = "Delete";
          else args.Text = "Undelete";
          break;
      }
    }

    #endregion
  }
}
