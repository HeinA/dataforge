﻿using Dataforge.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views
{
  public interface ISelectableViewModel : IViewModel
  {
    bool IsSelected { get; set; }
    bool MayDelete { get; }
  }
}
