﻿using Dataforge.Business;
using Dataforge.Business.Activities;
using Dataforge.Business.Collections;
using Dataforge.Business.Security;
using Dataforge.Prism.Events;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views
{
  class OperationsContainer
  {
    #region Constructors

    public OperationsContainer(ViewModel viewModel)
    {
      if (!typeof(IOperationTarget).IsAssignableFrom(viewModel.GetType())) throw new ArgumentException("viewModel must implement IOperationTarget");

      _viewModel = viewModel;
      GlobalOperationsProvider = new ReadOnlyCollectionProvider<OperationBase, OperationViewModel>(viewModel, CreateOperationViewModel, OnViewModelCreated);
      ContextOperationsProvider = new ReadOnlyCollectionProvider<OperationBase, OperationViewModel>(viewModel, CreateOperationViewModel, OnViewModelCreated);
      Operations = new BasicCollection<Operation>();
    }

    #endregion

    #region void OnViewModelCreated(OperationViewModel viewModel, OperationBase OperationBase)

    private void OnViewModelCreated(OperationViewModel viewModel, OperationBase OperationBase)
    {
      if (OperationDictionary.ContainsKey(OperationBase)) 
        OperationDictionary.Remove(OperationBase);
      OperationDictionary.Add(OperationBase, viewModel);
    }

    #endregion

    #region OperationViewModel GetOperationViewModel(OperationBase OperationBase)

    Dictionary<OperationBase, OperationViewModel> _operationDictionary = new Dictionary<OperationBase, OperationViewModel>();
    public Dictionary<OperationBase, OperationViewModel> OperationDictionary
    {
      get { return _operationDictionary; }
    }

    public OperationViewModel GetOperationViewModel(OperationBase OperationBase)
    {
      if (OperationDictionary.ContainsKey(OperationBase)) return OperationDictionary[OperationBase];
      return null;
    }

    #endregion

    #region OperationViewModel CreateOperationViewModel(OperationBase op)

    OperationViewModel CreateOperationViewModel(OperationBase opb)
    {
      OperationViewModel opvm = null;
      try
      {
        Operation op = opb as Operation;
        opvm = new OperationViewModel((IOperationTarget)ViewModel, op);
        if (!opb.IsContextOperation)
        {
          opvm = new OperationViewModel((IOperationTarget)ViewModel, op);
          if (op == null) return opvm;
          OperationOverrideEventArgs args = new OperationOverrideEventArgs(op, null, op.Visible);
          ViewModel.LocalEventAggregator.GetEvent<OperationOverrideEvent>().Publish(args);
          opvm.TextOverride = args.Text;
          op.Visible = args.Visible;
        }
      }
      catch (Exception ex)
      {
        if (ExceptionPolicy.HandleException(ex, "Default Policy"))
          throw;
      }
      return opvm;
    }

    #endregion

    #region ViewModel

    ViewModel _viewModel;
    public ViewModel ViewModel
    {
      get { return _viewModel; }
    }

    #endregion

    #region ReadOnlyCollectionProvider<OperationBase, TViewModel> GlobalOperationsProvider

    ReadOnlyCollectionProvider<OperationBase, OperationViewModel> GlobalOperationsProvider;
    public ReadOnlyObservableCollection<OperationViewModel> GlobalOperationViewModels
    {
      get { return GlobalOperationsProvider.ViewModelCollection; }
    }
    public BasicCollection<OperationBase> GlobalOperations
    {
      get { return GlobalOperationsProvider.ModelCollection; }
      private set { GlobalOperationsProvider.ModelCollection = value; }
    }

    #endregion

    #region ReadOnlyCollectionProvider<OperationBase, OperationViewModel> ContextOperationsProvider

    ReadOnlyCollectionProvider<OperationBase, OperationViewModel> ContextOperationsProvider;
    public ReadOnlyObservableCollection<OperationViewModel> ContextOperationViewModels
    {
      get { return ContextOperationsProvider.ViewModelCollection; }
    }

    public BasicCollection<OperationBase> ContextOperations
    {
      get { return ContextOperationsProvider.ModelCollection; }
      private set { ContextOperationsProvider.ModelCollection = value; }
    }

    #endregion

    Activity _activity;
    public Activity Activity
    {
      get { return _activity; }
      set
      {
        if (_activity != value)
        {
          _activity = value;
          Operations = Activity.Operations;
        }
      }
    }

    #region BasicCollection<Operation> Operations

    BasicCollection<Operation> _operations;
    BasicCollection<Operation> Operations
    {
      get { return _operations; }
      set
      {
        try
        {
          if (Operations != null) Operations.CollectionChanged -= Operations_CollectionChanged;
          ContextOperations.Clear();
          GlobalOperations.Clear();
          _operations = value;
          string globalGroupName = null;
          string contextGroupName = null;
          foreach (Operation op in Operations)
          {
            if (!Activity.CanExecute(op)) continue;

            if (op.IsContextOperation)
            {
              if (op.Group.GroupName != contextGroupName)
              {
                if (contextGroupName != null)
                {
                  ContextOperations.Add(new OperationSeperator());
                }
                contextGroupName = op.Group.GroupName;
              }
            }
            else
            {
              if (op.Group.GroupName != globalGroupName)
              {
                if (globalGroupName != null)
                {
                  GlobalOperations.Add(new OperationSeperator());
                }
                globalGroupName = op.Group.GroupName;
              }
            }
            var args = new OperationReplaceEventArgs(op);
            ViewModel.LocalEventAggregator.GetEvent<OperationReplaceEvent>().Publish(args);
            if (args.ReplacementOperations.Count > 0)
            {
              foreach (OperationBase op1 in args.ReplacementOperations)
              {
                if (op.IsContextOperation) ContextOperations.Add(op1);
                else GlobalOperations.Add(op1);
              }
            }
            else
            {
              if (op.IsContextOperation) ContextOperations.Add(op);
              else GlobalOperations.Add(op);
            }
          }
          Operations.CollectionChanged += Operations_CollectionChanged;
        }
        catch (Exception ex)
        {
          if (ExceptionPolicy.HandleException(ex, "Default Policy"))
            throw;
        }
      }
    }

    void Operations_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      try
      {
        switch (e.Action)
        {
          case NotifyCollectionChangedAction.Add:
            foreach (Operation op in e.NewItems)
            {
              if (op.IsContextOperation) ContextOperations.Add(op);
              else GlobalOperations.Add(op);
            }
            break;

          case NotifyCollectionChangedAction.Remove:
            foreach (Operation op in e.OldItems)
            {
              if (op.IsContextOperation) ContextOperations.Remove(op);
              else GlobalOperations.Remove(op);
            }
            break;

          case NotifyCollectionChangedAction.Reset:
            ContextOperations.Clear();
            GlobalOperations.Clear();
            break;
        }
      }
      catch (Exception ex)
      {
        if (ExceptionPolicy.HandleException(ex, "Default Policy"))
          throw;
      }
    }

    #endregion
  }
}
