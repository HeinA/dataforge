﻿using Dataforge.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views
{
  public interface IStateProvider
  {
    void SetState<TPropertyType>(IIdentifiableObject io, string propertyName, TPropertyType value);
    TPropertyType GetState<TPropertyType>(IIdentifiableObject io, string propertyName);
  }
}
