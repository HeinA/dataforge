﻿using Dataforge.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views.UserManagement
{
  public class UserOrganizationalUnitListViewItemViewModel : SelectableViewModel<OrganizationalUnit>
  {
    #region Constructors

    public UserOrganizationalUnitListViewItemViewModel(User user, OrganizationalUnit ou, IMultiModelViewModel owner)
      : base(ou, owner)
    {
      _user = user;
    }

    #endregion

    #region User User

    User _user;
    public User User
    {
      get { return _user; }
    }

    #endregion

    #region Model Propertry string Name

    public string Name
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Name;
      }
      set { Model.Name = value; }
    }

    #endregion

    #region bool HasOrganizationalUnit

    public bool HasOrganizationalUnit
    {
      get { return User.OrganizationalUnits.Contains(Model); }
      set
      {
        if (value) User.OrganizationalUnits.Add(Model);
        else User.OrganizationalUnits.Remove(Model);
        OnPropertyChanged("HasOrganizationalUnit");
      }
    }

    #endregion
  }
}
