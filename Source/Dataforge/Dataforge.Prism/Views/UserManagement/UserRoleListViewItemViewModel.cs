﻿using Dataforge.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Dataforge.Prism.Views.UserManagement
{
  public class UserRoleListViewItemViewModel : SelectableViewModel<Role>
  {
    #region Constructors

    public UserRoleListViewItemViewModel(User user, Role model, UserRolesViewModel owner)
      : base(model, owner)
    {
      _user = user;
    }

    #endregion

    #region User User

    User _user;
    public User User
    {
      get { return _user; }
    }

    #endregion

    #region Model Propertry string Name

    public string Name
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Name;
      }
      set { Model.Name = value; }
    }

    #endregion

    #region bool HasRole

    public bool HasRole
    {
      get { return User.Roles.Contains(Model); }
      set
      {
        if (value) User.Roles.Add(Model);
        else User.Roles.Remove(Model);
        OnPropertyChanged("HasRole");
      }
    }

    #endregion

    public Visibility CheckBoxVisibility
    {
      get { return IsReadOnly ? Visibility.Collapsed : Visibility.Visible; }
    }
  }
}
