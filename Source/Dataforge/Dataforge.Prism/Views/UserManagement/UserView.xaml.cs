﻿using Dataforge.Prism.Extensions;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Dataforge.Prism.Views.UserManagement
{
  /// <summary>
  /// Interaction logic for UserView.xaml
  /// </summary>
  [Export]
  [PartCreationPolicy(CreationPolicy.Shared)]
  public partial class UserView : UserControl
  {
    public UserView()
    {
      InitializeComponent();
      if (ServiceLocator.Current == null) return;
      UserViewModel vm = ServiceLocator.Current.GetInstance<UserViewModel>();
      this.DataContext = vm;
      
      //Password.PasswordChanged += Password_PasswordChanged;
      //PasswordVerification.PasswordChanged += PasswordVerification_PasswordChanged;
    }

    //void PasswordVerification_PasswordChanged(object sender, RoutedEventArgs e)
    //{
    //  if (string.IsNullOrWhiteSpace(PasswordVerification.Password)) PasswordBoxExtension.SetEncryptedPassword(PasswordVerification, null);
    //  else
    //  {
    //    HashAlgorithm algoritm = new SHA256Managed();
    //    PasswordBoxExtension.SetEncryptedPassword(PasswordVerification, ASCIIEncoding.ASCII.GetString(algoritm.ComputeHash(ASCIIEncoding.ASCII.GetBytes(PasswordVerification.Password))));
    //  }
    //  ValidatePasswords();
    //}

    //void Password_PasswordChanged(object sender, RoutedEventArgs e)
    //{
    //  if (string.IsNullOrWhiteSpace(Password.Password)) PasswordBoxExtension.SetEncryptedPassword(Password, null);
    //  else
    //  {
    //    HashAlgorithm algoritm = new SHA256Managed();
    //    PasswordBoxExtension.SetEncryptedPassword(Password, ASCIIEncoding.ASCII.GetString(algoritm.ComputeHash(ASCIIEncoding.ASCII.GetBytes(Password.Password))));
    //  }
    //  ValidatePasswords();
    //}

    //void ValidatePasswords()
    //{
    //  BindingExpression expression = Password.GetBindingExpression(PasswordBoxExtension.EncryptedPasswordProperty);
    //  if (!PasswordVerification.Password.Equals(Password.Password))
    //  {
    //    System.Windows.Controls.Validation.MarkInvalid(expression, new ValidationError(new ExceptionValidationRule(), expression, "Passwords do not match", null));
    //  }
    //  else
    //  {
    //    expression.ValidateWithoutUpdate();
    //  }
    //}
  }
}
