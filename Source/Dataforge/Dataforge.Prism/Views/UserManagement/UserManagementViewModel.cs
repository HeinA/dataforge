﻿using Dataforge.Business;
using Dataforge.Business.Activities;
using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using Dataforge.Business.Security;
using Dataforge.Business.Service;
using Dataforge.Prism.Contants;
using Dataforge.Prism.Views.Dialogs;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views.UserManagement
{
  [Export]
  [PartCreationPolicy(CreationPolicy.NonShared)]
  public class UserManagementViewModel : ManagementViewModel<User, UserListViewItemViewModel>
  {
    public const string TabRegion = "TabRegion";
    public const string DetailsRegion = "DetailsRegion";
    public const string RolesRegion = "RolesRegion";
    public const string OURegion = "OURegion";
    
    #region Constructors

    public UserManagementViewModel()
    {
      Title = "User Management";
      IsFocused = true;
    }

    public UserManagementViewModel(BasicCollection<User> users)
      : base(users)
    {
      Title = "User Management";
      IsFocused = true;
    }

    #endregion

    #region Import UserView UserView & UserViewModel UserViewModel

    #region Import UserView UserView

    [Import]
    UserView _userView = null;
    public UserView UserView
    {
      get { return _userView; }
    }

    #endregion

    #region Import UserViewModel UserViewModel

    [Import]
    UserViewModel _userViewModel = null;
    public UserViewModel UserViewModel
    {
      get { return _userViewModel; }
    }

    #endregion

    #endregion

    #region Import UserRolesView UserRolesView & UserRolesViewModel UserRolesViewModel

    #region Import UserRolesView UserRolesView

    [Import]
    UserRolesView _userRolesView = null;
    public UserRolesView UserRolesView
    {
      get { return _userRolesView; }
    }

    #endregion

    #region Import UserRolesViewModel UserRolesViewModel

    [Import]
    UserRolesViewModel _userRolesViewModel = null;
    public UserRolesViewModel UserRolesViewModel
    {
      get { return _userRolesViewModel; }
    }

    #endregion

    #endregion

    #region Import UserOrganizationalUnitsView UserOrganizationalUnitsView & UserOrganizationalUnitsViewModel UserOrganizationalUnitsViewModel

    #region Import UserOrganizationalUnitsView UserOrganizationalUnitsView

    [Import]
    UserOrganizationalUnitsView _userOrganizationalUnitsView = null;
    public UserOrganizationalUnitsView UserOrganizationalUnitsView
    {
      get { return _userOrganizationalUnitsView; }
    }

    #endregion

    #region Import UserOrganizationalUnitsViewModel UserOrganizationalUnitsViewModel

    [Import]
    UserOrganizationalUnitsViewModel _userOrganizationalUnitsViewModel = null;
    public UserOrganizationalUnitsViewModel UserOrganizationalUnitsViewModel
    {
      get { return _userOrganizationalUnitsViewModel; }
    }

    #endregion

    #endregion

    #region Overrides

    #region int ActivityId

    public override int ActivityId
    {
      get { return Activities.UserManagement; }
    }

    #endregion

    #region void Load()

    public override void Load()
    {
      using (var svc = ServiceFactory.GetService<IDataforgeService>())
      {
        ModelCollection = svc.ExecuteFunction<BasicCollection<User>>(i => i.LoadUsers());
      }
    }

    #endregion

    #region void Save()

    public override void Save()
    {
      using (var svc = ServiceFactory.GetService<IDataforgeService>())
      {
        ModelCollection = svc.ExecuteFunction<BasicCollection<User>>(i => i.SaveUsers(ModelCollection));
      }
    }

    #endregion

    #region UserListItemViewModel CreateViewModel(User model)

    public override UserListViewItemViewModel CreateViewModel(User model)
    {
      return new UserListViewItemViewModel(model, this);
    }

    #endregion

    #region void OnFocusedViewModelChanged(ISelectableViewModel viewModel)

    protected override void OnFocusedViewModelChanged(ISelectableViewModel viewModel)
    {
      if (viewModel is UserListViewItemViewModel)
      {
        UserListViewItemViewModel vm = viewModel as UserListViewItemViewModel;
        ShowInRegion(DetailsRegion, UserView);
        ShowInRegion(RolesRegion, UserRolesView);
        ShowInRegion(OURegion, UserOrganizationalUnitsView);
        UserViewModel.Model = vm.Model;
        UserViewModel.IsReadOnly = !Activity.CanEdit;
        UserRolesViewModel.User = vm.Model;
        UserRolesViewModel.ModelCollection = Cache.Instance.GetObjects<Role>(r => r.Name, true);
        UserRolesViewModel.IsReadOnly = !Activity.CanEdit;
        UserOrganizationalUnitsViewModel.User = vm.Model;
        UserOrganizationalUnitsViewModel.ModelCollection = Cache.Instance.GetObjects<OrganizationalUnit>(ou => ou.UserAssignable, ou => ou.Name, true);
        UserOrganizationalUnitsViewModel.IsReadOnly = !Activity.CanEdit;
      }
      else
      {
        RemoveFromRegion(DetailsRegion, UserView);
        RemoveFromRegion(RolesRegion, UserRolesView);
        RemoveFromRegion(OURegion, UserOrganizationalUnitsView);
      }

      base.OnFocusedViewModelChanged(viewModel);
    }

    #endregion

    #region bool OnCanExecuteOperation(Operation operation)

    protected override bool OnCanExecuteOperation(Operation operation)
    {
      return base.OnCanExecuteOperation(operation);
    }

    #endregion

    #region void OnExecuteOperation(Operation operation)

    int _iCount = 1;
    protected override void OnExecuteOperation(Operation operation)
    {
      switch (operation.Id)
      {
        case Operations.User_AddUser:
          User user = new User();
          user.Username = string.Format("New User {0}", _iCount++);
          ModelCollection.Add(user);
          ISelectableViewModel uvm = GetViewModel(user);
          SetSingleSelection(uvm);
          UserViewModel.IsFocused = true;
          break;

        case Operations.User_SetPassword:
          SetPasswordDialog spd = ServiceLocator.Current.GetInstance<SetPasswordDialog>();
          ThreadBlockingDialogHelper.Show(spd);
          ((User)FocusedModel).PasswordHash = spd.ViewModel.PasswordHash;
          break;
      }

      base.OnExecuteOperation(operation);
    }

    #endregion

    #endregion
  }
}
