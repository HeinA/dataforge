﻿using Dataforge.Business.Security;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Dataforge.Prism.Views.UserManagement
{
  [Export]
  [PartCreationPolicy(CreationPolicy.Shared)]
  public class UserViewModel : ViewModel<User>
  {
    #region Model Propertry string Username

    public string Username
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Username;
      }
      set { Model.Username = value; }
    }

    #endregion

    #region Model Propertry string Firstnames

    public string Firstnames
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Firstnames;
      }
      set { Model.Firstnames = value; }
    }

    #endregion

    #region Model Propertry string Lastname

    public string Lastname
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Lastname;
      }
      set { Model.Lastname = value; }
    }

    #endregion
  }
}
