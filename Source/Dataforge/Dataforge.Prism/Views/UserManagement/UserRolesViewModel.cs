﻿using Dataforge.Business.Security;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Dataforge.Prism.Views.UserManagement
{
  [Export]
  [PartCreationPolicy(CreationPolicy.Shared)]
  public class UserRolesViewModel : MultiModelViewModel<Role, UserRoleListViewItemViewModel>
  {
    #region User User

    User _user;
    public User User
    {
      get { return _user; }
      set { _user = value; }
    }

    #endregion

    #region DelegateCommand KeyDownCommand

    DelegateCommand<KeyEventArgs> _keyDownCommand;
    public DelegateCommand<KeyEventArgs> KeyDownCommand
    {
      get { return _keyDownCommand ?? (_keyDownCommand = new DelegateCommand<KeyEventArgs>(ExecuteKeyDown)); }
    }

    void ExecuteKeyDown(KeyEventArgs e)
    {
      if (e.Key != Key.Space) return;

      foreach (UserRoleListViewItemViewModel vm in SelectedViewModels)
      {
        vm.HasRole = !vm.HasRole;
      }

      e.Handled = true;
    }

    #endregion

    #region Overrides

    public override UserRoleListViewItemViewModel CreateViewModel(Role model)
    {
      return new UserRoleListViewItemViewModel(User, model, this) { IsReadOnly = this.IsReadOnly };
    }

    #endregion
  }
}
