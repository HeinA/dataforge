﻿using Dataforge.Business.Security;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views.UserManagement
{
  public class UserListViewItemViewModel : SelectableViewModel<User>
  {
    #region Constructors

    public UserListViewItemViewModel(User user, IMultiModelViewModel owner)
      : base(user, owner)
    {
    }

    #endregion

    #region Model Propertry string Username

    public string Username
    {
      get
      {
        if (Model == null) return default(string);
        return string.IsNullOrWhiteSpace(Model.Username) ? "** User **" : Model.Username;
      }
      set { Model.Username = value; }
    }

    #endregion

    #region Model Propertry string Firstnames

    public string Firstnames
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Firstnames;
      }
      set { Model.Firstnames = value; }
    }

    #endregion

    #region Model Propertry string Lastname

    public string Lastname
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Lastname;
      }
      set { Model.Lastname = value; }
    }

    #endregion
  }
}
