﻿using Dataforge.Business.Activities;
using Dataforge.Business.Security;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Dataforge.Prism.Views
{
  public class OperationViewModel : ViewModel<OperationBase>
  {
    #region Constructors

    public OperationViewModel(IOperationTarget target, OperationBase operation) //ReadOnlyObservableCollection<OperationViewModel> operations, 
      : base(operation)
    {
      _target = target;
      //_operations = operations;
      AddModelPropertyMapping(Operation.PN_VISIBLE, "Visibility");
    }

    #endregion

    public Operation OperationModel
    {
      get { return Model as Operation; }
    }

    #region IOperationTarget Target

    IOperationTarget _target;
    public IOperationTarget Target
    {
      get { return OperationModel != null ? _target : null; }
    }

    #endregion

    #region string Text

    public string Text
    {
      get
      {
        if (!string.IsNullOrWhiteSpace(TextOverride)) return TextOverride;
        return OperationModel != null ? OperationModel.Text : null;
      }
    }

    #endregion

    #region string TextOverride

    string _textOverride;
    public string TextOverride
    {
      get { return _textOverride; }
      set { SetValue<string>(ref _textOverride, value, "TextOverride", "Text"); }
    }

    #endregion

    #region string Description

    public string Description
    {
      get { return OperationModel != null ? OperationModel.Description : null; }
    }

    #endregion

    #region Visibility Visibility

    Visibility _visibility = Visibility.Visible;
    public Visibility Visibility
    {
      get
      {
        if (OperationModel != null) return OperationModel.Visible ? Visibility.Visible : Visibility.Collapsed;
        return _visibility;
      }
      set { SetValue<Visibility>(ref _visibility, value, "Visibility"); }
    }

    #endregion

    #region Visibility ImageVisibility

    public Visibility ImageVisibility
    {
      get
      {
        if (Model == null || OperationModel == null) return Visibility.Collapsed;
        if (string.IsNullOrWhiteSpace(OperationModel.Base64Image)) return Visibility.Collapsed;
        return Visibility.Visible;
      }
    }

    #endregion

    #region ImageSource ImageSource

    ImageSource _imageSource;
    public ImageSource ImageSource
    {
      get
      {
        if (Model == null || OperationModel == null) return default(ImageSource);
        if (string.IsNullOrWhiteSpace(OperationModel.Base64Image)) return null;
        if (_imageSource == null)
        {
          byte[] imgData = Convert.FromBase64String(OperationModel.Base64Image);
          BitmapImage img = new BitmapImage();
          using (MemoryStream ms = new MemoryStream(imgData))
          {
            img.BeginInit();
            img.CacheOption = BitmapCacheOption.OnLoad;
            img.StreamSource = ms;
            img.EndInit();
          }
          _imageSource = img as ImageSource;
        }
        return _imageSource;
      }
    }

    #endregion

    public Image Image
    {
      get { return new Image() { Source = ImageSource }; }
    }

    #region DelegateCommand OperationCommand

    DelegateCommand _operationCommand;
    public DelegateCommand OperationCommand
    {
      get
      {
        if (_operationCommand == null) _operationCommand = new DelegateCommand(Execute, CanExecute);
        return _operationCommand;
      }
    }

    private void Execute()
    {
      if (OperationModel != null) Target.ExecuteOperation(OperationModel);
    }

    private bool CanExecute()
    {
      return OperationModel != null ? Target.CanExecuteOperation(OperationModel) : false;
    }

    #endregion

    #region void RaiseCanExecuteChanged()

    public void RaiseCanExecuteChanged()
    {
      OperationCommand.RaiseCanExecuteChanged();
    }

    #endregion

    public override void OnPropertyChanged(string propertyName)
    {
      base.OnPropertyChanged(propertyName);

      if (propertyName == "Visible")
      {
        Target.VisibilityChanged(this);
      }
    }
  }
}
