﻿using Dataforge.Business;
using Dataforge.Business.Activities;
using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using Dataforge.Business.Security;
using Dataforge.Prism.Contants;
using Dataforge.Prism.Events;
using Dataforge.Prism.Views.Dialogs;
using Dataforge.PrismAvalonExtensions;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views
{
  public abstract class MultiModelFrameViewModel<TModel, TViewModel> : MultiModelOperationsAwareViewModel<TModel, TViewModel>, IFrameViewModel 
    where TModel : BusinessObject
    where TViewModel : ViewModel<TModel>, ISelectableViewModel
  {
    #region Constructors

    public MultiModelFrameViewModel()
    {
    }

    public MultiModelFrameViewModel(BasicCollection<TModel> models)
      : base(models)
    {
    }

    #endregion
  }
}
