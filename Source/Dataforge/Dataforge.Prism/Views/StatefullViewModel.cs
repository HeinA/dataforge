﻿using Dataforge.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views
{
  public abstract class StatefullViewModel<T> : ViewModel<T>
    where T : BusinessObject, IIdentifiableObject
  {
    #region Constructors

    public StatefullViewModel(T model, IStateProvider provider)
      : base(model)
    {
      _stateProvider = provider;
    }

    #endregion

    #region IStateProvider StateProvider

    IStateProvider _stateProvider;
    public IStateProvider StateProvider
    {
      get { return _stateProvider; }
    }

    #endregion

    #region void SetState<TPropertyType>(string propertyName, TPropertyType value)

    protected void SetState<TPropertyType>(string propertyName, TPropertyType value)
    {
      StateProvider.SetState<TPropertyType>(this.Model, propertyName, value);
      OnPropertyChanged(propertyName);
    }

    #endregion

    #region TPropertyType GetState<TPropertyType>(string propertyName)

    protected TPropertyType GetState<TPropertyType>(string propertyName)
    {
      return StateProvider.GetState<TPropertyType>(this.Model, propertyName);
    }

    #endregion
  }
}
