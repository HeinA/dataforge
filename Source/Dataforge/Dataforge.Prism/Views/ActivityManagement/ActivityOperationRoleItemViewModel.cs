﻿using Dataforge.Business.Activities;
using Dataforge.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Dataforge.Prism.Views.ActivityManagement
{
  public class ActivityOperationRoleItemViewModel : SelectableViewModel<Role>
  {
    #region Constructors

    public ActivityOperationRoleItemViewModel(ActivityOperation activityOperation, Role model, IMultiModelViewModel owner)
      : base(model, owner)
    {
      _activityOperation = activityOperation;
      ActivityOperation.PropertyChanged += (sender, e) =>
      {
        if (e.PropertyName == "IsDeleted")
        {
          OnPropertyChanged("Visibility");
        }
      };
      IsReadOnly = owner.IsReadOnly;
    }

    #endregion

    #region ActivityOperation ActivityOperation

    ActivityOperation _activityOperation;
    public ActivityOperation ActivityOperation
    {
      get { return _activityOperation; }
    }

    #endregion

    #region Model Propertry string Name

    public string Name
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Name;
      }
      set { Model.Name = value; }
    }

    #endregion

    #region bool HasRole

    public bool HasRole
    {
      get { return ActivityOperation.Roles.Contains(Model); }
      set
      {
        if (value) ActivityOperation.Roles.Add(Model);
        else ActivityOperation.Roles.Remove(Model);
        
        OnPropertyChanged("HasRole");
      }
    }

    #endregion

    public Visibility Visibility
    {
      get { return ActivityOperation.IsDeleted ? Visibility.Collapsed : Visibility.Visible; }
    }
  }
}
