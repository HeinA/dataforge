﻿using Dataforge.Business.Activities;
using Dataforge.Business.Collections;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views.ActivityManagement
{
  public class ActivityNodeViewModel : TreeNodeViewModel<Activity>
  {
    #region Constructors

    public ActivityNodeViewModel(Activity activity, IMultiModelViewModel owner)
      : base(activity, owner)
    {
      Operations = new BasicCollection<ActivityOperation>(Model.GetAssociativeObjects<ActivityOperation>().Where(op => !op.Reference.IsContextOperation).OrderBy(op => op.SortOrder).ToList());
      ContextOperations = new BasicCollection<ActivityOperation>(Model.GetAssociativeObjects<ActivityOperation>().Where(op => op.Reference.IsContextOperation).OrderBy(op => op.SortOrder).ToList());
      activity.Operations.CollectionChanged += Operations_CollectionChanged;

      _folderViewModels.Add(new OperationFolderNodeViewModel(Model, new OperationFolder(Model, false, "Operations"), Operations, Owner) { IsReadOnly = owner.IsReadOnly });
      _folderViewModels.Add(new OperationFolderNodeViewModel(Model, new OperationFolder(Model, true, "Context Operations"), ContextOperations, Owner) { IsReadOnly = owner.IsReadOnly });
    }

    void Operations_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
    {
      if (e.Action == NotifyCollectionChangedAction.Replace) return;

      Operations.Clear();
      foreach (ActivityOperation op in Model.GetAssociativeObjects<ActivityOperation>().Where(op => !op.Reference.IsContextOperation).OrderBy(op => op.SortOrder))
      {
        Operations.Add(op);
      }

      ContextOperations.Clear();
      foreach (ActivityOperation op in Model.GetAssociativeObjects<ActivityOperation>().Where(op => op.Reference.IsContextOperation).OrderBy(op => op.SortOrder))
      {
        ContextOperations.Add(op);
      }
    }

    #endregion

    #region Model Propertry string Name

    public string Name
    {
      get
      {
        if (Model == null) return default(string);
        return string.IsNullOrWhiteSpace(Model.Name) ? "** Activity **" : Model.Name;
      }
      set { Model.Name = value; }
    }

    #endregion

    #region Model Propertry string Description

    public string Description
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Description;
      }
      set { Model.Description = value; }
    }

    #endregion

    BasicCollection<ActivityOperation> _operations;
    public BasicCollection<ActivityOperation> Operations
    {
      get { return _operations; }
      private set { _operations = value; }
    }

    BasicCollection<ActivityOperation> _contextOperations;
    public BasicCollection<ActivityOperation> ContextOperations
    {
      get { return _contextOperations; }
      private set { _contextOperations = value; }
    }

    #region ReadOnlyCollection<OperationFolderNodeViewModel> FolderViewModels

    BasicCollection<OperationFolderNodeViewModel> _folderViewModels = new BasicCollection<OperationFolderNodeViewModel>();
    ReadOnlyCollection<OperationFolderNodeViewModel> _rofolderViewModels;

    public ReadOnlyCollection<OperationFolderNodeViewModel> FolderViewModels
    {
      get { return _rofolderViewModels ?? (_rofolderViewModels = new ReadOnlyCollection<OperationFolderNodeViewModel>(_folderViewModels.ToList())); }
    }

    #endregion
  }
}
