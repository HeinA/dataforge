﻿using Dataforge.Business.Activities;
using Dataforge.Business.Collections;
using Dataforge.Business.Security;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Dataforge.Prism.Views.ActivityManagement
{
  public class ActivityOperationItemViewModel : SelectableViewModel<ActivityOperation>
  {
    #region Constructors

    public ActivityOperationItemViewModel(ActivityOperation activityOperation, IMultiModelViewModel owner)
      : base(activityOperation, owner)
    {
      RoleViewModelsProvider = new ReadOnlyCollectionProvider<Role, ActivityOperationRoleItemViewModel>(this, activityOperation.Activity.Roles, CreateActivityOperationRoleItemViewModel, OnActivityOperationRoleItemViewModelCreated);
    }

    #endregion

    #region Model Propertry string Text

    public string Text
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Reference.Text;
      }
    }

    #endregion

    #region Visibility ImageVisibility

    public Visibility ImageVisibility
    {
      get
      {
        if (Model == null) return Visibility.Collapsed;
        if (string.IsNullOrWhiteSpace(Model.Reference.Base64Image)) return Visibility.Collapsed;
        return Visibility.Visible;
      }
    }

    #endregion

    #region ImageSource ImageSource

    ImageSource _imageSource;
    public ImageSource ImageSource
    {
      get
      {
        if (Model == null) return default(ImageSource);
        if (string.IsNullOrWhiteSpace(Model.Reference.Base64Image)) return null;
        if (_imageSource == null)
        {
          byte[] imgData = Convert.FromBase64String(Model.Reference.Base64Image);
          BitmapImage img = new BitmapImage();
          using (MemoryStream ms = new MemoryStream(imgData))
          {
            img.BeginInit();
            img.CacheOption = BitmapCacheOption.OnLoad;
            img.StreamSource = ms;
            img.EndInit();
          }
          _imageSource = img as ImageSource;
        }
        return _imageSource;
      }
    }

    #endregion

    #region ReadOnlyCollectionProvider RoleViewModels

    ////////////////////////////////////////////////////////
    // Add to Constructor:
    // RoleViewModelsProvider = new ReadOnlyCollectionProvider<Role, ActivityOperationRoleItemViewModel>(this, /* Optionally add model collection here */ CreateActivityOperationRoleItemViewModel, OnActivityOperationRoleItemViewModelCreated);
    ////////////////////////////////////////////////////////

    ReadOnlyCollectionProvider<Role, ActivityOperationRoleItemViewModel> RoleViewModelsProvider;

    public ReadOnlyObservableCollection<ActivityOperationRoleItemViewModel> RoleViewModels
    {
      get { return RoleViewModelsProvider.ViewModelCollection; }
    }

    CollectionViewSource _roleViewModelViewSource;
    public ICollectionView RoleCollectionView
    {
      get
      {
        if (_roleViewModelViewSource == null)
        {
          _roleViewModelViewSource = new CollectionViewSource();
          _roleViewModelViewSource.Source = RoleViewModels;
          _roleViewModelViewSource.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
        }
        return _roleViewModelViewSource.View;
      }
    }
    public BasicCollection<Role> Roles
    {
      get { return RoleViewModelsProvider.ModelCollection; }
      set { RoleViewModelsProvider.ModelCollection = value; }
    }

    ActivityOperationRoleItemViewModel CreateActivityOperationRoleItemViewModel(Role model)
    {
      return new ActivityOperationRoleItemViewModel(Model, model, Owner);
    }

    void OnActivityOperationRoleItemViewModelCreated(ActivityOperationRoleItemViewModel viewModel, Role model)
    {
    }

    #endregion
  }
}
