﻿using Dataforge.Business.Activities;
using Microsoft.Practices.Prism.PubSubEvents;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Dataforge.Prism.Views.ActivityManagement
{
  public class ActivityOperationNodeViewModel : TreeNodeViewModel<ActivityOperation>
  {
    #region Constructors

    public ActivityOperationNodeViewModel(ActivityOperation op, IMultiModelViewModel owner)
      : base(op, owner)
    {
    }

    #endregion

    #region Model Propertry string Text

    public string Text
    {
      get
      {
        if (Model == null) return default(string);
        IEventAggregator ea = LocalEventAggregator;
        return Model.Reference.Text;
      }
    }

    #endregion

    #region Visibility ImageVisibility

    public Visibility ImageVisibility
    {
      get
      {
        if (Model == null) return Visibility.Collapsed;
        if (string.IsNullOrWhiteSpace(Model.Reference.Base64Image)) return Visibility.Collapsed;
        return Visibility.Visible;
      }
    }

    #endregion

    #region ImageSource ImageSource

    ImageSource _imageSource;
    public ImageSource ImageSource
    {
      get
      {
        if (Model == null) return default(ImageSource);
        if (string.IsNullOrWhiteSpace(Model.Reference.Base64Image)) return null;
        if (_imageSource == null)
        {
          byte[] imgData = Convert.FromBase64String(Model.Reference.Base64Image);
          BitmapImage img = new BitmapImage();
          using (MemoryStream ms = new MemoryStream(imgData))
          {
            img.BeginInit();
            img.CacheOption = BitmapCacheOption.OnLoad;
            img.StreamSource = ms;
            img.EndInit();
          }
          _imageSource = img as ImageSource;
        }
        return _imageSource;
      }
    }

    #endregion
  }
}
