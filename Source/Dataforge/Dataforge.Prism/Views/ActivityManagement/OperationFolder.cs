﻿using Dataforge.Business;
using Dataforge.Business.Activities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views.ActivityManagement
{
  public class OperationFolder : IdentifiableObject
  {
    public OperationFolder(Activity activity, bool contextOperations, string folderName)
    {
      _activity = activity;
      _folderName = folderName;
      _contextOperations = contextOperations;
      StringIdentifier = string.Format("{0}:{1}", Activity.StringIdentifier, FolderName);
    }

    Activity _activity;
    public Activity Activity
    {
      get { return _activity; }
    }

    bool _contextOperations = false;
    public bool ContextOperations
    {
      get { return _contextOperations; }
    }

    #region string FolderName

    string _folderName;
    public string FolderName
    {
      get { return _folderName; }
    }

    #endregion
  }
}
