﻿using Dataforge.Business.Activities;
using Dataforge.Business.Collections;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views.ActivityManagement
{
  public class OperationFolderNodeViewModel : TreeNodeViewModel<OperationFolder>
  {
    #region Constructors

    public OperationFolderNodeViewModel(Activity activity, OperationFolder folder, BasicCollection<ActivityOperation> operations, IMultiModelViewModel owner)
      : base(folder, owner)
    {
      _activity = activity;
      OperationViewModelsProvider = new ReadOnlyCollectionProvider<ActivityOperation, ActivityOperationNodeViewModel>(this, operations, CreateActivityOperationNodeViewModel, OnActivityOperationNodeViewModelCreated);
    }

    #endregion

    #region Activity Activity

    Activity _activity;
    public Activity Activity
    {
      get { return _activity; }
    }

    #endregion

    #region Model Propertry string FolderName

    public string FolderName
    {
      get
      {
        if (Model == null) return default(string);
        return Model.FolderName;
      }
    }

    #endregion

    #region ReadOnlyCollectionProvider OperationViewModels

    ////////////////////////////////////////////////////////
    // Add to Constructor:
    // OperationViewModelsProvider = new ReadOnlyCollectionProvider<ActivityOperation, ActivityOperationNodeViewModel>(this, /* Optionally add model collection here */ CreateActivityOperationNodeViewModel, OnActivityOperationNodeViewModelCreated);
    ////////////////////////////////////////////////////////

    ReadOnlyCollectionProvider<ActivityOperation, ActivityOperationNodeViewModel> OperationViewModelsProvider;

    public ReadOnlyObservableCollection<ActivityOperationNodeViewModel> OperationViewModels
    {
      get { return OperationViewModelsProvider.ViewModelCollection; }
    }

    public BasicCollection<ActivityOperation> Operations
    {
      get { return OperationViewModelsProvider.ModelCollection; }
      set { OperationViewModelsProvider.ModelCollection = value; }
    }

    ActivityOperationNodeViewModel CreateActivityOperationNodeViewModel(ActivityOperation model)
    {
      return new ActivityOperationNodeViewModel(model, Owner);
    }

    void OnActivityOperationNodeViewModelCreated(ActivityOperationNodeViewModel viewModel, ActivityOperation model)
    {
    }

    #endregion

    #region bool MayDelete

    public override bool MayDelete
    {
      get { return false; }
    }

    #endregion
  }
}
