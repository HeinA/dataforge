﻿using Dataforge.Business.Activities;
using Dataforge.Business.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Dataforge.Prism.Views.ActivityManagement
{
  public class ActivityRoleItemViewModel : SelectableViewModel<Role>
  {
    #region Constructors

    public ActivityRoleItemViewModel(Activity activity, Role model, IMultiModelViewModel owner)
      : base(model, owner)
    {
      _activity = activity;
    }

    #endregion

    #region Activity Activity

    Activity _activity;
    public Activity Activity
    {
      get { return _activity; }
    }

    #endregion

    #region Model Propertry string Name

    public string Name
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Name;
      }
      set { Model.Name = value; }
    }

    #endregion

    #region bool HasRole

    public bool HasRole
    {
      get { return Activity.Roles.Contains(Model); }
      set
      {
        if (value) Activity.Roles.Add(Model);
        else Activity.Roles.Remove(Model);
        OnPropertyChanged("HasRole");
        OnPropertyChanged("AllowEditVisibility");
      }
    }

    #endregion

    #region bool EditPermission

    public bool EditPermission
    {
      get { return Activity.GetAssociativeObject<ActivityRole>(Model).EditPermission; }
      set
      {
        Activity.GetAssociativeObject<ActivityRole>(Model).EditPermission = value;
        OnPropertyChanged("AllowEdit");
      }
    }

    #endregion

    public Visibility EditPermissionVisibility
    {
      get { return HasRole == true ? Visibility.Visible : Visibility.Collapsed; }
    }
  }
}
