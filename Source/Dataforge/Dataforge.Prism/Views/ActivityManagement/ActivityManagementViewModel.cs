﻿using Dataforge.Business;
using Dataforge.Business.Activities;
using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using Dataforge.Business.Security;
using Dataforge.Business.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Dataforge.Prism.Views.ActivityManagement
{
  [Export]
  [PartCreationPolicy(CreationPolicy.NonShared)]
  public class ActivityManagementViewModel : ManagementViewModel<Activity, ActivityNodeViewModel>
  {
    public const string ContextRegion = "ContextRegion";

    #region Constructors

    public ActivityManagementViewModel()
    {
      Title = "Activity Management";
      IsFocused = true;
    }

    public ActivityManagementViewModel(BasicCollection<Activity> activities)
      : base(activities)
    {
      Title = "Activity Management";
      IsFocused = true;
    }

    #endregion

    #region Views

    #region Import ActivityView ActivityView & ActivityViewModel ActivityViewModel

    #region Import ActivityView ActivityView

    [Import]
    ActivityView _activityView = null;
    public ActivityView ActivityView
    {
      get { return _activityView; }
    }

    #endregion

    #region Import ActivityViewModel ActivityViewModel

    [Import]
    ActivityViewModel _activityViewModel = null;
    public ActivityViewModel ActivityViewModel
    {
      get { return _activityViewModel; }
    }

    #endregion

    #endregion

    #region Import ActivityRolesView ActivityRolesView & ActivityRolesViewModel ActivityRolesViewModel

    #region Import ActivityRolesView ActivityRolesView

    [Import]
    ActivityRolesView _activityRolesView = null;
    public ActivityRolesView ActivityRolesView
    {
      get { return _activityRolesView; }
    }

    #endregion

    #region Import ActivityRolesViewModel ActivityRolesViewModel

    [Import]
    ActivityRolesViewModel _activityRolesViewModel = null;
    public ActivityRolesViewModel ActivityRolesViewModel
    {
      get { return _activityRolesViewModel; }
    }

    #endregion

    #endregion

    #region Import ActivityFolderView ActivityFolderView & ActivityFolderViewModel ActivityFolderViewModel

    #region Import ActivityFolderView ActivityFolderView

    [Import]
    ActivityFolderView _activityFolderView = null;
    public ActivityFolderView ActivityFolderView
    {
      get { return _activityFolderView; }
    }

    #endregion

    #region Import ActivityFolderViewModel ActivityFolderViewModel

    [Import]
    ActivityFolderViewModel _activityFolderViewModel = null;
    public ActivityFolderViewModel ActivityFolderViewModel
    {
      get { return _activityFolderViewModel; }
    }

    #endregion

    #endregion

    #endregion

    #region Overrides

    #region int ActivityId

    public override int ActivityId
    {
      get { return Activities.ActivityManagement; }
    }

    #endregion

    #region void Load()

    public override void Load()
    {
      using (var svc = ServiceFactory.GetService<IDataforgeService>())
      {
        ModelCollection = svc.ExecuteFunction<BasicCollection<Activity>>(i => i.LoadActivities());
      }
    }

    #endregion

    #region void Save()

    public override void Save()
    {
      using (var svc = ServiceFactory.GetService<IDataforgeService>())
      {
        ModelCollection = svc.ExecuteFunction<BasicCollection<Activity>>(i => i.SaveActivities(ModelCollection));
      }
    }

    #endregion

    #region ActivityNodeViewModel CreateViewModel(Activity model)

    public override ActivityNodeViewModel CreateViewModel(Activity model)
    {
      return new ActivityNodeViewModel(model, this);
    }

    #endregion

    protected override void OnFocusedViewModelChanged(ISelectableViewModel viewModel)
    {
      if (viewModel is ActivityNodeViewModel)
      {
        ActivityNodeViewModel vm = viewModel as ActivityNodeViewModel;
        DockPanel.SetDock(ActivityView, Dock.Top);
        ShowInRegion(ContextRegion, ActivityView);
        ActivityViewModel.IsReadOnly = !Activity.CanEdit;
        ActivityViewModel.Model = vm.Model;
        ShowInRegion(ContextRegion, ActivityRolesView);
        ActivityRolesViewModel.Activity = vm.Model;
        ActivityRolesViewModel.IsReadOnly = !Activity.CanEdit;
        ActivityRolesViewModel.ModelCollection = Cache.Instance.GetObjects<Role>(r => r.Name, true);
      }
      else
      {
        RemoveFromRegion(ContextRegion, ActivityView);
        RemoveFromRegion(ContextRegion, ActivityRolesView);
      }

      if (viewModel is OperationFolderNodeViewModel)
      {
        OperationFolderNodeViewModel vm = viewModel as OperationFolderNodeViewModel;
        ShowInRegion(ContextRegion, ActivityFolderView);
        ActivityFolderViewModel.IsReadOnly = !Activity.CanEdit;
        ActivityFolderViewModel.TargetActivity = vm.Activity;
        ActivityFolderViewModel.ModelCollection = vm.Operations;
      }
      else
      {
        RemoveFromRegion(ContextRegion, ActivityFolderView);
      }

      base.OnFocusedViewModelChanged(viewModel);
    }

    #endregion
  }
}
