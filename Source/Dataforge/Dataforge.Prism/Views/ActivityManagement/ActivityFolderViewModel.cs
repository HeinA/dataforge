﻿using Dataforge.Business.Activities;
using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using Dataforge.Business.Security;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views.ActivityManagement
{
  [Export]
  [PartCreationPolicy(CreationPolicy.Shared)]
  public class ActivityFolderViewModel : MultiModelOperationsAwareViewModel<ActivityOperation, ActivityOperationItemViewModel> //OperationsAware
  {
    public override int ActivityId
    {
      get { return Activities.ActivityManagement; }
    }

    #region Activity Activity

    Activity _targetActivity;
    public Activity TargetActivity
    {
      get { return _targetActivity; }
      set { _targetActivity = value; }
    }

    #endregion

    #region DelegateCommand MoveUpCommand

    DelegateCommand _moveUpCommand;
    public DelegateCommand MoveUpCommand
    {
      get { return _moveUpCommand ?? (_moveUpCommand = new DelegateCommand(ExecuteMoveUp, CanExecuteMoveUp)); }
    }

    bool CanExecuteMoveUp()
    {
      return true;
    }

    void ExecuteMoveUp()
    {
      try
      {
        ActivityOperation aop = (ActivityOperation)SelectedModels[0];
        Operation op = aop.Reference;

        List<ActivityOperation> sortedList = TargetActivity.GetAssociativeObjects<ActivityOperation>()
          .Where(o => o.Reference.IsContextOperation == op.IsContextOperation)
          .OrderBy(o => o.SortOrder)
          .ToList();

        int index = sortedList.IndexOf(aop);
        if (index == 0) return;

        ActivityOperation aop1 = sortedList[index - 1];
        Operation op1 = aop1.Reference;

        aop.SortOrder = index - 1; 
        aop1.SortOrder = index; 

        TargetActivity.Operations.FireReset();

        ISelectableViewModel svm = GetViewModel(aop);
        svm.IsSelected = true;
      }
      catch (Exception ex)
      {
        if (ExceptionPolicy.HandleException(ex, "Default Policy"))
          throw;
      }
    }

    #endregion

    #region DelegateCommand MoveDownCommand

    DelegateCommand _moveDownCommand;
    public DelegateCommand MoveDownCommand
    {
      get { return _moveDownCommand ?? (_moveDownCommand = new DelegateCommand(ExecuteMoveDown, CanExecuteMoveDown)); }
    }

    bool CanExecuteMoveDown()
    {
      return true;
    }

    void ExecuteMoveDown()
    {
      try
      {
        ActivityOperation aop = (ActivityOperation)SelectedModels[0];
        Operation op = aop.Reference;

        List<ActivityOperation> sortedList = TargetActivity.GetAssociativeObjects<ActivityOperation>()
          .Where(o => o.Reference.IsContextOperation == op.IsContextOperation)
          .OrderBy(o => o.SortOrder)
          .ToList();

        int index = sortedList.IndexOf(aop);
        if (index == sortedList.Count - 1) return;

        ActivityOperation aop1 = sortedList[index + 1];
        Operation op1 = aop1.Reference;

        aop.SortOrder = index + 1;
        aop1.SortOrder = index;

        TargetActivity.Operations.FireReset();

        ISelectableViewModel svm = GetViewModel(aop);
        svm.IsSelected = true;
      }
      catch (Exception ex)
      {
        if (ExceptionPolicy.HandleException(ex, "Default Policy"))
          throw;
      }
    }

    #endregion

    #region bool MovementButtonsEnabled

    public bool MovementButtonsEnabled
    {
      get { return SelectedViewModels.Count > 0 && !IsReadOnly; }
    }

    #endregion

    #region Overrides

    #region OperationItemViewModel CreateViewModel(Operation model)

    public override ActivityOperationItemViewModel CreateViewModel(ActivityOperation model)
    {
      return new ActivityOperationItemViewModel(model, this);
    }

    #endregion

    #region void OnSelectionChanged()

    protected override void OnSelectionChanged()
    {
      OnPropertyChanged("MovementButtonsEnabled");
      base.OnSelectionChanged();
    }

    #endregion

    #endregion
  }
}
