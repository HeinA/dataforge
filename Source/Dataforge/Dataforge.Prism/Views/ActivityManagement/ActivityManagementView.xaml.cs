﻿using Dataforge.Prism.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Dataforge.Prism.Views.ActivityManagement
{
  /// <summary>
  /// Interaction logic for ActivityManagementView.xaml
  /// </summary>
  [Export]
  [PartCreationPolicy(CreationPolicy.Shared)]
  public partial class ActivityManagementView : ViewControl
  {
    public ActivityManagementView()
    {
      InitializeComponent();
    }
  }
}
