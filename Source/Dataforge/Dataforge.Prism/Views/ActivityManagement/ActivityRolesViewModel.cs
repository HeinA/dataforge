﻿using Dataforge.Business.Activities;
using Dataforge.Business.Security;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Dataforge.Prism.Views.ActivityManagement
{
  [Export]
  [PartCreationPolicy(CreationPolicy.Shared)]
  public class ActivityRolesViewModel : MultiModelViewModel<Role, ActivityRoleItemViewModel>
  {
    #region Activity Activity

    Activity _activity;
    public Activity Activity
    {
      get { return _activity; }
      set { _activity = value; }
    }

    #endregion

    #region DelegateCommand KeyDownCommand

    DelegateCommand<KeyEventArgs> _keyDownCommand;
    public DelegateCommand<KeyEventArgs> KeyDownCommand
    {
      get { return _keyDownCommand ?? (_keyDownCommand = new DelegateCommand<KeyEventArgs>(ExecuteKeyDown)); }
    }

    void ExecuteKeyDown(KeyEventArgs e)
    {
      switch (e.Key)
      {
        case Key.Space:
          foreach (ActivityRoleItemViewModel vm in SelectedViewModels)
          {
            vm.HasRole = !vm.HasRole;
          }
          e.Handled = true;
          break;

        case Key.E:
          foreach (ActivityRoleItemViewModel vm in SelectedViewModels)
          {
            vm.EditPermission = !vm.EditPermission;
          }
          e.Handled = true;
          break;
      }
    }

    #endregion

    #region Overrides

    public override ActivityRoleItemViewModel CreateViewModel(Role model)
    {
      return new ActivityRoleItemViewModel(Activity, model, this) { IsReadOnly = this.IsReadOnly };
    }

    #endregion
  }
}
