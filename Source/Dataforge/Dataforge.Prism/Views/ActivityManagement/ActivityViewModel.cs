﻿using Dataforge.Business.Activities;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Views.ActivityManagement
{
  [Export]
  [PartCreationPolicy(CreationPolicy.Shared)]
  public class ActivityViewModel : ViewModel<Activity>
  {
    #region Model Propertry int Id

    public int Id
    {
      get
      {
        if (Model == null) return default(int);
        return Model.Id;
      }
      set { Model.Id = value; }
    }

    #endregion

    #region Model Propertry string Name

    public string Name
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Name;
      }
      set { Model.Name = value; }
    }

    #endregion

    #region Model Propertry string Description

    public string Description
    {
      get
      {
        if (Model == null) return default(string);
        return Model.Description;
      }
      set { Model.Description = value; }
    }

    #endregion
  }
}
