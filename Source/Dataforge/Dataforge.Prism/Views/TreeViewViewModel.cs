﻿using Dataforge.Business;
using Dataforge.Business.Collections;
using Dataforge.Business.Security;
using Dataforge.Prism.Views;
using Dataforge.Prism.Views.Operations;
using Dataforge.Prism.Views.OrganizationalStructure;
using Microsoft.Practices.Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Dataforge.Prism
{
  public abstract class TreeViewViewModel<TModel, TViewModel> : CollectionViewModel<TModel, TViewModel>, IOperationTarget, ITreeViewViewModel
    where TModel : IdentifiableObject
    where TViewModel : ITreeViewNodeViewModel
  {
    #region Constructors

    public TreeViewViewModel(BasicCollection<TModel> modelCollection)
      : base(modelCollection)
    {
      if (ViewModelCollection.Count > 0) ViewModelCollection[0].IsSelected = true;
    }

    #endregion

    public void RegisterNodeViewModel(ITreeViewNodeViewModel viewModel)
    {
    }

    Dictionary<BusinessObject, object> _viewModelDictionary;

    #region Commands

    #region DelegateCommand SelectPreviousNodeCommand

    DelegateCommand _selectPreviousNodeCommand;
    public DelegateCommand SelectPreviousNodeCommand
    {
      get
      {
        if (_selectPreviousNodeCommand == null) _selectPreviousNodeCommand = new DelegateCommand(OnSelectPreviousNode);
        return _selectPreviousNodeCommand;
      }
    }

    #endregion

    #region void OnSelectPreviousNode()

    private void OnSelectPreviousNode()
    {
      if (SelectedNodeViewModel == null && _selectPreviousNode)
        PreviousNodeViewModel.IsSelected = true;
    }

    #endregion

    #endregion

    #region Operations

    #region ReadOnlyCollectionProvider<Operation, OperationViewModel> OperationCollectionProvider

    ReadOnlyCollectionProvider<Operation, OperationViewModel> _operationCollectionProvider;
    public ReadOnlyCollectionProvider<Operation, OperationViewModel> OperationCollectionProvider
    {
      get
      {
        if (_operationCollectionProvider == null) _operationCollectionProvider = new ReadOnlyCollectionProvider<Operation, OperationViewModel>(CreateOperationViewModel);
        return _operationCollectionProvider;
      }
    }

    #endregion

    #region OperationViewModel CreateOperationViewModel(Operation operation)

    private OperationViewModel CreateOperationViewModel(Operation operation)
    {
      return new OperationViewModel(this, operation);
    }

    #endregion

    #region ReadOnlyObservableCollection<OperationViewModel> OperationViewModels

    public ReadOnlyObservableCollection<OperationViewModel> OperationViewModels
    {
      get { return OperationCollectionProvider.ViewModelCollection; }
    }

    #endregion

    #endregion

    #region bool IsFocused

    bool _isFocused = true;
    public bool IsFocused
    {
      get { return _isFocused; }
      set { SetValue<bool>(ref _isFocused, value, "IsFocused"); }
    }

    #endregion

    #region TreeViewNodeViewModel<TModel, TViewModel> PreviousNodeViewModel

    private TreeViewNodeViewModel<TModel, TViewModel> _previousNodeViewModel;
    TreeViewNodeViewModel<TModel, TViewModel> PreviousNodeViewModel
    {
      get { return _previousNodeViewModel; }
      set
      {
        if (value != null && _previousNodeViewModel != value)
        {
          _previousNodeViewModel = value;
        }
      }
    }

    #endregion

    #region TreeViewNodeViewModel<TModel, TViewModel> SelectedNodeViewModel

    TreeViewNodeViewModel<TModel, TViewModel> _selectedNodeViewModel;
    public TreeViewNodeViewModel<TModel, TViewModel> SelectedNodeViewModel
    {
      get { return _selectedNodeViewModel; }
      set
      {
        if (_selectedNodeViewModel != value)
        {
          if (_selectedNodeViewModel != null) _previousNodeViewModel = _selectedNodeViewModel;
          _selectedNodeViewModel = value;
          OrganizationalStructureElementViewModel o = _selectedNodeViewModel as OrganizationalStructureElementViewModel;
          Console.WriteLine(o == null ? "NULL" : o.Name);
        }
      }
    }

    #endregion

    #region TModel SelectedModel

    protected TModel SelectedModel
    {
      get
      {
        if (SelectedNodeViewModel == null) return null;
        return SelectedNodeViewModel.Model;
      }
    }

    #endregion

    #region bool IsContextMenuOpen

    bool _isContextOpen = false;
    public bool IsContextMenuOpen
    {
      get { return _isContextOpen; }
      set
      {
        _isContextOpen = value;
        if (value) _selectPreviousNode = true;
      }
    }

    #endregion

    #region bool ITreeViewViewModel.SelectPreviousNode

    bool _selectPreviousNode = true;
    bool ITreeViewViewModel.SelectPreviousNode
    {
      get { return _selectPreviousNode; }
      set { _selectPreviousNode = value; }
    }

    #endregion

    public abstract bool CanExecuteOperation(Operation operation);
    public abstract void ExecuteOperation(Operation operation);

    #region void RaiseCanExecuteChanged()

    public void RaiseCanExecuteChanged()
    {
      foreach (var operationVM in OperationViewModels)
      {
        operationVM.OperationCommand.RaiseCanExecuteChanged();
      }
    }

    #endregion
  }
}
