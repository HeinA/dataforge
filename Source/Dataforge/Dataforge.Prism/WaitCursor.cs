﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Dataforge.Prism
{
  public class WaitCursor : IDisposable
  {
    Cursor _previousCursor;

    public WaitCursor()
    {
      _previousCursor = Mouse.OverrideCursor;
      Mouse.SetCursor(Cursors.Wait);
    }

    public void Dispose()
    {
      Mouse.SetCursor(_previousCursor);
      Mouse.UpdateCursor();
    }
  }
}
