﻿using Dataforge.Business;
using Dataforge.Prism.Contants;
using Dataforge.Prism.Views;
using Dataforge.PrismAvalonExtensions;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace Dataforge.Prism
{
  public static class DockingHelper
  {
    public static TViewModel ShowManagementView<TView, TViewModel>()
      where TView : FrameworkElement
      where TViewModel : class, IManagementViewModel
    {
      IRegionManager rm = ServiceLocator.Current.GetInstance<IRegionManager>();
      TView v = ServiceLocator.Current.GetInstance<TView>();

      try
      {
        using (new WaitCursor())
        {
          TViewModel vm = null;
          if (rm.Regions[RegionNames.DockingRegion].Views.Contains(v))
          {
            rm.Regions[RegionNames.DockingRegion].Activate(v);
            vm = (TViewModel)v.DataContext;
          }
          else
          {
            vm = ServiceLocator.Current.GetInstance<TViewModel>();
            vm.Load();
            v.DataContext = vm;

            rm.Regions[RegionNames.DockingRegion].Add(new DockingMetadata(v, new DocumentDockStrategy()), null, true);
          }

          v.Dispatcher.BeginInvoke(new System.Action(() =>
          {
            vm.Initialize();
          }), DispatcherPriority.Background, null);
          return vm;
        }
      }
      catch (Exception ex)
      {
        if (rm.Regions[RegionNames.DockingRegion].Views.Contains(v))
          rm.Regions[RegionNames.DockingRegion].Remove(v);

        if (ExceptionPolicy.HandleException(ex, "Default Policy"))
          throw;
      }

      return null;
    }
  }
}
