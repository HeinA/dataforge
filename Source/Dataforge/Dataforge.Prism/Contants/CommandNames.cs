﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Contants
{
  public class CommandNames
  {
    public const string ToggleDelete = "Dataforge.ToggleDelete";
    public const string Save = "Dataforge.Save";
  }
}
