﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.Contants
{
  public class RegionNames
  {
    public const string DockingRegion = "DockingRegion";

    public const string RibbonRegion = "RibbonRegion";
    public const string CompanySetupRegion = "CompanySetupRegion";
    public const string AdministrationRegion = "AdministrationRegion";
    public const string ActivityRegion = "ActivityRegion";
    
    public const string DialogRegion = "DialogRegion";
    public const string WaitAnimationRegion = "WaitAnimationRegion";
    public const string ExceptionRegion = "ExceptionRegion";
  }
}
