﻿using Microsoft.Practices.Prism.MefExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.ComponentModel.Composition.Hosting;
using Dataforge.Prism.Contants;
using Microsoft.Practices.Prism.Regions;
using Xceed.Wpf.AvalonDock;
using Dataforge.PrismAvalonExtensions.Regions;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Prism.Modularity;
using Fluent;
using Dataforge.Prism.Regions;
using Dataforge.Prism.Views;
using System.IO;
using Dataforge.PrismAvalonExtensions;
using System.ComponentModel;
using Xceed.Wpf.AvalonDock.Layout.Serialization;
using Xceed.Wpf.Toolkit;
using Dataforge.Prism.Views.Shell;
using Dataforge.Business.Client;
using System.Reflection;
using System.Windows.Controls;

namespace Dataforge.Prism
{
  public class DataforgeMefBootstrapper : MefBootstrapper
  {
    RibbonMdiLayout _layout;
    RibbonMdiLayout Layout
    {
      get { return _layout; }
    }

    protected override DependencyObject CreateShell()
    {
      _layout = ServiceLocator.Current.GetInstance<RibbonMdiLayout>();
      return _layout;
    }

    protected override void InitializeShell()
    {
      base.InitializeShell();
      Application.Current.MainWindow = (Window)this.Shell;
      Application.Current.MainWindow.Show();
    }

    protected override RegionAdapterMappings ConfigureRegionAdapterMappings()
    {
      RegionAdapterMappings mappings = base.ConfigureRegionAdapterMappings();
      IRegionBehaviorFactory factory = ServiceLocator.Current.GetInstance<IRegionBehaviorFactory>();
      mappings.RegisterMapping(typeof(DockingManager), new DockingManagerRegionAdapter(factory));
      mappings.RegisterMapping(typeof(Ribbon), new RibbonRegionAdapter(factory));
      mappings.RegisterMapping(typeof(ChildWindow), new ChildWindowRegionAdapter(factory));
      mappings.RegisterMapping(typeof(StackPanel), new StackPanelRegionAdapter(factory));
      mappings.RegisterMapping(typeof(DockPanel), new DockPanelRegionAdapter(factory));
      mappings.RegisterMapping(typeof(RibbonGroupBox), new RibbonGroupBoxRegionAdapter(factory));
      return mappings;
    }

    protected override IRegionBehaviorFactory ConfigureDefaultRegionBehaviors()
    {
      var behaviours = base.ConfigureDefaultRegionBehaviors();
      behaviours.AddIfMissing(RegionAwareBehaviour.RegionAwareBehaviourKey, typeof(RegionAwareBehaviour));
      return behaviours;
    }

    protected override void ConfigureAggregateCatalog()
    {
      base.ConfigureAggregateCatalog();
      this.AggregateCatalog.Catalogs.Add(new DirectoryCatalog(@".\"));
      this.AggregateCatalog.Catalogs.Add(new AssemblyCatalog(Assembly.GetEntryAssembly()));
    }

    public override void Run(bool runWithDefaultConfiguration)
    {
      Utilities.Initialize();
      base.Run(runWithDefaultConfiguration);
      //SerializationHelper.Deserialize(Layout.DockingManager, RegionNames.DockingRegion, "Layout.xml");
    }
  }
}
