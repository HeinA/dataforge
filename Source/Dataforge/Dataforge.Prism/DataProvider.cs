﻿using Dataforge.Business.Collections;
using Dataforge.Business.Documents;
using Dataforge.Business.Service;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism
{
  class DataProvider
  {
    [Export]
    BasicCollection<OrganizationalStructureElement> LoadOrganizationalStructure
    {
      get
      {
        using (var svc = ServiceFactory.GetService<IDataforgeService>())
        {
          return svc.ExecuteFunction<BasicCollection<OrganizationalStructureElement>>(i => i.LoadOrganizationalStructure());
        }
      }
    }
  }
}
