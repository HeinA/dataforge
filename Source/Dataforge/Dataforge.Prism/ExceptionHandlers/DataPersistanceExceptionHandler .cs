﻿using Dataforge.Prism.Contants;
using Dataforge.Prism.Views.Dialogs;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Configuration;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.Prism.ExceptionHandlers
{
  [ConfigurationElementType(typeof(CustomHandlerData))]
  public class DataPersistanceExceptionHandler  : IExceptionHandler
  {
    public DataPersistanceExceptionHandler(NameValueCollection values)
    {
    }

    #region IExceptionHandler Members

    public Exception HandleException(Exception exception, Guid handlingInstanceId)
    {
      IRegionManager rm = ServiceLocator.Current.GetInstance<IRegionManager>();
      DataPersistanceExceptionView ev = ServiceLocator.Current.GetInstance<DataPersistanceExceptionView>();
      ev.DataContext = new DataPersistanceExceptionViewModel(exception);
      rm.Regions[RegionNames.ExceptionRegion].Add(ev);

      return null;
    }

    #endregion  
  }
}
