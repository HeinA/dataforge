﻿using Dataforge.Prism.Contants;
using Dataforge.Prism.Views;
using Dataforge.Prism.Views.Dialogs;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Configuration;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Dataforge.Prism.ExceptionHandlers
{
  [ConfigurationElementType(typeof(CustomHandlerData))]
  public class ExceptionHandler : IExceptionHandler
  {
    public ExceptionHandler(NameValueCollection values)
    {
    }

    #region IExceptionHandler Members

    public Exception HandleException(Exception exception, Guid handlingInstanceId)
    {
      Application.Current.Dispatcher.Invoke(() =>
      {
        IRegionManager rm = ServiceLocator.Current.GetInstance<IRegionManager>();
        ExceptionView ev = ServiceLocator.Current.GetInstance<ExceptionView>();
        ev.DataContext = new ExceptionViewModel(exception);
        //rm.Regions[RegionNames.ExceptionRegion].Add(ev);
        ThreadBlockingDialogHelper.Show(ev, RegionNames.ExceptionRegion);
      });

      return null;
    }

    #endregion
  }
}
