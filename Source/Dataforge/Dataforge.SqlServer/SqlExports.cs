﻿using Dataforge.Data;
using System;
using System.Collections.Generic;
using System.Composition;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.SqlServer
{
  class SqlExports
  {
    [Export(typeof(IDbCommand))]
    public IDbCommand DbCommand
    {
      get
      {
        return new SqlCommand(string.Empty, ConnectionScope.Current.SqlConnection("Default"));
      }
    }
  }
}
