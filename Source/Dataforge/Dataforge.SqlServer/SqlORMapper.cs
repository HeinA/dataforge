﻿using Dataforge.Business;
using Dataforge.Business.Attributes;
using Dataforge.Business.Collections;
using Dataforge.Business.Constants;
using Dataforge.Business.Data;
using Dataforge.Business.Security;
using Dataforge.Business.Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Composition;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Dataforge.SqlServer
{
  [Export(typeof(ORMapper<>))]
  [Shared]
  class SqlORMapper<T> : ORMapper<T>
    where T : BusinessObject
  {
    #region Utility

    string GetParameterName(ref int i)
    {
      return string.Format("@p_{0}", i++);
    }

    string GetFilterString(Collection<DataFilterCondition> filters, SqlCommand cmd, ref int iCmd)
    {
      string filterString = null;
      foreach (DataFilterCondition filter in filters.Where(o => o.ApplicableType == typeof(T) || typeof(T).IsSubclassOf(o.ApplicableType)))
      {
        string paramName = GetParameterName(ref iCmd);
        if (filter.FilterType == FilterType.IsNotNull || filter.FilterType == FilterType.IsNull)
          filterString += string.Format(string.IsNullOrWhiteSpace(filterString) ? "{0}.{1} {2}" : " AND {0}.{1} {2}", FullTableName, EncapsulateName(GetColumnName(filter.Column)), GetFilterType(filter.FilterType));
        else
        {
          filterString += string.Format(string.IsNullOrWhiteSpace(filterString) ? "{0}.{1} {2} {3}" : " AND {0}.{1} {2} {3}", FullTableName, EncapsulateName(GetColumnName(filter.Column)), GetFilterType(filter.FilterType), paramName);
          //filterString += string.Format(filterString == null ? "{0} {1} {2}" : " AND {0} {1} {2}", EncapsulateName(filter.Column), GetFilterType(filter.FilterType), paramName);
          cmd.Parameters.AddWithValue(paramName, filter.Filter);
        }
      }
      if (!Global && SecurityContext.User != null && !typeof(IAssociativeObject).IsAssignableFrom(ObjectType))
      {
        string paramName = GetParameterName(ref iCmd);
        filterString += string.Format(filterString == null ? "({0} {1} {2} OR {0} {3})" : " AND ({0} {1} {2} OR {0} {3})", EncapsulateName(ApplicationDomainColumn), GetFilterType(FilterType.Equals), paramName, GetFilterType(FilterType.IsNull));
        cmd.Parameters.AddWithValue(paramName, SecurityContext.User.ApplicationDomain);
      }
      return filterString;
    }

    public override string GetInsertValues(System.Data.IDbCommand cmd, BusinessObject bo, bool insertId, ref int iCmd)
    {
      SqlCommand cmdSql = (SqlCommand)cmd;
      string values = null;

      if (IsGlobalIdentifiable)
      {
        IdentifiableObject io = bo as IdentifiableObject;
        string cmdName = GetParameterName(ref iCmd);
        values += values == null ? cmdName : string.Format(", {0}", cmdName);
        cmdSql.Parameters.AddWithValue(cmdName, io.GlobalIdentifier);
      }

      bool appDomainSpecified = false;
      foreach (PropertyInfo pi in PropertyInfoCollection.Where(prop => IsWriteColumn(prop)))
      {
        if (GetColumnName(pi) == ApplicationDomainColumn) appDomainSpecified = true;

        string cmdName = GetParameterName(ref iCmd);
        values += values == null ? cmdName : string.Format(", {0}", cmdName);

        if (ReferencePropertyInfoCollection.Contains(pi))
        {
          if (IsReferenceColumn(pi))
          {
            int i = bo.GetReferenceValue(pi.Name);
            cmdSql.Parameters.AddWithValue(cmdName, i == 0 ? DBNull.Value : (object)i);
          }
          else
          {
            BusinessObject bor = pi.GetValue(bo) as BusinessObject;
            cmdSql.Parameters.AddWithValue(cmdName, bor == null ? DBNull.Value : (object)bor.Id);
          }
        }
        else
        {
          cmdSql.Parameters.AddWithValue(cmdName, pi.GetValue(bo) ?? DBNull.Value);
        }
      }

      if (OwnerColumn != null)
      {
        string cmdName = GetParameterName(ref iCmd);
        values += values == null ? cmdName : string.Format(", {0}", cmdName);
        int owner = bo.GetReferenceValue(BusinessObject.PN_OWNER);
        cmdSql.Parameters.AddWithValue(cmdName, owner == 0 ? DBNull.Value : (object)owner);
      }

      if (!Global && !appDomainSpecified && SecurityContext.User != null && !typeof(IAssociativeObject).IsAssignableFrom(ObjectType))
      {
        IGlobalAware ga = bo as IGlobalAware;
        if (ga == null || !ga.IsGlobal)
        {
          string cmdName = GetParameterName(ref iCmd);
          values += values == null ? cmdName : string.Format(", {0}", cmdName);
          cmdSql.Parameters.AddWithValue(cmdName, SecurityContext.User.ApplicationDomain);
        }
      }

      if (RowColumn != null)
      {
        string cmdName = GetParameterName(ref iCmd);
        values += values == null ? cmdName : string.Format(", {0}", cmdName);
        cmdSql.Parameters.AddWithValue(cmdName, ((ITableCell)bo).RowId);
      }

      if (ColumnColumn != null)
      {
        string cmdName = GetParameterName(ref iCmd);
        values += values == null ? cmdName : string.Format(", {0}", cmdName);
        cmdSql.Parameters.AddWithValue(cmdName, ((ITableCell)bo).ColumnId);
      }

      if (ReferenceColumn != null && bo is IAssociativeObject)
      {
        string cmdName = GetParameterName(ref iCmd);
        values += values == null ? cmdName : string.Format(", {0}", cmdName);
        cmdSql.Parameters.AddWithValue(cmdName, ((IAssociativeObject)bo).ReferenceId);
      }

      if (BaseMapper != null || insertId)
      {
        string cmdName = GetParameterName(ref iCmd);
        values += values == null ? cmdName : string.Format(", {0}", cmdName);
        cmdSql.Parameters.AddWithValue(cmdName, bo.Id);
      }

      return values;
    }

    public override string GetUpdateColumns(System.Data.IDbCommand cmd, BusinessObject bo, ref int iCmd)
    {
      SqlCommand cmdSql = (SqlCommand)cmd;
      string columns = null;

      bool appDomainSpecified = false;
      foreach (PropertyInfo pi in PropertyInfoCollection.Where(prop => IsWriteColumn(prop)))
      {
        if (GetColumnName(pi) == ApplicationDomainColumn) appDomainSpecified = true;
        string cmdName = GetParameterName(ref iCmd);

        if (ReferencePropertyInfoCollection.Contains(pi))
        {
          if (IsReferenceColumn(pi))
          {
            columns += string.Format(columns == null ? "{0}.{1}={2}" : ", {0}.{1}={2}", FullTableName, EncapsulateName(GetColumnName(pi)), cmdName);
            int i = bo.GetReferenceValue(pi.Name);
            cmdSql.Parameters.AddWithValue(cmdName,  i == 0 ? DBNull.Value : (object)i);
          }
          else
          {
            columns += string.Format(columns == null ? "{0}.{1}={2}" : ", {0}.{1}={2}", FullTableName, EncapsulateName(GetColumnName(pi)), cmdName);
            BusinessObject bor = pi.GetValue(bo) as BusinessObject;
            cmdSql.Parameters.AddWithValue(cmdName, bor == null ? DBNull.Value : (object)bor.Id);
          }
        }
        else
        {
          object val = pi.GetValue(bo);
          if (val == null && !IgnoreNullColumn(pi) || val != null)
          {
            columns += string.Format(columns == null ? "{0}.{1}={2}" : ", {0}.{1}={2}", FullTableName, EncapsulateName(GetColumnName(pi)), cmdName);
            cmdSql.Parameters.AddWithValue(cmdName, val ?? DBNull.Value);
          }
        }
      }

      if (OwnerColumn != null)
      {
        string cmdName = GetParameterName(ref iCmd);
        columns += string.Format(columns == null ? "{0}.{1}={2}" : ", {0}.{1}={2}", FullTableName, EncapsulateName(OwnerColumn), cmdName);
        int owner = bo.GetReferenceValue(BusinessObject.PN_OWNER);
        cmdSql.Parameters.AddWithValue(cmdName, owner == 0 ? DBNull.Value : (object)owner);
      }

      if (!Global && !appDomainSpecified && SecurityContext.User != null && !typeof(IAssociativeObject).IsAssignableFrom(ObjectType))
      {
        IGlobalAware ga = bo as IGlobalAware;
        if (ga != null)
        {
          string cmdName = GetParameterName(ref iCmd);
          columns += string.Format(columns == null ? "{0}.{1}={2}" : ", {0}.{1}={2}", FullTableName, EncapsulateName(ApplicationDomainColumn), cmdName);
          if (!ga.IsGlobal)
          {
            cmdSql.Parameters.AddWithValue(cmdName, SecurityContext.User.ApplicationDomain);
          }
          else
          {
            cmdSql.Parameters.AddWithValue(cmdName, DBNull.Value);
          }
        }
      }

      if (RowColumn != null)
      {
        string cmdName = GetParameterName(ref iCmd);
        columns += string.Format(columns == null ? "{0}.{1}={2}" : ", {0}.{1}={2}", FullTableName, EncapsulateName(RowColumn), cmdName);
        cmdSql.Parameters.AddWithValue(cmdName, ((ITableCell)bo).RowId);
      }

      if (ColumnColumn != null)
      {
        string cmdName = GetParameterName(ref iCmd);
        columns += string.Format(columns == null ? "{0}.{1}={2}" : ", {0}.{1}={2}", FullTableName, EncapsulateName(ColumnColumn), cmdName);
        cmdSql.Parameters.AddWithValue(cmdName, ((ITableCell)bo).ColumnId);
      }

      if (ReferenceColumn != null && bo is IAssociativeObject)
      {
        string cmdName = GetParameterName(ref iCmd);
        columns += string.Format(columns == null ? "{0}.{1}={2}" : ", {0}.{1}={2}", FullTableName, EncapsulateName(ReferenceColumn), cmdName);
        cmdSql.Parameters.AddWithValue(cmdName, ((IAssociativeObject)bo).ReferenceId);
      }

      return columns;
    }

    public override string GetUpdateConditions(System.Data.IDbCommand cmd, BusinessObject bo, ref int iCmd)
    {
      SqlCommand cmdSql = (SqlCommand)cmd;
      string conditions = null;

      string cmdName1 = GetParameterName(ref iCmd);
      conditions += string.Format(conditions == null ? "{0}.{1}={2}" : " AND {0}.{1}={2}", FullTableName, EncapsulateName(IdColumn), cmdName1);
      cmdSql.Parameters.AddWithValue(cmdName1, bo.Id);

      bool appDomainSpecified = false;
      foreach (PropertyInfo pi in PropertyInfoCollection.Where(prop => IsWriteColumn(prop) && !IgnoreNullColumn(prop)))
      {
        if (IgnoreConcurrencyColumn(pi)) continue;

        if (GetColumnName(pi) == ApplicationDomainColumn) appDomainSpecified = true;
        string cmdName = GetParameterName(ref iCmd);
        object ov = bo.GetOriginalValue(pi);

        if (IsReferenceColumn(pi))
        {
          conditions += string.Format(conditions == null ? "{0}.{1}={2}" : " AND {0}.{1}={2}", FullTableName, EncapsulateName(GetColumnName(pi)), cmdName);
          int i = bo.GetReferenceValue(pi.Name);
          cmdSql.Parameters.AddWithValue(cmdName, i == 0 ? DBNull.Value : (object)i);
        }
        else
        {
          if (ov != null)
          {
            conditions += string.Format(conditions == null ? "{0}.{1}={2}" : " AND {0}.{1}={2}", FullTableName, EncapsulateName(GetColumnName(pi)), cmdName);
            cmdSql.Parameters.AddWithValue(cmdName, ov);
          }
          else
          {
            conditions += string.Format(conditions == null ? "{0}.{1} IS NULL" : " AND {0}.{1} IS NULL", FullTableName, EncapsulateName(GetColumnName(pi)));
          }
        }
      }

      if (OwnerColumn != null)
      {
        BusinessObject owner = (BusinessObject)bo.GetOriginalValue("Owner");
        if (owner == null)
        {
          conditions += string.Format(conditions == null ? "{0}.{1} IS NULL" : " AND {0}.{1} IS NULL", FullTableName, EncapsulateName(OwnerColumn));
        }
        else
        {
          string cmdName = GetParameterName(ref iCmd);
          conditions += string.Format(conditions == null ? "{0}.{1}={2}" : " AND {0}.{1}={2}", FullTableName, EncapsulateName(OwnerColumn), cmdName);
          cmdSql.Parameters.AddWithValue(cmdName, owner == null ? DBNull.Value : (object)owner.Id);
        }
      }

      if (!Global && !appDomainSpecified && SecurityContext.User != null && !typeof(IAssociativeObject).IsAssignableFrom(ObjectType))
      {
        IGlobalAware ga = bo as IGlobalAware;
        if (ga != null)
        {
          //if (!ga.IsGlobal)
          //{
            string cmdName = GetParameterName(ref iCmd);
            conditions += string.Format(conditions == null ? "({0}.{1}={2} OR {0}.{1} IS NULL)" : " AND ({0}.{1}={2} OR {0}.{1} IS NULL)", FullTableName, EncapsulateName(ApplicationDomainColumn), cmdName);
            cmdSql.Parameters.AddWithValue(cmdName, SecurityContext.User.ApplicationDomain);
          //}
          //else
          //{
          //  conditions += string.Format(conditions == null ? "{0}.{1} IS NULL" : " AND {0}.{1} IS NULL", FullTableName, EncapsulateName(ApplicationDomainColumn));
          //}
        }
      }

      if (ReferenceColumn != null && bo is IAssociativeObject)
      {
        string cmdName = GetParameterName(ref iCmd);
        conditions += string.Format(conditions == null ? "{0}.{1}={2}" : " AND {0}.{1}={2}", FullTableName, EncapsulateName(ReferenceColumn), cmdName);
        object o = bo.GetOriginalValue("ReferenceId");
        if (o == null) o = ((IAssociativeObject)bo).ReferenceId;
        cmdSql.Parameters.AddWithValue(cmdName, o);
      }
      
      return conditions;
    }

    #endregion

    #region DataView Fillers

    public override void FillDataView(DataView dv, Collection<DataFilterCondition> conditions)
    {
      foreach (IORMapper m in SubClassMappers)
      {
        m.FillDataView(dv, conditions);
      }
      if (typeof(T).IsAbstract) return;
      int rows = 0;
      int parameterCount = 1;

      using (SqlCommand cmd = (SqlCommand)DataManager.Instance.GetCommand())
      {
        string columns = GetReadColumns();
        string orderBy = GetOrderByClause();
        string join = GetJoin();
        string filters = GetFilterString(DataManager.Instance.GetDataFilterConditions<T>(dv.FilterKey, conditions), cmd, ref parameterCount);
        string sql = string.Format(filters == null ? "SELECT {0} FROM {1} {3}" : "SELECT {0} FROM {1} {3}WHERE {2}", columns, FullTableName, filters, join);
        sql = string.Format(orderBy == null ? "{0}" : "{0} {1}", sql, orderBy);
        cmd.CommandText = sql;
        if ((string.IsNullOrWhiteSpace(dv.FilterKey) || dv.FilterKey == DataFilters.CacheLoad) && (conditions == null || conditions.Count == 0))
          rows = dv.ExecuteCommand(typeof(T), IdColumn, cmd, true);
        else
          rows = dv.ExecuteCommand(typeof(T), IdColumn, cmd);
      }

      if (rows > 0) base.FillDataView(dv, conditions);
    }

    public override void FillDataViewById(DataView dv, int id, Collection<DataFilterCondition> conditions)
    {
      foreach (IORMapper m in SubClassMappers)
      {
        m.FillDataViewById(dv, id, conditions);
      }
      if (typeof(T).IsAbstract) return;
      int parameterCount = 1;
      int rows = 0;

      using (SqlCommand cmd = (SqlCommand)DataManager.Instance.GetCommand())
      {
        string columns = GetReadColumns();
        string idParamName = GetParameterName(ref parameterCount);
        cmd.Parameters.AddWithValue(idParamName, id);
        string join = GetJoin();
        string filters = GetFilterString(DataManager.Instance.GetDataFilterConditions<T>(dv.FilterKey, conditions), cmd, ref parameterCount);
        cmd.CommandText = string.Format(filters == null ? "SELECT {0} FROM {1} {5}WHERE {1}.{3}={4}" : "SELECT {0} FROM {1} {5}WHERE {1}.{3}={4} AND {2}", columns, FullTableName, filters, EncapsulateName(IdColumn), idParamName, join);
        rows = dv.ExecuteCommand(ObjectType, IdColumn, cmd);
      }

      if (rows > 0) base.FillDataViewById(dv, id, conditions);
    }

    public override void FillDataViewByReference(DataView dv, string referenceColumn, int? reference, Collection<DataFilterCondition> conditions)
    {
      foreach (IORMapper m in SubClassMappers)
      {
        m.FillDataViewByReference(dv, referenceColumn, reference, conditions);
      }
      if (typeof(T).IsAbstract) return;
      int parameterCount = 1;
      int rows = 0;

      using (SqlCommand cmd = (SqlCommand)DataManager.Instance.GetCommand())
      {
        string columns = GetReadColumns();
        string orderBy = GetOrderByClause();
        string join = GetJoin();
        string refParamName = GetParameterName(ref parameterCount);
        cmd.Parameters.AddWithValue(refParamName, reference.HasValue ? reference.Value : (object)DBNull.Value);
        string filters = GetFilterString(DataManager.Instance.GetDataFilterConditions<T>(dv.FilterKey, conditions), (SqlCommand)cmd, ref parameterCount);
        string sql = string.Format(filters == null ? "SELECT {0} FROM {1} {4}WHERE {3}" : "SELECT {0} FROM {1} {4}WHERE {2} AND {3}", columns, FullTableName, filters, string.Format(reference.HasValue ? "{0}={1}" : "{0} IS NULL", EncapsulateName(referenceColumn), refParamName), join);
        sql = string.Format(orderBy == null ? "{0}" : "{0} {1}", sql, orderBy);
        cmd.CommandText = sql;
        rows = dv.ExecuteCommand(typeof(T), IdColumn, cmd);
      }

      if (rows > 0) base.FillDataViewByReference(dv, referenceColumn, reference, conditions);
    }

    public override void FillDataViewByReferences(DataView dv, string referenceColumn, Collection<int> references, Collection<DataFilterCondition> conditions)
    {
      if (references.Count == 0) return;
      foreach (IORMapper m in SubClassMappers)
      {
        m.FillDataViewByReferences(dv, referenceColumn, references, conditions);
      }
      if (typeof(T).IsAbstract) return;
      int parameterCount = 1;
      int rows = 0;

      using (SqlCommand cmd = (SqlCommand)DataManager.Instance.GetCommand())
      {
        string columns = GetReadColumns();
        string orderBy = GetOrderByClause();
        string join = GetJoin();
        string filters = GetFilterString(DataManager.Instance.GetDataFilterConditions<T>(dv.FilterKey, conditions), (SqlCommand)cmd, ref parameterCount);
        string sql = null;
        if (OwnerType != ObjectType) //!IsCircularReference)
          sql = string.Format(filters == null ? "SELECT {0} FROM {1} {5}WHERE {3} in ({4})" : "SELECT {0} FROM {1} {5}WHERE {2} AND {3} in ({4})", columns, FullTableName, filters, EncapsulateName(referenceColumn), string.Join(",", references), join);
        else
        {
          sql = string.Format(filters == null ? "SELECT {0} FROM {1} {5}WHERE {3} in ({4}) AND {6} NOT IN ({4})" : "SELECT {0} FROM {1} {5}WHERE {2} AND {3} in ({4}) AND {6} NOT IN ({4})", columns, FullTableName, filters, EncapsulateName(referenceColumn), string.Join(",", references), join, EncapsulateName(IdColumn));
        }
        sql = string.Format(orderBy == null ? "{0}" : "{0} {1}", sql, orderBy);
        cmd.CommandText = sql;
        rows = dv.ExecuteCommand(typeof(T), IdColumn, cmd);
      }

      if (rows > 0) base.FillDataViewByReferences(dv, referenceColumn, references, conditions);
    }

    #endregion

    #region Persistance

    public override void Insert(BusinessObject obj)
    {
      int parameterCount = 1;
      using (SqlCommand cmd = (SqlCommand)DataManager.Instance.GetCommand())
      {
        string columns = obj.Id < 0 ? GetInsertColumns(obj, false) : GetInsertColumns(obj, true);
        string values = obj.Id < 0 ? GetInsertValues(cmd, obj, false, ref parameterCount) : GetInsertValues(cmd, obj, true, ref parameterCount);
        cmd.CommandText = obj.Id < 0 ? string.Format("INSERT INTO {0} ({1}) VALUES ({2}); SELECT CAST(@@IDENTITY AS INT)", FullTableName, columns, values) : string.Format("INSERT INTO {0} ({1}) VALUES ({2}); SELECT CAST({3} AS INT)", FullTableName, columns, values, obj.Id);

#if SQL
        Debug.WriteLine(cmd.CommandText);
        Debug.IndentLevel++;
        foreach (SqlParameter p in cmd.Parameters)
        {
          Debug.WriteLine("{0} -> {1}", p.ParameterName, p.Value != DBNull.Value ? p.Value : "{NULL}");
        }
        Debug.IndentLevel--;
#endif

        try
        {
          obj.Id = (int)cmd.ExecuteScalar();
          Debug.WriteLine("ID -> {0}", obj.Id);
        }
        catch (SqlException sqlEx)
        {
          switch (sqlEx.ErrorCode)
          {
            case 547:
              throw new ReferentialIntegrityViolationException(string.Format(CultureInfo.CurrentCulture, "Referential Integrity Violation inserting object of type {0} ({1})", ObjectType, obj.Id));

            case 2601:
              throw new UniqueIndexViolationException(string.Format(CultureInfo.CurrentCulture, "Unique Index Violation inserting object of type {0} ({1})", ObjectType, obj.Id));

            default:
              throw;
          }
        }
        catch
        {
          throw;
        }
      }
    }

    public override void Update(BusinessObject obj)
    {
      int parameterCount = 1;
      using (SqlCommand cmd = (SqlCommand)DataManager.Instance.GetCommand())
      {
        string columns = GetUpdateColumns(cmd, obj, ref parameterCount);
        string conditions = GetUpdateConditions(cmd, obj, ref parameterCount);
        cmd.CommandText = string.Format("UPDATE {0} SET {1} WHERE {2}", FullTableName, columns, conditions);

#if SQL
        Debug.WriteLine(cmd.CommandText);
        Debug.IndentLevel++;
        foreach (SqlParameter p in cmd.Parameters)
        {
          Debug.WriteLine("{0} -> {1}", p.ParameterName, p.Value != DBNull.Value ? p.Value : "{NULL}");
        }
        Debug.IndentLevel--;
#endif

        try
        {
          int rows = cmd.ExecuteNonQuery();          
          if (rows == 0) throw new OptimisticConcurrencyException(string.Format(CultureInfo.CurrentCulture, "Optimistic Concurrency Violation updating object of type {0} ({1})", ObjectType, obj.Id));
        }
        catch (SqlException sqlEx)
        {
          switch (sqlEx.ErrorCode)
          {
            case 547:
              throw new ReferentialIntegrityViolationException(string.Format(CultureInfo.CurrentCulture, "Referential Integrity Violation updating object of type {0} ({1})", ObjectType, obj.Id));

            case 2601:
              throw new UniqueIndexViolationException(string.Format(CultureInfo.CurrentCulture, "Unique Index Violation updating object of type {0} ({1})", ObjectType, obj.Id));

            default:
              throw;
          }
        }
        catch
        {
          throw;
        }
      }
    }

    public override void Delete(BusinessObject obj)
    {
      int parameterCount = 1;
      using (SqlCommand cmd = (SqlCommand)DataManager.Instance.GetCommand())
      {
        string paramName = GetParameterName(ref parameterCount);
        cmd.Parameters.AddWithValue(paramName, obj.Id);
        cmd.CommandText = string.Format("DELETE FROM {0} WHERE {1}={2}", FullTableName, EncapsulateName(IdColumn), paramName);

#if SQL
        Debug.WriteLine(cmd.CommandText);
        Debug.IndentLevel++;
        foreach (SqlParameter p in cmd.Parameters)
        {
          Debug.WriteLine("{0} -> {1}", p.ParameterName, p.Value != DBNull.Value ? p.Value : "{NULL}");
        }
        Debug.IndentLevel--;
#endif

        try
        {
          cmd.ExecuteNonQuery();
        }
        catch (SqlException sqlEx)
        {
          switch (sqlEx.ErrorCode)
          {
            case 547:
              throw new ReferentialIntegrityViolationException(string.Format(CultureInfo.CurrentCulture, "Referential Integrity Violation deleting object of type {0} ({1})", ObjectType, obj.Id));

            default:
              throw;
          }
        }
        catch
        {
          throw;
        }
      }
    }

    #endregion

    #region Searcing

    public override string GetSearchConditions(Collection<DataFilterCondition> filter, ref int iParam, System.Data.IDbCommand cmd)
    {
      SqlCommand cmd1 = (SqlCommand)cmd;
      string conditions = null;
      if (BaseMapper != null) conditions += BaseMapper.GetSearchConditions(filter, ref iParam, cmd);
      foreach (DataFilterCondition condition in filter.Where(f => f.ApplicableType.Equals(ObjectType)))
      {
        string paramName = GetParameterName(ref iParam);
        if (condition.FilterType == FilterType.IsNotNull || condition.FilterType == FilterType.IsNull)
          conditions += string.Format(string.IsNullOrWhiteSpace(conditions) ? "{0}.{1} {2}" : " AND {0}.{1} {2}", FullTableName, EncapsulateName(GetColumnName(condition.Column)), GetFilterType(condition.FilterType));
        else 
          conditions += string.Format(string.IsNullOrWhiteSpace(conditions) ? "{0}.{1} {2} {3}" : " AND {0}.{1} {2} {3}", FullTableName, EncapsulateName(GetColumnName(condition.Column)), GetFilterType(condition.FilterType), paramName);
        cmd1.Parameters.AddWithValue(paramName, condition.Filter);
      }
      foreach (IORMapper mapper in SearchJoins)
      {
        string joinedConditions = mapper.GetSearchConditions(filter, ref iParam, cmd);
        if (joinedConditions != null) conditions += string.Format(string.IsNullOrWhiteSpace(conditions) ? "{0}" : " AND {0}", joinedConditions);
      }
      return conditions;
    }

    public override System.Data.DataSet GetSearchDataSet(Collection<DataFilterCondition> filter)
    {
      int parameterCount = 1;
      using (SqlCommand cmd = (SqlCommand)DataManager.Instance.GetCommand())
      {
        string parentJoin = GetSearchParentJoin(this);
        string referenceJoins = GetSearchReferenceJoin(this);
        string searchColumns = GetSearchColumns();
        string conditions = GetSearchConditions(filter, ref parameterCount, cmd);
        string orderBy = GetSearchOrderBy();
        cmd.CommandText = string.Format("SELECT {1}.{5}, {0} FROM {1} {2} {3} {4} {6}", searchColumns, FullTableName, parentJoin, referenceJoins, string.Format(string.IsNullOrWhiteSpace(conditions) ? "" : " WHERE {0}", conditions), EncapsulateName(IdColumn), string.Format(string.IsNullOrWhiteSpace(orderBy) ? "" : "ORDER BY {0}", orderBy));

#if SQL
        Debug.WriteLine(cmd.CommandText);
        Debug.IndentLevel++;
        foreach (SqlParameter p in cmd.Parameters)
        {
          Debug.WriteLine("{0} -> {1}", p.ParameterName, p.Value != DBNull.Value ? p.Value : "{NULL}");
        }
        Debug.IndentLevel--;
#endif
        
        System.Data.DataSet ds = new System.Data.DataSet();
        ds.Locale = CultureInfo.InvariantCulture;
        using (System.Data.IDataReader r = cmd.ExecuteReader())
        {
          ds.Load(r, System.Data.LoadOption.OverwriteChanges, string.Empty);
        }

        return ds;
      }
    }

    #endregion
  }
}
