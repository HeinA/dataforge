﻿using Dataforge.PrismAvalonExtensions.Events;
using Dataforge.PrismAvalonExtensions.Properties;
using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using System.Text;
using System.Windows;
using System.Xml;
using System.Xml.Serialization;
using Xceed.Wpf.AvalonDock.Layout;

namespace Dataforge.PrismAvalonExtensions.Regions
{
  public class DockingManagerRegion : Region
  {
    public DockingManagerRegion()
    {
      EventAggregator.GetEvent<CloseAllDocumentsEvent>().Subscribe(OnCloseAllDocuments);
    }

    private void OnCloseAllDocuments(CloseAllDocumentsEventArgs args)
    {
      Queue<object> queue = new Queue<object>(Documents.Keys);
      while(queue.Count > 0)
      {
        FrameworkElement view = (FrameworkElement)queue.Dequeue();
        if (Documents.ContainsKey(view))
        {
          IClosingValidator cv = view.DataContext as IClosingValidator;
          if (cv != null)
          {
            if (cv.IsDirty)
            {
              if (cv.OnClosing())
              {
                Documents[view].Close();
              }
              else
              {
                args.Cancel = true;
                break;
              }
            }
          }
        }
      }
    }

    IEventAggregator _eventAggregator = null;
    public IEventAggregator EventAggregator
    {
      get
      {
        if (_eventAggregator == null) _eventAggregator = ServiceLocator.Current.GetInstance<IEventAggregator>();
        return _eventAggregator;
      }
    }

    public override IRegionManager Add(object view, string viewName, bool createRegionManagerScope)
    {
      DockingMetadata md = view as DockingMetadata;
      IRegionManager rm = ObtainAppropriateRegionManager(createRegionManagerScope, md.View);
      if (md != null)
      {
        DockingMetadataDictionary.Add(md.View, md);        
        //Instead of calling:
        //rm = base.Add(md.View, viewName, createRegionManagerScope);
        //I copied the Inner Add of Prism's Region in order to assign the scoped region if it exists already.
        this.InnerAdd(md.View, viewName, rm);
        DockingMetadataDictionary.Remove(md.View);
      }
      else
      {
        throw new InvalidOperationException("DockingMetadata expected.");
      }

      return rm;
    }

    private IRegionManager ObtainAppropriateRegionManager(bool createRegionManagerScope, object view)
    {
      IRegionManager formerRegionManager = null;
      DependencyObject viewAsDependencyObject = view as DependencyObject;
      if (viewAsDependencyObject != null)
      {
        formerRegionManager = Microsoft.Practices.Prism.Regions.RegionManager.GetRegionManager(viewAsDependencyObject);
      }

      if (formerRegionManager != null)
      {
        if (!createRegionManagerScope)
        {
          throw new InvalidOperationException("The view already has a its own scoped region manager. In order to add this view, set the createRegionManagerScope parameter to true");
        }
        else
        {
          return formerRegionManager;
        }
      }
      else
      {
        return createRegionManagerScope ? this.RegionManager.CreateRegionManager() : this.RegionManager;
      }
    }

    private void InnerAdd(object view, string viewName, IRegionManager scopedRegionManager)
    {
      if (this.ItemMetadataCollection.FirstOrDefault(x => x.Item == view) != null)
      {
        throw new InvalidOperationException(Resources.RegionViewExistsException);
      }

      ItemMetadata itemMetadata = new ItemMetadata(view);
      if (!string.IsNullOrEmpty(viewName))
      {
        if (this.ItemMetadataCollection.FirstOrDefault(x => x.Name == viewName) != null)
        {
          throw new InvalidOperationException(String.Format(CultureInfo.InvariantCulture, Resources.RegionViewNameExistsException, viewName));
        }
        itemMetadata.Name = viewName;
      }

      DependencyObject dependencyObject = view as DependencyObject;

      if (dependencyObject != null)
      {
        Microsoft.Practices.Prism.Regions.RegionManager.SetRegionManager(dependencyObject, scopedRegionManager);
      }

      this.ItemMetadataCollection.Add(itemMetadata);
    }

    Collection<object> _busyRemoving = new Collection<object>();
    public override void Remove(object view)
    {
      if (_busyRemoving.Contains(view)) return;
      try
      {
        _busyRemoving.Add(view);
        base.Remove(view);
      }
      finally
      {
        _busyRemoving.Remove(view);
      }
    }

    Dictionary<object, DockingMetadata> _dockStrategyDictionary = new Dictionary<object, DockingMetadata>();
    Dictionary<object, DockingMetadata> DockingMetadataDictionary
    {
      get { return _dockStrategyDictionary; }
    }

    public DockingMetadata GetDockingMetadata(object view)
    {
      if (!DockingMetadataDictionary.ContainsKey(view)) return null;
      return DockingMetadataDictionary[view];
    }

    Dictionary<object, LayoutDocument> _documents;
    Dictionary<object, LayoutDocument> Documents
    {
      get
      {
        if (_documents == null) _documents = new Dictionary<object, LayoutDocument>();
        return _documents;
      }
    }

    public void RegisterCloseAction(object view, LayoutDocument document)
    {
      Documents.Add(view, document);
    }

    public void RemoveCloseAction(object view)
    {
      Documents.Remove(view);
    }
  }
}
