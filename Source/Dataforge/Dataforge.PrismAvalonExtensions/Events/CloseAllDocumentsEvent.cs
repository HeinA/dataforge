﻿using Microsoft.Practices.Prism.Events;
using Microsoft.Practices.Prism.PubSubEvents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dataforge.PrismAvalonExtensions.Events
{
  public class CloseAllDocumentsEvent : PubSubEvent<CloseAllDocumentsEventArgs>
  {
  }
}
