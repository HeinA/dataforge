﻿using Dataforge.Prism.Contants;
using Fluent;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Dataforge.RibbonMdiLayout
{
  /// <summary>
  /// Interaction logic for RibbonMdiLayout.xaml
  /// </summary>
  [Export(ContractNames.ShellWindow, typeof(Window))]
  public partial class RibbonMdiLayout : RibbonWindow
  {
    public RibbonMdiLayout()
    {
      InitializeComponent();
    }
  }
}
