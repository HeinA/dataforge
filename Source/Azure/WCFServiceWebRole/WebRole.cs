using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
using System.Reflection;
using System.Web;
using System.IO;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Dataforge.Business;

namespace WCFServiceWebRole
{
  public class WebRole : RoleEntryPoint
  {
    public override bool OnStart()
    {
      IConfigurationSource config = ConfigurationSourceFactory.Create();
      DatabaseFactory.SetDatabaseProviderFactory(new DatabaseProviderFactory(config));
      Logger.SetLogWriter(new LogWriterFactory(config).Create());
      ExceptionPolicyFactory factory = new ExceptionPolicyFactory(config);
      ExceptionManager exManager = factory.CreateManager(); ExceptionPolicy.SetExceptionManager(exManager);

      Cache.Instance.LoadCache(Cache.LoadCache());

      return base.OnStart();
    }
  }
}
