﻿using Dataforge.Business.Collections;
using Dataforge.Business.Documents;
using Dataforge.Business.Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WCFServiceWebRole
{
  [ServiceContract]
  //[ServiceKnownType("GetKnownTypes", typeof(KnownTypesProvider))]
  [BusinessTypes]
  public interface IDataforgeService
  {

    [OperationContract]
    Collection<Document> GetDocuments(int documentType);
  }
}
