﻿using Dataforge.Business;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace WebHost
{
  public class Global : System.Web.HttpApplication
  {

    protected void Application_Start(object sender, EventArgs e)
    {
      IConfigurationSource config = ConfigurationSourceFactory.Create();
      DatabaseFactory.SetDatabaseProviderFactory(new DatabaseProviderFactory(config));
      Logger.SetLogWriter(new LogWriterFactory(config).Create());
      ExceptionPolicyFactory factory = new ExceptionPolicyFactory(config);
      ExceptionManager exManager = factory.CreateManager(); ExceptionPolicy.SetExceptionManager(exManager);

      Cache.Instance.LoadCache(Cache.LoadCache());
    }

    protected void Session_Start(object sender, EventArgs e)
    {

    }

    protected void Application_BeginRequest(object sender, EventArgs e)
    {

    }

    protected void Application_AuthenticateRequest(object sender, EventArgs e)
    {

    }

    protected void Application_Error(object sender, EventArgs e)
    {

    }

    protected void Session_End(object sender, EventArgs e)
    {

    }

    protected void Application_End(object sender, EventArgs e)
    {

    }
  }
}