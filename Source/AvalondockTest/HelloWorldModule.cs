﻿using AvalondockTest.Views;
using Dataforge.PrismAvalonExtensions.DockStrategies;
using Microsoft.Practices.Prism.MefExtensions.Modularity;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows;

namespace AvalondockTest
{
  [ModuleExport(typeof(HelloWorldModule))]
  public class HelloWorldModule : IModule
  {
    [ImportingConstructor]
    public HelloWorldModule(IRegionViewRegistry registry)
    {
      _regionViewRegistry = registry;
    }

    readonly IRegionViewRegistry _regionViewRegistry;
    public IRegionViewRegistry RegionViewRegistry
    {
      get { return _regionViewRegistry; }
    } 

    [Import]
    DocumentView _documentView = null;
    public DocumentView DocumentView
    {
      get { return _documentView; }
    }

    [Import]
    AnotherToolView _anotherToolView = null;
    public AnotherToolView AnotherToolView
    {
      get { return _anotherToolView; }
    }

    [Import]
    ToolView _toolView = null;
    public ToolView ToolView
    {
      get { return _toolView; }
    }

    public void Initialize()
    {
      RegionViewRegistry.RegisterViewWithRegion("DockingRegion", () =>
      {
        DocumentDockStrategy dds = new DocumentDockStrategy(DocumentView, "Document 1");
        return dds;
      });

      RegionViewRegistry.RegisterViewWithRegion("DockingRegion", () =>
      {
        SideDockStrategy sds = new SideDockStrategy(ToolView, "Some Tool", DockSide.Left, new GridLength(200));
        return sds;
      });

      RegionViewRegistry.RegisterViewWithRegion("DockingRegion", () =>
      {
        NestedDockStrategy nds = new NestedDockStrategy(AnotherToolView, ToolView, "Another Tool", NestedDockPosition.Inside, new GridLength(200));
        return nds;
      });
    }
  }
}
