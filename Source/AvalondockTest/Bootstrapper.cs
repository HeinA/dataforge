﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;
using System.Windows.Controls;
using Microsoft.Practices.Prism.MefExtensions;
using Xceed.Wpf.AvalonDock.Layout;
using Xceed.Wpf.AvalonDock;
using Microsoft.Practices.Prism.MefExtensions.Regions;
using System.ComponentModel.Composition.Hosting;
using Dataforge.PrismAvalonExtensions.Regions;

namespace AvalondockTest
{
  class Bootstrapper : MefBootstrapper
  {
    protected override DependencyObject CreateShell()
    {
      return this.Container.GetExportedValue<MainWindow>();
    }

    protected override void InitializeShell()
    {
      base.InitializeShell();
      App.Current.MainWindow = (Window)this.Shell;
      App.Current.MainWindow.Show();
    }

    RegionAdapterMappings _mappings;
    RegionAdapterMappings Mappings
    {
      get { return _mappings; }
      set { _mappings = value; }
    }

    protected override RegionAdapterMappings ConfigureRegionAdapterMappings()
    {
      Mappings = base.ConfigureRegionAdapterMappings();
      Mappings.RegisterMapping(typeof(DockingManager), new DockingManagerRegionAdapter(ServiceLocator.Current.GetInstance<IRegionBehaviorFactory>()));
      return Mappings;
    }

    protected override void ConfigureModuleCatalog()
    {
      base.ConfigureModuleCatalog();
      ModuleCatalog moduleCatalog = (ModuleCatalog)this.ModuleCatalog;
      moduleCatalog.AddModule(typeof(HelloWorldModule));
    }

    protected override void ConfigureAggregateCatalog()
    {
      base.ConfigureAggregateCatalog();
      this.AggregateCatalog.Catalogs.Add(new AssemblyCatalog(typeof(Bootstrapper).Assembly));
    }

    protected override IRegionBehaviorFactory ConfigureDefaultRegionBehaviors()
    {
      IRegionBehaviorFactory factory = base.ConfigureDefaultRegionBehaviors();
      return factory;
    }
  }
}