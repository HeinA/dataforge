﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AvalondockTest.Views
{
  /// <summary>
  /// Interaction logic for AnotherToolView.xaml
  /// </summary>
  [Export(typeof(AnotherToolView))]
  [PartCreationPolicy(CreationPolicy.Shared)]
  public partial class AnotherToolView : UserControl
  {
    public AnotherToolView()
    {
      InitializeComponent();
    }
  }
}
