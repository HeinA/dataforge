<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <configSections>
    <section name="loggingConfiguration" type="Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.LoggingSettings, Microsoft.Practices.EnterpriseLibrary.Logging" requirePermission="true"/>
    <section name="exceptionHandling" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Configuration.ExceptionHandlingSettings, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling" requirePermission="true"/>
  </configSections>

  <loggingConfiguration name="Logging Application Block" tracingEnabled="true" defaultCategory="Dataforge Service Host" logWarningsWhenNoCategoriesMatch="true">
    <listeners>
      <add name="Database Listener" type="Microsoft.Practices.EnterpriseLibrary.Logging.Database.FormattedDatabaseTraceListener, Microsoft.Practices.EnterpriseLibrary.Logging.Database" listenerDataType="Microsoft.Practices.EnterpriseLibrary.Logging.Database.Configuration.FormattedDatabaseTraceListenerData, Microsoft.Practices.EnterpriseLibrary.Logging.Database" databaseInstanceName="Default" writeLogStoredProcName="WriteLog" addCategoryStoredProcName="AddCategory" formatter="Text Formatter" traceOutputOptions="LogicalOperationStack, DateTime, Timestamp, ProcessId, ThreadId, Callstack"/>
    </listeners>

    <formatters>
      <add type="Microsoft.Practices.EnterpriseLibrary.Logging.Formatters.TextFormatter, Microsoft.Practices.EnterpriseLibrary.Logging" template="Timestamp: {timestamp}{newline}
Message: {message}{newline}
Category: {category}{newline}
Priority: {priority}{newline}
EventId: {eventid}{newline}
Severity: {severity}{newline}
Title:{title}{newline}
Machine: {localMachine}{newline}
App Domain: {localAppDomain}{newline}
ProcessId: {localProcessId}{newline}
Process Name: {localProcessName}{newline}
Thread Name: {threadName}{newline}
Win32 ThreadId:{win32ThreadId}{newline}
Extended Properties: {dictionary({key} - {value}{newline})}" name="Text Formatter"/>
    </formatters>

    <categorySources>
      <add switchValue="All" name="Dataforge Service Host">
        <listeners>
          <add name="Database Listener"/>
        </listeners>
      </add>
    </categorySources>

    <specialSources>
      <allEvents switchValue="All" name="All Events">
        <listeners>
          <add name="Database Listener"/>
        </listeners>
      </allEvents>
      <notProcessed switchValue="All" name="Unprocessed Category">
        <listeners>
          <add name="Database Listener"/>
        </listeners>
      </notProcessed>
      <errors switchValue="All" name="Logging Errors &amp; Warnings">
        <listeners>
          <add name="Database Listener"/>
        </listeners>
      </errors>
    </specialSources>
  </loggingConfiguration>

  <exceptionHandling>
    <exceptionPolicies>
      <add name="WCF Exception Shielding">
        <exceptionTypes>

          <add name="Exception" type="System.Exception, mscorlib" postHandlingAction="None">
            <exceptionHandlers>
              <add name="LoggingExceptionHandler" type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging.LoggingExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging" logCategory="Dataforge Service Host" eventId="100" severity="Error" title="Unhandled Exception" formatterType="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.TextExceptionFormatter, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling" priority="0"/>
            </exceptionHandlers>
          </add>

          <add name="SessionExpiredException" type="Dataforge.Business.Service.SessionExpiredException, Dataforge.Business" postHandlingAction="ThrowNewException">
            <exceptionHandlers>
              <add type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF.FaultContractExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF" faultContractType="Dataforge.Business.Service.SessionExpiredFault, Dataforge.Business" name="SessionExpiredFault"/>
            </exceptionHandlers>
          </add>

          <add name="AuthorizationException" type="Dataforge.Business.Security.AuthorizationException, Dataforge.Business" postHandlingAction="ThrowNewException">
            <exceptionHandlers>
              <add type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF.FaultContractExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF" faultContractType="Dataforge.Business.Security.AuthorizationFault, Dataforge.Business" name="AuthorizationFault"/>
            </exceptionHandlers>
          </add>

          <add name="OptimisticConcurrencyException" type="Dataforge.Business.Data.OptimisticConcurrencyException, Dataforge.Business" postHandlingAction="ThrowNewException">
            <exceptionHandlers>
              <add type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF.FaultContractExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF" faultContractType="Dataforge.Business.Data.OptimisticConcurrencyFault, Dataforge.Business" name="OptimisticConcurrencyFault"/>
            </exceptionHandlers>
          </add>

          <add name="ReferentialIntegrityViolationException" type="Dataforge.Business.Data.ReferentialIntegrityViolationException, Dataforge.Business" postHandlingAction="ThrowNewException">
            <exceptionHandlers>
              <add type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF.FaultContractExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF" faultContractType="Dataforge.Business.Data.ReferentialIntegrityViolationFault, Dataforge.Business" name="ReferentialIntegrityViolationFault"/>
            </exceptionHandlers>
          </add>

          <add name="UniqueIndexViolationException" type="Dataforge.Business.Data.UniqueIndexViolationException, Dataforge.Business" postHandlingAction="ThrowNewException">
            <exceptionHandlers>
              <add type="Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF.FaultContractExceptionHandler, Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF" faultContractType="Dataforge.Business.Data.UniqueIndexViolationFault, Dataforge.Business" name="UniqueIndexViolationFault"/>
            </exceptionHandlers>
          </add>

        </exceptionTypes>
      </add>
    </exceptionPolicies>
  </exceptionHandling>

  <system.diagnostics>
    <sources>
      <source name="System.ServiceModel" switchValue="Error, Warning" propagateActivity="true">
        <listeners>
          <add name="traceListener" type="System.Diagnostics.XmlWriterTraceListener" initializeData="c:\Logs\Dataforge.svclog"/>
          <!--Ensure Network service may write/modify there-->
        </listeners>
      </source>
    </sources>
  </system.diagnostics>

  
  
  
  <connectionStrings>
    <add name="Default" connectionString="data source=dred-dv7;Initial Catalog=Dataforge;User ID=sa;Password=dred1234" providerName="System.Data.SqlClient"/>
  </connectionStrings>

  <system.serviceModel>
    <diagnostics>
      <messageLogging logEntireMessage="true" logMalformedMessages="true" logMessagesAtServiceLevel="true" logMessagesAtTransportLevel="true"/>
    </diagnostics>

    <bindings>
      <basicHttpBinding>
        <binding maxReceivedMessageSize="204800">
          <security mode="None" />
        </binding>
      </basicHttpBinding>
      <netTcpBinding>
        <binding maxReceivedMessageSize="204800">
          <security mode="None" />
        </binding>
      </netTcpBinding>
    </bindings>

    <extensions>
      <behaviorExtensions>
        <add name="dataforgeEndpointBehaviorExtension" type="Dataforge.Business.Service.Wcf.DataforgeEndpointBehaviourElement, Dataforge.Business"/>
        <add name="dataforgeServiceBehaviorExtension" type="Dataforge.Business.Service.Wcf.DataforgeServiceBehaviourElement, Dataforge.Business"/>
      </behaviorExtensions>
    </extensions>

    <behaviors>
      <serviceBehaviors>
        <behavior>
          <!--<serviceMetadata httpGetEnabled="true"/>-->
          <serviceDebug includeExceptionDetailInFaults="false"/>
          <dataforgeServiceBehaviorExtension/>
        </behavior>
      </serviceBehaviors>

      <endpointBehaviors>
        <behavior>
          <dataforgeEndpointBehaviorExtension/>
        </behavior>
      </endpointBehaviors>
    </behaviors>

    <services>
      <service name="Dataforge.Business.Service.DataforgeService">
        <host>
          <baseAddresses>
            <add baseAddress="http://localhost:81/DataforgeService.svc"/>
          </baseAddresses>
        </host>

        <endpoint address="" binding="basicHttpBinding" contract="Dataforge.Business.Service.IDataforgeService">
        </endpoint>
      </service>

      <service name="Test.Business.Service.TestService">
        <host>
          <baseAddresses>
            <add baseAddress="http://localhost:81/TestService.svc"/>
          </baseAddresses>
        </host>

        <endpoint address="" binding="basicHttpBinding" contract="Test.Business.Service.ITestService">
        </endpoint>      
      </service>


      <service name="TestHost.Program">
        <host>
          <baseAddresses>
            <add baseAddress="http://localhost:81/TestHost.svc"/>
          </baseAddresses>
        </host>

        <endpoint address="" binding="basicHttpBinding" contract="Dataforge.Business.Service.IServiceHost">
        </endpoint>
      </service>
    </services>


    <!--<services>
      <service name="Dataforge.Business.Service.DataforgeService">
        <host>
          <baseAddresses>
            <add baseAddress="net.tcp://localhost:81/DataforgeService.svc"/>
          </baseAddresses>
        </host>

        <endpoint address="" binding="netTcpBinding" contract="Dataforge.Business.Service.IDataforgeService">
        </endpoint>
      </service>

      <service name="Test.Business.Service.TestService">
        <host>
          <baseAddresses>
            <add baseAddress="net.tcp://localhost:81/TestService.svc"/>
          </baseAddresses>
        </host>

        <endpoint address="" binding="netTcpBinding" contract="Test.Business.Service.ITestService">
        </endpoint>      
      </service>


      <service name="TestHost.Program">
        <host>
          <baseAddresses>
            <add baseAddress="net.tcp://localhost:81/TestHost.svc"/>
          </baseAddresses>
        </host>

        <endpoint address="" binding="netTcpBinding" contract="Dataforge.Business.Service.IServiceHost">
        </endpoint>
      </service>
    </services>-->

    <serviceHostingEnvironment multipleSiteBindingsEnabled="true"/>
  </system.serviceModel>

<startup><supportedRuntime version="v4.0" sku=".NETFramework,Version=v4.5.1"/></startup></configuration>
