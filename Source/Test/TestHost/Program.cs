﻿using Dataforge.Business;
using Dataforge.Business.Service;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Test.Business.Service;

namespace TestHost
{
  [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
  public class Program : IServiceHost
  {
    static Program _instance;
    bool _run = true;
    const Int32 SW_MINIMIZE = 6;

    [DllImport("Kernel32.dll", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
    private static extern IntPtr GetConsoleWindow();

    [DllImport("User32.dll", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool ShowWindow([In] IntPtr hWnd, [In] Int32 nCmdShow);

    private static void MinimizeConsoleWindow()
    {
      IntPtr hWndConsole = GetConsoleWindow();
      ShowWindow(hWndConsole, SW_MINIMIZE);
    }

    Program()
    {
      MinimizeConsoleWindow();

      IConfigurationSource config = ConfigurationSourceFactory.Create();
      DatabaseFactory.SetDatabaseProviderFactory(new DatabaseProviderFactory(config));
      Logger.SetLogWriter(new LogWriterFactory(config).Create());
      ExceptionPolicyFactory factory = new ExceptionPolicyFactory(config);
      ExceptionManager exManager = factory.CreateManager(); ExceptionPolicy.SetExceptionManager(exManager);

      Cache.Instance.LoadCache(Cache.LoadCache());

      using (ServiceHost thisHost = new ServiceHost(this))
      using (ServiceHost dataforgeHost = new ServiceHost(typeof(DataforgeService)))
      using (ServiceHost testHost = new ServiceHost(typeof(TestService)))
      {
        thisHost.Open();
        dataforgeHost.Open();
        testHost.Open();

        Console.WriteLine("The service is ready.");
        while (_run) ;

        dataforgeHost.Close();
        testHost.Close();
        thisHost.Close();
      }
    }

    static void Main(string[] args)
    {
      _instance = new Program();
    }

    public void Terminate()
    {
      _run = false;
    }
  }
}
