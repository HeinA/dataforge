﻿#if DEBUG

using Dataforge.Prism.Debugging;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApplication
{
  [Export(typeof(DebugCredentials))]
  class HeinA : DebugCredentials
  {
    public override string Username
    {
      get { return "HeinA"; }
    }

    public override string Password
    {
      get { return "dred1234"; }
    }
  }
}

#endif
