﻿using Dataforge.Business.Service;
using Dataforge.Prism;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace TestApplication
{
  /// <summary>
  /// Interaction logic for App.xaml
  /// </summary>
  public partial class App : Application
  {
    protected override void OnStartup(StartupEventArgs e)
    {
      base.OnStartup(e);
      DataforgeMefBootstrapper bootstrapper = new DataforgeMefBootstrapper();
      bootstrapper.Run();
    }

#if DEBUG
    protected override void OnExit(ExitEventArgs e)
    {
      using (var svc = ServiceFactory.GetService<IServiceHost>())
      {
        svc.ExecuteAction(i => i.Terminate());
      }
      base.OnExit(e);
    }
#endif
  }
}
