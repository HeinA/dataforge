﻿using Dataforge.Business;
using Dataforge.Business.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Test.Business.Constants;

namespace Test.Business
{
  [Table(Schema = "Test", OwnerColumn = "PurchaseOrderId", OwnerType = typeof(PurchaseOrder))]
  [DataContract(IsReference = true, Namespace = Namespaces.Test)]
  public class PurchaseOrderDetail : BusinessObject
  {
    #region DataMember string Product

    public const string PN_PRODUCT = "Product";
    [DataMember(Name = PN_PRODUCT, EmitDefaultValue = false)]
    string _product;
    [Column]
    [Search]
    public string Product
    {
      get { return _product; }
      set { SetValue<string>(ref _product, value, PN_PRODUCT); }
    }

    #endregion
  }
}
