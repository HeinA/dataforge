﻿using Dataforge.Business;
using Dataforge.Business.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Test.Business.Constants;

namespace Test.Business
{
  [Table(Schema = "Test")]
  [DataContract(IsReference = true, Namespace = Namespaces.Test)]
  public class Test : BusinessObject
  {
    #region DataMember string Text

    public const string PN_TEXT = "Text";
    [DataMember(Name = PN_TEXT, EmitDefaultValue = false)]
    string _text;
    [Column]
    public string Text
    {
      get { return _text; }
      set { SetValue<string>(ref _text, value, PN_TEXT); }
    }

    #endregion
  }
}
