﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Business.Constants
{
  public class Namespaces : Dataforge.Business.Constants.Namespaces
  {
    public const string Test = "http://dataforgeconsulting.com/Test";
  }
}
