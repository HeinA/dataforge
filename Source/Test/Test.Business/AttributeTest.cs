﻿using Dataforge.Business;
using Dataforge.Business.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Test.Business.Constants;

namespace Test.Business
{
  [Table(Schema = "Test", OwnerColumn = "PurchaseOrderId", OwnerType = typeof(PurchaseOrder))]
  [DataContract(IsReference = true, Namespace = Namespaces.Test)]
  public class AttributeTest : ExtensionObject
  {
    #region DataMember string PayLoad

    public const string PN_PAYLOAD = "PayLoad";
    [DataMember(Name = PN_PAYLOAD, EmitDefaultValue = false)]
    string _payLoad;
    [Column]
    public string PayLoad
    {
      get { return _payLoad; }
      set { SetValue<string>(ref _payLoad, value, PN_PAYLOAD); }
    }

    #endregion
  }
}
