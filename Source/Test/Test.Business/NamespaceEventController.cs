﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Business.Constants;

namespace Test.Business
{
  [Export(typeof(Dataforge.Business.NamespaceEventController))]
  public class NamespaceEventController : Dataforge.Business.NamespaceEventController
  {
    public override void AppendKnownNamespaces(Collection<string> namespaces)
    {
      namespaces.Add(Namespaces.Test);
    }
  }
}
