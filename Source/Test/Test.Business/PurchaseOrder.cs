﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Collections;
using Dataforge.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Test.Business.Constants;

namespace Test.Business
{
  [Table(Schema = "Test")]
  [DataContract(IsReference = true, Namespace = Namespaces.Test)]
  public class PurchaseOrder : Document
  {
    public override int DocumentTypeId
    {
      get { return 10001; }
    }

    #region DataMember string Supplier

    public const string PN_SUPPLIER = "Supplier";
    [DataMember(Name = PN_SUPPLIER, EmitDefaultValue = false)]
    string _supplier;
    [Search]
    [Column]
    public string Supplier
    {
      get { return _supplier; }
      set { SetValue<string>(ref _supplier, value, PN_SUPPLIER); }
    }

    #endregion

    #region DataMember BusinessObjectCollection<PurchaseOrderDetail> Details

    [DataMember(Name = "Details")]
    BusinessObjectCollection<PurchaseOrderDetail> _details;
    [Search]
    public BusinessObjectCollection<PurchaseOrderDetail> Details
    {
      get
      {
        if (_details == null) _details = new BusinessObjectCollection<PurchaseOrderDetail>(this);
        return _details;
      }
    }

    #endregion

    #region DataMember AssociativeObjectCollection<AssociativeTest, Test> Tests

    AssociativeObjectCollection<AssociativeTest, Test> _tests;
    public AssociativeObjectCollection<AssociativeTest, Test> Tests
    {
      get
      {
        if (_tests == null) _tests = new AssociativeObjectCollection<AssociativeTest, Test>(this);
        return _tests;
      }
    }

    #endregion
  }
}
