﻿using Dataforge.Business;
using Dataforge.Business.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Test.Business.Constants;

namespace Test.Business
{
  [Table(Schema = "Test", OwnerColumn = "TableId", OwnerType = typeof(TestTable), RowColumn = "TestRowId", ColumnColumn = "TestColumnId")]
  [DataContract(IsReference = true, Namespace = Namespaces.Test)]
  public class TestCell : TableCell<TestColumn, TestRow>
  {
    #region DataMember string Name

    public const string PN_NAME = "Name";
    [DataMember(Name = PN_NAME, EmitDefaultValue = false)]
    string _name;
    [Column]
    public string Name
    {
      get { return _name; }
      set { SetValue<string>(ref _name, value, PN_NAME); }
    }

    #endregion
  }
}
