﻿using Dataforge.Business.Data;
using System;
using System.Collections.Generic;
using System.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Business.Data
{
  [Export(typeof(StorageEventController<PurchaseOrder>))]
  public class PurchaseOrderDataEventController : StorageEventController<PurchaseOrder>
  {
    public override void OnAfterLoad(PurchaseOrder obj, string filterKey)
    {
    }

    public override bool OnBeforeSave(PurchaseOrder obj)
    {
      return true;
    }

    public override void OnAfterSave(PurchaseOrder obj)
    {
    }

    public override bool OnBeforeDelete(PurchaseOrder obj)
    {
      return true;
    }
  }
}
