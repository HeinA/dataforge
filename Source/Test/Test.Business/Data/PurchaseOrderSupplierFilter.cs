﻿using Dataforge.Business.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.Business.Constants;

namespace Test.Business.Data
{
  [Export(DataFilters.PurchaseOrdersSupplierFilter, typeof(DataFilter<PurchaseOrder>))]
  public class PurchaseOrderSupplierFilter : DataFilter<PurchaseOrder>
  {
    public override void GetConditions(Collection<DataFilterCondition> conditions)
    {
      conditions.Add(new DataFilterCondition(typeof(PurchaseOrder), "Supplier", FilterType.Like, "Hein%"));
    }
  }
}
