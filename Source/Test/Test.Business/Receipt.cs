﻿using Dataforge.Business.Attributes;
using Dataforge.Business.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Test.Business.Constants;

namespace Test.Business
{
  [Table(Schema = "Test")]
  [DataContract(IsReference = true, Namespace = Namespaces.Test)]
  public class Receipt : Document
  {
    public override int DocumentTypeId
    {
      get { return 10002; }
    }

    #region DataMember string Supplier

    public const string PN_SUPPLIER = "Supplier";
    [DataMember(Name = PN_SUPPLIER, EmitDefaultValue = false)]
    string _supplier;
    [Column]
    public string Supplier
    {
      get { return _supplier; }
      set { SetValue<string>(ref _supplier, value, PN_SUPPLIER); }
    }

    #endregion
  }
}
