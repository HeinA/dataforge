﻿using Dataforge.Business;
using Dataforge.Business.Data;
using Dataforge.Business.Security;
using Dataforge.Business.Service;
using Dataforge.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Test.Business.Constants;

namespace Test.Business.Service
{
  [ServiceBehavior(Namespace = Namespaces.Test)]
  [ExceptionShielding]
  public class TestService : ITestService
  {
    public Collection<TestTable> LoadTestTables()
    {
      Stopwatch sw = new Stopwatch();
      sw.Start();

      using (new ConnectionScope())
      {
        Collection<TestTable> col = DataManager.Instance.GetObjectStore<TestTable>().Load();
        sw.Stop();
        Debug.WriteLine("{0:#,##0}", sw.ElapsedMilliseconds);

        return col;
      }
    }

    public Collection<TestTable> SaveTestTables(Collection<TestTable> col)
    {
      using (new ConnectionScope())
      using(TransactionScope scope = new TransactionScope(TransactionScopeOption.Required))
      {
        IObjectStore<TestTable> store = DataManager.Instance.GetObjectStore<TestTable>();
        store.Save(col);
        scope.Complete();
        return store.Load();
      }
    }

    public Collection<Receipt> LoadReceipts()
    {
      using (new ConnectionScope())
      {
        return DataManager.Instance.GetObjectStore<Receipt>().Load();
      }
    }

    public SearchResults SearchPurchaseOrders(Collection<DataFilterCondition> filter)
    {
      using (new ConnectionScope())
      {
        IORMapper mapper = (IORMapper)DataManager.Instance.GetObjectStore<PurchaseOrder>();
        return mapper.Search(filter);
      }
    }
  }
}
