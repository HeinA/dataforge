﻿using Dataforge.Business;
using Dataforge.Business.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Test.Business.Constants;

namespace Test.Business.Service
{
  [ServiceContract(Namespace = Namespaces.Test)]
  public interface ITestService
  {
    [OperationContract]
    Collection<TestTable> LoadTestTables();

    [OperationContract]
    Collection<TestTable> SaveTestTables(Collection<TestTable> col);

    [OperationContract]
    Collection<Receipt> LoadReceipts();

    [OperationContract]
    SearchResults SearchPurchaseOrders(Collection<DataFilterCondition> filter);
  }
}
