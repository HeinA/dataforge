﻿using Dataforge.Business;
using Dataforge.Business.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Test.Business.Constants;

namespace Test.Business
{
  [Table(Schema = "Test")]
  [DataContract(IsReference = true, Namespace = Namespaces.Test)]
  public class TestTable : TableObject<TestColumn, TestRow, TestCell>
  {
    #region DataMember string Name

    public const string PN_NAME = "Name";
    [DataMember(Name = PN_NAME, EmitDefaultValue = false)]
    string _name;
    [Column]
    public string Name
    {
      get { return _name; }
      set { SetValue<string>(ref _name, value, PN_NAME); }
    }

    #endregion

    protected override Func<TestColumn, object> ColumnSorter
    {
      get { return o => o.Name; }
    }

    protected override Func<TestRow, object> RowSorter
    {
      get { return o => o.Name; }
    }
  }
}
