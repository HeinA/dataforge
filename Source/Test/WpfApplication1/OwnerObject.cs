﻿using Dataforge.Business;
using Dataforge.Business.Attributes;
using Dataforge.Business.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace WpfApplication1
{
  [Table(Schema="dfc")]
  public class OwnerObject : BusinessObject
  {
    string _name;
    [Column(SortOrder = 1)]
    public string Name
    {
      get { return _name; }
      set { SetValue<string>(ref _name, value, "Name"); }
    }

    string _lastName;
    [Column(SortOrder = 0)]
    public string LastName
    {
      get { return _lastName; }
      set { SetValue<string>(ref _lastName, value, "LastName"); }
    }

    BusinessObjectCollection<ChildObject> _children;
    public BusinessObjectCollection<ChildObject> Children
    {
      get
      {
        if (_children == null) _children = new BusinessObjectCollection<ChildObject>(this);
        return _children;
      }
    }

    Brush _nodeColor = new SolidColorBrush(SystemColors.WindowColor);
    public Brush NodeColor
    {
      get { return _nodeColor; }
      private set { SetValue<Brush>(ref _nodeColor, value, "NodeColor"); }
    }

    protected override void OnErrorChanged()
    {
      if (string.IsNullOrWhiteSpace(Error))
      {
        NodeColor = new SolidColorBrush(SystemColors.WindowColor);
      }
      else
      {
        NodeColor = new SolidColorBrush(Colors.Red);
      }
      base.OnErrorChanged();
    }
  }
}
