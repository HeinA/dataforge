﻿using Dataforge.Business;
using Dataforge.Business.Common;
using Dataforge.Business.Data;
using Dataforge.Business.Documents;
using Dataforge.Business.Security;
using Dataforge.Business.Service;
using Dataforge.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Test.Business;
using Test.Business.Constants;
using Test.Business.Service;

namespace WpfApplication1
{
  /// <summary>
  /// Interaction logic for MainWindow.xaml
  /// </summary>
  public partial class MainWindow : Window
  {
    public MainWindow()
    {
      InitializeComponent();

      try
      {

        using (var svc = ServiceFactory.GetService<IDataforgeService>())
        {
          svc.ExecuteAction(instance => Cache.Instance.LoadCache(instance.LoadCache()));

          AuthenticationResult ar = svc.ExecuteFunction<AuthenticationResult>(i => i.Login("HeinA", "1234"));
          SecurityContext.AddAuthentication(ar);

          Role r = Cache.Instance.GetObject<Role>(10001);
          SecurityContext.User.Roles.RemoveAt(0);
          SecurityContext.User.Roles.Add(r);

          Collection<User> users = new Collection<User>();
          users.Add(SecurityContext.User);
          users = svc.ExecuteFunction<Collection<User>>(i => i.SaveUsers(users));

          //Collection<Document> documents = svc.ExecuteFunction<Collection<Document>>(i => i.LoadDocuments(10001));
          //documents[0].DocumentNumber = Guid.NewGuid().ToString();
          //svc.Instance.SaveDocument(documents[0]);

        }

        using (var svc = ServiceFactory.GetService<ITestService>())
        {
          Collection<DataFilterCondition> conds = new Collection<DataFilterCondition>();

          SearchResults r = svc.ExecuteFunction<SearchResults>(instance => instance.SearchPurchaseOrders(conds));

          conds.Add(new DataFilterCondition(typeof(PurchaseOrderDetail), PurchaseOrderDetail.PN_PRODUCT, FilterType.Like, "%wood%"));
          SearchResults r1 = null;
          svc.ExecuteAction(instance => r1 = instance.SearchPurchaseOrders(conds));

          //svc.Instance.LoadTestTables();
        }
      }
      catch
      {
      }
    }
  }
}
