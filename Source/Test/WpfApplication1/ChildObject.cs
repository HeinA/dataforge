﻿using Dataforge.Business;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApplication1
{
  public class ChildObject : BusinessObject
  {
    string _name;
    public string Name
    {
      get { return _name; }
      set { SetValue<string>(ref _name, value, "Name"); }
    }

    protected override Collection<string> GetErrors(string propertyName)
    {
      Collection<string> errors = base.GetErrors(propertyName);

      if (propertyName == string.Empty || propertyName == "Name")
      {
        if (string.IsNullOrWhiteSpace(Name)) errors.Add("Name is a mandatory field.");
      }

      return errors;
    }
  }
}
